#include <mutex>
#include "Actor.h"

struct Actor::Impl {
	struct {
		std::mutex m;
		EntityProps props;
	} share;
};

Actor::Actor() : pImpl(new Impl) {
}

Actor::~Actor() {
	delete pImpl;
}

void Actor::SetProps(const EntityProps &newProps) noexcept {
	std::lock_guard l(pImpl->share.m);
	pImpl->share.props = newProps;
}

void Actor::ApplyPropsChanges(const EntityProps &newProps) noexcept {
	std::lock_guard l(pImpl->share.m);
	pImpl->share.props += newProps;
}

EntityProps Actor::GetProps() const noexcept {
	std::lock_guard l(pImpl->share.m);
	return pImpl->share.props;
}