#pragma once
#include <cstdint>
#include <optional>
#include <assert.h>
#include <viet/MessageSerializer.h>

#ifdef GetMessage
#undef GetMessage
#endif

template <class Msg>
class MsgWrapper {
public:
	MsgWrapper(Viet::MessageSerializer *seri_, const uint8_t *data_, size_t length_, void *userData_ = nullptr) noexcept
		: seri(seri_), data(data_), length(length_), userData(userData_) {
		assert(seri_);
	}

	MsgWrapper(Viet::MessageSerializer *seri_, const Msg &msg_, void *userData_ = nullptr) noexcept 
		: seri(seri_), msg(msg_), userData(userData_) {
		assert(seri_);
	}

	void Assign(const Msg &msg_) noexcept {
		this->msg = msg_;
		this->data = nullptr;
		this->length = 0;
	}

	void Assign(const uint8_t *data_, size_t length_) noexcept {
		this->msg = std::nullopt;
		this->data = data_;
		this->length = length_;
	}

	const uint8_t *GetData() noexcept {
		this->PrepareBinary();
		assert(this->data);
		return this->data;
	}

	std::pair<const uint8_t *, size_t> GetBinary() noexcept {
		if (this->length > 0 && this->data) {
			return { this->data, this->length };
		}
		assert(this->msg);
		uint8_t *outData = nullptr;
		size_t outLength = 0;
		this->seri->Write(*this->msg, outData, outLength, Viet::SeriToUse::This, this->userData);
		assert(outLength > 0);
		assert(outData);
		return { outData, outLength };
	}

	const Msg &GetMessage() noexcept {
		this->PrepareMessage();
		assert(this->msg);
		return *this->msg;
	}

private:
	void PrepareBinary() noexcept {
		if (!this->data) {
			assert(this->msg);
			this->seri->Write(*this->msg, const_cast<uint8_t *&>(data), this->length, Viet::SeriToUse::This, this->userData);
			assert(this->length > 0);
			assert(this->data);
		}
	}

	void PrepareMessage() noexcept {
		if (!this->msg) {
			assert(this->data);
			assert(this->length > 0);
			this->msg = Msg();
			this->seri->Read(*this->msg, this->data, this->length, Viet::SeriToUse::This, this->userData);
		}
	}

	Viet::MessageSerializer *const seri;
	const uint8_t *data = nullptr;
	size_t length = 0;
	std::optional<Msg> msg;
	void *userData = nullptr;
};