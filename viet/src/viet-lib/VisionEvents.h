#pragma once
#include <cstdint>
#include <variant>
#include <optional>
#include <EntityInfo.h>

enum class DespawnReason {
	Disabled
};

namespace vision {
	struct ActorPropertyChange {
		int entityId = -1;
		EntityProps changes;
	};

	struct MyPropertyChange {
		int entityId = -1;
		EntityProps changes;
	};

	struct StreamIn {
		int entityId = -1;
		EntityProps allProps;
	};

	struct StreamOut {
	};

	template <class OwnerT>
	class Event {
	public:
		OwnerT owner; // Should be ignored for events without owner
		std::variant<ActorPropertyChange, MyPropertyChange, StreamIn, StreamOut> evn;
	};
}