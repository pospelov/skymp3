#pragma once
#include <cstring> // strcmp
#include <cassert>
#include <string>
#include <variant>
#include <optional>
#include <tuple>
#include <map>
#include <EntityProps.h>
#include <EntityManipulation.h>
#include <FindMethodCall.h>
#include <EntityInfo.h>
#include <viet/FindRequest.h>
#include <viet/Transaction.h>

enum class EventId {
	Invalid = -1,

	//
	// Callbacks
	//
	/// Internal, you don't want to use it in SendEvent
	CallResult,
	/// For use in JavaScript:
	ScriptInit,
	UserConnect,
	UserDisconnect,
	UserCustomPacket,
	RaceMenuExit,

	//
	// Methods
	//
	GetMaxPlayers,
	ShowBrowserWindow,
	HideBrowserWindow,
	SendCustomPacket,
	FindEntity,
	ManipulateEntity,
	SetActor,
	GetActor,
	RunTransaction,

	COUNT
};

namespace evn {
	// Do not use size_t, it is used in serialization
	using CallId = uint32_t;

	struct CallResult {
		static constexpr auto Id = EventId::CallResult;
		static constexpr auto JSName = "callResult";

		// Note: EntityInfo may be passed as std::string
		using ReturnValue = std::optional<std::variant<
			std::string, 
			double, 
			EntityManipulationResult, 
			std::vector<EntityInfo>,
			Viet::TransactionResult
		>>;

		CallResult() = default;
		explicit CallResult(CallId callId_) noexcept {
			this->callId = callId_;
		}

		CallId callId = 0;
		ReturnValue returnValue;
		std::string error; // empty means success

		template <class Archive>
		void save(Archive &ar, unsigned int version) const {
			ar & callId & error & returnValue.has_value();
			if (returnValue.has_value()) {
				auto idx = (uint8_t)returnValue->index();
				ar & idx;
				if (idx == 0) {
					const std::string &s = std::get<0>(*returnValue);
					ar & s;
				}
				else if (idx == 1) {
					const double &d = std::get<1>(*returnValue);
					ar & d;
				}
				else if (idx == 2) {
					const EntityManipulationResult &ans = std::get<2>(*returnValue);
					ar & ans;
				}
				else if (idx == 3) {
					const std::vector<EntityInfo> &v = std::get<3>(*returnValue);
					ar & v;
				}
				else if (idx == 4) {
					const Viet::TransactionResult &v = std::get<4>(*returnValue);
					ar & v;
				}
				else {
					assert(0 && "Unknown ReturnValue type");
				}
			}
		}
		template<class Archive>
		void load(Archive &ar, unsigned int version) {
			bool hasValue = false;
			uint8_t idx = ~0;
			ar & callId & error & hasValue;
			if (hasValue) {
				ar & idx;
				if (idx == 0) {
					std::string s;
					ar & s;
					returnValue = s;
				}
				else if (idx == 1) {
					double d;
					ar & d;
					returnValue = d;
				}
				else if (idx == 2) {
					EntityManipulationResult ans;
					ar & ans;
					returnValue = std::move(ans);
				}
				else if (idx == 3) {
					std::vector<EntityInfo> v;
					ar & v;
					returnValue = std::move(v);
				}
				else if (idx == 4) {
					Viet::TransactionResult v;
					ar & v;
					returnValue = std::move(v);
				}
				else {
					assert(0 && "Unknown ReturnValue type");
				}
			}

		}
		SERIALIZATION_SPLIT_MEMBER();
	};

	struct ScriptInit {
		static constexpr auto Id = EventId::ScriptInit;
		static constexpr auto JSName = "scriptInit";

		bool error = false;
		std::map<std::string, std::vector<std::string>> propertiesByEntityName;

		template <class Archive>
		void serialize(Archive &ar, unsigned int version) {
			ar & error & propertiesByEntityName;
		}

		int callId = 0; // unused
	};

	struct UserConnect {
		static constexpr auto Id = EventId::UserConnect;
		static constexpr auto JSName = "userEnter";

		EntityInfo user;

		template <class Archive>
		void serialize(Archive &ar, unsigned int version) {
			ar & user;
		}

		int callId = 0; // unused
	};

	struct UserDisconnect {
		static constexpr auto Id = EventId::UserDisconnect;
		static constexpr auto JSName = "userExit";

		EntityInfo user;

		template <class Archive>
		void serialize(Archive &ar, unsigned int version) {
			ar & user;
		}

		int callId = 0; // unused
	};

	struct UserCustomPacket {
		static constexpr auto Id = EventId::UserCustomPacket;
		static constexpr auto JSName = "userCustomPacket";

		EntityInfo user;
		std::string data;

		template <class Archive>
		void serialize(Archive &ar, unsigned int version) {
			ar & user & data;
		}

		int callId = 0; // unused
	};

	struct RaceMenuExit {
		static constexpr auto Id = EventId::RaceMenuExit;
		static constexpr auto JSName = "raceMenuExit";

		EntityInfo actor;

		template <class Archive>
		void serialize(Archive &ar, unsigned int version) {
			ar & actor;
		}

		int callId = 0; // unused
	};

	struct GetMaxPlayers {
		static constexpr auto Id = EventId::GetMaxPlayers;
		static constexpr auto JSName = "getMaxPlayers";

		CallId callId;

		template <class Archive>
		void serialize(Archive &ar, unsigned int version) {
			ar & callId;
		}
	};

	struct ShowBrowserWindow {
		static constexpr auto Id = EventId::ShowBrowserWindow;
		static constexpr auto JSName = "showBrowserWindow";

		CallId callId;
		EntityInfo session;
		std::string winId;
		std::string jsonProperties;

		template <class Archive>
		void serialize(Archive &ar, unsigned int version) {
			ar & callId & session & winId & jsonProperties;
		}
	};

	struct HideBrowserWindow {
		static constexpr auto Id = EventId::HideBrowserWindow;
		static constexpr auto JSName = "hideBrowserWindow";

		CallId callId;
		EntityInfo session;
		std::string winId;

		template <class Archive>
		void serialize(Archive &ar, unsigned int version) {
			ar & callId & session & winId;
		}
	};

	struct SendCustomPacket {
		static constexpr auto Id = EventId::SendCustomPacket;
		static constexpr auto JSName = "sendCustomPacket";

		CallId callId;
		EntityInfo session;
		std::string jsonContent;

		template <class Archive>
		void serialize(Archive &ar, unsigned int version) {
			ar & callId & session & jsonContent;
		}
	};

	struct FindEntity {
		static constexpr auto Id = EventId::FindEntity;
		static constexpr auto JSName = "findEntity";

		CallId callId;
		std::string coll;
		Viet::FindRequest findReq;

		template <class Archive>
		void serialize(Archive &ar, unsigned int version) {
			ar & callId & coll & findReq;
		}
	};

	struct ManipulateEntity {
		static constexpr auto Id = EventId::ManipulateEntity;
		static constexpr auto JSName = "manipulateEntity";

		CallId callId;
		std::string coll;
		EntityInfo entityToEdit; // If valid, edits entity instead of create
		EntityManipulation entityManip;

		template <class Archive>
		void serialize(Archive &ar, unsigned int version) {
			ar & callId & coll & entityToEdit & entityManip;
		}
	};

	struct SetActor {
		static constexpr auto Id = EventId::SetActor;
		static constexpr auto JSName = "setActor";

		CallId callId;
		EntityInfo user;
		std::string coll;
		EntityInfo newActor;

		template <class Archive>
		void serialize(Archive &ar, unsigned int version) {
			ar & callId & user & coll & newActor;
		}
	};

	struct GetActor {
		static constexpr auto Id = EventId::GetActor;
		static constexpr auto JSName = "getActor";

		CallId callId;
		EntityInfo user;
		std::string entityName;

		template <class Archive>
		void serialize(Archive &ar, unsigned int version) {
			ar & callId & user & entityName;
		}
	};

	struct RunTransaction {
		static constexpr auto Id = EventId::RunTransaction;
		static constexpr auto JSName = "runTransaction";

		CallId callId;
		Viet::Transaction transaction;

		template <class Archive>
		void serialize(Archive &ar, unsigned int version) {
			ar & callId & transaction;
		}
	};

	// All events (callbacks and methods) must be listed here
	using Any = std::variant<CallResult, ScriptInit, UserConnect, UserDisconnect, UserCustomPacket, RaceMenuExit, GetMaxPlayers, ShowBrowserWindow, HideBrowserWindow, SendCustomPacket, FindEntity, ManipulateEntity, SetActor, GetActor, RunTransaction>;

	inline CallId GetCallId(const Any &any) {
		return std::visit([](auto &concrete) {
			return (CallId)concrete.callId;
		}, any);
	}

	inline const char *GetJsName(const Any &any) {
		return std::visit([](auto &concrete) {
			return concrete.JSName;
		}, any);
	}

	// Only for callbacks
	inline EventId IdByJsName(const char *jsName) {
		if (!strcmp(jsName, ScriptInit::JSName)) return ScriptInit::Id;
		if (!strcmp(jsName, UserConnect::JSName)) return UserConnect::Id;
		if (!strcmp(jsName, UserDisconnect::JSName)) return UserDisconnect::Id;
		if (!strcmp(jsName, UserCustomPacket::JSName)) return UserCustomPacket::Id;
		if (!strcmp(jsName, RaceMenuExit::JSName)) return RaceMenuExit::Id;
		return EventId::Invalid;
	}
}
