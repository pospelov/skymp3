#pragma once
#include <functional>
#include <cassert>
#include <string>
#include <vector>
#include <map>
#include <variant>
#include <optional>
#include <ostream>
#include <nlohmann/json.hpp>
#include <viet/Networking.h>
#include <viet/Serialization.h>
#include <viet/Logging.h>
#include <viet/Promise.h>
#include <viet/FindRequest.h>
#include <viet/Instance.h>
#include <viet/Actions.h>
#include <viet/Transaction.h>

namespace Viet {
	using json = nlohmann::json;

	class ApiClient;

	enum class ComparisonOperator {
		Equal,
		Greater,
		GreaterOrEqual,
		Less,
		LessOrEqual
	};

	class ApiClient {
	public:
		struct Settings {
			std::string serverId;
			std::string devPassword;
			Viet::Networking::Plugin gamemodeNetPlugin;
			Viet::Serialization::Plugin seriPlugin; 
			Viet::Logging::Plugin loggingPlugin;
			std::map<std::string, std::vector<uint8_t>> frontEndFiles;
		};

		struct ServerEntityList {
			std::map<std::string, std::vector<std::string>> propertiesByEntityName;
		};

		struct Listeners {
			std::function<void(std::string error, const ServerEntityList &)> onConnect;
			std::function<void(Viet::Instance)> onUserEnter, onUserExit;
			std::function<void(Viet::Instance, std::string)> onUserCustomPacket;
		};

		ApiClient(const Settings &settings, const Listeners &listeners);
		~ApiClient();

		Promise<int> GetMaxPlayers() noexcept;
		Promise<std::vector<Viet::Instance>> Find(const char *entityName, FindRequest findRequest) noexcept;
		Promise<Viet::ActionsResult> Create(const char *entityName, Viet::Actions actions) noexcept;
		Promise<Viet::ActionsResult> Edit(const char *entityName, const Viet::Instance &instance, Viet::Actions actions) noexcept;
		Promise<Void> SetAttachedInstance(const Viet::Instance &user, const char *entityName, const std::optional<Viet::Instance> &instance) noexcept;
		Promise<std::optional<Viet::Instance>> GetAttachedInstance(const Viet::Instance &user, const char *entityName) noexcept;
		Promise<Void> SendCustomPacket(const Viet::Instance &user, const char *content) noexcept;
		Promise<Viet::TransactionResult> RunTransaction(const Viet::Transaction &transaction) noexcept;
		
		void Tick() noexcept;

		Viet::Logging::Logger &GetLogger() noexcept;

	private:
		struct Impl;
		Impl *const pImpl;
	};
}