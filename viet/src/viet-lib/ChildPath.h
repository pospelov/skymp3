#pragma once
#include <string>
#include <vector>
#include <optional>
#include <nlohmann/json.hpp>

class ChildPath {
public:
	ChildPath() = default;
	ChildPath(std::vector<std::string> tokens_) : tokens(tokens_) {
	}

	ChildPath(const std::string &s) {
		this->tokens = this->split(s, this->delimiter);
	}

	bool IsEmpty() const noexcept {
		return tokens.empty();
	}

	const std::string &GetString() const noexcept {
		static std::string g_emptyString;
		if (this->IsEmpty()) return g_emptyString;

		if (!asStr) {
			asStr = std::string();
			bool needDelimiter = false;
			for (auto &token : tokens) {
				if (needDelimiter) {
					*asStr += this->delimiter;
				}
				needDelimiter = true;
				*asStr += token;
			}
		}
		return *asStr;
	}

	const std::vector<std::string> &GetTokens() const noexcept {
		return tokens;
	}

	static nlohmann::json GetObjectChild(nlohmann::json j, const ChildPath &childPath) {
		static const nlohmann::json g_null = nullptr;
		nlohmann::json res = j;
		for (auto &token : childPath.GetTokens()) {
			nlohmann::json v;
			if (res.is_object())
				v = res[token];
			if (res.is_array())
				v = res[atoi(token.data())];

			if (v.is_null()) {
				return nullptr;
			}
			res = v;
		}
		return res;
	}

private:
	//https://stackoverflow.com/questions/14265581/parse-split-a-string-in-c-using-string-delimiter-standard-c
	std::vector<std::string> split(const std::string &str, const std::string &delim) {
		std::vector<std::string> tokens;
		size_t prev = 0, pos = 0;
		do {
			pos = str.find(delim, prev);
			if (pos == std::string::npos) pos = str.length();
			std::string token = str.substr(prev, pos - prev);
			if (!token.empty()) tokens.push_back(token);
			prev = pos + delim.length();
		} while (pos < str.length() && prev < str.length());
		return tokens;
	}

	std::vector<std::string> tokens;
	mutable std::optional<std::string> asStr;
	const std::string delimiter = ".";
};