#pragma once
#include <memory>

namespace Viet {
	class IPropertyValue {
	public:
		virtual ~IPropertyValue() = default;

		virtual std::unique_ptr<IPropertyValue> Clone() const = 0;
		///virtual bool Equals(const IPropertyValue *other) const = 0;
	};

	template <class T>
	class PropertyValue : public IPropertyValue {
	public:
		std::unique_ptr<IPropertyValue> Clone() const override {
			std::unique_ptr<IPropertyValue> res;
			res.reset(new T(static_cast<const T &>(*this)));
			return res;
		}

		///bool Equals(const IPropertyValue *other) const override {
		///	if (auto otherConcrete = dynamic_cast<const T *>(other)) {
		///		return *otherConcrete == *this;
		///	}
		///	return false;
		///}
	};
};