#pragma once
#include <ICellProcess.h>
#include <Properties.h>
#include <viet/Entity.h>

class CellProcess : public ICellProcess {
public:
	void SetOnVisionEvent(OnVisionEvent cb) noexcept override;
	void SetOnSaveRequired(OnSaveRequired cb) noexcept override;
	ActorId AddActor(int entityId, std::unique_ptr<IActor> ac) noexcept override;
	void DisableActor(const InstanceId &inst) noexcept override;
	std::optional<ActorWaste> GetNextActorWaste(int entityId) noexcept override;
	EditRes Edit(const InstanceId &inst, const EntityProps &entityProps, bool sendChangesToMe) noexcept override;
	std::optional<EntityProps> GetEntityProps(const InstanceId &inst) const noexcept override;
	bool HasActor(const InstanceId &inst) const noexcept override;
	std::vector<ActorId> FindMatches(int entityId, const Viet::FindCondition &cond, uint32_t limit) const noexcept override;
	void StreamOutAll(const InstanceId &inst) noexcept override;

	CellProcess(std::vector<Viet::Entity> entities) noexcept;
	~CellProcess();

	// May be invoked from another thread
	void Tick() noexcept;

private:
	struct Impl;
	Impl *const pImpl;
};