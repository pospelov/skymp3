FROM gcc:8.2

RUN apt-get update -yq && \
  apt-get upgrade -y && \
  apt-get install -y git && \
  apt-get install -y texinfo && \
  apt-get install -y libsasl2-dev

# NodeJS
RUN apt-get update -yq \
  && apt-get install curl gnupg -yq \
  && curl -sL https://deb.nodesource.com/setup_10.x | bash \
  && apt-get install -yq nodejs

# GDB
RUN wget "http://ftp.gnu.org/gnu/gdb/gdb-8.3.tar.gz" && tar -xvzf gdb-8.3.tar.gz && \
  cd gdb-8.3 && ./configure && make && make install && gdb --version

# CMake
RUN wget https://cmake.org/files/v3.15/cmake-3.15.1-Linux-x86_64.sh && \
  chmod 777 ./cmake-3.15.1-Linux-x86_64.sh && \
  ./cmake-3.15.1-Linux-x86_64.sh --skip-license

WORKDIR /usr/src/viet
COPY ./third-party ./third-party
COPY ./CMakeLists.txt .

RUN mkdir -p build && cd build && mkdir -p Debug && mkdir -p Release
RUN cd ./build/Release && \
  cmake -DONLY_VCPKG_INSTALL=ON -DCMAKE_BUILD_TYPE=Release ../.. && \
  cmake --build . && \
  cd ../Debug && \
  cmake -DONLY_VCPKG_INSTALL=ON -DCMAKE_BUILD_TYPE=Debug ../.. && \
  cmake --build .

COPY ./main.cmake .
COPY ./viet ./viet

RUN cd ./build/Release && \
  cmake -DONLY_VCPKG_INSTALL=OFF -DCMAKE_BUILD_TYPE=Release ../.. && \
  cmake --build . && \
  cd ../Debug && \
  cmake -DONLY_VCPKG_INSTALL=OFF -DCMAKE_BUILD_TYPE=Debug ../.. && \
  cmake --build .

COPY ./tests ./tests
ARG TEST_MONGO_URI
RUN cd ./build/Release && \
  cmake -DTEST_MONGO_URI=$TEST_MONGO_URI -DCMAKE_BUILD_TYPE=Release ../.. && \
  cmake --build . && \
  cd ../Debug && \
  cmake -DTEST_MONGO_URI=$TEST_MONGO_URI -DCMAKE_BUILD_TYPE=Debug ../.. && \
  cmake --build .

WORKDIR /usr/src/viet/build
