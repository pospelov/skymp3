#pragma once
#include <forms.h>

class Actor : public IActor {
public:
	Actor();
	~Actor();

	EntityProps GetProps() const noexcept override;
	void ApplyPropsChanges(const EntityProps &entityProps) noexcept override;
	void SetProps(const EntityProps &entityProps) noexcept override;

private:
	struct Impl;
	Impl *const pImpl;
};