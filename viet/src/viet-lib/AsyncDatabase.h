#pragma once
#include <functional>
#include <memory>
#include <viet/NoSqlDatabase.h>
#include <IAsyncDatabase.h>

class AsyncDatabase : public IAsyncDatabase {
public:
	AsyncDatabase(std::shared_ptr<Viet::NoSqlDatabase::INoSqlDatabase>);
	~AsyncDatabase();

	void Tick();

	void Find(const std::string &coll, json filter, const FindCallback &cb) noexcept override;
	void FindOne(const std::string &coll, json filter, const FindOneCallback &cb) noexcept override;
	void InsertOne(const std::string &coll, json doc, const InsertOneCallback &cb) noexcept override;
	void UpdateOne(const std::string &coll, json filter, json upd, bool upsert, const UpdateOneCallback &cb) noexcept override;
	void UpdateMany(const std::string &coll, std::vector<std::pair<json, json>> filterAndUpdate, bool upsert, const UpdateManyCallback &cb) noexcept override;
	void Drop(const std::string &coll, const DropCallback &cb) noexcept override;

private:
	struct Impl;
	std::shared_ptr<Impl> pImpl;
};