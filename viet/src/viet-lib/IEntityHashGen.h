#pragma once
#include <string>

class IEntityHashGen {
public:
	virtual std::string Generate() noexcept = 0;

	virtual ~IEntityHashGen() = default;
};