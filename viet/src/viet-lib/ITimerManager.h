#pragma once
#include <cstdint>
#include <functional>

class ITimerManager {
public:
	// Must be threadsafe
	virtual void SetTimeout(uint32_t ms, std::function<void()> fn) noexcept = 0;
};