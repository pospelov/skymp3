#pragma once
#include <memory>
#include <IAsyncDatabase.h>
#include <ITimerManager.h>
#include <viet/Networking.h>
#include <viet/Logging.h>
#include <EntityInfo.h>

class SessionsLoader {
public:
	using OnConnect = std::function<void(const EntityInfo &)>;
	using OnDisconnect = std::function<void(const EntityInfo &)>;

	SessionsLoader(OnConnect onConnect, 
		OnDisconnect onDisconnect,
		std::shared_ptr<IAsyncDatabase> db,
		std::shared_ptr<ITimerManager> tm, 
		std::shared_ptr<Viet::Logging::Logger> logger);
	~SessionsLoader();

	bool IsStarted() const noexcept;
	void MakeSessionOffline(const EntityInfo &sesisonKey) noexcept;
	bool Handshake(const EntityInfo &sessionKey, Viet::Networking::UserId id) noexcept;
	Viet::Networking::UserId GetUserId(const EntityInfo &sessionKey) const noexcept; // returns g_invalidUserId if no active user with such sessionKey
	std::map<std::string, std::string> GetBrowserWindows(const EntityInfo &sessionKey) const noexcept; // returns "" on error

	// returns false if session not found: 
	bool AddBrowserWindow(const EntityInfo &sessionKey, std::string id, std::string jsonProps) noexcept;
	bool RemoveBrowserWindow(const EntityInfo &sessionKey, std::string id) noexcept;
	bool SetActor(const EntityInfo &sessionKey, const EntityInfo &newActor) noexcept; // newActor can be empty

	 // result can be empty:
	const EntityInfo &GetSessionByActor(const EntityInfo &actor) const noexcept;
	const EntityInfo &GetActor(const EntityInfo &sessionKey) const noexcept;

	void Tick() noexcept;

private:
	struct Impl;
	Impl *const pImpl;
};