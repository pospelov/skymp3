#pragma once
#include <memory>
#include <functional>
#include <api.h>
#include <viet/Networking.h>
#include <viet/Logging.h>
#include <viet/MessageSerializer.h>
#include <ApiEvents.h>
#include <FrontEndFiles.h>

class ApiController : public IApiController {
public:
	ApiController(
		std::shared_ptr<Viet::Networking::IClient> reconClient,
		std::shared_ptr<Viet::MessageSerializer> seri,
		std::shared_ptr<Viet::Logging::Logger> logger,
		std::string serverId,
		std::string devPassword,
		const FrontEndFiles &frontEnd
	);

	~ApiController() override;

	std::shared_ptr<std::list<evn::Any>> Tick();

	evn::CallId GetMaxPlayers() noexcept override;
	evn::CallId ShowBrowserWindow(const EntityInfo &user, std::string winId, std::string jsonProperties) noexcept override;
	evn::CallId HideBrowserWindow(const EntityInfo &user, std::string winId) noexcept override;
	evn::CallId SendCustomPacket(const EntityInfo &user, std::string jsonContent) noexcept override;
	evn::CallId FindEntity(std::string collection, const Viet::FindRequest &findReq) noexcept override;
	evn::CallId ManipulateEntity(std::string collection, const EntityInfo &optionalExistingEntity, const EntityManipulation &entityManip) noexcept override;
	evn::CallId SetActor(const EntityInfo &user, std::string collection, std::optional<EntityInfo> actor) noexcept override;
	evn::CallId GetActor(const EntityInfo &user, std::string entityName) noexcept override;
	evn::CallId RunTransaction(const Viet::Transaction &transaction) noexcept override;

	void Shutdown() noexcept override;

private:
	struct Impl;
	std::shared_ptr<Impl> pImpl;
};