#pragma once
#include <cassert>
#include <memory>
#include <mutex>
#include <vector>
#include <viet/Entity.h>
#include <viet/Networking.h>
#include <viet/MessageSerializer.h>
#include <IMessageListener.h>
#include <INetController.h>

class NetController : public INetController {
public:
	NetController(std::shared_ptr<Viet::Networking::IClient> cl_, 
		std::shared_ptr<Viet::MessageSerializer> seri_, 
		std::string mySessionHash,
		std::vector<Viet::Entity> entities_ // For serialization
	);

	// INetController
	void Send(const msg::UpdateActorProperties &m) noexcept override;
	void Send(const msg::CustomPacket &m) noexcept override;

	void Tick(MessageListeners &listeners) noexcept;

private:
	template <class T>
	void Send_(const T &m, bool reliable, void *seriUserData) {
		std::lock_guard l(this->m);
		assert(cl != nullptr);
		assert(seri != nullptr);
		if (!cl || !seri) return;
		uint8_t *data = nullptr;
		size_t len = 0;
		seri->Write(m, data, len, Viet::SeriToUse::This, seriUserData);
		assert(data);
		assert(len > 0);
		if (data && len) {
			cl->Send(data, len, reliable);
		}
	}

	std::shared_ptr<Viet::Networking::IClient> cl;
	std::shared_ptr<Viet::MessageSerializer> seri;
	std::recursive_mutex m;
	std::string mySessionHash;
	std::vector<Viet::Entity> entities;
};
