#pragma once
#include <viet/Networking.h>
#include <memory>
#include <vector>

class ComposerServer : public Viet::Networking::IServer {
public:
	using ServersVector = std::vector<std::shared_ptr<Viet::Networking::IServer>>;

	ComposerServer(ServersVector realServers) noexcept;
	~ComposerServer();

	// net::IServer
	void Tick(const OnPacket &onPacket) noexcept override;
	void Send(Viet::Networking::UserId id, const unsigned char *packet, size_t length, bool reliable) noexcept override;

private:
	struct Impl;
	Impl *const pImpl;
};