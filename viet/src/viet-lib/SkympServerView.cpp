#include <vector>
#include <deque>
#include <map>
#include <optional>
#include <cassert>
#include <fstream>
#include <filesystem>
#include <functional>
#include <algorithm>
#include <thread>
#include <nlohmann/json.hpp>
#include <EntityInfoMap.h>
#include <Messages.h>
#include <SessionsLoader.h>
#include <HashToEntityInfo.h>
#include <UserManager.h>
#include <World.h>
#include <EntityAccess.h>
#include <ActionsToManip.h>

namespace fs = std::filesystem;
using json = nlohmann::json;

#include "SkympServerView.h"

namespace {
	using ManipTarget = std::optional<World::ActorFindResult>;

	inline EntityProps FilterPropsBeforeSend(const EntityProps &props, Properties *propList) {
		assert(propList);
		auto props_ = props;
		props_.Filter(Viet::PropertyFlags::ClientReadable, propList);
		return props_;
	}
}

struct SkympServerView::Impl : public SkympServerView::Settings {
	std::thread::id myThreadId;
	std::shared_ptr<SessionsLoader> sessionLoader;
	std::shared_ptr<UserManager> um;
	std::shared_ptr<World> world;

	Viet::Networking::IServer::OnPacket onPacket;

	FrontEndFiles frontEndFiles;

	std::pair<std::optional<World::ActorAddress>, EntityInfo> FindActorLocally(const char *fn, Viet::Networking::UserId userId) {
		const auto &session = this->um->GetSession(userId);
		if (session.IsEmpty()) {
			this->logger->Error("%s failed: No session found for userId %d", fn, userId);
			return { std::nullopt, {} };
		}
		const auto &actor = this->sessionLoader->GetActor(session);
		if (actor.IsEmpty()) {
			this->logger->Error("%s failed: Actor not found", fn);
			return { std::nullopt, {} };
		}
		return { this->world->FindActorLocally(actor), actor };
	}
	struct ApplyManipulationRes {
		std::vector<Viet::JsonWrapper> opResults;
		EntityProps propsToSave;
	};

	int GetEntityId(const std::string &name);
	ApplyManipulationRes ApplyManipulation(const EntityProps &currentProps, const EntityManipulation &manip, Properties *propList);
};

SkympServerView::SkympServerView(const Settings &settings) noexcept : pImpl(new Impl) {
	assert(settings.maxPlayers > 0);
	assert(settings.server);
	assert(settings.seri);
	assert(settings.hashGen);
	assert(settings.gamemodeController);
	assert(settings.timerManager);
	assert(settings.db);
	assert(settings.entities.size() > 0);

	dynamic_cast<Settings &>(*pImpl) = settings;

	pImpl->onPacket = [this](const auto &p, auto userId) {
		return this->OnPacket(p, userId);
	};

	auto onConnect = [this](const EntityInfo &ei) {
		pImpl->gamemodeController->SendEvent(evn::UserConnect{ ei });
	};
	auto onDisconnect = [this](const EntityInfo &ei) {
		pImpl->gamemodeController->SendEvent(evn::UserDisconnect{ ei });
	};
	pImpl->sessionLoader.reset(new SessionsLoader(onConnect, onDisconnect, settings.db, settings.timerManager, settings.logger));

	pImpl->um.reset(new UserManager(settings.maxPlayers));
	
	auto onVisionEvent = [=](const EntityInfo &viewerEi, vision::Event<EntityInfo> evn) {
		int idx = evn.evn.index();
		const auto &session = pImpl->sessionLoader->GetSessionByActor(viewerEi);
		if (session.IsEmpty()) {
			auto s = viewerEi.ToString();
			pImpl->logger->Debug("Ignoring vision event for %s", s.data());
			return;
		}

		// ActorPropertyChange, MyPropertyChange, StreamIn, StreamOut

		assert(std::this_thread::get_id() == pImpl->myThreadId);

		if (idx == 0) {
			const auto &acPropChange = std::get<vision::ActorPropertyChange>(evn.evn);
			this->SendActorProperties(session, evn.owner, acPropChange.entityId, acPropChange.changes);
		}
		else if (idx == 1) {
			const auto &myPropChange = std::get<vision::MyPropertyChange>(evn.evn);
			this->SendMyProperties(session, myPropChange.entityId, myPropChange.changes);
			const auto &actorEi = viewerEi;
			assert(actorEi.IsEmpty() == false);
			if (actorEi.IsEmpty() == false) {
				auto acAddr = pImpl->world->FindActorLocally(actorEi);
				assert(acAddr);
				if (acAddr) {
					this->SendActorId(session, acAddr->inst, actorEi);
				}
			}
		}
		else if (idx == 2) {
			const auto &streamIn = std::get<vision::StreamIn>(evn.evn);
			this->CreateActor(session, streamIn.entityId, evn.owner, streamIn.allProps);
		}
		else if (idx == 3) {
			const auto &streamOut = std::get<vision::StreamOut>(evn.evn);
			this->DestroyActor(session, evn.owner);
		}
		else {
			assert(0 && "Unknown vision event");
		}
	};
	assert(settings.dbOps);
	pImpl->world.reset(new World(settings.cellProcessFactory, settings.actorFactory, settings.db, *settings.dbOps, settings.logger, settings.timerManager, onVisionEvent, pImpl->entities));

	pImpl->myThreadId = std::this_thread::get_id();
}

void SkympServerView::Tick() noexcept {
	pImpl->sessionLoader->Tick();
	if (!pImpl->sessionLoader->IsStarted()) return;

	pImpl->server->Tick(pImpl->onPacket);
	pImpl->world->Tick();

	while (const auto evn = pImpl->gamemodeController->GetNextMethodCalled()) {
		this->OnApiMethodCallCommon(*evn);
	}

	while (const auto frontEnd = pImpl->gamemodeController->GetNextFrontEndChange()) {
		json files = json::array();
		for (auto &[path, content] : frontEnd->data) {
			if (path.find("..") != std::string::npos) {
				pImpl->logger->Error("FrontEnd - bad path %s", path.data());
				continue;
			}
			pImpl->frontEndFiles.data[path] = content; // Overwrite content of the previous file
			files.push_back(path);
		}
		auto filesDump = files.dump();
		pImpl->logger->Info("FrontEnd - Adding files %s", filesDump.data());

		// Update gui for all users
		for (Viet::Networking::UserId i = 0; i < (Viet::Networking::UserId)pImpl->maxPlayers; ++i) {
			if (pImpl->um->IsConnected(i)) {
				auto &session = pImpl->um->GetSession(i);
				if (session.IsEmpty() == false) {
					this->UpdateGui(session);
				}
			}
		}
	}
}

void SkympServerView::OnPacket(const Viet::Networking::Packet &p, Viet::Networking::UserId userId) noexcept {
	switch (p.type) {
	case Viet::Networking::PacketType::UserConnect:
		return this->OnConnect(userId);
	case Viet::Networking::PacketType::UserDisconnect:
		return this->OnDisconnect(userId);
	case Viet::Networking::PacketType::Message: {
		const auto id = userId;
		const bool connected = pImpl->um->IsConnected(id);
		assert(connected);
		if (!connected) return;

		const auto sessionKey = pImpl->um->GetSession(id);

		const auto msgId = (MessageId)p.data[0];
		if (sessionKey.IsEmpty() && msgId != msg::Handshake::Id) {
			pImpl->logger->Error("User %d is not handshaked, ignoring packet (Id=%d)", (int)id, (int)msgId);
			return;
		}

		switch (msgId) {
		case msg::UpdateActorProperties::Id: {
			auto entityId = MsgWrapper<msg::UpdateActorProperties>(&*pImpl->seri, p.data, p.length).GetMessage().entityId;

			assert(entityId >= 0);
			assert(entityId < (int)pImpl->entities.size());
			if (entityId < 0 || entityId >= (int)pImpl->entities.size()) return;

			auto propList = EntityAccess::GetPropList_(pImpl->entities[entityId]);
			assert(propList);
			if (!propList) return;

			MsgWrapper<msg::UpdateActorProperties> msgWrapper(&*pImpl->seri, p.data, p.length, propList);

			assert(msgWrapper.GetMessage().props.IsEmpty() == false);

			return this->OnMessage(userId, msgWrapper);
		}
		case msg::UpdateMyProperties::Id: {
			return pImpl->logger->Error("UpdateMyProperties is not implemented, use UpdateActorProperties instead");
		}
		case msg::CustomPacket::Id: {
			MsgWrapper<msg::CustomPacket> msgWrapper(&*pImpl->seri, p.data, p.length);
			return this->OnMessage(userId, msgWrapper);
		}
		case msg::Handshake::Id: {
			MsgWrapper<msg::Handshake> msgWrapper(&*pImpl->seri, p.data, p.length);
			return this->OnMessage(userId, msgWrapper);
		}
		default:
			break;
		}
		break;
	}
	default:
		assert(0);
		break;
	}
}

void SkympServerView::OnConnect(Viet::Networking::UserId id) noexcept {
	pImpl->logger->Info("Connected %d", (int)id);
	pImpl->um->AddConnected(id);

	// TODO: Add handshake timeout, kick non-handshaking users
}

void SkympServerView::OnDisconnect(Viet::Networking::UserId id) noexcept {
	assert(pImpl->um->IsConnected(id));
	if (!pImpl->um->IsConnected(id)) return;

	const auto &sessionKey = pImpl->um->GetSession(id);

	if (sessionKey.IsEmpty()) {
		pImpl->logger->Warning("User %d is disconnecting without handshake", id);
	}
	else {
		pImpl->sessionLoader->MakeSessionOffline(sessionKey);
	}

	pImpl->logger->Info("Disconnected %d", (int)id);
	pImpl->um->RemoveConnected(id);
}

void SkympServerView::OnMessage(Viet::Networking::UserId id, MsgWrapper<msg::UpdateActorProperties> &msgWrapper) noexcept {
	const auto &msg = msgWrapper.GetMessage();
	const auto msgInst = ICellProcess::InstanceId(msg.entityId, msg.instanceId.idx);

	auto[acAddr, actor] = pImpl->FindActorLocally("UpdateActorProperties", id);
	if (acAddr) {
		if (auto cp = pImpl->world->GetCellProcLocally(acAddr->cellProcessId)) {
			if (acAddr->inst != msgInst) {
				return pImpl->logger->Error("UpdateActorProperties failed: Unexpected ActorId");
			}
			if (actor.GetUInt64Hash() != msg.instanceId.eiHash) {
				return pImpl->logger->Error("UpdateActorProperties failed: Wrong hash");
			}

			bool err = false;
			msg.props.ForEach([&](PropId id, const std::unique_ptr<IPropertyValue> &value) {
				auto propList = EntityAccess::GetPropList_(pImpl->entities[acAddr->inst.entityId]);
				assert(propList);
				if (!propList) return;
				auto prop = propList->GetById(id);
				assert(prop);
				if (!prop) return;
				auto flags = prop->GetFlags();
				bool editable = !!(flags & Viet::PropertyFlags::ClientWritable);
				if (!editable) {
					err = true;
					return pImpl->logger->Error("UpdateActorProperties failed: Property %s is not editable from client", prop->GetName());
				}
			});
			if (err) return;

			// TODO: Check ability to edit for concrete values
			// For example, reject look edits for users with raceMenuShown == false

			// TODO: Prevent editing other user's actors

			assert(msg.props.IsEmpty() == false);
			if (msg.props.IsEmpty() == false) {
				cp->Edit(acAddr->inst, msg.props, false);
			}
		}
	}
}

void SkympServerView::OnMessage(Viet::Networking::UserId id, MsgWrapper<msg::CustomPacket> &msgWrapper) noexcept {
	const auto &msg = msgWrapper.GetMessage();
	pImpl->logger->Info("CustomPacket from userId=%d - %s", id, msg.data.data());

	const auto &sessionKey = pImpl->um->GetSession(id);
	if (sessionKey.IsEmpty()) return;

	pImpl->gamemodeController->SendEvent(evn::UserCustomPacket{ sessionKey, msg.data.data() });
}

void SkympServerView::OnMessage(Viet::Networking::UserId id, MsgWrapper<msg::Handshake> &msgWrapper) noexcept {
	const auto &msg = msgWrapper.GetMessage();
	assert(msg.mySessionHash.size() > 0);

	const auto session = pImpl->um->GetSession(id);
	if (session.IsEmpty() == false) {
		auto sessionStr = session.ToString();
		pImpl->logger->Warning("User %d has already handshaked (session is %s)", id, sessionStr.data());
		return;
	}
	EntityInfo sessionKey = HashToEntityInfo(msg.mySessionHash);
	assert(sessionKey.hash.size() > 0);

	if (pImpl->sessionLoader->Handshake(sessionKey, id)) {
		pImpl->um->AttachSession(id, sessionKey);
		this->UpdateGui(sessionKey);
	}
}

void SkympServerView::OnApiMethodCallCommon(const evn::Any &evn) noexcept {
	if (evn.index() == 0) {
		return pImpl->logger->Error("OnApiMethodCallCommon - suspicious index() value");
	}

	auto fGetMaxPlayers = [&](evn::GetMaxPlayers e)->Viet::AnyPromise {
		return this->GetMaxPlayers().Then([this, e](uint32_t maxPlayers) {
			evn::CallResult res(e.callId);
			res.returnValue = maxPlayers;
			this->ProcessCallResult(res, e.JSName);
		});
	};

	auto fSendCustomPacket = [&](evn::SendCustomPacket e)->Viet::AnyPromise{
		return this->SendCustomPacket(e.session, e.jsonContent).Then([this, e](Viet::Void) {
			evn::CallResult res(e.callId);
			this->ProcessCallResult(res, e.JSName);
		});
	};

	auto fFind = [&](evn::FindEntity e)->Viet::AnyPromise {
		return this->Find(e.coll, e.findReq).Then([this, e](std::vector<EntityInfo> found) {
			evn::CallResult res(e.callId);
			res.returnValue = found;
			this->ProcessCallResult(res, e.JSName);
		});
	};

	auto fManipulate = [&](evn::ManipulateEntity e)->Viet::AnyPromise {
		return this->Manipulate(e.coll, e.entityToEdit, e.entityManip).Then([this, e](EntityManipulationResult manipRes) {
			evn::CallResult res(e.callId);
			res.returnValue = manipRes;
			this->ProcessCallResult(res, e.JSName);
		});
	};

	auto fSetActor = [&](evn::SetActor e)->Viet::AnyPromise {
		return this->SetActor(e.user, e.coll, e.newActor).Then([this, e](Viet::Void) {
			evn::CallResult res(e.callId);
			this->ProcessCallResult(res, e.JSName);
		});
	};

	auto fGetActor = [&](evn::GetActor e)->Viet::AnyPromise {
		return this->GetActor(e.entityName, e.user).Then([this, e](EntityInfo ei) {
			evn::CallResult res(e.callId);
			if (!ei.IsEmpty()) {
				res.returnValue = ei.ToString();
			}
			this->ProcessCallResult(res, e.JSName);
		});
	};

	auto fRunTransaction = [&](evn::RunTransaction e)->Viet::AnyPromise {
		return this->RunTransaction(e.transaction).Then([this, e](Viet::TransactionResult transRes) {
			evn::CallResult res(e.callId);
			res.returnValue = transRes;
			this->ProcessCallResult(res, e.JSName);
		});
	};

	auto fDefault = [&](auto e)->Viet::AnyPromise {
		return Viet::Promise<Viet::Void>().Then([](auto) {});
	};

	auto callId = evn::GetCallId(evn);
	auto jsName = evn::GetJsName(evn);
	auto promise = std::visit(overloaded{ fGetMaxPlayers, fSendCustomPacket, fFind, fManipulate, fSetActor, fGetActor, fRunTransaction, fDefault }, evn);
	promise.Catch([this, callId, jsName](const char *err) {
		evn::CallResult res(callId);
		res.error = err;
		this->ProcessCallResult(res, jsName);
	});
}

int SkympServerView::Impl::GetEntityId(const std::string &name) {
	auto it = std::find_if(this->entities.begin(), this->entities.end(), [&](const Viet::Entity &entity) {
		return entity.GetName() == name;
	});
	if (it == this->entities.end()) {
		return -1;
	}
	return int(it - this->entities.begin());
}

SkympServerView::Impl::ApplyManipulationRes SkympServerView::Impl::ApplyManipulation(const EntityProps &currentProps, const EntityManipulation &manip, Properties *propList) {
	auto props = currentProps;
	EntityProps editedProps;

	auto fEmCallMethod = [this, &props, &editedProps, propList](const EntityManipulation::EMCallMethod &s) {
		Viet::JsonWrapper result;
		auto &propNames = propList->GetPropertyNames();
		for (auto propNameC : propNames) {
			const PropId propId = propList->GetPropertyId(propNameC);
			auto prop = propList->GetByName(propNameC);
			assert(prop);
			if (!prop) continue;
			auto propMethods = prop->GetMethods();

			std::unique_ptr<IPropertyValue> propValue; 
			
			if (auto &previousEdit = editedProps.GetPropertyValue(propNameC, propList)) {
				propValue = previousEdit->Clone();
			}
			if (!propValue) {
				if (auto &v = props.GetPropertyValue(propNameC, propList)) {
					propValue = v->Clone();
				}
			}
			if (!propValue) {
				propValue = prop->GetDefaultValue();
				assert(propValue);
				editedProps += {propId, propValue->Clone()};
			}
			assert(propValue);

			{
				auto it = std::find_if(propMethods.apiMethods.begin(), propMethods.apiMethods.end(), [&](const auto &method) {
					return method.name == s.name;
				});
				if (it != propMethods.apiMethods.end()) {
					result = it->fn(propValue, s.args);
					editedProps += {propId, propValue->Clone()}; // Since it's non-const method
				}
			}
			{
				auto it = std::find_if(propMethods.apiMethodsConst.begin(), propMethods.apiMethodsConst.end(), [&](const auto &method) {
					return method.name == s.name;
				});
				if (it != propMethods.apiMethodsConst.end()) {
					result = it->fn(propValue, s.args);
				}
			}
		}
		return result;
	};

	auto fEmGetProp = [this, &props, propList](const EntityManipulation::EMGetProp &s) {
		json res = json(nullptr);

		auto prop = propList->GetByName(s.name.data());
		assert(prop);
		if (!prop) return Viet::JsonWrapper(res);

		if (auto &val = props.GetPropertyValue(s.name.data(), propList)) {
			res = prop->ToApiFormat(val);
		}
		if (res == nullptr) {
			res = prop->ToApiFormat(prop->GetDefaultValue());
		}
		return Viet::JsonWrapper(res);
	};

	auto fEmSetProp = [this, &editedProps, propList](const EntityManipulation::EMSetProp &s) {
		editedProps += s.GetPropAssigns(propList);
		return Viet::JsonWrapper(json(nullptr));
	};

	std::vector<Viet::JsonWrapper> results(manip.manipulations.size(), json(nullptr));
	for (size_t i = 0; i < manip.manipulations.size(); ++i) {
		results[i] = std::visit(overloaded{ fEmCallMethod, fEmSetProp, fEmGetProp }, manip.manipulations[i]);
	}

	return { results, editedProps };
}

Viet::Promise<uint32_t> SkympServerView::GetMaxPlayers() noexcept {
	Viet::Promise<uint32_t> promise;
	pImpl->timerManager->SetTimeout(0, [=] {
		promise.Resolve(pImpl->maxPlayers);
	});
	return promise;
}

Viet::Promise<Viet::Void> SkympServerView::SendCustomPacket(const EntityInfo &user, const std::string &jsonContent) noexcept {
	Viet::Promise<Viet::Void> promise;

	try {
		auto userId = pImpl->sessionLoader->GetUserId(user);
		if (userId != Viet::Networking::g_invalidUserId) {
			MsgWrapper<msg::CustomPacket> msgWrapper(&*pImpl->seri, { jsonContent });
			pImpl->server->Send(userId, msgWrapper.GetBinary().first, msgWrapper.GetBinary().second, true);
		}
		pImpl->timerManager->SetTimeout(0, [promise] {
			promise.Resolve(Viet::Void());
		});
	}
	catch (std::exception &e) {
		std::string error = e.what();
		pImpl->timerManager->SetTimeout(0, [promise, error] {
			promise.Reject(error.data());
		});
	}
	return promise;
}

Viet::Promise<std::vector<EntityInfo>> SkympServerView::Find(const std::string &entityName, const Viet::FindRequest &findReq) noexcept {
	Viet::Promise<std::vector<EntityInfo>> promise;

	const int entityId = pImpl->GetEntityId(entityName);
	if (entityId == -1) {
		std::string err = "Unknwon entity name '" + entityName + "'";
		pImpl->timerManager->SetTimeout(0, [promise, err] {
			promise.Reject(err.data());
		});
		return promise;
	}
	pImpl->world->ResolveDbActors(entityId, findReq).Then([=](std::vector<EntityInfo> ei) {
		auto locally = pImpl->world->ResolveActorsLocally(entityId, findReq);

		std::sort(ei.begin(), ei.end());
		std::sort(locally.begin(), locally.end());

		std::vector<EntityInfo> dest;
		dest.reserve(locally.size() + ei.size());
		std::set_union(locally.begin(), locally.end(), ei.begin(), ei.end(), std::back_inserter(dest));

		promise.Resolve(std::move(dest));
	}).Catch([=](std::string err) {
		assert(0);
		err = "Something gone wrong in ResolveActors function: " + err;
		promise.Reject(err.data());
	});

	return promise;
}

Viet::Promise<EntityManipulationResult> SkympServerView::Manipulate(const std::string &entityName, const EntityInfo &optionalExistingEi, const EntityManipulation &manip) noexcept {
	auto hookFn = [&] (EntityInfo actor, const EntityManipulation &manip, SkympServerView::Impl::ApplyManipulationRes &manipRes) {
		for (size_t i = 0; i < manipRes.opResults.size(); ++i) {
				auto &m = manip.manipulations[i];
				auto fGet = [&](const EntityManipulation::EMGetProp &v) {
					if (v.name == "user") {
						if (!actor.IsEmpty()) {
							manipRes.opResults[i] = json(pImpl->sessionLoader->GetSessionByActor(actor).ToString());
						}
						else {
							manipRes.opResults[i] = json(std::string());
						}
					}
					if (v.name == "customPacket") {
						assert(0 && "Not implemented");
					}
				};
				auto fSet = [&](const EntityManipulation::EMSetProp &v) {
					if (v.name == "user") {
						assert(v.newValue.v.is_string());
						assert(!actor.IsEmpty());
						if (v.newValue.v.is_string() && !actor.IsEmpty()) {
							auto userStr = v.newValue.v.get<std::string>();
							EntityInfo user = userStr.size() ? EntityInfo::FromString(userStr) : EntityInfo();
							auto lastUser = pImpl->sessionLoader->GetSessionByActor(actor);
							if (lastUser != user) {
								pImpl->sessionLoader->SetActor(lastUser, {});
								pImpl->sessionLoader->SetActor(user, actor);
							}
						}
					}
					if (v.name == "customPacket") {
						auto user = pImpl->sessionLoader->GetSessionByActor(actor);
						if (user.IsEmpty() == false) {
							this->SendCustomPacket(user, v.newValue.v.dump());
						}
					}
				};
				auto fDefault = [](const auto &) {};
				std::visit(overloaded{ fGet, fSet, fDefault }, m);
			}
	};
	
	Viet::Promise<EntityManipulationResult> promise;

	pImpl->timerManager->SetTimeout(0, [=] {
		const int entityId = pImpl->GetEntityId(entityName);
		if (entityId == -1) {
			std::string error = "Unknown entity name '" + entityName + "'";
			return promise.Reject(error.data());
		}

		const bool isCreate = optionalExistingEi.IsEmpty();
		const auto entityInfo = isCreate ? HashToEntityInfo(pImpl->hashGen->Generate()) : optionalExistingEi;

		auto propList = EntityAccess::GetPropList_(pImpl->entities[entityId]);
		assert(propList);
		if (!propList) return promise.Resolve({});

		if (isCreate) {

			std::string outUnfoundProperty;
			auto propAssigns = manip.GetPropAssigns(propList, outUnfoundProperty);
			if (!outUnfoundProperty.empty()) {
				std::string error = "Unknown property name '" + outUnfoundProperty + "'";
				return promise.Reject(error.data());
			}

			auto manipRes = pImpl->ApplyManipulation(propAssigns, manip, propList);
			hookFn(entityInfo, manip, manipRes);

			EntityProps propsUnion = propAssigns;
			propsUnion += manipRes.propsToSave;

			pImpl->world->CreateActor(entityId, entityInfo, propsUnion, [=] {
				pImpl->world->FindActor(entityId, entityInfo, [=](std::optional<World::ActorFindResult> findRes) {
					assert(findRes);
					promise.Resolve(EntityManipulationResult({ manipRes.opResults, entityInfo }));
				});
			});
		}

		if (!isCreate) {
			pImpl->world->FindActor(entityId, entityInfo, [=](std::optional<World::ActorFindResult> findRes) {
				if (!findRes) {
					return promise.Reject("Instance not found");
				}

				auto manipRes = pImpl->ApplyManipulation(findRes->GetProperties(), manip, propList);
				hookFn(entityInfo, manip, manipRes);

				auto returnValue = EntityManipulationResult({ manipRes.opResults, entityInfo });

				if (auto promise2 = findRes->Edit(manipRes.propsToSave, propList)) {
					promise2->Then([=](Viet::Void) {
						promise.Resolve(returnValue);
					}).Catch([=](const char *err) {
						assert(0);
						promise.Reject(err);
					});
				}
				else {
					promise.Resolve(returnValue);
				}
			});
		}
	});

	return promise;
}

Viet::Promise<Viet::Void> SkympServerView::SetActor(const EntityInfo &user, const std::string &entityName, const EntityInfo &newActor) noexcept {
	Viet::Promise<Viet::Void> promise;

	pImpl->timerManager->SetTimeout(0, [=] {
		auto newActorStr = newActor.ToString();
		auto userStr = user.ToString();
		pImpl->logger->Info("SetActor(coll='%s', user='%s', newActor='%s')", entityName.data(), userStr.data(), newActorStr.data());

		const int entityId = pImpl->GetEntityId(entityName);
		if (entityId == -1) {
			std::string error = "Unknwon entity name '" + entityName + "'";
			return promise.Reject(error.data());
		}

		auto propList = EntityAccess::GetPropList_(pImpl->entities[entityId]);
		assert(propList);
		if (!propList) return promise.Resolve(Viet::Void());

		const bool success = pImpl->sessionLoader->SetActor(user, newActor);
		if (!success) {
			return promise.Reject("User not found");
		}

		if (newActor.IsEmpty()) {
			this->ClearMyProperties(user, entityId);
			this->SendActorId(user, ICellProcess::InstanceId(-1, msg::g_badId), newActor);
			promise.Resolve(Viet::Void());
		}
		else {
			pImpl->world->FindActor(entityId, newActor, [=](std::optional<World::ActorFindResult> findRes) {
				assert(findRes);
				if (!findRes) return promise.Reject("Instance not found");
				if (!findRes->IsInCache()) {
					// It's OK. If you attach uncached instance to user, then the user is expected to see nothing
					return promise.Resolve(Viet::Void());
				}
				auto acAddr = pImpl->world->FindActorLocally(newActor);
				assert(acAddr);
				if (!acAddr) return promise.Reject("Bad acAddr");
				//if (!acAddr) return promise.Resolve(Viet::Void());
				acAddr->cp.StreamOutAll(acAddr->inst);

				// Re-Edit allows user to see WorldState with it's actor without sending movement from client:
				/// let actor = await remoteServer.createActor().setEnabled(true);
				/// await user.setActor(actor);
				/// /* Expect to see myself in the WorldState here! */
				acAddr->cp.Edit(acAddr->inst, findRes->GetProperties(), true);
				pImpl->logger->Debug("StreamOutAll and Re-Edit");

				this->SendMyProperties(user, entityId, findRes->GetProperties());
				this->SendActorId(user, acAddr->inst, newActor);

				promise.Resolve(Viet::Void());
			});
		}
	});

	return promise;
}

Viet::Promise<EntityInfo> SkympServerView::GetActor(const std::string &entityName, const EntityInfo &user) noexcept {
	Viet::Promise<EntityInfo> promise;
	
	pImpl->timerManager->SetTimeout(0, [=] {
		auto &ac = pImpl->sessionLoader->GetActor(user);
		if (ac.IsEmpty()) {
			return promise.Resolve({});
		}

		int entityId = pImpl->GetEntityId(entityName);
		if (entityId == -1) {
			std::string str = "Unknown entity name '" + entityName + "'";
			return promise.Reject(str.data());
		}

		// Move instance into cache if it isn't cached
		pImpl->world->FindActor(entityId, ac, [=](std::optional<World::ActorFindResult> findRes) {
			if (findRes) {
				return promise.Resolve(ac);
			}
			else {
				return promise.Resolve({});
			}
		});
	});

	return promise;
}

namespace Viet {
	class TransactionEntryAccess {
	public:
		static auto &Actions(const TransactionEntry &e) {
			return e.actions;
		}

		static auto &SelectorType(const TransactionEntry &e) {
			return e.selectorType;
		}

		static auto &Selector(const TransactionEntry &e) {
			return e.selector;
		}

		static auto &FindReqSelector(const TransactionEntry &e) {
			return e.findReqSelector;
		}
	};
}

Viet::Promise<Viet::TransactionResult> SkympServerView::RunTransaction(const Viet::Transaction &trans) noexcept {
	using Access = Viet::TransactionEntryAccess;

	using PerEntryPromise = Viet::Promise<std::vector<EntityManipulationResult>>;
	std::vector<PerEntryPromise> progress;
	progress.resize(trans.entries.size());

	for (size_t entryId = 0; entryId < trans.entries.size(); ++entryId) {
		auto &entry = trans.entries[entryId];
		auto manips = Viet::ActionsAccess::ActionsToManip(Access::Actions(entry));
		auto sel = Access::Selector(entry);
		auto findReqSel = Access::FindReqSelector(entry);

		switch (Access::SelectorType(entry)) {
		case Viet::SelectorType::ArrayOfInstances:
			assert(!sel.empty());
			if (!sel.empty()) {
				std::vector<Viet::Promise<EntityManipulationResult>> pr;
				const std::string &entityName = sel[0];
				for (size_t i = 1; i < sel.size(); ++i) {
					auto ei = EntityInfo::FromString(sel[i]);
					pr.push_back(this->Manipulate(entityName, ei, manips));
				}
				progress[entryId] = Viet::Promise<EntityManipulationResult>::All(pr);
			}
			break;
		case Viet::SelectorType::Instance:
			assert(sel.size() == 2);
			if (sel.size() == 2) {
				const std::string &entityName = sel[0];
				const auto ei = EntityInfo::FromString(sel[1]);

				progress[entryId] = Viet::Promise<EntityManipulationResult>::All({ this->Manipulate(entityName, ei, manips) });
			}
			break;
		case Viet::SelectorType::NewInstance:
			assert(sel.size() == 1);
			if (sel.size() == 1) {
				const std::string &entityName = sel[0];
				progress[entryId] = Viet::Promise<EntityManipulationResult>::All({ this->Manipulate(entityName, EntityInfo(), manips) });
			}
			break;
		case Viet::SelectorType::FindOne:
		case Viet::SelectorType::FindMany:
			assert(!sel.empty());
			assert(findReqSel.has_value());
			if (!sel.empty() && findReqSel.has_value()) {
				const std::string &entityName = sel[0];

				Viet::Promise<std::vector<EntityManipulationResult>> promise;
				this->Find(entityName, *findReqSel).Then([this, promise, entityName, manips](const std::vector<EntityInfo> &findResults) {
					if (!findResults.empty()) {
						std::vector<Viet::Promise<EntityManipulationResult>> pr;
						for (auto &findResult : findResults) {
							pr.push_back(this->Manipulate(entityName, findResult, manips));
						}
						Viet::Promise<EntityManipulationResult>::All(pr).Then(promise);
					}
					else {
						promise.Reject("Assertion failed: findResults.size() == 1");
					}
				});

				progress[entryId] = promise;
			}
			break;
		default:
			assert(0 && "Unknown selector");
			break;
		}
	}

	Viet::Promise<Viet::TransactionResult> promise;

	PerEntryPromise::All(progress).Then([promise](const std::vector<std::vector<EntityManipulationResult>> &results) {
		Viet::TransactionResult transRes;

		transRes.entries.resize(results.size());
		for (size_t entry = 0; entry < results.size(); ++entry) {
			auto &actionsResults = transRes.entries[entry].perInstanceResults;
			for (auto &manipRes : results[entry]) {
				actionsResults.push_back(manipRes.ToActionsResult());
			}
		}

		promise.Resolve(transRes);
	}).Catch([promise](const char *err) {
		promise.Reject(err);
	});

	return promise;
}

void SkympServerView::ProcessCallResult(const evn::CallResult &cr, const char *jsEvnName) noexcept {
	pImpl->gamemodeController->SendEvent(cr);
}

void SkympServerView::UpdateGui(const EntityInfo &sessionKey) noexcept {
	const auto userId = pImpl->sessionLoader->GetUserId(sessionKey);
	assert(userId != Viet::Networking::g_invalidUserId);
	if (userId == Viet::Networking::g_invalidUserId) return;

	try {
		MsgWrapper<msg::GuiJson> msgWrapper(&*pImpl->seri, { pImpl->frontEndFiles });
		pImpl->server->Send(userId, msgWrapper.GetBinary().first, msgWrapper.GetBinary().second, true);
	}
	catch (std::exception &e) {
		pImpl->logger->Error("UpdateGui - %s", e.what());
	}
}

void SkympServerView::CreateActor(const EntityInfo &session, int entityId, const EntityInfo &actor, const EntityProps &props) noexcept {
	assert(entityId >= 0);
	assert(entityId < (int)pImpl->entities.size());
	if (entityId < 0 || entityId >= (int)pImpl->entities.size()) return;

	auto propList = EntityAccess::GetPropList_(pImpl->entities[entityId]);
	assert(propList);
	if (!propList) return;

	const auto userId = pImpl->sessionLoader->GetUserId(session);
	if (userId != Viet::Networking::g_invalidUserId) {
		auto acAddr = pImpl->world->FindActorLocally(actor);
		assert(acAddr);
		if (acAddr) {
			const msg::ServerAcId serverAcId(acAddr->inst.actorId, actor.GetUInt64Hash());
			MsgWrapper msgWrapper(&*pImpl->seri, msg::CreatePlayer{ acAddr->inst.entityId, serverAcId, FilterPropsBeforeSend(props, propList) }, propList);
			pImpl->server->Send(userId, msgWrapper.GetBinary().first, msgWrapper.GetBinary().second, true);
		}
	}
}

void SkympServerView::DestroyActor(const EntityInfo &session, const EntityInfo &actor) noexcept {
	const auto userId = pImpl->sessionLoader->GetUserId(session);
	if (userId != Viet::Networking::g_invalidUserId) {
		MsgWrapper<msg::DestroyPlayer> msgWrapper(&*pImpl->seri, { actor.GetUInt64Hash() });
		pImpl->server->Send(userId, msgWrapper.GetBinary().first, msgWrapper.GetBinary().second, true);
	}
}

void SkympServerView::SendActorProperties(const EntityInfo &session, const EntityInfo &ownerActor, int entityId, const EntityProps &props) noexcept {
	assert(entityId >= 0);
	assert(entityId < (int)pImpl->entities.size());
	if (entityId < 0 || entityId >= (int)pImpl->entities.size()) return;

	auto propList = EntityAccess::GetPropList_(pImpl->entities[entityId]);
	assert(propList);
	if (!propList) return;

	const auto userId = pImpl->sessionLoader->GetUserId(session);
	if (userId != Viet::Networking::g_invalidUserId) {

		auto acAddr = pImpl->world->FindActorLocally(ownerActor);
		assert(acAddr);
		if (acAddr) {
			EntityProps reliable = props, unreliable = props;
			unreliable.Exclude(Viet::PropertyFlags::ReliableNetworking, propList);
			reliable.Filter(Viet::PropertyFlags::ReliableNetworking, propList);

			for (auto props : { &unreliable, &reliable }) {
				*props = FilterPropsBeforeSend(*props, propList);
				if (props->IsEmpty()) continue;

				msg::UpdateActorProperties msg;
				msg.props = *props;
				msg.instanceId.idx = acAddr->inst.actorId;
				msg.instanceId.eiHash = ownerActor.GetUInt64Hash();
				msg.entityId = acAddr->inst.entityId;

				MsgWrapper msgWrapper(&*pImpl->seri, msg, propList);
				auto[ptr, length] = msgWrapper.GetBinary();
				std::vector<uint8_t> v(length);
				memcpy(v.data(), ptr, length);

				bool isReliable = props == &reliable;
				pImpl->server->Send(userId, ptr, length, isReliable);
			}
		}
	}
}

void SkympServerView::SendMyProperties(const EntityInfo &session, int entityId, const EntityProps &props) noexcept {
	assert(props.IsEmpty() == false);

	assert(entityId >= 0);
	assert(entityId < (int)pImpl->entities.size());
	if (entityId < 0 || entityId >= (int)pImpl->entities.size()) return;

	auto propList = EntityAccess::GetPropList_(pImpl->entities[entityId]);
	assert(propList);
	if (!propList) return;

	const auto userId = pImpl->sessionLoader->GetUserId(session);
	if (userId != Viet::Networking::g_invalidUserId) {
		auto filteredProps = FilterPropsBeforeSend(props, propList);
		if (!filteredProps.IsEmpty()) {
			MsgWrapper msgWrapper(&*pImpl->seri, msg::UpdateMyProperties{ entityId, filteredProps }, propList);
			bool reliable = true; // Always reliable since SendMyProperties are not so often called
			pImpl->server->Send(userId, msgWrapper.GetBinary().first, msgWrapper.GetBinary().second, reliable);
		}
	}
}

void SkympServerView::ClearMyProperties(const EntityInfo &session, int entityId) noexcept {
	assert(entityId >= 0);
	assert(entityId < (int)pImpl->entities.size());
	if (entityId < 0 || entityId >= (int)pImpl->entities.size()) return;

	const auto userId = pImpl->sessionLoader->GetUserId(session);
	if (userId != Viet::Networking::g_invalidUserId) {
		Properties *propList = nullptr;
		MsgWrapper msgWrapper(&*pImpl->seri, msg::UpdateMyProperties{ entityId, EntityProps(), UpdatePropertyOp::Assign }, propList);
		pImpl->server->Send(userId, msgWrapper.GetBinary().first, msgWrapper.GetBinary().second, true);
	}
}

void SkympServerView::SendActorId(const EntityInfo &session, const ICellProcess::InstanceId &inst, const EntityInfo &actor) noexcept {
	const auto userId = pImpl->sessionLoader->GetUserId(session);
	if (userId != Viet::Networking::g_invalidUserId) {
		msg::ActorId m;
		m.entityId = inst.entityId;
		m.actorId.idx = inst.actorId;
		m.actorId.eiHash = actor.GetUInt64Hash();
		MsgWrapper msgWrapper(&*pImpl->seri, m);
		pImpl->server->Send(userId, msgWrapper.GetBinary().first, msgWrapper.GetBinary().second, true);
	}
}