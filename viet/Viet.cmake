find_package(nlohmann_json)
find_package(Boost)

file(GLOB_RECURSE VIETLIB_SOURCES "viet/src/viet-lib/*" "viet/include/viet/*")

file(GLOB_RECURSE tmp "viet/include/viet/*")
source_group(include FILES ${tmp})
file(GLOB_RECURSE tmp "viet/src/viet-lib/*")
source_group(sources FILES ${tmp})

add_library(vietlib STATIC ${VIETLIB_SOURCES})

target_link_libraries(vietlib PRIVATE nlohmann_json::nlohmann_json)
target_link_libraries(vietlib PRIVATE ${Boost_LIBRARIES})
target_include_directories(vietlib PRIVATE ${Boost_INCLUDE_DIRS})

target_include_directories(vietlib PRIVATE
  "viet/src/viet-lib"
  "viet/src/third-party"
  ${VIET_INCLUDE_DIRS}
)

set(VIET_INCLUDE ${VIET_INCLUDE_DIRS})

include(${CMAKE_CURRENT_SOURCE_DIR}/viet/src/viet-raknet/_VietRaknet.cmake)
include(${CMAKE_CURRENT_SOURCE_DIR}/viet/src/viet-mongocxx/_VietMongocxx.cmake)
include(${CMAKE_CURRENT_SOURCE_DIR}/viet/src/viet-node/_VietNode.cmake)
