#pragma once
#include <string>
#include <cstdint>
#include <EntityInfo.h>

namespace {
	inline EntityInfo HashToEntityInfo(const std::string &hash) {
		std::hash<std::string> hashFn;
		const auto v = (uint16_t)hashFn(hash);
		return { hash, v };
	}
}
