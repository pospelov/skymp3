#include <viet-mongocxx/VietMongocxx.h>

#include <viet/Logging.h>

#include <bsoncxx/json.hpp>
#include <mongocxx/client.hpp>
#include <mongocxx/stdx.hpp>
#include <mongocxx/uri.hpp>
#include <mongocxx/instance.hpp>
#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/document/view_or_value.hpp>
#include <bsoncxx/document/view.hpp>
#include <bsoncxx/document/value.hpp>
#include <bsoncxx/document/element.hpp>
#include <nlohmann/json.hpp>

using bsoncxx::builder::stream::close_array;
using bsoncxx::builder::stream::close_document;
using bsoncxx::builder::stream::document;
using bsoncxx::builder::stream::finalize;
using bsoncxx::builder::stream::open_array;
using bsoncxx::builder::stream::open_document;
using json = nlohmann::json;

namespace {
	class MongoOperators : public Viet::NoSqlDatabase::INoSqlOperators {
	public:
		const char *And() const noexcept override {
			return "$and";
		}

		const char *Or() const noexcept override {
			return "$or";
		}

		const char *Eq() const noexcept override {
			return "$eq";
		}

		const char *Gt() const noexcept override {
			return "$gt";
		}

		const char *Gte() const noexcept override {
			return "$gte";
		}

		const char *Lt() const noexcept override {
			return "$lt";
		}

		const char *Lte() const noexcept override {
			return "$lte";
		}

		const char *Ne() const noexcept override {
			return "$ne";
		}

		const char *Set() const noexcept override {
			return "$set";
		}

		const char *Unset() const noexcept override {
			return "$unset";
		}

		const char *Exist() const noexcept override {
			return "$exists";
		}
	};

	class MongoCollection : public Viet::NoSqlDatabase::INoSqlCollection {
	public:
		MongoCollection(mongocxx::collection coll_, std::shared_ptr<Viet::Logging::Logger> logger_) : coll(coll_), logger(logger_) {
		}

		std::vector<json> Find(const json &filter) noexcept override {
			try {
				auto bsonFilter = bsoncxx::from_json(filter.dump());
				mongocxx::cursor cursor = this->coll.find(std::move(bsonFilter));

				std::vector<json> res;
				for (auto doc : cursor) {
					auto j = json::parse(bsoncxx::to_json(doc));
					j.erase("_id");
					res.push_back(j);
				}
				return res;
			}
			catch (std::exception &e) {
				this->logger->Error("MongoCollection::Find %s", e.what());
				return {};
			}
		}

		json FindOne(const json &filter) noexcept override {
			try {
				auto bsonFilter = bsoncxx::from_json(filter.dump());
				auto optResult = this->coll.find_one(std::move(bsonFilter));
				if (!optResult) return nullptr;
				auto j = json::parse(bsoncxx::to_json(*optResult));
				j.erase("_id");
				return j;
			}
			catch (std::exception &e) {
				this->logger->Error("MongoCollection::FindOne %s", e.what());
				return nullptr;
			}
		}

		void InsertOne(const json &doc) noexcept override {
			try {
				auto bsonDoc = bsoncxx::from_json(doc.dump());
				this->coll.insert_one(std::move(bsonDoc));
			}
			catch (std::exception &e) {
				this->logger->Error("MongoCollection::InsertOne %s", e.what());
			}
		}

		bool IsReplace(const json &j) {
			for (auto it = j.begin(); it != j.end(); ++it) {
				if (it.key().size() > 0 && it.key().front() == '$') return false;
				if (it.value().is_object()) {
					if (!IsReplace(it.value())) {
						return false;
					}
				}
			}
			return true;
		}

		void UpdateOne(const json &filter, const json &update, bool upsert) noexcept override {
			try {
				auto bsonFilter = bsoncxx::from_json(filter.dump());
				auto bsonUpd = bsoncxx::from_json(update.dump());

				const bool replace = IsReplace(update);

				if (replace) {
					auto ops = mongocxx::v_noabi::options::replace().upsert(upsert);
					this->coll.replace_one(std::move(bsonFilter), std::move(bsonUpd), ops);
				}
				else {
					auto ops = mongocxx::v_noabi::options::update().upsert(upsert);
					this->coll.update_one(std::move(bsonFilter), std::move(bsonUpd), ops);
				}
			}
			catch (std::exception &e) {
				auto updateDump = update.dump();
				this->logger->Error("MongoCollection::UpdateOne %s (update=%s)", e.what(), updateDump.data());
			}
		}

		void UpdateMany(const std::vector<std::pair<json, json>> &filterAndUpdate, bool upsert) noexcept override {
			try {
				auto bulk = coll.create_bulk_write();
				for (auto &[filter, upd] : filterAndUpdate) {
					auto bsonFilter = bsoncxx::from_json(filter.dump());
					auto bsonUpd = bsoncxx::from_json(upd.dump());

					bool replace = true;
					for (auto it = upd.begin(); it != upd.end(); ++it) {
						if (it.key().size() > 0 && it.key().front() == '$') replace = false;
					}
					
					if (replace) {
						mongocxx::model::replace_one op{ std::move(bsonFilter), std::move(bsonUpd) };
						op.upsert(upsert);
						bulk.append(op);
					}
					else {
						mongocxx::model::update_one op{ std::move(bsonFilter), std::move(bsonUpd) };
						op.upsert(upsert);
						bulk.append(op);
					}
				}
				auto result = bulk.execute();
			}
			catch (std::exception &e) {
				this->logger->Error("MongoCollection::UpdateMany %s", e.what());
			}
		}

		void Drop() noexcept override {
			try {
				this->coll.drop();
			}
			catch (std::exception &e) {
				this->logger->Error("MongoCollection::Drop %s", e.what());
			}
		}

		mongocxx::collection coll;
		std::shared_ptr<Viet::Logging::Logger> logger;
	};
}

struct Viet::NoSqlDatabase::Mongo::Impl {
	const std::string dbName;
	const std::shared_ptr<Viet::Logging::Logger> logger;
	std::shared_ptr<mongocxx::client> client;
	MongoOperators ops;
};

Viet::NoSqlDatabase::Mongo::Mongo(const Settings &settings) {
	static mongocxx::instance instance;
	pImpl.reset(new Impl{ settings.dbName, settings.logger });
	pImpl->client.reset(new mongocxx::client(mongocxx::uri(settings.mongoUri.data())));

	if (settings.startsWith.size() > 0) {
		int i = 0;
		for (auto cursor : pImpl->client->list_databases()) {
			auto j = json::parse(bsoncxx::to_json(cursor));
			auto dbName = j.at("name").get<std::string>();
			if (settings.dbIgnore.count(dbName) == 0) {
				if (!memcmp(dbName.data(), settings.startsWith.data(), settings.startsWith.size())) {
					(*pImpl->client)[dbName].drop();
					settings.logger->Notice("Dropped DB %d: %s", i++, dbName.data());
				}
			}
		}
	}
}

std::shared_ptr<Viet::NoSqlDatabase::INoSqlCollection> Viet::NoSqlDatabase::Mongo::GetCollection(const char *collectionName) noexcept {
	mongocxx::database db = (*pImpl->client)[pImpl->dbName];
	return std::shared_ptr<MongoCollection>(new MongoCollection(db[collectionName], pImpl->logger));
}

Viet::NoSqlDatabase::INoSqlOperators &Viet::NoSqlDatabase::Mongo::GetOperators() const noexcept {
	return pImpl->ops;
}