#include <viet/Client.h>

Viet::InstanceModel::InstanceModel(const char *entityName_, uint32_t idx_, const IWorldState *wst_) : entityName(entityName_), idx(idx_), wst(wst_) {
}

bool Viet::InstanceModel::IsValid() const noexcept {
	return this->wst->IsInstanceValid(this->entityName, this->idx);
}

bool Viet::InstanceModel::IsMe() const noexcept {
	return this->wst->IsInstanceMe(this->entityName, this->idx);
}

Viet::WorldStateValue Viet::InstanceModel::GetValue(const char *propName) const noexcept {
	return this->wst->GetInstancePropertyValue(this->entityName, this->idx, propName);
}

std::vector<const char *> Viet::InstanceModel::GetKeys() const noexcept {
	return this->wst->GetInstancePropertyNames(this->entityName, this->idx);
}

// ***

Viet::InstancesModel::InstancesModel(const char *entityName_, const IWorldState *wst_) : entityName(entityName_), wst(wst_) {
}

Viet::InstanceModel Viet::InstancesModel::operator[](uint32_t instanceIndex) const noexcept {
	return InstanceModel(this->entityName.data(), instanceIndex, this->wst);
}

uint32_t Viet::InstancesModel::GetPoolSize() const noexcept {
	return this->wst->GetEntityPoolSize(this->entityName.data());
}

const char *Viet::InstancesModel::GetEntityName() const noexcept {
	return this->entityName.data();
}