#include <mutex>
#include <cassert>
#include <unordered_map>
#include <atomic>
#include <vector>
#include <list>
#include <set>
#include <ctime>
#include <functional>
#include <EntityProps.h>
#include <GameplayPresenter.h>
#include <viet/Entity.h>

namespace {
	constexpr int g_numRetry = 8;
	constexpr float g_retryIntervalSeconds = 0.1f;

	struct RetryState {
		int retryAttemptsRemaining = g_numRetry;
		clock_t lastRetry = 0;
	};

	template <class Msg>
	class Tasks {
	public:
		void Tick(std::function<bool(const Msg &)> tryApplyFn) {
			auto it = tasks.begin();
			while (it != tasks.end()) {
				auto &[msg, retryState] = *it;

				if (clock() - retryState.lastRetry >= CLOCKS_PER_SEC * g_retryIntervalSeconds) {
					if (tryApplyFn(msg)) {
						it = tasks.erase(it);
					}
					else {
						retryState.retryAttemptsRemaining--;
						retryState.lastRetry = clock();

						if (retryState.retryAttemptsRemaining <= 0) {
							it = tasks.erase(it);
						}
					}
				}
				if (it == tasks.end()) break;
				++it;
			}
		}

		void Add(const Msg &msg) {
			tasks.push_back({ msg, {} });
		}

	private:
		std::list<std::pair<Msg, RetryState>> tasks;
	};
}

struct GameplayPresenter::Impl {
	const std::vector<Viet::Entity> entities;

	struct {
		std::recursive_mutex m;

		std::unique_ptr<GameplayModel> model;
		Tasks<msg::DestroyPlayer> destroyTasks;
		Tasks<msg::UpdateActorProperties> updateTasks;
		std::list<std::pair<std::string, std::shared_ptr<IPropertyValue>>> outputTasks;
		std::shared_ptr<INetController> netCont;
		bool ticked = false;

		bool IsMyIdValid() const noexcept {
			const auto entityId = model->localEntityId;
			const auto instId = model->localInstanceId;
			const bool isMyIdValid = entityId >= 0
				&& entityId < (int)model->contents.size()
				&& instId.idx >= 0
				&& instId.eiHash != 0
				&& instId.idx < (int)model->contents[entityId].size();
			return isMyIdValid;
		}
	} share;

	bool TryApplyDestroy(const msg::DestroyPlayer &msg) {
		assert(msg.instanceEiHash != 0);
		if (msg.instanceEiHash == 0) return false;

		std::lock_guard l(share.m);

		for (size_t entityId = 0; entityId < share.model->contents.size(); ++entityId) {
			auto &acs = share.model->contents[entityId];
			for (auto &ac : acs) {
				if (ac.eiHash == msg.instanceEiHash) {
					ac = {};
					while (!acs.empty() && !acs.back().valid) {
						acs.pop_back();
					}
					return true;
				}
			}
		}

		return false;
	}

	bool TryApplyUpdate(const msg::UpdateActorProperties &msg) {
		assert(msg.instanceId.idx >= 0);
		assert(msg.instanceId.eiHash != 0);
		assert(msg.entityId >= 0);
		if (msg.instanceId.idx < 0 || msg.instanceId.eiHash == 0 || msg.entityId < 0) return false;

		std::lock_guard l(share.m);

		if (msg.entityId >= (int)share.model->contents.size()) {
			return false;
		}

		auto &acs = share.model->contents[msg.entityId];
		if (acs.size() <= (size_t)msg.instanceId.idx || !acs[msg.instanceId.idx].valid || acs[msg.instanceId.idx].eiHash != msg.instanceId.eiHash) {
			return false;
		}

		auto &ac = acs[msg.instanceId.idx];
		ac.props += msg.props;
		msg.props.ForEach([&](PropId propId, const std::unique_ptr<IPropertyValue> &value) {
			if ((PropId)ac.numChanges.size() <= propId) {
				ac.numChanges.resize(propId + 1, 0);
			}
			++ac.numChanges[propId];
		});
		return true;
	}
};

GameplayPresenter::GameplayPresenter(std::shared_ptr<INetController> netCont, std::vector<Viet::Entity> entities) noexcept : pImpl(new Impl{ entities }) {
	pImpl->share.model.reset(new GameplayModel(entities));
	pImpl->share.netCont = netCont;
}

GameplayPresenter::~GameplayPresenter() {
	delete pImpl;
}

void GameplayPresenter::OnConnect(ConnectResult res) noexcept {
	std::lock_guard l(pImpl->share.m);
	pImpl->share.model.reset(new GameplayModel(pImpl->entities));

	assert(pImpl->share.ticked);
}

void GameplayPresenter::OnDisconnect() noexcept {
}

void GameplayPresenter::OnMessage(const msg::ActorId &m) noexcept {
	std::lock_guard l(pImpl->share.m);
	pImpl->share.model->localInstanceId = m.actorId;
	pImpl->share.model->localEntityId = m.entityId;
}

void GameplayPresenter::OnMessage(const msg::CreatePlayer &m) noexcept {
	assert(m.entityId >= 0);
	assert(m.instanceId.idx >= 0);
	assert(m.instanceId.eiHash != 0);
	if (m.entityId < 0 || m.instanceId.idx < 0 || m.instanceId.eiHash == 0) return;

	std::lock_guard l(pImpl->share.m);
	
	auto &cont = pImpl->share.model->contents;
	if ((int)cont.size() <= m.entityId) {
		cont.resize(m.entityId + 1);
	}

	auto &instances = cont[m.entityId];
	if ((int)instances.size() <= m.instanceId.idx) {
		instances.resize(m.instanceId.idx + 1);
	}

	auto &instance = instances[m.instanceId.idx];
	instance = {};
	instance.eiHash = m.instanceId.eiHash;
	instance.valid = true;
	instance.props = m.props;
}

void GameplayPresenter::OnMessage(const msg::DestroyPlayer &m) noexcept {
	assert(m.instanceEiHash != 0);

	std::lock_guard l(pImpl->share.m);
	pImpl->share.destroyTasks.Add(m);
}

void GameplayPresenter::OnMessage(const msg::SimpleCommand &) noexcept {
	assert(0 && "Not implemented");
};

void GameplayPresenter::OnMessage(const msg::UpdateActorProperties &m) noexcept {
	assert(m.entityId >= 0);
	assert(m.instanceId.idx >= 0);
	assert(m.instanceId.eiHash != 0);
	if (m.entityId < 0 || m.instanceId.idx < 0 || m.instanceId.eiHash == 0) return;

	std::lock_guard l(pImpl->share.m);
	pImpl->share.updateTasks.Add(m);
}

void GameplayPresenter::OnMessage(const msg::UpdateMyProperties &m) noexcept {
	std::lock_guard l(pImpl->share.m);
	auto &data = *pImpl->share.model;

	if (m.op == UpdatePropertyOp::Update) {
		pImpl->share.model->props += m.props;

		m.props.ForEach([&](PropId propId, const std::unique_ptr<IPropertyValue> &value) {
			if ((PropId)data.numChanges.size() <= propId) {
				data.numChanges.resize(propId + 1, 0);
			}
			++data.numChanges[propId];
		});

	}
	else if (m.op == UpdatePropertyOp::Assign) {
		pImpl->share.model->props = m.props;

		auto propsSum = data.props;
		propsSum += m.props;
		std::set<PropId> propIds;
		propsSum.ForEach([&](PropId propId, const std::unique_ptr<IPropertyValue> &) {
			propIds.insert(propId);
		});
		for (PropId propId : propIds) {
			if ((PropId)data.numChanges.size() <= propId) {
				data.numChanges.resize(propId + 1, 0);
			}
			++data.numChanges[propId];
		}
	}
	else {
		assert(0 && "Unknown UpdatePropertyOp");
	}
}

void GameplayPresenter::ProcessModel(const Callback &cb) noexcept {
	std::lock_guard l(pImpl->share.m);

	const bool isMyIdValid = pImpl->share.IsMyIdValid();

	if (isMyIdValid) {
		const auto entityId = pImpl->share.model->localEntityId;
		const auto instId = pImpl->share.model->localInstanceId;
		auto &myAc = pImpl->share.model->contents[entityId][instId.idx];
		assert(myAc.eiHash == instId.eiHash);

		auto backup = myAc.valid;
		myAc.valid = true;
		cb(*pImpl->share.model);
		myAc.valid = backup;
	}
	else {
		cb(*pImpl->share.model);
	}
}

void GameplayPresenter::SendPropertyChange(const char *propertyName, const IPropertyValue *propertyValue) noexcept {
	std::lock_guard l(pImpl->share.m);

	assert(propertyValue);
	if (!propertyValue) return;

	std::shared_ptr<IPropertyValue> propValue(propertyValue->Clone().release());
	pImpl->share.outputTasks.push_back({propertyName, propValue});
}

void GameplayPresenter::Tick() {
	std::lock_guard l(pImpl->share.m);
	pImpl->share.ticked = true;

	assert(pImpl->share.netCont);
	if (!pImpl->share.netCont) return;

	if (pImpl->share.IsMyIdValid()) {
		EntityProps props;
		bool propsEmpty = true;
		const auto entityId = pImpl->share.model->localEntityId;
		const auto instId = pImpl->share.model->localInstanceId;

		for (auto &[propertyName, propertyValue] : pImpl->share.outputTasks) {

			auto propList = EntityAccess::GetPropList_(pImpl->entities[entityId]);
			assert(propList);
			if (!propList) return;

			const PropId propId = propList->GetPropertyId(propertyName.data());
			props += {propId, propertyValue->Clone()};
			propsEmpty = false;
		}
		if (!propsEmpty) {
			pImpl->share.netCont->Send(msg::UpdateActorProperties({ entityId, instId, props }));
		}

		pImpl->share.outputTasks.clear();
	}

	pImpl->share.updateTasks.Tick([this](const msg::UpdateActorProperties &msg) {
		return pImpl->TryApplyUpdate(msg);
	});
	pImpl->share.destroyTasks.Tick([this](const msg::DestroyPlayer &msg) {
		return pImpl->TryApplyDestroy(msg);
	});
}