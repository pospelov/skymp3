#pragma once
#include <string>
#include <optional>
#include <napi.h>
#include <viet/Client.h> // Viet::json

class Bot : public Napi::ObjectWrap<Bot> {
public:
	static Napi::Object Init(Napi::Env env, Napi::Object exports);
	Bot(const Napi::CallbackInfo &info);

private:
	void Connect(const Napi::CallbackInfo &info);
	void Connect(Napi::Env env, const char *ip, int port, std::optional<std::string> sessionHash);

	void Kill(const Napi::CallbackInfo &info);
	void Kill(Napi::Env env);

	void ProcessWorldState(const Napi::CallbackInfo &info);

	void Send(const Napi::CallbackInfo &info);
	void Send(std::string propertyName, const Viet::json &newValue);

	Napi::Value NextCustomPacket(const Napi::CallbackInfo &info);

	static Napi::FunctionReference constructor;

	struct Impl;
	std::shared_ptr<Impl> pImpl;
};