#pragma once
#include <functional>
#include <vector>
#include <map>
#include <cstdint>
#include <optional>
#include <string>

#include <viet/Entity.h>
#include <viet/Networking.h>
#include <viet/Serialization.h>
#include <viet/InstanceModel.h>
#include <viet/WorldStateValue.h>
#include <viet/InstanceModel.h>

namespace Viet {
	class IWorldState {
		friend class Viet::InstanceModel;
		friend class Viet::InstancesModel;
	public:
		// Deprecated
		using Value = Viet::WorldStateValue;

		virtual Value GetMyPropertyValue(const char *propName) const noexcept = 0;
		virtual std::vector<const char *> GetMyPropertyNames() const noexcept = 0;

		template <class PropClass>
		auto GetMyPropertyValue() const noexcept {
			return this->GetMyPropertyValue(PropClass::name).As<typename PropClass::Value>();
		}

		virtual const char *GetMyEntity() const noexcept = 0;

		auto GetInstances(const char *entityName) const noexcept {
			return Viet::InstancesModel{ entityName, this };
		}

	protected:
		virtual bool IsInstanceValid(const char *entityName, uint32_t instanceIndex) const noexcept = 0;
		virtual bool IsInstanceMe(const char *entityName, uint32_t instanceIndex) const noexcept = 0;
		virtual Value GetInstancePropertyValue(const char *entityName, uint32_t instanceIndex, const char *propName) const noexcept = 0;
		virtual std::vector<const char *> GetInstancePropertyNames(const char *entityName, uint32_t instanceIndex) const noexcept = 0;
		virtual uint32_t GetNumEntities() const noexcept = 0;
		virtual uint32_t GetEntityPoolSize(const char *entityName) const noexcept = 0;

	public:
		// Deprecated
		using Instance = Viet::InstanceModel;
		using Instances = Viet::InstancesModel;
	};

	class IBaseContext {
	public:
		virtual ~IBaseContext() = default;
	};

	class IInstanceView {
	public:
		virtual ~IInstanceView() = default;
	};

	class IInstanceViewHolder {
	public:
		virtual IInstanceView *Find(const char *entityName, uint32_t index) = 0;
		virtual IInstanceView *Create(const char *entityName, uint32_t index) = 0;
		virtual void Delete(const char *entityName, uint32_t index) = 0;
		virtual bool IsStateResetNeeded(const char *entityName, uint32_t index) = 0;

		virtual ~IInstanceViewHolder() = default;
	};

	class Client {
	public:
		Client(std::vector<Entity> entities, const Networking::Plugin &netPlugin, const Serialization::Plugin &seriPlugin, std::optional<std::string> sessionHashOrEi = std::nullopt);
		~Client();

		void Tick() noexcept;

		//
		// Unsynchronized access allowed:
		//

		using Callback = std::function<void(const IWorldState *)>;
		void ProcessWorldState(const Callback &cb);

		void UpdateProperties(IBaseContext &baseContext, IInstanceViewHolder *instanceViewHolder, bool showMe) noexcept;

		template <class Event>
		void SendContextEvent(Event &evn) noexcept {
			std::shared_ptr<void> e(new Event(evn), [](void *ptr) { delete (Event *)ptr; });
			this->SendContextEventImpl(typeid(Event), e);
		}

		template <class PropClass, class PropValue>
		void SendPropertyChange(const PropValue &v) noexcept {
			this->SendPropertyChangeImpl(PropClass::name, &v);
		}
		void SendPropertyChange(const char *propertyName, const std::unique_ptr<IPropertyValue> &propertyValue) noexcept;

		void SendCustomPacket(const char *content) noexcept;

		Viet::json ToApiFormat(const char *entityName, const char *propertyName, const IPropertyValue *value) noexcept;
		std::unique_ptr<IPropertyValue> FromApiFormat(const char *entityName, const char *propertyName, Viet::json value) noexcept;

		std::map<std::string, std::vector<uint8_t>> GetFrontEndFiles() const noexcept;
		int GetNumFrontChanges() const noexcept;

		std::optional<std::string> NextCustomPacket() noexcept;

	private:
		void SendPropertyChangeImpl(const char *propertyName, const IPropertyValue *propertyValue) noexcept;
		void SendContextEventImpl(const std::type_info &typeInfo, const std::shared_ptr<void> &evn) noexcept;

		void UpdateImpl(IInstanceView *view, const Viet::Entity &entity, uint32_t instanceIndex, IBaseContext &base, const IWorldState *wst);

		struct Impl;
		Impl *const pImpl;
	};
}