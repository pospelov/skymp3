#pragma once
#include <memory>
#include <set>
#include <viet/NoSqlDatabase.h>
#include <viet/Logging.h>

namespace Viet {
	namespace NoSqlDatabase {
		class Mongo : public INoSqlDatabase {
		public:
			struct Settings {
				std::string mongoUri;
				std::string dbName;
				std::shared_ptr<Viet::Logging::Logger> logger;
				std::string startsWith; 
				std::set<std::string> dbIgnore;
			};

			Mongo(const Settings &settings);

			std::shared_ptr<INoSqlCollection> GetCollection(const char *collectionName) noexcept override;
			INoSqlOperators &GetOperators() const noexcept override;

		private:
			struct Impl;
			std::shared_ptr<Impl> pImpl;
		};
	}
}