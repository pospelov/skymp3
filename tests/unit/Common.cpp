#include "Common.h"

#include <iostream>
#include <viet-raknet/VietRakNet.h>
#include <viet-mongocxx/VietMongocxx.h>

namespace {
	
	class StdoutLogger : public Viet::Logging::ILoggerOutput {
	public:
		void Write(Viet::Logging::Severity severity, const char *txt) noexcept {
			std::cout << "StdoutLogger says [" << Viet::Logging::GetSeverityName(severity) << "] " << txt << std::endl;
		};
	};

	struct IncomingPacket {

		IncomingPacket() = default;

		IncomingPacket(const unsigned char *packet, size_t length) {
			this->optDataHolder.reset(new std::vector<uint8_t>(length));
			std::copy(packet, packet + length, this->optDataHolder->begin());

			this->p.reset(new Viet::Networking::Packet);
			this->p->data = this->optDataHolder->data();
			this->p->length = length;
			this->p->type = Viet::Networking::PacketType::Message;
		}

		std::shared_ptr<Viet::Networking::Packet> p;
		std::shared_ptr<std::vector<uint8_t>> optDataHolder;
	};

	class LocalServer : public Viet::Networking::IServer {
	public:
		LocalServer() {
		}

		Viet::Networking::IClient *CreateClient() {
			auto n = std::count(this->clients.begin(), this->clients.end(), nullptr);
			if (!n) this->clients.push_back(nullptr);

			for (size_t i = 0; i < this->clients.size(); ++i) {
				const auto userId = static_cast<Viet::Networking::UserId>(i);
				if (!this->clients[i]) {
					this->clients[i] = (new LocalClient(this, userId));

					IncomingPacket p;
					p.p.reset(new Viet::Networking::Packet{ Viet::Networking::PacketType::UserConnect });
					this->incomingPackets.push_back({ userId, p });

					IncomingPacket p2;
					p2.p.reset(new Viet::Networking::Packet{ Viet::Networking::PacketType::ConnectionAccepted });
					this->clients[i]->incomingPackets.push_back(p2);

					std::cout << "Connect" << std::endl;

					return this->clients[i];
				}
			}
			assert(0);
			return nullptr;
		}

		void Tick(const OnPacket &onPacket) noexcept {
			for (auto &[userId, packet] : this->incomingPackets) {
				onPacket(*packet.p, userId);
			}
			this->incomingPackets.clear();
		}

		void Send(Viet::Networking::UserId userId, const unsigned char *packet, size_t length, bool reliable) noexcept {
			assert(packet && packet[0] >= Viet::Networking::MinPacketId);

			if (this->clients.size() <= userId) return;
			if (this->clients[userId] == nullptr) return;

			IncomingPacket p(packet, length);
			this->clients[userId]->incomingPackets.push_back(p);
		}

	private:
		class LocalClient : public Viet::Networking::IClient {
		public:
			LocalClient(LocalServer *svr_, Viet::Networking::UserId myId_) : svr(svr_), myId(myId_) {
			}

			void Tick(const OnPacket &onPacket) noexcept override {
				for (auto &packet : this->incomingPackets) {
					onPacket(*packet.p);
				}
				this->incomingPackets.clear();
			}

			void Send(const unsigned char *packet, size_t length, bool reliable) noexcept override {
				assert(packet && packet[0] >= Viet::Networking::MinPacketId);

				IncomingPacket p(packet, length);
				this->svr->incomingPackets.push_back({ this->myId, p });
			}

			std::vector<IncomingPacket> incomingPackets;
			LocalServer *const svr;
			const Viet::Networking::UserId myId;
		};

		std::vector<LocalClient *> clients;
		std::vector<std::pair<Viet::Networking::UserId, IncomingPacket>> incomingPackets;
	};
}

struct VietEnv::Impl {
	std::unique_ptr<Viet::Client> cl;
	std::unique_ptr<Viet::Server> svr;
	std::unique_ptr<Viet::ApiClient> api;

	LocalServer *localServer = nullptr, *localGmServer = nullptr;
};

VietEnv::VietEnv(std::vector<Viet::Entity> entities, Viet::ApiClient::Listeners apiClientListeners_) {
	if (apiClientListeners_.onConnect == nullptr) apiClientListeners_.onConnect = [](auto, auto) {};
	if (apiClientListeners_.onUserCustomPacket == nullptr) apiClientListeners_.onUserCustomPacket = [](auto, auto) {};
	if (apiClientListeners_.onUserEnter == nullptr) apiClientListeners_.onUserEnter = [](auto) {};
	if (apiClientListeners_.onUserExit == nullptr) apiClientListeners_.onUserExit = [](auto) {};
	
	pImpl.reset(new Impl);

	// Common:

	Viet::Serialization::Plugin seri;
	seri.serializerFactory = [=] {
		return new Viet::Serialization::RakSerializer;
	};

	// Create server:

	Viet::Server::Settings settings;
	settings.devPassword = "qwerty";
	settings.maxPlayers = 1000;
	settings.entities = entities;
	settings.netPlugin.serverFactory = [=] {
		return pImpl->localServer = new LocalServer;
	};
	settings.gamemodeNetPlugin.serverFactory = [=] {
		return pImpl->localGmServer = new LocalServer;
	};
	settings.seriPlugin = seri;
	settings.loggingPlugin.loggerFactory = [] {
		return new StdoutLogger;
	};
	settings.nosqlPlugin.databaseFactory = [=] {
		const std::string dbName = "skymp_db_test_server_UNIT";

		Viet::NoSqlDatabase::Mongo::Settings s;
		auto envVal = TEST_MONGO_URI;
		assert(envVal);
		if (envVal) {
			s.mongoUri = envVal;
		}
		printf("SKYMP_TEST_MONGO_URI = %s\n", s.mongoUri.data());
		s.dbName = dbName;
		s.startsWith = "skymp_db_test_server_UNIT";
		s.dbIgnore = {};
		s.logger.reset(new Viet::Logging::Logger(new StdoutLogger));

		auto db = new Viet::NoSqlDatabase::Mongo(s);
		return db;
	};

	pImpl->svr.reset(new Viet::Server(settings));

	// Create client

	Viet::Networking::Plugin net;
	net.clientFactory = [=] {
		assert(pImpl->localServer);
		return &*pImpl->localServer->CreateClient();
	};
	pImpl->cl.reset(new Viet::Client(entities, net, seri));

	// Create API Client

	Viet::ApiClient::Settings s;
	s.devPassword = settings.devPassword;
	s.frontEndFiles = {};
	s.gamemodeNetPlugin.clientFactory = [=] {
		assert(pImpl->localGmServer);
		return pImpl->localGmServer->CreateClient();
	};
	s.loggingPlugin.loggerFactory = [] { return new StdoutLogger; };
	s.seriPlugin = seri;
	s.serverId = "wtfidk";

	Viet::ApiClient::Listeners listeners = apiClientListeners_;

	pImpl->api.reset(new Viet::ApiClient(s, listeners));
}

Viet::Client &VietEnv::Client() {
	return *pImpl->cl;
}

Viet::Server &VietEnv::Server() {
	return *pImpl->svr;
}

Viet::ApiClient &VietEnv::ApiClient() {
	return *pImpl->api;
}