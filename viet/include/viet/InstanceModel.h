#pragma once
#include <cstdint>
#include <viet/WorldStateValue.h>

namespace Viet {
	class IWorldState;

	class InstanceModel {
	public:
		InstanceModel(const char *entityName_, uint32_t idx_, const IWorldState *wst_);

		bool IsValid() const noexcept;
		bool IsMe() const noexcept;
		WorldStateValue GetValue(const char *propName) const noexcept;
		std::vector<const char *> GetKeys() const noexcept;

		template <class PropClass>
		auto GetValue() const noexcept {
			using V = typename PropClass::Value;
			const char *name = PropClass::name;
			std::optional<V> res;
			if (auto p = this->GetValue(name).As<V>()) {
				res = *p;
			}
			return res;
		}

	private:
		const char *const entityName;
		const uint32_t idx;
		const IWorldState *const wst;
	};

	class InstancesModel {
	public:
		InstancesModel(const char *entityName_, const IWorldState *wst_);

		InstanceModel operator[](uint32_t instanceIndex) const noexcept;
		uint32_t GetPoolSize() const noexcept;
		const char *GetEntityName() const noexcept;

	private:
		const std::string entityName;
		const IWorldState *const wst;
	};
}