#pragma once
#include <IProperty.h>

struct FindMethodCall {
	std::string propName;
	std::string method;
	PropertyMethodArguments arguments;

	template <class Archive>
	void serialize(Archive &ar, unsigned int version) {
		ar & propName & method & arguments;
	}
};