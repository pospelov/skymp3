#pragma once
#include <viet/Networking.h>
#include <EntityInfo.h>


class UserManager {
public:
	using UserId = Viet::Networking::UserId;

	UserManager(int maxPlayers);
	~UserManager();

	void AddConnected(UserId id) noexcept;
	void AttachSession(UserId id, const EntityInfo &sessionKey) noexcept;
	void RemoveConnected(UserId id) noexcept;

	bool IsConnected(UserId id) const noexcept;
	const EntityInfo &GetSession(UserId id) const noexcept; // Returns an empty EntityInfo if user with id isn't connected

private:
	struct Impl;
	Impl *const pImpl;
};