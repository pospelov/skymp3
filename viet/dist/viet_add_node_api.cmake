function (viet_add_node_api TARGET SOURCES)
  set(node_dir "${CMAKE_CURRENT_BINARY_DIR}/node_dir_${TARGET}")
  if (NOT EXISTS ${node_dir})
    file(MAKE_DIRECTORY ${node_dir})
  endif()

  if (EXISTS ${VIET_DIST_PATH}/npm/package.json)
    set(basedir ${VIET_DIST_PATH}/npm)
  elseif(EXISTS ${CMAKE_SOURCE_DIR}/viet/src/viet-node/package.json)
    set(basedir ${CMAKE_SOURCE_DIR}/viet/src/viet-node)
  else()
    message(FATAL_ERROR "cmake-js stuff not found")
  endif()

  file(COPY ${basedir}/package.json DESTINATION ${node_dir})
  file(COPY ${basedir}/CMakeLists.txt DESTINATION ${node_dir})

  set(NODE_DIR ${node_dir})

  if (EXISTS ${VIET_DIST_PATH}/npm.cmake)
    include(${VIET_DIST_PATH}/npm.cmake)
  elseif(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/viet/dist/npm.cmake)
    include(${CMAKE_CURRENT_SOURCE_DIR}/viet/dist/npm.cmake)
  else()
    message(FATAL_ERROR "npm.cmake not found")
  endif()

  add_library(${TARGET} SHARED ${SOURCES} ${NPM_SRC})
  target_link_libraries(${TARGET} PRIVATE ${NPM_LIB})
  target_include_directories(${TARGET} PRIVATE ${NPM_INC})

  set(node_dir ${node_dir} PARENT_SCOPE)
endfunction()
