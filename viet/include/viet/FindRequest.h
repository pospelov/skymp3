#pragma once
#include <string>
#include <vector>
#include <optional>
#include <viet/JsonWrapper.h>

namespace Viet {
	class FindCondition {
	public:
		enum class ComparisonOperator {
			Equal,
			NotEqual,
			Greater,
			GreaterOrEqual,
			Less,
			LessOrEqual
		};

		FindCondition &Compare(const char *path, ComparisonOperator op, const json &value) {
			this->entries.push_back({});
			this->entries.back().comp = EntryCompare{ path, op, value };
			return *this;
		}

		FindCondition &Or(std::vector<FindCondition> conditions) {
			this->entries.push_back({});
			this->entries.back()._or = EntryOr{ conditions };
			return *this;
		}

		FindCondition &And(std::vector<FindCondition> conditions) {
			this->entries.push_back({});
			this->entries.back()._and = EntryAnd{ conditions };
			return *this;
		}

		template <class Archive>
		void serialize(Archive &a, int) {
			a & entries;
		}

		struct EntryCompare {
			std::string path;
			ComparisonOperator op;
			JsonWrapper value;

			template <class Archive>
			void serialize(Archive &a, int) {
				a & path & op & value;
			}
		};

		struct EntryOr {
			std::vector<FindCondition> childs;

			template <class Archive>
			void serialize(Archive &a, int) {
				a & childs;
			}
		};

		struct EntryAnd {
			std::vector<FindCondition> childs;

			template <class Archive>
			void serialize(Archive &a, int) {
				a & childs;
			}
		};

		struct Entry {
			std::optional<EntryCompare> comp;
			std::optional<EntryOr> _or;
			std::optional<EntryAnd> _and;

			template <class Archive>
			void serialize(Archive &a, int) {
				a & comp & _or & _and;
			}
		};

		std::vector<Entry> entries;
	};

	class FindRequest {
	public:
		FindCondition condition;
		uint32_t limit = 0;

		template <class Archive>
		void serialize(Archive &a, int) {
			a & condition & limit;
		}
	};
}