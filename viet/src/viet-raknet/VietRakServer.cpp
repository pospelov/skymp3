#include <memory>
#include <vector>
#include <stdexcept>
#include <raknet/MessageIdentifiers.h>
#include <raknet/RakPeer.h>
#include <raknet/PacketPriority.h>

#include <viet-raknet/VietRakNet.h>

#include "IdManager.h"


using RakGUID = RakNet::RakNetGUID;

using namespace Viet::Networking;

struct RakServer::Impl {
	std::unique_ptr<RakNet::RakPeerInterface> peer;
	std::unique_ptr<RakNet::SocketDescriptor> socket;
	std::unique_ptr<IdManager> idManager;

	Packet packetDisconnect{ PacketType::UserDisconnect, nullptr, 0 };
	Packet packetConnect{ PacketType::UserConnect, nullptr, 0 };
};

RakServer::RakServer(UserId maxConnections, unsigned short port, int timeoutTimeMs) : pImpl(new Impl) {
	pImpl->idManager.reset(new IdManager(maxConnections));
	pImpl->peer.reset(new RakNet::RakPeer);
	pImpl->socket.reset(new RakNet::SocketDescriptor(port, nullptr));
	const auto res = pImpl->peer->Startup(maxConnections, &*pImpl->socket, 1);
	if (res != RakNet::StartupResult::CRABNET_STARTED) {
		throw std::runtime_error("peer startup failed with code " + std::to_string((int)res));
	}
	pImpl->peer->SetMaximumIncomingConnections(maxConnections);
	pImpl->peer->SetTimeoutTime(timeoutTimeMs, {});
}

RakServer::~RakServer() {
	delete pImpl;
}

void RakServer::Tick(const OnPacket &onPacket) noexcept {
	RakNet::Packet *packet;
	for (packet = pImpl->peer->Receive(); packet; pImpl->peer->DeallocatePacket(packet), packet = pImpl->peer->Receive()) {
		const auto packetId = packet->data[0];
		UserId userId;

		switch (packetId) {
		case ID_DISCONNECTION_NOTIFICATION:
		case ID_CONNECTION_LOST:
			userId = pImpl->idManager->find(packet->guid);
			if (userId != g_invalidUserId) {
				pImpl->idManager->freeId(userId);
			}
			onPacket(pImpl->packetDisconnect, userId);
			break;
		case ID_NEW_INCOMING_CONNECTION:
			userId = pImpl->idManager->allocateId(packet->guid);
			if (userId == g_invalidUserId) {
				pImpl->peer->CloseConnection(packet->guid, true);
			}
			else {
				onPacket(pImpl->packetConnect, userId);
			}
			break;
		default:
			userId = pImpl->idManager->find(packet->guid);
			if (packetId >= MinPacketId) {
				Packet p{ PacketType::Message, packet->data, packet->length };
				onPacket(p, userId);
			}
			break;
		}
	}
}

void RakServer::Send(UserId id, const unsigned char *packet, size_t length, bool reliable) noexcept {
	assert(packet);
	assert(packet[0] >= MinPacketId);
	assert(length > 0);

	const auto guid = pImpl->idManager->find(id);
	if (guid != RakNet::UNASSIGNED_CRABNET_GUID) {
		pImpl->peer->Send((const char *)packet, length, MEDIUM_PRIORITY, reliable ? RELIABLE_ORDERED : UNRELIABLE, 0, guid, false);
	}
}