#pragma once
#include <memory>
#include <vector>
#include <map>
#include <string>
#include <cstdint>
#include <IGamemodeController.h>
#include <viet/Networking.h>
#include <viet/Logging.h>
#include <ITimerManager.h>

class RemoteGamemodeController : public IGamemodeController {
public:
	RemoteGamemodeController(std::string devPassword,
		std::shared_ptr<Viet::Logging::Logger> logger, 
		std::shared_ptr<Viet::Networking::IServer> server, 
		std::shared_ptr<Viet::MessageSerializer> seri,
		std::shared_ptr<ITimerManager> timerManager,
		std::map<std::string, std::vector<std::string>> propertiesByEntityName);
	~RemoteGamemodeController();

	// IGamemodeController
	void SendEvent(const evn::Any &event) noexcept override;
	std::optional<evn::Any> GetNextMethodCalled() noexcept override;
	std::optional<FrontEndFiles> GetNextFrontEndChange() noexcept override;

	int GetNumActiveGamemodes() const noexcept;
	void Tick();

private:
	using gmid = int;

	void SendEventImpl(const evn::Any &event, gmid gmId) noexcept;
	void OnPacket(const Viet::Networking::Packet &p, Viet::Networking::UserId userId);
	void OnConnect(Viet::Networking::UserId id);
	void OnDisconnect(Viet::Networking::UserId id);
	void OnMessage(Viet::Networking::UserId id, const uint8_t *data, size_t length);

	struct Impl;
	std::shared_ptr<Impl> pImpl;
};