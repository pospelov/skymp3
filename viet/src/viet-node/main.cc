#include <napi.h>
#include <viet/Logging.h>
#include <viet-node/VietNode.h>
#include <iostream>

#include <assets.h>
#include "RemoteServer.h"
#include "Bot.h"

//
// Used by RemoteServer.cpp
//

Viet::Logging::Plugin GetLoggingPlugin() {
	class StdLoggerOutput : public Viet::Logging::ILoggerOutput {
	public:
		void Write(Viet::Logging::Severity severity, const char *text) noexcept override {
			std::cout << '[' << Viet::Logging::GetSeverityName(severity) << "] " << text << std::endl;
		}
	};

	Viet::Logging::Plugin p;
	p.loggerFactory = [] {return new StdLoggerOutput; };
	return p;
}

//

Napi::Object Init(Napi::Env env, Napi::Object exports) {
	RemoteServer::Init(env, exports);
	Bot::Init(env, exports);

	auto eval = env.Global().Get("eval").As<Napi::Function>();;

	const auto patchNames = { "patch.js" };
	for (auto patchName : patchNames) {
		auto [ptr, size] = assets_get_file(patchName);
		eval.Call({ Napi::String::New(env, std::string(ptr, size)) });
	}

	auto applyPatch = env.Global().Get("applyPatch").As<Napi::Function>();
	exports = applyPatch.Call({ exports }).As<Napi::Object>();

	return exports;
}
