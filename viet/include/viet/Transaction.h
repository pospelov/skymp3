#pragma once
#include <vector>
#include <string>
#include <cstdint>
#include <viet/Instance.h>
#include <viet/Actions.h>
#include <viet/FindRequest.h>

namespace Viet {
	enum class SelectorType : uint8_t {
		ArrayOfInstances,
		Instance,
		NewInstance,
		FindOne,
		FindMany,
		Invalid = (uint8_t)-1
	};
	
	class TransactionEntryAccess;

	class TransactionEntry {
		friend class Viet::TransactionEntryAccess;
	public:
		TransactionEntry() = default;

		SelectorType GetSelectorType() const noexcept {
			return this->selectorType;
		}

		static TransactionEntry ForArrayOfInstances(const std::string &entityName, std::vector<Viet::Instance> &instances, const Viet::Actions &actions) {
			TransactionEntry e;
			e.selectorType = SelectorType::ArrayOfInstances;
			e.selector.push_back(entityName);
			for (auto &inst : instances) {
				e.selector.push_back(inst.uniqueId);
			}
			e.actions = actions;
			return e;
		}

		static TransactionEntry ForInstance(const std::string &entityName, const Viet::Instance &instance, const Viet::Actions &actions) {
			TransactionEntry e;
			e.selectorType = SelectorType::Instance;
			e.selector.push_back(entityName);
			e.selector.push_back(instance.uniqueId);
			e.actions = actions;
			return e;
		}

		static TransactionEntry ForNewInstance(const std::string &entityName, const Viet::Actions &actions) {
			TransactionEntry e;
			e.selectorType = SelectorType::NewInstance;
			e.selector.push_back(entityName);
			e.actions = actions;
			return e;
		}

		static TransactionEntry ForFindOne(const std::string &entityName, const Viet::FindCondition &cond, const Viet::Actions &actions) {
			TransactionEntry e;
			e.selectorType = SelectorType::FindOne;
			e.findReqSelector = Viet::FindRequest{ cond, 1 };
			e.selector.push_back(entityName);
			e.actions = actions;
			return e;
		}

		static TransactionEntry ForFindMany(const std::string &entityName, const Viet::FindCondition &cond, uint32_t limit, const Viet::Actions &actions) {
			TransactionEntry e;
			e.selectorType = SelectorType::FindMany;
			e.findReqSelector = Viet::FindRequest{ cond, limit };
			e.selector.push_back(entityName);
			e.actions = actions;
			return e;
		}

		template <class Archive>
		void serialize(Archive &ar, unsigned int version) {
			ar & selectorType & selector & findReqSelector & actions;
		}

	private:
		SelectorType selectorType = SelectorType::Invalid;
		std::vector<std::string> selector;
		std::optional<Viet::FindRequest> findReqSelector;
		Viet::Actions actions;
	};

	struct Transaction {
		std::vector<TransactionEntry> entries;

		template <class Archive>
		void serialize(Archive &ar, unsigned int version) {
			ar & entries;
		}
	};

	struct TransactionResult {
		struct EntryResult {
			std::vector<Viet::ActionsResult> perInstanceResults;

			template <class Archive>
			void serialize(Archive &ar, unsigned int version) {
				ar & perInstanceResults;
			}
		};

		std::vector<EntryResult> entries;

		template <class Archive>
		void serialize(Archive &ar, unsigned int version) {
			ar & entries;
		}
	};
}