#pragma once
#include <map>
#include <vector>
#include <string>
#include <serialization.h>
#include <IProperty.h>
#include <viet/JsonWrapper.h>
#include <viet/Actions.h>
#include <EntityInfo.h>

class EntityManipulation {
public:

	struct EMCallMethod {
		std::string name;
		PropertyMethodArguments args;

		template <class Archive>
		void serialize(Archive &ar, unsigned int version) {
			ar & name & args;
		}
	};

	struct EMGetProp {
		std::string name;

		template <class Archive>
		void serialize(Archive &ar, unsigned int version) {
			ar & name;
		}
	};

	struct EMSetProp {
		std::string name;
		Viet::JsonWrapper newValue;

		template <class Archive>
		void serialize(Archive &ar, unsigned int version) {
			ar & name & newValue;
		}

		EntityProps GetPropAssigns(Properties *propList) const noexcept {
			EntityProps props;

			assert(propList);
			if (!propList) return props;

			auto prop = propList->GetByName(this->name.data());
			if (!prop) return props;

			props += { propList->GetPropertyId(this->name.data()), prop->FromApiFormat(this->newValue.v) };

			return props;
		}
	};

	static constexpr uint32_t g_maxManipulations = 1000;

	std::vector<std::variant<EMCallMethod, EMGetProp, EMSetProp>> manipulations;

	EntityProps GetPropAssigns(Properties *propList, std::string &outUnfoundProperty) const noexcept {
		EntityProps props;

		assert(propList);
		if (!propList) return props;

		for (auto &manip : manipulations) {
			try {
				auto &emSetProp = std::get<EMSetProp>(manip);
				auto propAssign = emSetProp.GetPropAssigns(propList);
				if (propAssign.IsEmpty()) {
					outUnfoundProperty = emSetProp.name;
					return EntityProps();
				}
				props += propAssign;
			}
			catch (...) {
				continue;
			}
		}
		return props;
	}

	template <class Archive>
	void load(Archive &ar, unsigned int version) {
		uint32_t numManips;
		ar & numManips;
		if (numManips > g_maxManipulations) numManips = g_maxManipulations;
		manipulations.resize(numManips);
		for (size_t i = 0; i < numManips; ++i) {
			uint8_t idx = 0;
			ar & idx;
			if (idx == 0) {
				EMCallMethod m;
				ar & m;
				manipulations[i] = m;
			}
			else if (idx == 1) {
				EMGetProp m;
				ar & m;
				manipulations[i] = m;
			}
			else if (idx == 2) {
				EMSetProp m;
				ar & m;
				manipulations[i] = m;
			}
			else {
				assert(0 && "Unknown entity manipulation type");
			}
		}
	}

	template <class Archive>
	void save(Archive &ar, unsigned int version) const {
		ar & (uint32_t)manipulations.size();
		for (auto &manip : manipulations) {
			const uint8_t idx = (uint8_t)manip.index();
			ar & idx;
			std::visit([&](auto &concrete) {
				ar & concrete;
			}, manip);
		}
	}

	SERIALIZATION_SPLIT_MEMBER();
};

struct EntityManipulationResult {
	EntityManipulationResult() = default;
	EntityManipulationResult(const std::vector<Viet::JsonWrapper> &opResults_, const EntityInfo &ei_) : opResults(opResults_), ei(ei_) {};

	std::vector<Viet::JsonWrapper> opResults;
	EntityInfo ei;

	Viet::ActionsResult ToActionsResult() const noexcept {
		Viet::ActionsResult res;
		res.instance = Viet::Instance(this->ei.ToString());
		for (auto &opResult : this->opResults) {
			res.returnedValues.push_back(opResult);
		}
		return res;
	}

	template <class Archive>
	void serialize(Archive &ar, unsigned int version) {
		ar & opResults & ei;
	}
};