#pragma once
#include <array>
#include <map>
#include <EntityInfo.h>

template <class T, size_t PoolSize = 4000>
class EntityInfoMap {
public:
	void Set(const EntityInfo &key, const T &value) noexcept {
		int idx = key.id % PoolSize;
		this->data[idx][key.hash] = value;
	}

	void Set(const EntityInfo &key, std::nullopt_t) noexcept {
		int idx = key.id % PoolSize;
		this->data[idx].erase(key.hash);
	}

	const T *Find(const EntityInfo &key) const noexcept {
		auto this_ = const_cast<T *>(this);
		return this_->Find(key);
	}

	T *Find(const EntityInfo &key) noexcept {
		int idx = key.id % PoolSize;
		auto &map = this->data[idx];
		auto it = map.find(key.hash);
		if (it == map.end()) return nullptr;
		return &it->second;
	}

protected:
	std::array<std::map<std::string, T>, PoolSize> data;
};