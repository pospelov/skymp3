it('writes to a property with ClientWritable flag', async () => {
  await setup();
  let bot = newBot();
  let [user] = await nextEvent(remoteServer, 'userEnter', 5000);

  let actor = await remoteServer.createActor()
    .setPos([1, 2, 100])
    .setCellOrWorld(0x3c)
    .setClientWritableInt(-1)
    .setNonClientWritableInt(22)
    .setEnabled(true);
  await user.setActor(actor);

  await see(bot, 'Actor', { pos: [1, 2, 100], clientWritableInt: -1, nonClientWritableInt: 22 });

  bot.send('clientWritableInt', 123);

  await see(bot, 'Actor', { pos: [1, 2, 100], clientWritableInt: 123, nonClientWritableInt: 22 });

  bot.send('nonClientWritableInt', -333);

  let err = null;
  try {
    await see(bot, 'Actor', { pos: [1, 2, 100], clientWritableInt: 123, nonClientWritableInt: -333 });
  }
  catch(e) {
    err = e;
  }
  expect(err).not.to.eql(null);
});
