#include <list>
#include <thread>
#include <mutex>
#include <atomic>
#include <chrono>
#include <set>
#include <nlohmann/json.hpp>

#include <Messages.h>
#include <MsgWrapper.h>

#include "RemoteApi.h"

using json = nlohmann::json;

constexpr evn::CallId g_minCallId = 100;

struct ApiController::Impl {
	std::string serverId, devPassword;
	std::shared_ptr<Viet::MessageSerializer> seri;
	std::shared_ptr<Viet::Logging::Logger> logger;

	evn::CallId next = g_minCallId;
	std::vector<std::function<void()>> netTasks;
	FrontEndFiles frontEnd;

	std::shared_ptr<Viet::Networking::IClient> cl;

	std::atomic<bool> dtorCalled = false;
};

ApiController::ApiController(
	std::shared_ptr<Viet::Networking::IClient> reconClient_, 
	std::shared_ptr<Viet::MessageSerializer> seri_, 
	std::shared_ptr<Viet::Logging::Logger> logger_, 
	std::string serverId_, 
	std::string devPassword_,
	const FrontEndFiles &frontEndFiles_) {
	assert(seri_);
	assert(logger_);
	assert(serverId_.size());
	assert(devPassword_.size());

	pImpl.reset(new Impl);
	pImpl->cl = reconClient_;
	pImpl->seri = seri_;
	pImpl->logger = logger_;
	pImpl->serverId = serverId_;
	pImpl->devPassword = devPassword_;
	pImpl->frontEnd = frontEndFiles_;
}

ApiController::~ApiController() {
	this->Shutdown();
}

std::shared_ptr<std::list<evn::Any>> ApiController::Tick() {
	std::shared_ptr<std::list<evn::Any>> pp(new std::list<evn::Any>);

	// tick client
	std::thread([=] {
		if (!pImpl->cl) {
			assert(0);
			return;
		}
		for (auto &f : pImpl->netTasks) {
			f();
		}
		pImpl->netTasks.clear();

		pImpl->cl->Tick([&](const Viet::Networking::Packet &p) {
			static const auto g_badScriptInit = evn::ScriptInit{ true };

			using PT = Viet::Networking::PacketType;
			switch (p.type) {
			case PT::ConnectionAccepted: {
				pImpl->logger->Notice("GameMode - Connection result is Accepted");
				MsgWrapper<msg::GMHandshake> msgWrapper(&*pImpl->seri, msg::GMHandshake{ pImpl->serverId, pImpl->devPassword, pImpl->frontEnd });
				auto[data, length] = msgWrapper.GetBinary();
				pImpl->cl->Send(data, length, true);
				break;
			}
			case PT::ConnectionDenied: {
				pp->push_back(g_badScriptInit);
				pImpl->logger->Error("GameMode - Connection result is Denied");
				break;
			}
			case PT::ConnectionFailed:
				pp->push_back(g_badScriptInit);
				pImpl->logger->Error("GameMode - Connection result is Failed");
				break;
			case PT::Message:
				break;
			case PT::Disconnect:
				pImpl->logger->Error("GameMode - Disconnect");
				break;
			default:
				pImpl->logger->Error("GameMode - Connection result is Unknown (%d)", (int)p.type);
				assert(0);
				break;
			}
			if (p.data && p.length > 0) {
				assert(p.data[0] == (int)msg::GMEvent::Id);
				if (p.data[0] == (int)msg::GMEvent::Id) {
					MsgWrapper<msg::GMEvent> msgWrapper(&*pImpl->seri, p.data, p.length);
					pp->push_back(msgWrapper.GetMessage().event);


					const char *jsName = std::visit([](auto &concrete) { return concrete.JSName; }, msgWrapper.GetMessage().event);

					pImpl->logger->Debug("Received '%s' event from the server", jsName);


					try {
						auto &cr = std::get<evn::CallResult>(msgWrapper.GetMessage().event);
						assert(cr.callId >= g_minCallId);
						if (cr.callId < g_minCallId) {
							pImpl->logger->Fatal("Anomally callId=%d, see server logs", (int)cr.callId);
						}
					}
					catch (...) {

					}
				}
			}
		});
	}).join();

	return pp;
}

evn::CallId ApiController::GetMaxPlayers() noexcept {
	assert(!pImpl->dtorCalled); 
	const evn::CallId id = pImpl->next++;
	pImpl->netTasks.push_back([=] {
		const auto &[data, length] = MsgWrapper<msg::GMEvent>(&*pImpl->seri, { evn::GetMaxPlayers{ id } }).GetBinary();
		pImpl->cl->Send(data, length, true);
	});
	return id;
}

evn::CallId ApiController::ShowBrowserWindow(const EntityInfo &user, std::string winId, std::string jsonProperties) noexcept {
	assert(!pImpl->dtorCalled); 
	const evn::CallId id = pImpl->next++;
	pImpl->netTasks.push_back([=] {
		const auto &[data, length] = MsgWrapper<msg::GMEvent>(&*pImpl->seri, { evn::ShowBrowserWindow{ id, user, winId, jsonProperties } }).GetBinary();
		pImpl->cl->Send(data, length, true);
	});
	return id;
}

evn::CallId ApiController::HideBrowserWindow(const EntityInfo &user, std::string winId) noexcept {
	assert(!pImpl->dtorCalled); 
	const evn::CallId id = pImpl->next++;
	pImpl->netTasks.push_back([=] {
		const auto &[data, length] = MsgWrapper<msg::GMEvent>(&*pImpl->seri, { evn::HideBrowserWindow{ id, user, winId } }).GetBinary();
		pImpl->cl->Send(data, length, true);
	});
	return id;
}

evn::CallId ApiController::SendCustomPacket(const EntityInfo &user, std::string jsonContent) noexcept {
	assert(!pImpl->dtorCalled); 
	const evn::CallId id = pImpl->next++;
	pImpl->netTasks.push_back([=] {
		const auto &[data, length] = MsgWrapper<msg::GMEvent>(&*pImpl->seri, { evn::SendCustomPacket{ id, user, jsonContent } }).GetBinary();
		pImpl->cl->Send(data, length, true);
	});
	return id;
}

evn::CallId ApiController::FindEntity(std::string collection, const Viet::FindRequest &findReq) noexcept {
	assert(!pImpl->dtorCalled); 
	const evn::CallId id = pImpl->next++;
	pImpl->netTasks.push_back([=] {
		const auto &[data, length] = MsgWrapper<msg::GMEvent>(&*pImpl->seri, { evn::FindEntity{ id, collection, findReq } }).GetBinary();
		pImpl->cl->Send(data, length, true);
	});
	return id;
}

evn::CallId ApiController::ManipulateEntity(std::string collection, const EntityInfo &optionalExistingEntity, const EntityManipulation &entityManip) noexcept {
	assert(!pImpl->dtorCalled);
	const evn::CallId id = pImpl->next++;
	pImpl->netTasks.push_back([=] {
		const auto &[data, length] = MsgWrapper<msg::GMEvent>(&*pImpl->seri, { evn::ManipulateEntity{ id, collection, optionalExistingEntity, entityManip } }).GetBinary();
		pImpl->cl->Send(data, length, true);
	});
	return id;
}

evn::CallId ApiController::SetActor(const EntityInfo &user, std::string collection, std::optional<EntityInfo> actor) noexcept {
	assert(!pImpl->dtorCalled);
	const evn::CallId id = pImpl->next++;
	pImpl->netTasks.push_back([=] {
		const auto &[data, length] = MsgWrapper<msg::GMEvent>(&*pImpl->seri, { evn::SetActor{ id, user, collection, actor ? *actor : EntityInfo() } }).GetBinary();
		pImpl->cl->Send(data, length, true);
	});
	return id;
}

evn::CallId ApiController::GetActor(const EntityInfo &user, std::string entityName) noexcept {
	assert(!pImpl->dtorCalled);
	const evn::CallId id = pImpl->next++;
	pImpl->netTasks.push_back([=] {
		const auto &[data, length] = MsgWrapper<msg::GMEvent>(&*pImpl->seri, { evn::GetActor{ id, user, entityName } }).GetBinary();
		pImpl->cl->Send(data, length, true);
	});
	return id;
}

evn::CallId ApiController::RunTransaction(const Viet::Transaction &transaction) noexcept {
	assert(!pImpl->dtorCalled);
	const evn::CallId id = pImpl->next++;
	pImpl->netTasks.push_back([=] {
		const auto &[data, length] = MsgWrapper<msg::GMEvent>(&*pImpl->seri, { evn::RunTransaction{ id, transaction } }).GetBinary();
		pImpl->cl->Send(data, length, true);
	});
	return id;
}

void ApiController::Shutdown() noexcept {
	//assert(!pImpl->dtorCalled); // Sometimes it happens
	if (pImpl->dtorCalled) return;

	std::thread([&] {
		pImpl->cl.reset();
	}).join();
	pImpl->dtorCalled = true;
}