enable_testing()

include(${CMAKE_CURRENT_SOURCE_DIR}/viet/dist/viet_add_node_api.cmake)

add_executable(test_server
  ${CMAKE_CURRENT_LIST_DIR}/src/Entities.h
  ${CMAKE_CURRENT_LIST_DIR}/src/Server.cpp
)
target_link_libraries(test_server PRIVATE vietlib viet-raknet viet-mongocxx nlohmann_json::nlohmann_json)
target_include_directories(test_server PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/viet/include)

viet_add_node_api(test_api
  ${CMAKE_CURRENT_LIST_DIR}/src/Api.cpp
)
target_link_libraries(test_api PRIVATE viet-node vietlib viet-raknet nlohmann_json::nlohmann_json)
target_include_directories(test_api PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/viet/include)
set_target_properties(test_api PROPERTIES PREFIX "" SUFFIX ".node")

#
# Install chai, mocha, etc...
#

set(NODE_DIR ${CMAKE_SOURCE_DIR}/tests/node)

if (WIN32)
  set(temp_bat "${CMAKE_CURRENT_BINARY_DIR}/temp.bat")
  set(npm_cmd ${temp_bat})
  set(npm_arg "")
  file(WRITE ${temp_bat} "npm i")
else()
  set(npm_cmd "npm")
  set(npm_arg "i")
endif()

message(STATUS "[viet] Doing 'npm install'")
execute_process(COMMAND ${npm_cmd} ${npm_arg}
  WORKING_DIRECTORY ${NODE_DIR}
  RESULT_VARIABLE npm_result
  OUTPUT_VARIABLE npm_out
)
message(STATUS "[viet] Finished 'npm install' with code ${npm_result}")
if(npm_result EQUAL "1")
    message(FATAL_ERROR "[viet] Bad exit status")
endif()

#
# Mocha
#

set(name "mocha")
add_test(NAME ${name} COMMAND
  ${CMAKE_COMMAND}
  -DSKYMP_TEST_SERVER_PATH=$<TARGET_FILE:test_server>
  -DSKYMP_TEST_NODE_API_PATH=$<TARGET_FILE:test_api>
  -DSKYMP_TEST_TEMP_DIR=${CMAKE_CURRENT_BINARY_DIR}/my_test_tmp
  -DSKYMP_TEST_MONGO_URI=${TEST_MONGO_URI}
  -DSKYMP_TEST_NODE_WORKDIR=${NODE_DIR}
  -DSKYMP_TEST_SPECS_DIR=${CMAKE_CURRENT_LIST_DIR}/specs
  -P ${CMAKE_CURRENT_LIST_DIR}/node/run_test.cmake
  WORKING_DIRECTORY ${CMAKE_CURRENT_LIST_DIR}
)
set_property(TEST ${name} APPEND PROPERTY
  FAIL_REGULAR_EXPRESSION "(.*CMake Error at.*)|(.*Error: .*)"
  # CMake and NodeJS errors
)
set_tests_properties(${name} PROPERTIES TIMEOUT 300)

#
# Cpp
#

file(GLOB tests_cpp_src "${CMAKE_CURRENT_LIST_DIR}/unit/*")
add_executable(tests_unit ${tests_cpp_src})
target_compile_definitions(tests_unit PRIVATE TEST_MONGO_URI=\"${TEST_MONGO_URI}\")

# Hacky solution to provide Catch2 header to tests_unit target
# In vcpkg, nlohmann_json and Catch2 headers are placed into the same folder
find_package(nlohmann_json)
target_link_libraries(tests_unit PRIVATE nlohmann_json::nlohmann_json)

target_link_libraries(tests_unit PRIVATE vietlib viet-raknet viet-mongocxx nlohmann_json::nlohmann_json)
target_include_directories(tests_unit PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/viet/include)

set(name "unit")
add_test(NAME ${name} COMMAND
  ${CMAKE_COMMAND}
  -DVIET_CURRENT_TEST_CMD=$<TARGET_FILE:tests_unit>
  -P ${CMAKE_CURRENT_LIST_DIR}/unit/run_test.cmake
)
set_property(TEST ${name} APPEND PROPERTY
  FAIL_REGULAR_EXPRESSION "(.*CMake Error at.*)|(.*Error: .*)"
)
set_tests_properties(${name} PROPERTIES TIMEOUT 300)
