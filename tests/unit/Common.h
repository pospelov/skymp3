#pragma once
#include <memory>
#include <map>
#include <string>
#include <thread>
#include <cstring>
#include <stdexcept>

#include <catch2/catch.hpp>

#include <viet/Client.h>
#include <viet/Server.h>
#include <viet/ApiClient.h>

template <class View>
class CommonViewHolder : public Viet::IInstanceViewHolder {
public:
	virtual Viet::IInstanceView *Find(const char *entityName, uint32_t index) override {
		auto it = this->data.find(MakeKey(entityName, index));
		if (it == this->data.end()) return nullptr;
		return &*it->second;
	}

	virtual Viet::IInstanceView *Create(const char *entityName, uint32_t index) override {
		this->data[MakeKey(entityName, index)].reset(new View);
		return this->Find(entityName, index);
	}

	virtual void Delete(const char *entityName, uint32_t index) override {
		this->data.erase(MakeKey(entityName, index));
	}

	virtual bool IsStateResetNeeded(const char *entityName, uint32_t index) override {
		return false;
	}

private:
	static std::string MakeKey(const char *entityName, uint32_t index) {
		return std::to_string(index) + "___________________" + entityName;
	}

	std::map<std::string, std::shared_ptr<View>> data;
};

class VietEnv {
public:
	VietEnv(std::vector<Viet::Entity> entities, Viet::ApiClient::Listeners apiClientListeners_ = {});

	Viet::Client &Client();
	Viet::Server &Server();
	Viet::ApiClient &ApiClient();

private:
	struct Impl;
	std::shared_ptr<Impl> pImpl;
};

