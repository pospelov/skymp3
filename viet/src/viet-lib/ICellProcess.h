#pragma once
#include <memory>
#include <cstdint>
#include <optional>
#include <forms.h>
#include <EntityInfo.h>
#include <VisionEvents.h>
#include <viet/Promise.h>
#include <viet/FindRequest.h>

struct VisionEventData;

class ICellProcess {
public:
	struct EditRes {
		// Will be resolved after saving, etc
		std::optional<Viet::Promise<Viet::Void>> promise;
	};

	using ActorId = int;

	static constexpr ActorId g_invalidActorId = -1;

	struct InstanceId {
		InstanceId() = default;
		explicit InstanceId(const uint64_t &v) noexcept {
			*this = (const InstanceId &)v;
		}
		InstanceId(int entityId_, ActorId actorId_) {
			this->entityId = entityId_;
			this->actorId = actorId_;
		}
		
		int entityId = -1;
		ActorId actorId = g_invalidActorId;

		uint64_t ToUInt64() const noexcept {
			return *reinterpret_cast<const uint64_t *>(this);
		}

		friend bool operator<(const InstanceId &lhs, const InstanceId &rhs) noexcept {
			return lhs.ToUInt64() < rhs.ToUInt64();
		}

		friend bool operator==(const InstanceId &lhs, const InstanceId &rhs) noexcept {
			return lhs.actorId == rhs.actorId && lhs.entityId == rhs.entityId;
		}

		friend bool operator!=(const InstanceId &lhs, const InstanceId &rhs) noexcept {
			return !(lhs == rhs);
		}
	};

	static_assert(sizeof(InstanceId) == sizeof(uint64_t));

	struct ActorWaste {
		std::unique_ptr<IActor> ac;
		InstanceId inst;
	};

	// Note that cb may be fired from another thread
	// It's implementation defined
	using OnVisionEvent = std::function<void(std::unique_ptr<VisionEventData>)>;
	using OnSaveRequired = std::function<Viet::Promise<Viet::Void>(InstanceId actorId, IActor &actor)>;

	virtual void SetOnVisionEvent(OnVisionEvent cb) noexcept = 0;
	virtual void SetOnSaveRequired(OnSaveRequired cb) noexcept = 0;

	virtual ActorId AddActor(int entityId, std::unique_ptr<IActor> ac) noexcept = 0; // Returns g_invalidActorId on failure
	virtual void DisableActor(const InstanceId &inst) noexcept = 0;
	virtual std::optional<ActorWaste> GetNextActorWaste(int entityId) noexcept = 0;
	virtual EditRes Edit(const InstanceId &inst, const EntityProps &entityProps, bool sendChangesToMe) noexcept = 0;
	virtual std::optional<EntityProps> GetEntityProps(const InstanceId &inst) const noexcept = 0;
	virtual bool HasActor(const InstanceId &inst) const noexcept = 0;
	virtual std::vector<ActorId> FindMatches(int entityId, const Viet::FindCondition &cond, uint32_t limit) const noexcept = 0;
	virtual void StreamOutAll(const InstanceId &inst) noexcept = 0;

	virtual ~ICellProcess() = default;
};

struct VisionEventData {
	VisionEventData() = default;
	VisionEventData(ICellProcess::InstanceId viewer_, vision::Event<ICellProcess::InstanceId> v_) : viewer(viewer_), v(v_) {
	};

	void *reservedCpId = nullptr;
	ICellProcess::InstanceId viewer;
	vision::Event<ICellProcess::InstanceId> v;
};