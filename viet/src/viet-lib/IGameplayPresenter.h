#pragma once
#include <functional>
#include <memory>
#include <GameplayModel.h>

// Must be threadsafe
class IGameplayPresenter {
public:
	using Callback = std::function<void(const GameplayModel &)>;
	virtual void ProcessModel(const Callback &cb) noexcept = 0;

	virtual ~IGameplayPresenter() = default;
};

using IGameplayPresenterPtr = std::shared_ptr<IGameplayPresenter>;
