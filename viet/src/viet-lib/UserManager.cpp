#include <vector>
#include <optional>
#include <cassert>
#include <memory>

#include "UserManager.h"

namespace {
	class UserInfo {
	public:
		// User has nullopt session if it's just connected and not handshaked yet
		// Messages except msg::Handshake should be ignored
		std::optional<EntityInfo> sessionKey;
	};
}

struct UserManager::Impl {
	const int maxPlayers;

	std::vector<std::unique_ptr<UserInfo>> users; // index is id, size is maxPlayers
};

UserManager::UserManager(int maxPlayers) : pImpl(new Impl{ maxPlayers }) {
	pImpl->users.resize(maxPlayers);
}

UserManager::~UserManager() {
	delete pImpl;
}

void UserManager::RemoveConnected(UserId id) noexcept {
	pImpl->users[id].reset();
}

void UserManager::AddConnected(UserId id) noexcept {
	assert(id < pImpl->maxPlayers);
	if (id >= pImpl->maxPlayers) return;
	assert(!pImpl->users[id]);
	pImpl->users[id].reset(new UserInfo());
}

void UserManager::AttachSession(UserId id, const EntityInfo &sessionKey) noexcept {
	assert(id < pImpl->maxPlayers);
	if (id >= pImpl->maxPlayers) return;
	assert(pImpl->users[id]);
	if (pImpl->users[id] == nullptr) return;
	assert(!pImpl->users[id]->sessionKey);
	pImpl->users[id]->sessionKey = sessionKey;
}

bool UserManager::IsConnected(UserId id) const noexcept {
	assert(id < pImpl->maxPlayers);
	if (id >= pImpl->maxPlayers) return false;
	return pImpl->users[id] != nullptr;
}

const EntityInfo &UserManager::GetSession(UserId id) const noexcept {
	static const EntityInfo g_emptyEntityInfo;
	assert(id < pImpl->maxPlayers);
	if (id < pImpl->maxPlayers) {
		if (pImpl->users[id]) {
			return *pImpl->users[id]->sessionKey;
		}
	}
	return g_emptyEntityInfo;
}