it('getActor returns Actor passed to setActor', async () => {
  await setup();
  let bot = newBot();

  let [user] = await nextEvent(remoteServer, 'userEnter', 5000);

  let actor = await remoteServer.createActor()
    .setPos([1, 2, 3]);

  expect(await user.getActor()).to.eql(null);

  await user.setActor(actor);

  let ac = await user.getActor();

  expect(ac.getId()).to.eql(actor.getId());

  await user.setActor(null);
  expect(await user.getActor()).to.eql(null);
});

it('Calling setActor on enabled actor allows bot to see WorldState', async () => {
  await setup();
  let bot = newBot();

  let [user] = await nextEvent(remoteServer, 'userEnter', 5000);

  let actor = await remoteServer.createActor()
    .setPos([1, 2, 100])
    .setCellOrWorld(0x3c)
    .setEnabled(true);

  await user.setActor(actor);
  await see(bot, 'Actor', {
    pos: [1, 2, 100]
  });
});
