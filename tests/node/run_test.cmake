if (SKYMP_TEST_TEMP_DIR)
  file(REMOVE_RECURSE ${SKYMP_TEST_TEMP_DIR})
  file(MAKE_DIRECTORY ${SKYMP_TEST_TEMP_DIR})
endif()

get_cmake_property(_variableNames VARIABLES)
list (SORT _variableNames)
foreach (_variableName ${_variableNames})
  if (_variableName MATCHES "^SKYMP_TEST_.*")
    set(ENV{${_variableName}} ${${_variableName}})
  endif()
endforeach()

set(npm_cmd npm run test)
if(WIN32)
  set(npm_cmd "npm run test")
  set(temp_bat "${SKYMP_TEST_TEMP_DIR}/temp2.bat")
  file(WRITE ${temp_bat} ${npm_cmd})
  set(npm_cmd ${temp_bat})
endif()

execute_process(
  COMMAND ${npm_cmd}
  WORKING_DIRECTORY ${SKYMP_TEST_NODE_WORKDIR}
  RESULTS_VARIABLE res
)

if (NOT ${res} STREQUAL "0")
  message(FATAL_ERROR "Bad node exit code: ${res}")
endif()
