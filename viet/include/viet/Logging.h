#pragma once
#include <memory>
#include <functional>

namespace Viet {
	namespace Logging {
		enum class Severity {
			Debug,
			Info,
			Notice,
			Warning,
			Error,
			Fatal
		};

		inline const char *GetSeverityName(Severity severity) {
			switch (severity) {
			case Severity::Debug:
				return "Debug";
			case Severity::Info:
				return "Info";
			case Severity::Notice:
				return "Notice";
			case Severity::Warning:
				return "Warning";
			case Severity::Error:
				return "Error";
			case Severity::Fatal:
				return "Fatal";
			}
			return "";
		}

		class ILoggerOutput {
		public:
			virtual void Write(Severity severity, const char *text) noexcept = 0;
			virtual ~ILoggerOutput() = default;
		};

		class Logger {
		public:
			explicit Logger(ILoggerOutput *out);
			explicit Logger(std::shared_ptr<ILoggerOutput> out);
			void SetOutput(std::shared_ptr<ILoggerOutput> out) noexcept;

#define DECLARE_SEVERITY(severity) template <class ... Ts> \
void severity(const char *fmt, Ts ... t) { \
this->Write(Severity::severity, fmt, t ...);}

			DECLARE_SEVERITY(Debug)
			DECLARE_SEVERITY(Info)
			DECLARE_SEVERITY(Notice)
			DECLARE_SEVERITY(Warning)
			DECLARE_SEVERITY(Error)
			DECLARE_SEVERITY(Fatal)

#undef DECLARE_SEVERITY

			void Write(Severity severity, const char *fmt, ...) noexcept;

		private:
			struct Impl;
			const std::shared_ptr<Impl> pImpl;
		};

		struct Plugin {
			std::function<ILoggerOutput *()> loggerFactory;
		};
	}
}