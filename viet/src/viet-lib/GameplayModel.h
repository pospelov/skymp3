#pragma once
#include <vector>
#include <string>
#include <utility>
#include <optional>
#include <EntityProps.h>
#include <serialization.h>
#include <viet/Client.h>
#include <EntityAccess.h>
#include <Messages.h>

namespace {
	std::vector<const char *> GetPropertyNames(const EntityProps &props, Properties *propList) {
		std::vector<const char *> res;
		props.ForEach([&](PropId id, const std::unique_ptr<IPropertyValue> &v) {
			res.push_back(propList->GetPropertyName(id));
		});
		return res;
	}
}

using NumChanges = uint64_t;

class GameplayModel final : public Viet::IWorldState {
public:
	GameplayModel(const std::vector<Viet::Entity> &entities_) {
		this->entities = entities_;
	}

	struct EntityInstance {
		bool valid = false;
		EntityProps props;
		std::vector<NumChanges> numChanges;
		uint64_t eiHash = 0;
	};

	std::vector<std::vector<EntityInstance>> contents;
	int localEntityId = -1;
	msg::ServerAcId localInstanceId;

	EntityProps props; // My props
	std::vector<NumChanges> numChanges;

	uint32_t GetNumEntities() const noexcept override {
		return (uint32_t)contents.size();
	}

	bool IsInstanceValid(const char *entityName, uint32_t instanceIndex) const noexcept override {
		const int entityId = this->FindEntityByName(entityName);
		if (entityId == -1) {
			return false;
		}
		if ((int)contents.size() <= entityId) {
			return false;
		}
		auto &data = contents[entityId];
		if (data.size() <= instanceIndex) {
			return false;
		}
		if (data[instanceIndex].valid == false) {
			return false;
		}
		return true;
	}

	bool IsInstanceMe(const char *entityName, uint32_t instanceIndex) const noexcept override {
		const int entityId = this->FindEntityByName(entityName);
		return this->localEntityId == entityId 
			&& this->localInstanceId.idx == instanceIndex 
			&& this->IsInstanceValid(entityName, instanceIndex);
	}

	Value GetInstancePropertyValue(const char *entityName, uint32_t instanceIndex, const char *propName) const noexcept override {
		if (!this->IsInstanceValid(entityName, instanceIndex)) {
			assert(0);
			return {};
		}
		const int entityId = this->FindEntityByName(entityName);
		auto &v = contents[entityId][instanceIndex];
		auto propList = EntityAccess::GetPropList_(this->entities[entityId]);
		assert(propList);
		if (!propList) return {};

		auto propId = propList->GetPropertyId(propName);
		auto &propValue = v.props.GetPropertyValue(propName, propList);
		if (!propValue) return {};

		return { propValue.get(), (int)v.numChanges.size() > propId ? v.numChanges[propId] : 0};
	}

	std::vector<const char *> GetInstancePropertyNames(const char *entityName, uint32_t instanceIndex) const noexcept override {
		if (!this->IsInstanceValid(entityName, instanceIndex)) {
			assert(0);
			return {};
		}
		const int entityId = this->FindEntityByName(entityName);
		auto &v = contents[entityId][instanceIndex];
		auto propList = EntityAccess::GetPropList_(this->entities[entityId]);
		assert(propList);
		if (!propList) return {};

		return GetPropertyNames(v.props, propList);
	}

	Value GetMyPropertyValue(const char *propName) const noexcept override {
		auto entityName = this->GetMyEntity();
		assert(entityName);
		if (!strlen(entityName)) return {};
		const int entityId = this->FindEntityByName(entityName);

		auto propList = EntityAccess::GetPropList_(this->entities[entityId]);
		assert(propList);
		if (!propList) return {};

		auto propId = propList->GetPropertyId(propName);
		auto &propValue = this->props.GetPropertyValue(propName, propList);
		if (!propValue) return {};

		return { propValue.get(), (int)this->numChanges.size() > propId ? this->numChanges[propId] : 0 };
	}

	std::vector<const char *> GetMyPropertyNames() const noexcept override {
		if (this->localEntityId < 0 || this->localEntityId >= this->entities.size()) {
			return {};
		}
		auto propList = EntityAccess::GetPropList_(this->entities[this->localEntityId]);
		assert(propList);
		if (!propList) return {};

		return GetPropertyNames(this->props, propList);
	}

	const char *GetMyEntity() const noexcept override {
		if (this->localEntityId <= -1) return "";
		return this->entities[this->localEntityId].GetName();
	}

	uint32_t GetEntityPoolSize(const char *entityName) const noexcept override {
		const int entityId = this->FindEntityByName(entityName);
		if (entityId <= -1 || (int)contents.size() <= entityId) return 0;
		return this->contents[entityId].size();
	}

private:
	int FindEntityByName(const char *entityName) const noexcept {
		if (strlen(entityName) > 0) {
			// Should be a letter if entityName is a valid pointer
			assert(tolower(entityName[0]) != toupper(entityName[0]));

			for (int i = 0; i < (int)this->entities.size(); ++i) {
				const Viet::Entity &e = this->entities[i];
				if (!strcmp(e.GetName(), entityName)) {
					return i;
				}
			}
		}
		return -1;
	}

	std::vector<Viet::Entity> entities;
};
