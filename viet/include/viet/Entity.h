#pragma once
#include <memory>
#include <vector>
#include <viet/Property.h>

namespace Viet {
	class Entity {
	public:
		// Throws
		Entity(const char *name, std::vector<Viet::Property> properties);
		~Entity();

		const char *GetName() const noexcept;

	protected:
		void *GetPropList() const noexcept;
		const std::vector<Viet::Property> &GetProperties() const noexcept;

	private:
		struct Impl;
		std::shared_ptr<Impl> pImpl;
	};
}