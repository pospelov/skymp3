#include <napi.h>
#include <cassert>
#include <viet-node/VietNode.h>
#include <viet-raknet/VietRakNet.h>
#include "Entities.h"

Viet::Networking::Plugin VietNode::GetNetworkingPlugin(std::string ip, uint16_t gamemodesPort) {
	Viet::Networking::Plugin p;
	p.clientFactory = [=] { return new Viet::Networking::RakClient(ip.data(), gamemodesPort, 4000); };
	p.serverFactory = [=] { assert(0); return nullptr; };
	return p;
}

Viet::Networking::Plugin VietNode::GetBotNetworkingPlugin(std::string ip, uint16_t port) {
	Viet::Networking::Plugin p;
	p.clientFactory = [=] { return new Viet::Networking::RakClient(ip.data(), port, 4000); };
	p.serverFactory = [=] { assert(0); return nullptr; };
	return p;
}

Viet::Serialization::Plugin VietNode::GetSeriPlugin() {
	Viet::Serialization::Plugin p;
	p.serializerFactory = [] { return new Viet::Serialization::RakSerializer; };
	return p;
}

std::vector<Viet::Entity> VietNode::GetEntities() {
	return GetTestEntities();
}

Napi::Object Init(Napi::Env env, Napi::Object exports);

NODE_API_MODULE(skymp, Init);
