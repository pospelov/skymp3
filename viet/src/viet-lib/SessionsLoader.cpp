#include <optional>
#include <EntityInfoMap.h>
#include <HashToEntityInfo.h>
#include <EntityInfo.h>

#include "SessionsLoader.h"

namespace {
	constexpr uint32_t g_exitTimeoutMs = 5000;
	constexpr uint32_t g_delayBetweenSavesMs = 2000;

	class SessionData {
	public:
		std::map<std::string, std::string> browserWindows;
		EntityInfo actor; // can be empty

		Viet::Networking::UserId userId = Viet::Networking::g_invalidUserId; // g_invalidUserId means offline
	};

	class SessionsMap : public EntityInfoMap<SessionData> {
	public:
		json Dump() {
			json j = json::object();
			for (auto &map : this->data) {
				for (auto &[hash, sessionData] : map) {
					const auto entityKey = HashToEntityInfo(hash).ToString();
					auto v = json::object();
					v["browserWindows"] = json::object();
					for (auto &[winKey, winValue] : sessionData.browserWindows) {
						v["browserWindows"][winKey] = winValue;
					}
					v["actor"] = sessionData.actor.IsEmpty() ? "" : sessionData.actor.ToString();
					j[entityKey] = v;
				}
			}
			return j;
		}
	};
}

struct SessionsLoader::Impl {
	std::shared_ptr<IAsyncDatabase> db;
	std::shared_ptr<ITimerManager> tm;
	std::shared_ptr<Viet::Logging::Logger> logger;
	OnConnect onConnect;
	OnDisconnect onDisconnect;
	SessionsMap sessionsMap;
	EntityInfoMap<EntityInfo> sessionByActor;
	bool started = false;

	struct {
		bool inProgress = false;
		bool force = false;
	} saveSessions;

	void TickSaveSessions();
	void ForceSaveSessions();
	bool RemoveSessionIfOffline(const EntityInfo &entityInfo);
};

void SessionsLoader::Impl::TickSaveSessions() {
	if (!this->saveSessions.inProgress && this->saveSessions.force) {
		this->saveSessions.force = false;
		this->saveSessions.inProgress = true;
		auto jDoc = this->sessionsMap.Dump();
		int size = jDoc.size();
		this->db->UpdateOne("session", json::object(), jDoc, true, [this, size] {
			this->tm->SetTimeout(g_delayBetweenSavesMs, [=] {
				this->saveSessions.inProgress = false;
				this->logger->Info("Saved %d sessions", size);
			});
		});
	}
}

void SessionsLoader::Impl::ForceSaveSessions() {
	this->saveSessions.force = true;
}

bool SessionsLoader::Impl::RemoveSessionIfOffline(const EntityInfo &sessionKey) {
	auto session = this->sessionsMap.Find(sessionKey);
	assert(session);
	if (!session || session->userId == Viet::Networking::g_invalidUserId) {
		this->logger->Info("Removing the session");
		this->onDisconnect(sessionKey);
		this->sessionsMap.Set(sessionKey, std::nullopt);
		this->ForceSaveSessions();
		return true;
	}
	else {
		this->logger->Info("The session has been restored for user %d, undo session removal", session->userId);
		return false;
	}
}

SessionsLoader::SessionsLoader(OnConnect onConnect, OnDisconnect onDisconnect, std::shared_ptr<IAsyncDatabase> db, std::shared_ptr<ITimerManager> tm, std::shared_ptr<Viet::Logging::Logger> logger) : pImpl(new Impl{ db, tm, logger, onConnect, onDisconnect }) {
	pImpl->db->FindOne("session", json::object(), [=](json doc) {
		int n = 0;
		std::vector<EntityInfo> sessionKeys;
		if (doc != nullptr) {
			for (auto it = doc.begin(); it != doc.end(); ++it) {
				const auto key = it.key().data();
				auto &session = it.value();
				assert(session.is_object());
				if (!session.is_object()) return;
				SessionData data;
				auto &brWindows = session["browserWindows"];
				assert(brWindows.is_object());
				if (brWindows.is_object()) {
					for (auto jt = brWindows.begin(); jt != brWindows.end(); ++jt) {
						assert(jt.value().is_string());
						if (jt.value().is_string()) {
							data.browserWindows[jt.key()] = jt.value();
						}
					}
				}

				const auto sessionKey = EntityInfo::FromString(key);

				auto &actor = session["actor"];
				assert(actor.is_string());
				if (actor.is_string()) {
					const auto s = actor.get<std::string>();
					if (s.size() > 0) {
						data.actor = EntityInfo::FromString(s);
					}
					if (!data.actor.IsEmpty()) {
						pImpl->sessionByActor.Set(data.actor, sessionKey);
					}
				}
				pImpl->sessionsMap.Set(sessionKey, data);
				sessionKeys.push_back(sessionKey);
				++n;

				const auto sessionDump = session.dump();
				pImpl->logger->Info("Restoring session: %s", sessionDump.data());
			}
		}
		pImpl->logger->Notice("Loaded %d sessions%s", n, doc != nullptr ? "" : " (not found in db)");
		pImpl->started = true;

		pImpl->tm->SetTimeout(g_exitTimeoutMs, [this, sessionKeys] {
			int numRemoved = 0;
			for (auto &sessionKey : sessionKeys) {
				if (pImpl->RemoveSessionIfOffline(sessionKey)) {
					numRemoved++;
				}
			}
			pImpl->logger->Notice("Removed %d stale sessions", numRemoved);
		});
	});
}

SessionsLoader::~SessionsLoader() {
	delete pImpl;
}

bool SessionsLoader::IsStarted() const noexcept {
	return pImpl->started;
}

void SessionsLoader::MakeSessionOffline(const EntityInfo &ei) noexcept {
	auto session = pImpl->sessionsMap.Find(ei);
	assert(session);
	if (!session) return;

	session->userId = Viet::Networking::g_invalidUserId; // Make session "offline"
	pImpl->tm->SetTimeout(g_exitTimeoutMs, [this, ei] {
		pImpl->RemoveSessionIfOffline(ei);
	});
}

bool SessionsLoader::Handshake(const EntityInfo &entityInfo, Viet::Networking::UserId id) noexcept {
	const auto it = pImpl->sessionsMap.Find(entityInfo);
	if (it != nullptr) {
		if (it->userId != Viet::Networking::g_invalidUserId) {
			pImpl->logger->Error("Handshake with user %d failed. Reason: Session hash is already in use", id);
			return false;
		}
		it->userId = id;
		const auto str = entityInfo.ToString();
		pImpl->logger->Info("Restoring the session %s for user %d", str.data(), id);
		goto success;
	}
	else {
		SessionData session;
		session.userId = id;
		pImpl->sessionsMap.Set(entityInfo, session);
		pImpl->ForceSaveSessions();

		pImpl->onConnect(entityInfo);
		goto success;
	}
success:
	std::string sessionHash = entityInfo.hash;
	pImpl->logger->Info("Successful handshake with user %d (found=%d, session=%s)", id, it != nullptr, sessionHash.data());
	return true;
}

Viet::Networking::UserId SessionsLoader::GetUserId(const EntityInfo &sessionKey) const noexcept {
	auto session = pImpl->sessionsMap.Find(sessionKey);
	if (!session) return Viet::Networking::g_invalidUserId;
	return session->userId; // can also be net::g_invalidUserId
}

std::map<std::string, std::string> SessionsLoader::GetBrowserWindows(const EntityInfo &sessionKey) const noexcept {
	if (auto session = pImpl->sessionsMap.Find(sessionKey)) {
		return session->browserWindows;
	}
	return {};
}

bool SessionsLoader::AddBrowserWindow(const EntityInfo &sessionKey, std::string id, std::string jsonProps) noexcept {
	if (auto session = pImpl->sessionsMap.Find(sessionKey)) {
		session->browserWindows[id] = jsonProps;
		pImpl->ForceSaveSessions();
		return true;
	}
	return false;
}

bool SessionsLoader::RemoveBrowserWindow(const EntityInfo &sessionKey, std::string id) noexcept {
	if (auto session = pImpl->sessionsMap.Find(sessionKey)) {
		session->browserWindows.erase(id);
		pImpl->ForceSaveSessions();
		return true;
	}
	return false;
}

bool SessionsLoader::SetActor(const EntityInfo &sessionKey, const EntityInfo &newActor) noexcept {
	if (auto session = pImpl->sessionsMap.Find(sessionKey)) {
		if (session->actor == newActor) {
			return true;
		}

		if (session->actor.IsEmpty() == false) {
			pImpl->sessionByActor.Set(session->actor, std::nullopt);
		}

		session->actor = newActor;
		pImpl->sessionByActor.Set(newActor, sessionKey);
		pImpl->ForceSaveSessions();
		return true;
	}
	return false;
}

const EntityInfo &SessionsLoader::GetSessionByActor(const EntityInfo &actor) const noexcept {
	static EntityInfo g_emptyEi;
	if (auto session = pImpl->sessionByActor.Find(actor)) {
		return *session;
	}
	return g_emptyEi;
}

const EntityInfo &SessionsLoader::GetActor(const EntityInfo &sessionKey) const noexcept {
	static EntityInfo g_emptyEi;
	if (auto session = pImpl->sessionsMap.Find(sessionKey)) {
		return session->actor;
	}
	return g_emptyEi;
}

void SessionsLoader::Tick() noexcept {
	if (pImpl->started) {
		pImpl->TickSaveSessions();
	}
}