#include <cassert>
#include <climits> // INT_MAX
#include <fstream>
#include "GameplayPresenterReplayable.h"
#include <Replay.h>
#include <Messages.h>

static std::string g_extention = ".skymp.replay";

GameplayPresenterReplayable::GameplayPresenterReplayable(IGameplayPresenter *base_, std::shared_ptr<Viet::MessageSerializer> seri_)
	: base(base_), seri(seri_) {
	assert(base_);
	assert(seri_);
}

void GameplayPresenterReplayable::ProcessModel(const Callback &cb) noexcept {
	assert(this->base);
	if (!this->base) return;

	std::lock_guard l(this->m);

	this->CalcCurrentFrame();

	this->base->ProcessModel([&](const GameplayModel &model) {
		const float now = ClocksToSeconds(clock());
		assert(this->accuracy > 0.f);

		if (now - this->rs.lastRecord > this->accuracy) {
			this->rs.lastRecord = now;
			if (this->rs.firstRecord < 0) this->rs.firstRecord = now;
			const int idx = int((now - this->rs.firstRecord) / this->accuracy);
			if (idx >= 0 && (size_t)idx >= this->rs.repl.size()) {
				if (this->recording)
					this->rs.repl.resize(idx + 1, model);
			}
		}

		int frame = this->rs.unsafeCurrentFrame;

		const bool isPast = (int64_t)this->rs.repl.size() - frame > 5;

		if (frame < 0 || (size_t)frame >= this->rs.repl.size()) {
			cb(model);
		}
		else {
			auto &m = this->rs.repl[frame];
			if (isPast) {
				m.localInstanceId = {};
			}
			cb(m);
		}
	});
}

int GameplayPresenterReplayable::GetNumFramesInternal() noexcept {
	assert(this->rs.repl.size() <= INT_MAX);
	return (int)this->rs.repl.size();
}

void GameplayPresenterReplayable::CalcCurrentFrame() noexcept {
	const float now = ClocksToSeconds(clock());

	if (this->rs.firstGetCurrentFrame < 0) {
		this->rs.firstGetCurrentFrame = now;
	}
	const int frame = int((now - this->rs.firstGetCurrentFrame) / this->accuracy);

	this->rs.framesPassedReal = frame;

	this->rs.unsafeCurrentFrame = this->rs.framesPassedReal + this->rs.framesOffset;
}