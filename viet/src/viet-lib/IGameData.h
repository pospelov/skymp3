#pragma once
#include <functional>
#include <memory>

class IForm {
public:
	enum class Type {
		Unknown,
		WorldSpace,
		Cell,
		Actor
	};

	virtual Type GetType() noexcept = 0;

	// Always false for non-Cell forms
	virtual bool IsInteriorCell() noexcept { return false; };

	virtual ~IForm() = default;
};

using IFormPtr = std::shared_ptr<IForm>;

class IGameData {
public:
	// Callbacks may be synchronous or asynchronous, it's implementation defined
	using FormCallback = std::function<void(const IFormPtr &)>;

	virtual void LookupFormById(uint32_t formId, const FormCallback &cb) noexcept = 0;

	virtual ~IGameData() = default;
};