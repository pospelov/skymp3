it('caching', async () => {
  await setup();
  let bot = newBot();
  let [user] = await nextEvent(remoteServer, 'userEnter', 5000);

  let actor = await remoteServer.createActor()
    .setPos([1, 2, 100])
    .setCellOrWorld(0x3c);
  await user.setActor(actor);

  for (let i = 0; i < 10; ++i) {
    await actor.setEnabled(true);

    expect(await actor.isEnabled()).to.eql(true);
    await see(bot, 'Actor', { pos: [1, 2, 100] });

    await actor.setEnabled(false);

    expect(await actor.isEnabled()).to.eql(false);
    await notSee(bot, 'Actor', { pos: [1, 2, 100] });
  }

  await actor.setEnabled(true);
  await see(bot, 'Actor', { pos: [1, 2, 100] });

  await actor.setEnabled(false);
  await notSee(bot, 'Actor', { pos: [1, 2, 100] });

  await actor.setEnabled(true).setPos(1, 2, 10);
  await see(bot, 'Actor', { pos: [1, 2, 10] });
});
