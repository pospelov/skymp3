#include <string>
#include <viet/Entity.h>

#include "Properties.h"

using namespace Viet;

namespace {
	using PropPtr = std::shared_ptr<Properties::IPropertyAny>;
}

struct Entity::Impl {
	std::vector<Viet::Property> properties;
	std::string name;

	std::vector<PropPtr> propsOwning;
	std::unique_ptr<Properties> propList;
};

Entity::Entity(const char *name_, std::vector<Viet::Property> properties_) : pImpl(new Impl) {
	pImpl->properties = properties_;
	pImpl->name = name_;
	for (auto &propSchema : pImpl->properties) {
		auto ptr = (Properties::IPropertyAny *)propSchema.AsPropertyAny();
		assert(ptr);
		PropPtr prop(ptr);
		pImpl->propsOwning.push_back(prop);
	}

	Properties::PropMap propMap;
	for (auto &propPtr : pImpl->propsOwning) {
		std::string propName = propPtr->GetName();
		if (propMap.count(propName) != 0) {
			throw std::logic_error("Property name must be unique ('" + propName + "')");
		}
		propMap[propName] = &*propPtr;
	}

	pImpl->propList.reset(new Properties(propMap));
}

const char *Entity::GetName() const noexcept {
	return pImpl->name.data();
}

Entity::~Entity() {
}

void *Entity::GetPropList() const noexcept {
	return pImpl->propList.get();
}

const std::vector<Viet::Property> &Entity::GetProperties() const noexcept {
	return pImpl->properties;
}