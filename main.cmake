# Options
option(TEST_MONGO_URI "mongoUri used for tests (do NOT use production database uri here)" OFF)

set(GITMODULES "${CMAKE_SOURCE_DIR}/gitmodules")
set(VIET_INCLUDE_DIRS "${CMAKE_SOURCE_DIR}/viet/include")

set(i 0)
file(WRITE ${CMAKE_CURRENT_BINARY_DIR}/copy.cmake
  "if (EXISTS \${SOURCE})
    file(COPY \${SOURCE} DESTINATION \${DEST})
  endif()"
)

macro(viet_add_plugin_static TARGET)
  get_target_property(tmp_link_libraries ${TARGET} LINK_LIBRARIES)
  foreach(lib ${tmp_link_libraries})
    if (${lib} MATCHES ".*cmake-js.*")
      # ...
    elseif (${lib} MATCHES ".*vcpkg.*")
      math(EXPR i "1 + ${i}" OUTPUT_FORMAT DECIMAL)
      add_custom_target(pack_${i} ALL
        COMMAND echo \"${lib}\"
        COMMAND ${CMAKE_COMMAND} -DSOURCE=$<GENEX_EVAL:${lib}> -DDEST=${CMAKE_BINARY_DIR}/dist/lib$<$<CONFIG:Debug>:_dbg> -P ${CMAKE_CURRENT_BINARY_DIR}/copy.cmake
      )
      add_dependencies(pack_${i} ${TARGET})
    elseif(TARGET ${lib})
      get_target_property(type ${lib} TYPE)
      if (NOT ${type} STREQUAL "INTERFACE_LIBRARY")
        math(EXPR i "1 + ${i}" OUTPUT_FORMAT DECIMAL)
        add_custom_target(pack_${i} ALL
          COMMAND echo \"${lib}\"
          COMMAND ${CMAKE_COMMAND} -DSOURCE=$<TARGET_FILE:${lib}> -DDEST=${CMAKE_BINARY_DIR}/dist/lib$<$<CONFIG:Debug>:_dbg> -P ${CMAKE_CURRENT_BINARY_DIR}/copy.cmake
        )
        add_dependencies(pack_${i} ${TARGET})
      endif()
    else()
      list(APPEND nontracked_libs ${lib})
    endif()
  endforeach()

  list(APPEND VIET_LIB_TARGETS ${TARGET})

  set(tmp_export_include_dir "${CMAKE_CURRENT_SOURCE_DIR}/viet/include/${TARGET}")
  if(EXISTS ${tmp_export_include_dir})
    list(APPEND EXPORT_INCLUDE_DIRS ${tmp_export_include_dir})
  endif()
endmacro()

include("viet/Viet.cmake")

file(WRITE ${CMAKE_BINARY_DIR}/non_tracked.txt "${nontracked_libs}")

list(APPEND VIET_LIB_TARGETS vietlib)
foreach(target ${VIET_LIB_TARGETS})
  set_target_properties(${target} PROPERTIES
    ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/dist/lib$<$<CONFIG:Debug>:_dbg>"
    LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/dist/lib$<$<CONFIG:Debug>:_dbg>"
  )
endforeach()

get_target_property(include_dirs nlohmann_json::nlohmann_json INTERFACE_INCLUDE_DIRECTORIES)
foreach(dir ${include_dirs})
  message(STATUS DIR ${dir})
endforeach()


add_custom_target(pack ALL
  COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_SOURCE_DIR}/viet/include ${CMAKE_BINARY_DIR}/dist/include
  COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_SOURCE_DIR}/viet/dist ${CMAKE_BINARY_DIR}/dist
  COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_BINARY_DIR}/non_tracked.txt ${CMAKE_BINARY_DIR}/dist
  COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_SOURCE_DIR}/viet/src/viet-node/package.json ${CMAKE_BINARY_DIR}/dist/npm/package.json
  COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_SOURCE_DIR}/viet/src/viet-node/CMakeLists.txt ${CMAKE_BINARY_DIR}/dist/npm/CMakeLists.txt
)
add_dependencies(pack ${VIET_LIB_TARGETS})

if (UNIX)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fpermissive")
endif()

if (EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/tests/tests.cmake)
  include(tests/tests.cmake)
endif()
