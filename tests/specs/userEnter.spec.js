it('emits userEnter and userExit', async () => {
  await setup();

  let bot = newBot();

  let userEnter = await nextEvent(remoteServer, 'userEnter', 5000);
  let userId = userEnter[0].getId();

  bot.kill();

  let userExit = await nextEvent(remoteServer, 'userExit', 20000);
  expect(userExit[0].getId()).to.eql(userId);
});
