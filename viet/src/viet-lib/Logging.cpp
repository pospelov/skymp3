#include <viet/Logging.h>

#include <cstdlib>
#include <cstdarg>
#include <mutex>
#include <cassert>

using namespace Viet::Logging;

struct Logger::Impl {
	std::shared_ptr<ILoggerOutput> out;
	std::mutex m;
	std::vector<char> buf;
};

Logger::Logger(ILoggerOutput *out) : Logger(std::shared_ptr<ILoggerOutput>(out)){
}

Logger::Logger(std::shared_ptr<ILoggerOutput> out_) : pImpl(new Impl) {
	this->SetOutput(out_);
}

void Logger::SetOutput(std::shared_ptr<ILoggerOutput> out_) noexcept {
	assert(out_);
	if (out_) {
		std::lock_guard l(pImpl->m);
		pImpl->out = out_;
	}
}

void Logger::Write(Severity severity, const char *fmt, ...) noexcept {
	std::lock_guard l(pImpl->m);
	if (!pImpl->out) return;
	va_list args1;
	va_start(args1, fmt);
	va_list args2;
	va_copy(args2, args1);
	pImpl->buf.resize(1 + std::vsnprintf(nullptr, 0, fmt, args1));
	va_end(args1);
	std::vsnprintf(pImpl->buf.data(), pImpl->buf.size(), fmt, args2);
	va_end(args2);
	pImpl->out->Write(severity, pImpl->buf.data());
}