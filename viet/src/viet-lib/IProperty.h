#pragma once
#include <string>
#include <vector>
#include <functional>
#include <sstream>
#include <viet/PropertyValue.h>
#include <viet/MessageSerializer.h>
#include <viet/PropertyFlags.h>
#include <viet/JsonWrapper.h>

#include <nlohmann/json.hpp>
using json = nlohmann::json;

using Viet::IPropertyValue;

struct PropertyMethodArguments {
	PropertyMethodArguments() = default;

	PropertyMethodArguments(const std::vector<json> &args_) {
		for (auto &arg : args_) {
			this->args.push_back(Viet::JsonWrapper(arg));
		}
	}

	PropertyMethodArguments(const std::vector<Viet::JsonWrapper> &args_) {
		for (auto &arg : args_) {
			this->args.push_back(arg);
		}
	}

	std::vector<Viet::JsonWrapper> args;

	template <class Archive>
	void serialize(Archive &ar, unsigned int version) {
		ar & args;
	}
};

template <class T>
class IProperty {
public:
	using ValueType = T;
	struct Method {
		std::string name;
		std::function<json(T &self, PropertyMethodArguments arguments)> fn;
	};

	struct MethodConst {
		std::string name;
		std::function<json(const T &self, PropertyMethodArguments arguments)> fn;
	};

	class MethodBadArgCount : public std::logic_error {
	public:
		MethodBadArgCount(int expected, int real) : logic_error(format(expected, real)) {
		}

	private:
		static std::string format(int expected, int real) noexcept {
			std::stringstream ss;
			ss << "Bad num arguments (expected " << expected << ", got " << real << ")";
			return ss.str();
		}
	};

	class MethodLogicError : public std::logic_error {
	public:
		MethodLogicError(const std::string &str) : logic_error(str) {
		}

	private:
	};

	struct Methods {
		std::vector<Method> apiMethods, clientMethods;
		std::vector<MethodConst> apiMethodsConst, clientMethodsConst;
	};

	virtual const char *GetName() noexcept = 0;
	virtual int GetValueFlags(const T &value) noexcept = 0;
	virtual int GetFlags() noexcept = 0;
	virtual std::optional<std::pair<int16_t, int16_t>> GetAffectsGrid(const T &value) noexcept = 0;
	virtual bool NeedsToBeSaved(const std::optional<T> &lastSavedValue, const T &currentValue) noexcept = 0;
	virtual std::optional<T> GetValueForcesEntityCaching() noexcept = 0;

	virtual json ToApiFormat(const T& value) noexcept = 0;
	virtual T FromApiFormat(const json &j) = 0; // May throw
	virtual json ToDbFormat(const T& value) noexcept = 0;
	virtual T FromDbFormat(const json &j) = 0;

	virtual void ToBinary(Viet::MessageSerializer &seri, Viet::SeriToUse seriToUse, const T& value, uint8_t *&outData, size_t &outLength) noexcept = 0;
	virtual T FromBinary(Viet::MessageSerializer &seri, Viet::SeriToUse seriToUse, const uint8_t *in, size_t length) noexcept = 0;

	virtual Methods GetMethods() noexcept = 0;

	virtual T GetDefaultValue() noexcept = 0;

	virtual ~IProperty() = default;
};

template <class T>
class IPropertySerializable : public IProperty<T> {
public:
	void ToBinary(Viet::MessageSerializer &seri, Viet::SeriToUse seriToUse, const T& value, uint8_t *&outData, size_t &outLength) noexcept override {
		return seri.Write<T, false>(value, outData, outLength, seriToUse);
	}

	T FromBinary(Viet::MessageSerializer &seri, Viet::SeriToUse seriToUse, const uint8_t *in, size_t length) noexcept override {
		T res;
		seri.Read<T, false>(res, in, length, seriToUse);
		return res;
	}

	json ToDbFormat(const T& value) noexcept override {
		return this->ToApiFormat(value);
	}

	T FromDbFormat(const json &j) override {
		return this->FromApiFormat(j);
	}

	std::optional<std::pair<int16_t, int16_t>> GetAffectsGrid(const T &value) noexcept override {
		return std::nullopt;
	}

	typename IProperty<T>::Methods GetMethods() noexcept override {
		return {};
	}

	std::optional<T> GetValueForcesEntityCaching() noexcept override {
		return std::nullopt;
	}

	T GetDefaultValue() noexcept override {
		return T();
	}
};