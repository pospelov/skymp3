let path = require('path');
let fs = require('fs');

const EventEmitter = require('events');
require('./test_utils');

chai = require('chai');
expect = chai.expect;

const gamemodesPort = 7000;

describe('skymp NodeJS module', () => {
  server = null;
  remoteServer = null;
  bots = [];

  reset = function () {
    if (server) {
      server.kill();
      server = null;
    }
    if (remoteServer) {
      remoteServer.kill();
      remoteServer = null;
    }
    bots.forEach(bot => bot.kill());
    bots = [];
  };

  setup = async function (serverId) {
    reset();
    server = createServer();
    remoteServer = new skymp.RemoteServer();
    remoteServer.connect({
      ip: '127.0.0.1',
      port: gamemodesPort,
      serverId: server.getConfig().serverId,
      devPassword: server.getConfig().devPassword
    });
    await nextEvent(remoteServer, 'connect');
  };

  afterEach(reset);

  let dir = process.env.SKYMP_TEST_SPECS_DIR;
  fs.readdirSync(dir).forEach(file => require(path.join(dir, file)));
});

let svr = createServer();

svr.kill();
