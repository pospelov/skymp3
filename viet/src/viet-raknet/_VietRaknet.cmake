set(GITMODULES ${CMAKE_CURRENT_BINARY_DIR}/external_viet_raknet/CrabNet/src)
include(${CMAKE_CURRENT_LIST_DIR}/_external.cmake)

# RakNet
set(RAKNET_TARGET _RakNet)
file(GLOB RAKNET_SRC "${GITMODULES}/CrabNet/include/raknet/*.h" "${GITMODULES}/CrabNet/Source/*.cpp" "${GITMODULES}/CrabNet/Source/Plugins/*.cpp" "${GITMODULES}/CrabNet/Source/Utils/*.cpp")

add_library(${RAKNET_TARGET} STATIC ${RAKNET_SRC})
target_include_directories(${RAKNET_TARGET} PUBLIC "${GITMODULES}/CrabNet/include/raknet" "${GITMODULES}/CrabNet/Source")
target_compile_definitions(${RAKNET_TARGET} PRIVATE SHA1_NO_WIPE_VARIABLES=1 _CRT_SECURE_NO_WARNINGS=1)

file(GLOB_RECURSE VIET_RAKNET_SOURCES "${CMAKE_CURRENT_LIST_DIR}/*")
add_library(viet-raknet STATIC ${VIET_RAKNET_SOURCES})

target_include_directories(viet-raknet PRIVATE "${GITMODULES}/CrabNet/include")
target_link_libraries(viet-raknet PRIVATE ${RAKNET_TARGET})

target_include_directories(viet-raknet PRIVATE "${VIET_INCLUDE}")

set(VIET_LIBS_RAKNET viet-raknet ${RAKNET_TARGET})

if(UNIX)
  target_link_libraries(${RAKNET_TARGET} PRIVATE stdc++fs pthread)
  target_link_libraries(viet-raknet PRIVATE stdc++fs pthread)
  list(APPEND VIET_LIBS_RAKNET stdc++fs)
  list(APPEND VIET_LIBS_RAKNET pthread)
elseif(WIN32)
  target_link_libraries(${RAKNET_TARGET} PRIVATE ws2_32)
  target_link_libraries(viet-raknet PRIVATE ws2_32)
  list(APPEND VIET_LIBS_RAKNET ws2_32)
endif()

viet_add_plugin_static(viet-raknet)
