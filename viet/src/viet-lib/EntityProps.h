#pragma once
#include <optional>
#include <memory>
#include <serialization.h>
#include <cassert>
#include <Properties.h>
#include <viet/PropertyFlags.h>

#include <nlohmann/json.hpp>
using json = nlohmann::json;

class EntityProps {
public:
	size_t GetSize() const noexcept {
		return (size_t)std::count_if(this->data.begin(), this->data.end(), [](const Entry &entry) {
			return entry.value != nullptr;
		});
	}

	bool IsEmpty() const noexcept {
		auto it = std::find_if(this->data.begin(), this->data.end(), [](const Entry &entry) {
			return entry.value != nullptr;
		});
		return it == this->data.end();
	}

	bool HasProperty(const char *propertyName, Properties *propList) const noexcept {
		return this->HasProperty(propList->GetPropertyId(propertyName));
	}

	bool HasProperty(PropId propId) const noexcept {
		return propId >= 0 && propId < Properties::g_maxProperties && this->data[propId].value;
	}

	EntityProps &operator=(const EntityProps &ep) {
		for (auto &entry : this->data) {
			entry.value.reset();
		}
		*this += ep;
		return *this;
	}

	EntityProps &operator+=(const EntityProps &ep) {
		for (size_t i = 0, n = Properties::g_maxProperties; i < n; ++i) {
			if (auto &v = ep.data[i].value) {
				this->data[i].value = v->Clone();
			}
		}
		return *this;
	}

	EntityProps &operator+=(const std::pair<PropId, std::unique_ptr<IPropertyValue>> &ep) {
		assert(ep.second != nullptr);
		assert(ep.first >= 0);
		assert(ep.first < Properties::g_maxProperties);
		if (ep.second && ep.first >= 0 && ep.first < Properties::g_maxProperties) {
			this->data[ep.first].value = ep.second->Clone();
		}
		return *this;
	}

	std::optional<std::pair<int16_t, int16_t>> GetAffectsGrid(Properties *propList) const noexcept {
		for (size_t i = 0, n = Properties::g_maxProperties; i < n; ++i) {
			if (auto &v = this->data[i].value) {
				auto prop = propList->GetById(i);
				assert(prop);
				if (prop) {
					if (auto affect = prop->GetAffectsGrid(v)) return affect;
				}
			}
		}
		return std::nullopt;
	}

	bool IsCachingRequired(Properties *propList) const noexcept {
		for (size_t i = 0, n = Properties::g_maxProperties; i < n; ++i) {
			if (auto prop = propList->GetById(i)) {
				if (auto forceCachingValue = prop->GetValueForcesEntityCaching()) {
					auto defaultValue = prop->GetDefaultValue();
					const auto &currentValue = this->data[i].value ? this->data[i].value : defaultValue;
					if (prop->ToDbFormat(*forceCachingValue) == prop->ToDbFormat(currentValue)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	EntityProps &Filter(Viet::PropertyFlags::Enum flags, Properties *propList) noexcept {
		return this->FilterImpl(flags, true, propList);
	}

	EntityProps &Exclude(Viet::PropertyFlags::Enum flags, Properties *propList) noexcept {
		return this->FilterImpl(flags, false, propList);
	}

	const std::unique_ptr<IPropertyValue> &GetPropertyValue(PropId propId) const noexcept {
		static const std::unique_ptr<IPropertyValue> nullProp;
		const std::unique_ptr<IPropertyValue> &res = nullProp;

		if (propId >= 0 && propId < (PropId)this->data.size()) {
			return this->data[propId].value;
		}

		return res;
	}

	const std::unique_ptr<IPropertyValue> &GetPropertyValue(const char *propertyName, Properties *propList) const noexcept {
		return this->GetPropertyValue(
			propList->GetPropertyId(propertyName)
		);
	}

	template <class F>
	void ForEach(const F &f) const {
		for (size_t i = 0, n = Properties::g_maxProperties; i < n; ++i) {
			if (auto &v = this->data[i].value) {
				f((PropId)i, v);
			}
		}
	}

	template <class Archive>
	void save(Archive &a, int) const {
		for (size_t i = 0, n = Properties::g_maxProperties; i < n; ++i) {
			auto &entry = this->data[i];
			const bool hasValue = entry.value != nullptr;
			a & hasValue;
			if (!hasValue) continue;

			auto propList = reinterpret_cast<Properties *>(a.GetUserData());
			assert(propList);
			if (!propList) continue;

			auto prop = propList->GetById(i);
			assert(prop);
			if (!prop) continue;

			uint8_t *outData;
			size_t outLength;
			Viet::MessageSerializer seri(a.GetSeri());
			prop->ToBinary(seri, Viet::SeriToUse::SubSeri, entry.value, outData, outLength);
			a & (uint32_t)outLength;
			for (size_t i = 0; i < outLength; ++i) {
				a & outData[i];
			}
		}
	}

	template <class Archive>
	void load(Archive &a, int) {
		for (size_t i = 0, n = Properties::g_maxProperties; i < n; ++i) {
			bool hasValue = false;
			a & hasValue;
			if (!hasValue) continue;

			auto propList = reinterpret_cast<Properties *>(a.GetUserData());
			///assert(propList);
			// It's OK to have nullptr propList here
			// We need to read a message without EntityProps part
			// And then we can determine a propList, write it into userData
			// And re-read our message
			// See NetController.cpp
			if (!propList) break;

			auto prop = propList->GetById(i);
			assert(prop);
			if (!prop) continue;

			uint32_t length = 0;
			a & length;

			thread_local std::vector<uint8_t> buffer;
			if (buffer.size() < length) {
				buffer.resize(length);
			}
			for (size_t i = 0; i < length; ++i) a & buffer[i];

			Viet::MessageSerializer seri(a.GetSeri());
			this->data[i].value = prop->FromBinary(seri, Viet::SeriToUse::SubSeri, buffer.data(), length);
		}
	}

	SERIALIZATION_SPLIT_MEMBER();

private:
	EntityProps &FilterImpl(Viet::PropertyFlags::Enum flags, bool needMatch, Properties *propList) noexcept {
		for (size_t i = 0, n = Properties::g_maxProperties; i < n; ++i) {
			if (auto &v = this->data[i].value) {
				auto prop = propList->GetById(i);
				assert(prop);
				if (prop) {
					const bool matches = prop->GetFlags() & flags;
					if (matches != needMatch) {
						this->data[i].value.reset();
					}
				}
			}
		}
		return *this;
	}

	struct Entry {
		std::unique_ptr<IPropertyValue> value;

		Entry() = default;
		Entry(const Entry &rhs) {
			*this = rhs;
		}

		Entry &operator=(const Entry &rhs) {
			if (this != &rhs) {
				this->value.reset();
				if (rhs.value) {
					this->value = rhs.value->Clone();
				}
			}
			return *this;
		}
	};

	std::array<Entry, Properties::g_maxProperties> data;
};