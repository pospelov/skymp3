if (WIN32)
  set(temp_bat "${CMAKE_CURRENT_BINARY_DIR}/temp.bat")
  set(npm_cmd ${temp_bat})
  set(npm_arg "")
  file(WRITE ${temp_bat} "npm i")
else()
  set(npm_cmd "npm")
  set(npm_arg "i")
endif()

message(STATUS "[viet] Doing 'npm install'")
execute_process(COMMAND ${npm_cmd} ${npm_arg}
  WORKING_DIRECTORY ${NODE_DIR}
  RESULT_VARIABLE npm_result
  OUTPUT_VARIABLE npm_out
)
message(STATUS "[viet] Finished 'npm install' with code ${npm_result}")
if(npm_result EQUAL "1")
    message(FATAL_ERROR "[viet] Bad exit status")
endif()

if (WIN32)
  file(WRITE ${temp_bat} "npm run build_Release")
else()
  set(npm_cmd "npm")
  set(npm_arg run build_Release)
endif()

message(STATUS "[viet] Doing 'cmake-js build' to retrieve nodejs files")
execute_process(COMMAND ${npm_cmd} ${npm_arg}
  WORKING_DIRECTORY ${NODE_DIR}
  RESULT_VARIABLE npm_result
  OUTPUT_VARIABLE npm_out
)
message(STATUS "[viet] Finished 'cmake-js build' with code ${npm_result}")
if(npm_result EQUAL "1")
    message(FATAL_ERROR "[viet] Bad exit status")
endif()

set(vars_dir "${NODE_DIR}/build/vars")
foreach(var_name CMAKE_JS_INC CMAKE_JS_SRC CMAKE_JS_LIB)
  file(READ "${vars_dir}/${var_name}" ${var_name})
endforeach()

set(NPM_INC ${CMAKE_JS_INC})
set(NPM_SRC ${CMAKE_JS_SRC})
set(NPM_LIB ${CMAKE_JS_LIB})
set(CMAKE_JS_INC "")
set(CMAKE_JS_SRC "")
set(CMAKE_JS_LIB "")

# Include N-API wrappers
execute_process(COMMAND node -p "require('node-addon-api').include"
  WORKING_DIRECTORY ${NODE_DIR}
  OUTPUT_VARIABLE NODE_ADDON_API_DIR
)
string(REPLACE "\n" "" NODE_ADDON_API_DIR ${NODE_ADDON_API_DIR})
string(REPLACE "\"" "" NODE_ADDON_API_DIR ${NODE_ADDON_API_DIR})
list(APPEND NPM_INC ${NODE_ADDON_API_DIR})

# https://github.com/cmake-js/cmake-js/issues/84
list(APPEND NPM_INC "${CMAKE_SOURCE_DIR}/node_modules/node-addon-api")
list(APPEND NPM_INC "${CMAKE_SOURCE_DIR}/node_modules/node-addon-api/src")
