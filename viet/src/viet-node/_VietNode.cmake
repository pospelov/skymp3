# install externals (cmrc)
include(${CMAKE_CURRENT_LIST_DIR}/external/_external.cmake)


# assets (patch.js)
include("${CMAKE_BINARY_DIR}/external_viet_node/cmrc/src/cmrc/CMakeRC.cmake")
set(ASSET_FILES
  ${CMAKE_CURRENT_LIST_DIR}/assets/patch.js
)
cmrc_add_resource_library(assetslib ${ASSET_FILES} ALIAS assets NAMESPACE assets WHENCE ${CMAKE_CURRENT_LIST_DIR}/assets)
cmrc_add_resource_library(assetslib_mt ${ASSET_FILES} ALIAS assets NAMESPACE assets WHENCE ${CMAKE_CURRENT_LIST_DIR}/assets)
set_target_properties(assetslib_mt PROPERTIES MSVC_RUNTIME_LIBRARY "MultiThreaded")


# viet-node
set(NODE_DIR ${CMAKE_CURRENT_LIST_DIR})
include(${CMAKE_CURRENT_SOURCE_DIR}/viet/dist/npm.cmake)

file(GLOB VIET_NODE_SRC "${CMAKE_CURRENT_LIST_DIR}/*")
add_library(viet-node STATIC ${VIET_NODE_SRC} ${NPM_SRC})
target_include_directories(viet-node PRIVATE ${NPM_INC})

target_link_libraries(viet-node PRIVATE ${NPM_LIB})

# Viet
target_link_libraries(viet-node PRIVATE nlohmann_json::nlohmann_json)
target_link_libraries(viet-node PRIVATE ${VIET_LIBS})
target_include_directories(viet-node PRIVATE ${VIET_INCLUDE})

# for assets (api patch)
target_include_directories(viet-node PRIVATE "${CMAKE_CURRENT_LIST_DIR}/assets-util")
target_link_libraries(viet-node PRIVATE assetslib)
if (NOT TARGET assetslib)
  message(FATAL_ERROR "assetslib is not a target")
endif()

viet_add_plugin_static(viet-node)
