#pragma once
#include <map>
#include <vector>

class IdManager {
public:
	using userid = Viet::Networking::UserId;

	IdManager(userid maxConnections_);

	userid allocateId(const RakNet::RakNetGUID &guid) noexcept; // returns IServer::invalidUserId on failure
	void freeId(userid id) noexcept;
	userid find(const RakNet::RakNetGUID &guid) const noexcept;
	RakNet::RakNetGUID find(userid id) const noexcept;

private:
	std::map<RakNet::RakNetGUID, userid> idByGuid;
	std::vector<RakNet::RakNetGUID> guidById;
	userid nextId = 0;
	const userid maxConnections = 0;
};