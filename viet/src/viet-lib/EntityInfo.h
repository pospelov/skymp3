#pragma once
#include <string>
#include <functional> // std::hash

struct EntityInfo {
	std::string hash; // guaranteed to be unique
	uint32_t id = (uint32_t)~0; // NOT guaranteed to be unique, may be used for FormIDs, indexes, etc.

	// IsEmpty must retunr true for default-constructed 
	bool IsEmpty() const noexcept {
		// Do not check id, it's OK
		return hash.empty();
	}

	// Do not store returned numbers in the db
	// The algorithm may change in the future
	uint64_t GetUInt64Hash() const noexcept {
		if (this->IsEmpty()) {
			return 0;
		}
		std::hash<std::string> hash_fn;
		size_t str_hash = hash_fn(this->ToString());
		return str_hash;
	}

	std::string ToString() const noexcept {
		return std::to_string(id) + ":" + hash;
	}

	static EntityInfo FromString(std::string s) noexcept {
		if (auto idx = s.find(':'); idx != std::string::npos) {
			s[idx] = 0;
			return { s.data() + idx + 1, (uint32_t)atoll(s.data()) };
		}
		return {};
	}

	friend bool operator<(const EntityInfo &lhs, const EntityInfo &rhs) noexcept {
		return std::make_tuple(lhs.hash, lhs.id) < std::make_tuple(rhs.hash, rhs.id);
	}

	friend bool operator==(const EntityInfo &lhs, const EntityInfo &rhs) noexcept {
		return lhs.id == rhs.id && lhs.hash == rhs.hash;
	}

	friend bool operator!=(const EntityInfo &lhs, const EntityInfo &rhs) noexcept {
		return !(lhs == rhs);
	}

	template <class Archive>
	void serialize(Archive &ar, unsigned int version) {
		ar & hash & id;
	}
};