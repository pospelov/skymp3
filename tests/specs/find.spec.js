let testFind  = async (isEnabled) => {
  let actor1 = await remoteServer.createActor()
    .setPos([1, 2, 3])
    .setCellOrWorld(1)
    .setEnabled(isEnabled);
  let actor2 = await remoteServer.createActor()
    .setPos([4, 5, 6])
    .setCellOrWorld(1)
    .setEnabled(isEnabled);

  // Find one
  let findRes1 = await remoteServer.findActor({ isEnabled, pos: [1, 2, 3] });
  expect(findRes1.getId()).to.eql(actor1.getId());

  let findRes2 = await remoteServer.findActor({ isEnabled, pos: [4, 5, 6] });
  expect(findRes2.getId()).to.eql(actor2.getId());

  expect(await remoteServer.findActor({ isEnabled, pos: [44444, 55555, 66666] })).to.eql(undefined);

  // Find many
  let findResMany1 = await remoteServer.findActors({ isEnabled, pos: [1, 2, 3] });
  expect(findResMany1.length).to.eql(1);
  expect(findResMany1[0].getId()).to.eql(actor1.getId());

  let findResMany2 = await remoteServer.findActors({ isEnabled, pos: [4, 5, 6] });
  expect(findResMany2.length).to.eql(1);
  expect(findResMany2[0].getId()).to.eql(actor2.getId());

  let findResMany3 = await remoteServer.findActors({ isEnabled, pos: [4222, 5, 6] });
  expect(findResMany3).to.eql([]);

  let findResMany4 = await remoteServer.findActors({ isEnabled, 'pos.0': { $greater: 0, $less: 10 } });
  expect(findResMany4.length).to.eql(2);

  // Find many limit
  let findResManyLimit1 = await remoteServer.findActors({ isEnabled, 'pos.0': { $greater: 0, $less: 10 } }, 0);
  expect(findResManyLimit1.length).to.eql(2);

  let findResManyLimit2 = await remoteServer.findActors({ isEnabled, 'pos.0': { $greater: 0, $less: 10 } }, 1);
  expect(findResManyLimit2.length).to.eql(1);


  // ***


  // Find by child value

  let ac1 = await remoteServer.createActor()
    .setJson({ a: { b: { c: 1 } } })
    .setCellOrWorld(1)
    .setEnabled(isEnabled);
  let ac2 = await remoteServer.createActor()
    .setJson({ a: { b: { c: 2 } } })
    .setCellOrWorld(1)
    .setEnabled(isEnabled);

  let res1 = await remoteServer.findActor({ isEnabled, 'json.a.b.c': 1 });
  expect(res1.getId()).to.eql(ac1.getId());
  res1 = await remoteServer.findActor({ isEnabled, 'json.a.b': { c: 1 } });
  expect(res1.getId()).to.eql(ac1.getId());

  let res2 = await remoteServer.findActor({ isEnabled, 'json.a.b.c': 2 });
  expect(res2.getId()).to.eql(ac2.getId());
  res2 = await remoteServer.findActor({ isEnabled, 'json.a': { b: { c: 2 } } });
  expect(res2.getId()).to.eql(ac2.getId());

  let res3 = await remoteServer.findActors({ isEnabled, 'json.a.b.c': { $greater: 0, $less: 3 } });
  expect(res3.length).to.eql(2);

  // or/and
  let r1 = await remoteServer.findActors({ isEnabled, $or: [{'json.a.b.c': 1}, {'json.a.b.c': 2}] });
  expect(r1.length).to.eql(2);

  let r2 = await remoteServer.findActors({ isEnabled, $and: [{'json.a.b.c': 1}, {'json.a.b.c': 2}] });
  expect(r2.length).to.eql(0);

  // search for default value
  let rr1 = await remoteServer.findActors({ isEnabled, defaultInt: 108 });
  expect(rr1.length).not.to.eql(0);

  let rr2 = await remoteServer.findActors({ isEnabled, defaultInt: 109 });
  expect(rr2.length).to.eql(0);

  let rr3 = await remoteServer.findActors({ isEnabled, defaultInt: { $greater: 0 } });
  expect(rr3.length).not.to.eql(0);

  let rr4 = await remoteServer.findActors({ isEnabled, defaultInt: { $less: 0 } });
  expect(rr4.length).to.eql(0);
};

it('finds cached actors', async () => {
  await setup();
  await testFind(true);
});

it('finds uncached actors', async () => {
  await setup();
  await testFind(false);
});
