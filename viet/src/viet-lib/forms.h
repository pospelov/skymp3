#pragma once
#include <EntityProps.h>
#include <IGameData.h>
#include <IProperty.h>

class IActor {
public:
	virtual EntityProps GetProps() const noexcept = 0;
	virtual void ApplyPropsChanges(const EntityProps &entityProps) noexcept = 0;
	virtual void SetProps(const EntityProps &entityProps) noexcept = 0;
};