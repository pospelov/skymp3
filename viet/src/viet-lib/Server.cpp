#include <atomic>
#include <thread>

#include <viet/Server.h>

#include <TimerManager.h>
#include <EntityHashGen.h>
#include <RemoteGamemodeController.h>
#include <AsyncDatabase.h>
#include <CellProcess.h>
#include <Actor.h>
#include <SkympServerView.h>
#include <EntityAccess.h>

namespace {
	class CellProcessHolder {
	public:
		CellProcessHolder(std::vector<Viet::Entity> entities_) {
			this->entities = entities_;
		}

		auto CreateCellProcess() {
			auto cp = std::make_shared<CellProcess>(this->entities);
			std::lock_guard l(this->m);
			this->cellProcs.push_back(cp);
			return cp;
		};

		void Tick() {
			std::lock_guard l(this->m);
			for (auto &cp : this->cellProcs) {
				cp->Tick();
			}
		}

		std::vector<std::shared_ptr<CellProcess>> cellProcs;
		std::mutex m;
		std::vector<Viet::Entity> entities;
	};
}

struct Viet::Server::Impl {
	const Settings settings;
	CellProcessHolder cpHolder;
	std::atomic<uint32_t> ticks;

	std::shared_ptr<TimerManager> timerManager;
	std::shared_ptr<Viet::Serialization::ISerializer> seri, gmSeri;
	std::shared_ptr<IEntityHashGen> entityHashGen;
	std::shared_ptr<Viet::Networking::IServer> server, gmServer;
	std::shared_ptr<RemoteGamemodeController> gmController;
	std::shared_ptr<AsyncDatabase> asyncDb;
	std::unique_ptr<SkympServerView> skympServerView;

	std::shared_ptr<std::atomic<bool>> destroyed;

	std::unique_ptr<std::thread> cpThread, checkThread;
};

Viet::Server::Server(const Settings &settings) : pImpl(new Impl{ settings, settings.entities }) {
	pImpl->destroyed.reset(new std::atomic<bool>(false));
	pImpl->timerManager.reset(new TimerManager);
	pImpl->entityHashGen.reset(new EntityHashGen);

	// Viet::Serialization
	pImpl->seri.reset(settings.seriPlugin.serializerFactory());
	pImpl->gmSeri.reset(settings.seriPlugin.serializerFactory());
	
	// Viet::Logging
	std::shared_ptr<Viet::Logging::Logger> logger(
		new Viet::Logging::Logger(std::shared_ptr<Viet::Logging::ILoggerOutput>(pImpl->settings.loggingPlugin.loggerFactory()))
	);

	// Viet::Networking
	pImpl->server.reset(settings.netPlugin.serverFactory());
	pImpl->gmServer.reset(settings.gamemodeNetPlugin.serverFactory());

	// Viet::NoSqlDatabase
	std::shared_ptr<Viet::NoSqlDatabase::INoSqlDatabase> db(settings.nosqlPlugin.databaseFactory());
	pImpl->asyncDb.reset(new AsyncDatabase(db));

	std::map<std::string, std::vector<std::string>> propertiesByEntityName;
	for (auto &e : settings.entities) {
		auto propList = EntityAccess::GetPropList_(e);
		assert(propList);
		if (!propList) continue;

		auto &propNames = propertiesByEntityName[e.GetName()];
		propList->ForEach([&](const char *name, Properties::IPropertyAny *prop) {
			propNames.push_back(name);
		});
	}

	pImpl->gmController.reset(
		new RemoteGamemodeController(settings.devPassword, logger, pImpl->gmServer, std::make_shared<Viet::MessageSerializer>(pImpl->gmSeri.get()), pImpl->timerManager, propertiesByEntityName)
	);

	SkympServerView::Settings s{ 
		(uint32_t)settings.maxPlayers, pImpl->server, pImpl->entityHashGen, pImpl->gmController, logger, pImpl->timerManager, pImpl->asyncDb, settings.entities 
	};
	s.cellProcessFactory = [&] {
		return pImpl->cpHolder.CreateCellProcess();
	};
	s.actorFactory = [] {
		return std::make_unique<Actor>();
	};
	s.seri.reset(new Viet::MessageSerializer(pImpl->seri.get()));
	s.dbOps = &db->GetOperators();
	pImpl->skympServerView.reset(new SkympServerView(s));

	const auto destroyed = pImpl->destroyed;
	auto pImpl = this->pImpl;

	pImpl->cpThread.reset(new std::thread([destroyed, pImpl, logger] {
		try {
			while (!*destroyed) {
				std::this_thread::sleep_for(std::chrono::milliseconds(1));
				pImpl->cpHolder.Tick();
			}
		}
		catch (std::exception &e) {
			logger->Fatal("CellProcs thread stopped with exception: %s", e.what());
			assert(0);
		}
	}));

	pImpl->checkThread.reset(new std::thread([destroyed, pImpl, logger] {
		while (!*destroyed) {
			uint32_t ticksWas = pImpl->ticks;
			std::this_thread::sleep_for(std::chrono::seconds(1));
			if (ticksWas == pImpl->ticks) {
				logger->Fatal("Main thread is not responding");
			}
		}
	}));
}

Viet::Server::~Server() {
	*pImpl->destroyed = true;
	pImpl->cpThread->join();
	pImpl->checkThread->join();

	// Crashing here?
	// Check your Viet::Networking::IServer factory
	// You must NOT own objects created by a factory, Viet::Server owns them
}

void Viet::Server::Tick() noexcept {
	pImpl->skympServerView->Tick();
	pImpl->gmController->Tick();
	pImpl->timerManager->Tick();
	pImpl->asyncDb->Tick();
	pImpl->ticks++;
}

int Viet::Server::GetNumActiveGamemodes() const noexcept {
	return pImpl->gmController->GetNumActiveGamemodes();
}