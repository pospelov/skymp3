#pragma once
#include <memory>
#include <GameplayModel.h>
#include <viet/PropertyValue.h>

using Viet::IPropertyValue;

// Must be threadsafe
class IGameplayController {
public:
	virtual void SendPropertyChange(const char *propertyName, const IPropertyValue *propertyValue) noexcept = 0;

	virtual ~IGameplayController() = default;
};

using IGameplayControllerPtr = std::shared_ptr<IGameplayController>;
