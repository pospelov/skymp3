#pragma once
#include <memory>
#include <optional>
#include <cstdint>
#include <map>
#include <list>
#include <functional>

#include <ApiEvents.h>
#include <EntityManipulation.h>
#include <viet/FindRequest.h>
#include <viet/Transaction.h>

// evn::CallId returned by the methods are ordinal and starts at 0 (0, 1, 2, ..)
class IApiController {
public:
	virtual evn::CallId GetMaxPlayers() noexcept = 0;
	virtual evn::CallId ShowBrowserWindow(const EntityInfo &user, std::string winId, std::string jsonProperties) noexcept = 0;
	virtual evn::CallId HideBrowserWindow(const EntityInfo &user, std::string winId) noexcept = 0;
	virtual evn::CallId SendCustomPacket(const EntityInfo &user, std::string jsonContent) noexcept = 0;
	virtual evn::CallId FindEntity(std::string collection, const Viet::FindRequest &findReq) noexcept = 0;
	virtual evn::CallId ManipulateEntity(std::string collection, const EntityInfo &optionalExistingEntity, const EntityManipulation &entityManip) noexcept = 0;
	virtual evn::CallId SetActor(const EntityInfo &user, std::string collection, std::optional<EntityInfo> actor) noexcept = 0;
	virtual evn::CallId GetActor(const EntityInfo &user, std::string collection) noexcept = 0;
	virtual evn::CallId RunTransaction(const Viet::Transaction &transaction) noexcept = 0;

	// Should act like dtor
	// Other methods should not be called after Shutdown since their behaviour will be implementation-defined
	// Multiple calls of Shutdown should have no effect
	virtual void Shutdown() noexcept = 0;

	virtual ~IApiController() = default;
};