#pragma once
#include <functional>
#include <utility>
#include <cstdint>
#include <memory>
#include <optional>
#include <cassert>

#include <nlohmann/json.hpp>

#include <viet/PropertyValue.h>
#include <viet/MessageSerializer.h>
#include <viet/PropertyFlags.h>
#include <viet/JsonWrapper.h>
#include <viet/ClientContext.h>

namespace Viet {
	class Client;
	class IBaseContext;
	class IInstanceView;

	template <class T>
	class JsonCastsDefault {
	public:
		Viet::json operator()(const T &v) const {
			return v;
		}

		T operator()(const Viet::json &v) const {
			return v;
		}
	};

	template <class T, class JsonCasts = JsonCastsDefault<T>>
	class SimplePropertyValue : public Viet::PropertyValue<SimplePropertyValue<T, JsonCasts>> {
	public:

		SimplePropertyValue() = default;
		SimplePropertyValue(const T &v_) : value(v_) {
		}

		T value;

		template <class Archive>
		void serialize(Archive &a, int) {
			a & value;
		}

		static Viet::json ToJson(const SimplePropertyValue<T> &v) {
			return JsonCasts{}(v.value);
		}

		static SimplePropertyValue<T> FromJson(const Viet::json &j) {
			SimplePropertyValue<T> res;
			res.value = JsonCasts{}(j);
			return res;
		}

		inline friend bool operator==(const SimplePropertyValue<T> &lhs, const SimplePropertyValue<T> &rhs) noexcept {
			return lhs.value == rhs.value;
		}

		inline friend bool operator!=(const SimplePropertyValue<T> &lhs, const SimplePropertyValue<T> &rhs) noexcept {
			return !(lhs == rhs);
		}
	};

	using json = nlohmann::json;

	enum class PropertyCastTarget {
		All,
		Database,
		Api
	};

	class Entity;
	class Client;

	class Property {
	public:
		static Property User();
		static Property CustomPacket();

		friend class Viet::Entity;
		friend class Viet::Client;

		using Value = std::unique_ptr<IPropertyValue>;

		Property();
		~Property();
		Property(const Property &);
		Property &operator=(const Property &);

		const char *GetName() const noexcept;

		template <class PropClass, class PropValue>
		class Builder {
			std::shared_ptr<Property> prop;
		public:
			explicit Builder(std::shared_ptr<Property> prop_) : prop(prop_) {
				this->SetSaveConditionAnyChange();
				this->AddSerialization();
				this->AddCasts();
				this->SetDefaultValue(PropValue());
			}

			operator const Property &() {
				return *this->prop;
			}

			template <class BaseContext, class InstanceView>
			auto &AddClient() {
				using Context = Viet::ClientContext<PropClass, BaseContext, InstanceView>;
				Viet::ClientContextFactory fac = Context::GetFactory();
				assert(fac);

				prop->SetClientContextFactory(fac);

				prop->SetOnUpdate([](ClientContextImpl *ctx) {
					assert(ctx);
					auto ctxConcrete = dynamic_cast<Context *>(ctx);
					assert(ctxConcrete);
					if (!ctxConcrete) return;
					PropClass::OnUpdate(*ctxConcrete);
				});
				return *this;
			}

			template <class F>
			auto &AttachToGrid(const F &f) {
				this->prop->AttachToGridImpl([f](const Value &value)->std::optional<std::pair<int16_t, int16_t>> {
					auto concrete = dynamic_cast<const PropValue *>(value.get());
					assert(concrete);
					if (!concrete) return std::nullopt;
					return f(*concrete);
				});
				return *this;
			}

			template <class F>
			auto &SetSaveCondition(const F &f) {
				this->prop->SetSaveConditionImpl([f](const Value &prevValue, const Value &newValue) {
					PropValue prevValueConcrete;

					assert(prevValue);
					if (prevValue) {
						auto ptr = dynamic_cast<const PropValue *>(prevValue.get());
						assert(ptr);
						if (ptr) {
							prevValueConcrete = *ptr;
						}
					}

					assert(newValue);
					PropValue newValueConcrete;
					if (newValue) {
						auto ptr = dynamic_cast<const PropValue *>(newValue.get());
						assert(ptr);
						if (ptr) {
							newValueConcrete = *ptr;
						}
					}
					return f(prevValueConcrete, newValueConcrete);
				});
				return *this;
			}

			template <class ToJsonFn, class FromJsonFn>
			auto &AddCast(PropertyCastTarget castTarget, const ToJsonFn &toJsonFn, const FromJsonFn &fromJsonFn) {
				this->prop->AddCastImpl(castTarget, [=](const Value &value) {
					auto ptr = dynamic_cast<const PropValue *>(value.get());
					assert(ptr);
					return ptr ? toJsonFn(*ptr) : json();
				}, [=](const json &j)->Value {
					return fromJsonFn(j).Clone();
				});
				return *this;
			}

			auto &SetDefaultValue(const PropValue &defaultValue) {
				auto v = defaultValue.Clone();
				this->prop->SetDefaultValueImpl(v);
				return *this;
			}

			auto &AddFlags(int flags) {
				this->prop->AddFlagImpl(flags);
				return *this;
			}

			auto &RemoveFlags(int flags) {
				this->prop->RemoveFlagImpl(flags);
				return *this;
			}

			auto &SetValueForcesCaching(const PropValue &v) {
				this->prop->SetValueForcesCachingImpl(v.Clone());
				return *this;
			}

		private:
			auto &AddSerialization() {
				this->prop->SetFromToBinary([](Viet::MessageSerializer &seri, Viet::SeriToUse seriTouse, const Value &v, uint8_t *&outData, size_t &outLength) {
					auto ptr = dynamic_cast<const PropValue *>(v.get());
					assert(ptr);
					if (!ptr) {
						outData = nullptr;
						outLength = 0;
						return;
					}
					seri.Write<PropValue, false>(*ptr, outData, outLength, seriTouse);
				}, [](Viet::MessageSerializer &seri, Viet::SeriToUse seriTouse, const uint8_t *data, size_t length) {
					auto ptr = new PropValue;
					seri.Read<PropValue, false>(*ptr, data, length, seriTouse);
					Value v(ptr);
					return v;
				});
				return *this;
			}

			auto &AddCasts() {
				this->AddCast(Viet::PropertyCastTarget::All, PropValue::ToJson, PropValue::FromJson);
				return *this;
			}

			auto &SetSaveConditionAnyChange() {
				this->SetSaveCondition([](const PropValue &prevValue, const PropValue &newValue) {
					return prevValue != newValue;
				});
				return *this;
			}
		};

		template <class PropClass>
		auto Register() {
			this->SetName(PropClass::name);
			return Builder<PropClass, typename PropClass::Value>(std::shared_ptr<Property>(new Property(*this)));
		}

	protected:
		using OnUpdate = std::function<void(ClientContextImpl *)>;

		using GridAttachFn = std::function<std::optional<std::pair<int16_t, int16_t>>(const Value &)>;
		using SaveConditionFn = std::function<bool(const Value &, const Value &)>;
		using ToJsonFn = std::function<json(const Value &)>;
		using FromJsonFn = std::function<Value(const json &)>;
		using ToBinaryFn = std::function<void(Viet::MessageSerializer &, Viet::SeriToUse, const Value &, uint8_t *&, size_t &)>;
		using FromBinaryFn = std::function<Value(Viet::MessageSerializer &, Viet::SeriToUse, const uint8_t *, size_t)>;

		void AttachToGridImpl(const GridAttachFn &fn);
		void SetSaveConditionImpl(const SaveConditionFn &fn);
		void AddCastImpl(PropertyCastTarget castTarget, const ToJsonFn &toJson, const FromJsonFn &fromJson);
		void SetDefaultValueImpl(Value &value);
		void SetFromToBinary(ToBinaryFn toBinary, FromBinaryFn fromBinary);
		void AddFlagImpl(int flag);
		void RemoveFlagImpl(int flag);
		void SetValueForcesCachingImpl(Value v);
		void SetName(const char *name);
		void SetClientContextFactory(const ClientContextFactory &fac);
		void SetOnUpdate(const OnUpdate &onUpdate);

		// Throws
		void *AsPropertyAny() const;

		ClientContextFactory GetClientContextFactory() const;

		OnUpdate GetOnUpdate() const;

	private:
		struct Impl;
		Impl *const pImpl;
	};
}