#include <MsgWrapper.h>
#include <EntityAccess.h>

#include "NetController.h"

namespace {
	Properties *FindPropList(const std::vector<Viet::Entity> &entities, int entityId) {
		assert(entityId >= 0);
		assert(entityId < (int)entities.size());
		if (entityId < 0 || entityId >= (int)entities.size()) return nullptr;
		return EntityAccess::GetPropList_(entities[entityId]);
	}
}

NetController::NetController(std::shared_ptr<Viet::Networking::IClient> cl_, std::shared_ptr<Viet::MessageSerializer> seri_, std::string mySessionHash_, std::vector<Viet::Entity> entities_) {
	assert(seri_ != nullptr);
	this->cl = cl_;
	this->seri = seri_;
	this->mySessionHash = mySessionHash_;
	this->entities = entities_;
}

void NetController::Send(const msg::UpdateActorProperties &m) noexcept {
	auto propList = FindPropList(this->entities, m.entityId);
	assert(propList);
	if (!propList) return;

	const bool reliable = 
		EntityProps(m.props).Exclude(Viet::PropertyFlags::ReliableNetworking, propList).IsEmpty();

	this->Send_(m, reliable, propList);
}

void NetController::Send(const msg::CustomPacket &m) noexcept {
	this->Send_(m, true, nullptr);
}

void NetController::Tick(MessageListeners &listeners) noexcept {
	assert(listeners.gameplayMsgListener);
	if (!listeners.gameplayMsgListener) return;

	auto &listener = *listeners.gameplayMsgListener;

	std::lock_guard l(this->m);

	assert(this->cl != nullptr);
	if (!this->cl) return;

	this->cl->Tick([&](const Viet::Networking::Packet &p) {
		msg::ActorId msgUserId;
		msg::DestroyPlayer destroyPl;
		msg::SimpleCommand simpleCmd;
		msg::GuiJson guiJson;
		msg::CustomPacket cp;

		using ConnectResult = IConnectionMessageListener::ConnectResult;
		using PacketType = Viet::Networking::PacketType;
		ConnectResult res;
		bool onConnect = false;
		switch (p.type) {
		case PacketType::ConnectionAccepted:
			res = ConnectResult::Accepted;
			onConnect = true;
			break;
		case PacketType::ConnectionFailed:
			res = ConnectResult::Failed;
			onConnect = true;
			break;
		case PacketType::ConnectionDenied:
			res = ConnectResult::Denied;
			onConnect = true;
			break;
		case PacketType::Disconnect:
			for (auto l : listeners.connMsgListeners) {
				assert(l);
				if (l) l->OnDisconnect();
			}
			break;
		case PacketType::Message:
			assert(p.data != nullptr);
			assert(p.length > 0);
			assert(this->seri != nullptr);
			if (p.data != nullptr && p.length > 0 && this->seri) {
				auto &seri = *this->seri;
				switch ((MessageId)p.data[0]) {
				case msg::ActorId::Id:
					seri.Read(msgUserId, p.data, p.length);
					listener.OnMessage(msgUserId);
					break;
				case msg::CreatePlayer::Id: {
					msg::CreatePlayer createPl;
					seri.Read(createPl, p.data, p.length);

					assert(createPl.entityId >= 0);
					assert(createPl.props.IsEmpty());

					auto propList = FindPropList(this->entities, createPl.entityId);
					assert(propList);

					seri.Read(createPl, p.data, p.length, Viet::SeriToUse::This, propList);
					assert(createPl.props.IsEmpty() == false);

					listener.OnMessage(createPl);
					break;
				}
				case msg::DestroyPlayer::Id:
					seri.Read(destroyPl, p.data, p.length);
					listener.OnMessage(destroyPl);
					break;
				case msg::SimpleCommand::Id:
					seri.Read(simpleCmd, p.data, p.length);
					listener.OnMessage(simpleCmd);
					break;
				case msg::GuiJson::Id:
					seri.Read(guiJson, p.data, p.length);
					assert(listeners.guiMsgListener);
					if (listeners.guiMsgListener) {
						listeners.guiMsgListener->OnMessage(guiJson);
					}
					break;
				case msg::UpdateMyProperties::Id: {
					msg::UpdateMyProperties updAcProps;
					seri.Read(updAcProps, p.data, p.length);
					assert(updAcProps.entityId >= 0);
					assert(updAcProps.props.IsEmpty());

					auto propList = FindPropList(this->entities, updAcProps.entityId);
					assert(propList);

					seri.Read(updAcProps, p.data, p.length, Viet::SeriToUse::This, propList);
					
					if (updAcProps.op == UpdatePropertyOp::Update) {
						assert(updAcProps.props.IsEmpty() == false);
					}

					listener.OnMessage(updAcProps);
					break;
				}
				case msg::UpdateActorProperties::Id: {
					msg::UpdateActorProperties updAcProps;
					seri.Read(updAcProps, p.data, p.length);
					assert(updAcProps.entityId >= 0);
					assert(updAcProps.props.IsEmpty());

					auto propList = FindPropList(this->entities, updAcProps.entityId);
					assert(propList);

					seri.Read(updAcProps, p.data, p.length, Viet::SeriToUse::This, propList);
					assert(updAcProps.props.IsEmpty() == false);

					listener.OnMessage(updAcProps);
					break;
				}
				case msg::CustomPacket::Id:
					seri.Read(cp, p.data, p.length);
					assert(listeners.cpMsgListener);
					if (listeners.cpMsgListener) {
						listeners.cpMsgListener->OnMessage(cp);
					}
					break;
				}
			}
			break;
		default:
			assert(0 && "Unsupported PacketType");
			break;
		}
		if (onConnect) {
			for (auto l : listeners.connMsgListeners) {
				assert(l);
				if (l) l->OnConnect(res);
			}
			if (res == ConnectResult::Accepted) {
				MsgWrapper<msg::Handshake> msg(&*this->seri, { this->mySessionHash });
				this->cl->Send(msg.GetBinary().first, msg.GetBinary().second, true);
			}
		}
	});
}