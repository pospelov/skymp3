#pragma once
#include <ctime>
#include <mutex>
#include <vector>
#include <IGameplayPresenter.h>
#include <viet/MessageSerializer.h>

// All methods must be threadsafe
class GameplayPresenterReplayable : public IGameplayPresenter {
public:
	GameplayPresenterReplayable(IGameplayPresenter *base, std::shared_ptr<Viet::MessageSerializer> seri);

	// IGameplayPresenter
	void ProcessModel(const Callback &cb) noexcept override;

	std::vector<GameplayModel> Release() noexcept {
		std::lock_guard l(this->m);
		const auto res = std::move(this->rs.repl);
		this->rs.repl.clear();
		return res;
	}

	void SetData(const std::vector<GameplayModel> &data) noexcept {
		std::lock_guard l(this->m);
		this->rs.repl = data;
	}

private:
	int GetNumFramesInternal() noexcept;
	void CalcCurrentFrame() noexcept;

	static constexpr float ClocksToSeconds(clock_t x) {
		return (float)x / CLOCKS_PER_SEC;
	}

	IGameplayPresenter *const base;
	const std::shared_ptr<Viet::MessageSerializer> seri;
	std::mutex m;

	const float accuracy = 0.05f;

	struct RecordState {
		float lastRecord = 0;
		float firstRecord = -1.f;
		int unsafeCurrentFrame = -1;
		float firstGetCurrentFrame = -1.f;

		int framesPassedReal = 0;
		int framesOffset = 0;

		std::vector<GameplayModel> repl;
	};

	bool recording = false;
	RecordState rs;
};