#pragma once
#include <viet/JsonWrapper.h>
#include <viet/Instance.h>

namespace Viet {
	class ApiClient;
	class ActionsAccess;

	class Actions {
		friend class Viet::ApiClient;
		friend class Viet::ActionsAccess;
	public:
		Actions &Get(const char *propName) noexcept {
			actions.push_back(MGet{ propName });
			return *this;
		}

		Actions &Set(const char *propName, const json &value) noexcept {
			actions.push_back(MSet{ propName, value });
			return *this;
		}

		Actions &Call(const char *methodName, const std::vector<json> &args) noexcept {
			std::vector<JsonWrapper> argsWrapped;
			argsWrapped.reserve(args.size());
			for (auto &arg : args) {
				argsWrapped.push_back(arg);
			}
			actions.push_back(MCallMethod{ methodName, argsWrapped });
			return *this;
		}

		friend std::ostream &operator<<(std::ostream &os, const Actions &d) {
			bool needWhitespace = false;
			for (auto &action : d.actions) {
				if (needWhitespace) os << ", ";
				needWhitespace = true;
				if (action.index() == 0) {
					os << std::get<0>(action);
				}
				else if (action.index() == 1) {
					os << std::get<1>(action);
				}
				else if (action.index() == 2) {
					os << std::get<2>(action);
				}
				else {
					assert(0 && "Unknown action");
				}
			}
			return os;
		}

		template <class Archive>
		void load(Archive &ar, unsigned int version) {
			uint32_t n;
			ar & n;
			actions.resize(n);
			for (size_t i = 0; i < n; ++i) {
				uint8_t idx = 0;
				ar & idx;
				if (idx == 0) {
					MCallMethod m;
					ar & m;
					actions[i] = m;
				}
				else if (idx == 1) {
					MGet m;
					ar & m;
					actions[i] = m;
				}
				else if (idx == 2) {
					MSet m;
					ar & m;
					actions[i] = m;
				}
				else {
					assert(0 && "Unknown action type");
				}
			}
		}

		template <class Archive>
		void save(Archive &ar, unsigned int version) const {
			ar & (uint32_t)actions.size();
			for (auto &manip : actions) {
				const uint8_t idx = (uint8_t)manip.index();
				ar & idx;
				std::visit([&](auto &concrete) {
					ar & concrete;
				}, manip);
			}
		}

		template <class Archive>
		void serialize(Archive &a, int) noexcept {
			if constexpr (Archive::is_load) {
				this->load(a, 0);
			}
			if constexpr (!Archive::is_load) {
				this->save(a, 0);
			}
		}

	private:
		struct MCallMethod {
			std::string name;
			std::vector<JsonWrapper> args;

			friend std::ostream &operator<<(std::ostream &os, const MCallMethod &d) {
				os << "CallMethod(" << d.name;
				for (auto &arg : d.args) {
					os << ", " << arg.v.dump();
				}
				return os << ')';
			}

			template <class Archive>
			void serialize(Archive &a, int) {
				a & name & args;
			}
		};

		struct MGet {
			std::string prop;

			friend std::ostream &operator<<(std::ostream &os, const MGet &d) {
				return os << "Get(" << d.prop << ")";
			}

			template <class Archive>
			void serialize(Archive &a, int) {
				a & prop;
			}
		};

		struct MSet {
			std::string prop;
			JsonWrapper newValue;

			friend std::ostream &operator<<(std::ostream &os, const MSet &d) {
				return os << "Set(" << d.prop << ", " << d.newValue.v.dump() << ")";
			}

			template <class Archive>
			void serialize(Archive &a, int) {
				a & prop & newValue;
			}
		};

		std::vector<std::variant<MCallMethod, MGet, MSet>> actions;
	};

	struct ActionsResult {
		std::optional<Instance> instance;
		std::vector<Viet::JsonWrapper> returnedValues;

		template <class Archive>
		void serialize(Archive &a, int) {
			a & instance & returnedValues;
		}
	};
}