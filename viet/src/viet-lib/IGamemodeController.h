#pragma once
#include <utility>
#include <ApiEvents.h>
#include <FrontEndFiles.h>

class IGamemodeController {
public:
	virtual void SendEvent(const evn::Any &event) noexcept = 0;

	virtual std::optional<evn::Any> GetNextMethodCalled() noexcept = 0;
	virtual std::optional<FrontEndFiles> GetNextFrontEndChange() noexcept = 0;
};