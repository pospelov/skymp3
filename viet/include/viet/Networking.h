#pragma once
#include <functional>

namespace Viet {
	namespace Networking {
		using UserId = unsigned short;
		static constexpr UserId g_invalidUserId = (UserId)-1;

		enum class PacketType {
			Invalid = -1,
			// Client-only:
			Disconnect,
			ConnectionAccepted,
			ConnectionFailed,
			ConnectionDenied,
			// Server-only:
			UserConnect,
			UserDisconnect,
			// Emitted on both sides:
			Message
		};

		enum : unsigned char {
			MinPacketId = 134
		};

		class Packet {
		public:
			PacketType type = PacketType::Invalid;
			unsigned char *data = nullptr;
			size_t length = 0;

			Packet() = default;
			Packet &operator=(const Packet &) = delete;
			Packet(const Packet &) = delete;
		};

		class IClient {
		public:
			using OnPacket = std::function<void(const Packet &)>;

			virtual void Tick(const OnPacket &onPacket) noexcept = 0;

			// First byte of 'packet' must represent id of the packet
			// Value must not be lower than MinPacketId
			virtual void Send(const unsigned char *packet, size_t length, bool reliable) noexcept = 0;

			virtual ~IClient() = default;
		};

		class IServer {
		public:
			using OnPacket = std::function<void(const Packet &, UserId)>;

			virtual void Tick(const OnPacket &onPacket) noexcept = 0;
			virtual void Send(UserId userId, const unsigned char *packet, size_t length, bool reliable) noexcept = 0;

			virtual ~IServer() = default;
		};

		struct Plugin {
			std::function<IClient *()> clientFactory;
			std::function<IServer *()> serverFactory;
		};
	}
}