#pragma once
#include <viet/JsonWrapper.h>

class Point3D : public Viet::PropertyValue<Point3D>
{
public:
	Point3D() {}
	Point3D(float X, float Y, float Z) : x(X), y(Y), z(Z) {};

	static Point3D FromJson(const Viet::json &v) {
		Point3D res;
		if (v.is_array() && v.size() >= 3) {
			res.x = v[0];
			res.y = v[1];
			res.z = v[2];
		}
		return res;
	}

	static Viet::json ToJson(const Point3D &v) {
		auto arr = Viet::json::array();
		arr.push_back(v.x);
		arr.push_back(v.y);
		arr.push_back(v.z);
		return arr;
	}

	bool operator==(const Point3D &rhs) const {
		return (x == rhs.x && y == rhs.y && z == rhs.z);
	}
	bool operator!=(const Point3D &rhs) const {
		return !operator==(rhs);
	}

	Point3D operator-(const Point3D &rhs) const {
		return Point3D(x - rhs.x, y - rhs.y, z - rhs.z);
	}

	float Length() const {
		return sqrt(x*x + y * y + z * z);
	}

	template <class Archive>
	void serialize(Archive &ar, unsigned int version) {
		ar & x & y & z;
	}

	// @members
	float	x = 0;	// 0
	float	y = 0;	// 4
	float	z = 0;	// 8
};

using Viet::SimplePropertyValue;

class PIsEnabled {
public:
	static constexpr auto name = "isEnabled";
	using Value = SimplePropertyValue<bool>;

  static Viet::Property Build() {
    return Viet::Property().Register<PIsEnabled>()
    .RemoveFlags(Viet::PropertyFlags::ClientWritable)
    .SetValueForcesCaching(PIsEnabled::Value(true));
  }
};

class PPos {
public:
	static constexpr auto name = "pos";
	using Value = Point3D;

	static Viet::Property Build() {
    return Viet::Property().Register<PPos>()
		.AttachToGrid([](const PPos::Value &pos) { return std::pair<int16_t, int16_t>(int16_t(pos.x / 4096), int16_t(pos.y / 4096)); })
		.SetSaveCondition([](const PPos::Value &prevPos, const PPos::Value &newPos) { return (prevPos- newPos).Length() > 1024.f; })
		.RemoveFlags(Viet::PropertyFlags::ReliableNetworking);
  }
};

class PAngle {
public:
	static constexpr auto name = "angle";
	using Value = Point3D;

	static Viet::Property Build() {
    return Viet::Property().Register<PAngle>()
		.RemoveFlags(Viet::PropertyFlags::ReliableNetworking);
  }
};

class PCellOrWorld {
public:
	static constexpr auto name = "cellOrWorld";
	using Value = SimplePropertyValue<uint32_t>;

	static Viet::Property Build() {
		return Viet::Property().Register<PCellOrWorld>()
			.AddFlags(Viet::PropertyFlags::AffectsCellProcessSelection)
			.RemoveFlags(Viet::PropertyFlags::ClientWritable);
	}
};

class PClientWritableInt {
public:
	static constexpr auto name = "clientWritableInt";
	using Value = SimplePropertyValue<int32_t>;

	static Viet::Property Build() {
		return Viet::Property().Register<PClientWritableInt>()
			.AddFlags(Viet::PropertyFlags::ClientWritable);
	}
};

class PNonClientWritableInt {
public:
	static constexpr auto name = "nonClientWritableInt";
	using Value = SimplePropertyValue<int32_t>;

	static Viet::Property Build() {
		return Viet::Property().Register<PNonClientWritableInt>()
			.RemoveFlags(Viet::PropertyFlags::ClientWritable);
	}
};

class PClientReadableInt {
public:
	static constexpr auto name = "clientReadableInt";
	using Value = SimplePropertyValue<int32_t>;

	static Viet::Property Build() {
		return Viet::Property().Register<PClientReadableInt>()
			.AddFlags(Viet::PropertyFlags::ClientReadable);
	}
};

class PNonClientReadableInt {
public:
	static constexpr auto name = "nonClientReadableInt";
	using Value = SimplePropertyValue<int32_t>;

	static Viet::Property Build() {
		return Viet::Property().Register<PNonClientReadableInt>()
			.RemoveFlags(Viet::PropertyFlags::ClientReadable);
	}
};

class PVisibleToNeighbourInt {
public:
	static constexpr auto name = "visibleToNeighbourInt";
	using Value = SimplePropertyValue<int32_t>;

	static Viet::Property Build() {
		return Viet::Property().Register<PVisibleToNeighbourInt>()
			.AddFlags(Viet::PropertyFlags::VisibleToNeighbour)
			.AddFlags(Viet::PropertyFlags::ClientReadable | Viet::PropertyFlags::ClientWritable);
	}
};

class PNonVisibleToNeighbourInt {
public:
	static constexpr auto name = "nonVisibleToNeighbourInt";
	using Value = SimplePropertyValue<int32_t>;

	static Viet::Property Build() {
		return Viet::Property().Register<PNonVisibleToNeighbourInt>()
			.RemoveFlags(Viet::PropertyFlags::VisibleToNeighbour)
			.AddFlags(Viet::PropertyFlags::ClientReadable | Viet::PropertyFlags::ClientWritable);
	}
};

class PJson {
public:
	static constexpr auto name = "json";
	using Value = SimplePropertyValue<Viet::JsonWrapper>;

	static Viet::Property Build() {
		return Viet::Property().Register<PJson>();
	}
};

class PDefaultInt {
public:
	static constexpr auto name = "defaultInt";
	using Value = SimplePropertyValue<int32_t>;

	static Viet::Property Build() {
		return Viet::Property().Register<PDefaultInt>()
			.SetDefaultValue(108);
	}
};


std::vector<Viet::Entity> GetTestEntities() {
	auto actorProperties = {
		PIsEnabled::Build(),
		PPos::Build(),
		PCellOrWorld::Build(),
		PClientWritableInt::Build(),
		PNonClientWritableInt::Build(),
		PClientReadableInt::Build(),
		PNonClientReadableInt::Build(),
		PVisibleToNeighbourInt::Build(),
		PNonVisibleToNeighbourInt::Build(),
		PJson::Build(),
		PDefaultInt::Build(),
		PAngle::Build(),
		Viet::Property::User(),
		Viet::Property::CustomPacket()
	};

	std::vector<Viet::Entity> res;
	res.push_back(
		Viet::Entity("Actor", actorProperties)
	);
	return res;
}
