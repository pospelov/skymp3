#pragma once
#include <string>
#include <cstdint>
#include <viet/Logging.h>
#include <viet/Networking.h>
#include <viet/Serialization.h>
#include <viet/Entity.h>

namespace VietNode {
  Viet::Networking::Plugin GetNetworkingPlugin(std::string ip, uint16_t gamemodesPort);
  Viet::Networking::Plugin GetBotNetworkingPlugin(std::string ip, uint16_t port);
  Viet::Serialization::Plugin GetSeriPlugin();
  std::vector<Viet::Entity> GetEntities();
}
