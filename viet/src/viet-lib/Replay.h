#pragma once
#include <vector>
#include <GameplayModel.h>

struct Replay {
	static constexpr uint32_t g_protocolVersion = 0;

	static constexpr int Id = -1; // Required for IMessageSerializer

	std::vector<GameplayModel> data;
	uint32_t protocolVersion = g_protocolVersion;
};
