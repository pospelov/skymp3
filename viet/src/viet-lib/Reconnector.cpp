#include <cassert>
#include <memory>
#include "Reconnector.h"

struct Reconnector::Impl {
	CreateClientFn createClient;
	std::unique_ptr<Viet::Networking::IClient> net;
	bool isOnline = false;
	bool needReset = false;
};

Reconnector::Reconnector(CreateClientFn createClient) noexcept : pImpl(new Impl) {
	pImpl->createClient = createClient;
}

Reconnector::~Reconnector() {
	delete pImpl;
}

void Reconnector::Tick(const OnPacket &onPacket) noexcept {
	if (!pImpl->net || pImpl->needReset) {
		pImpl->needReset = false;
		pImpl->isOnline = false;
		pImpl->net.reset(pImpl->createClient());
	}

	pImpl->net->Tick([&](const Viet::Networking::Packet &packet) {
		if (packet.type == Viet::Networking::PacketType::Disconnect) {
			pImpl->needReset = true;
		}
		else if (packet.type == Viet::Networking::PacketType::ConnectionAccepted
			|| packet.type == Viet::Networking::PacketType::ConnectionDenied
			|| packet.type == Viet::Networking::PacketType::ConnectionFailed) {
			if (packet.type != Viet::Networking::PacketType::ConnectionAccepted) {
				pImpl->needReset = true;
			}
			else {
				pImpl->isOnline = true;
			}
		}
		onPacket(packet);
	});
}

void Reconnector::Send(const unsigned char *packet, size_t length, bool reliable) noexcept {
	if (!pImpl->net || !pImpl->isOnline) return;
	pImpl->net->Send(packet, length, reliable);
}