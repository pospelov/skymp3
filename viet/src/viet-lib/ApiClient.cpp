#include <unordered_map>
#include <RemoteApi.h>
#include <viet/ApiClient.h>
#include <RemoteApi.h>
#include <Reconnector.h>
#include <ActionsToManip.h>

namespace {
	// https://en.cppreference.com/w/cpp/utility/variant/visit
	template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
	template<class... Ts> overloaded(Ts...)->overloaded<Ts...>;

	Viet::ActionsResult ManipResultToActionsResult(EntityManipulationResult &v) {
		Viet::ActionsResult res;
		if (v.ei.IsEmpty() == false) {
			res.instance = Viet::Instance(v.ei.ToString());
		}
		res.returnedValues.reserve(v.opResults.size());
		for (auto &j : v.opResults) {
			res.returnedValues.push_back(std::move(j.v));
		}
		return res;
	}

	std::vector<Viet::Instance> VecEiToVecInstance(std::vector<EntityInfo> &v) {
		std::vector<Viet::Instance> r;
		r.reserve(v.size());
		for (auto &vv : v) {
			r.push_back(Viet::Instance(vv.ToString()));
		}
		return r;
	}
}

struct Viet::ApiClient::Impl {
	Viet::Networking::Plugin gamemodeNetPlugin;
	Viet::Serialization::Plugin seriPlugin;
	Listeners listeners;

	std::unique_ptr<ApiController> apic;
	std::shared_ptr<Viet::Logging::Logger> logger;

	std::unique_ptr<Viet::Serialization::ISerializer> seriBase;
	std::shared_ptr<Viet::MessageSerializer> seri;

	using AnyPromise = std::variant<Promise<int>, Promise<Void>, Promise<std::optional<Instance>>, Promise<ActionsResult>, Promise<std::vector<Instance>>, Promise<TransactionResult>>;
	std::unordered_map<evn::CallId, AnyPromise> promises;

	template <class T>
	Promise<T> MakePromise(evn::CallId callId) {
		Viet::Promise<T> p;
		this->promises[callId] = p;
		return p;
	}

	void ProcessCallResult(evn::CallResult &e) {
		auto it = this->promises.find(e.callId);
		assert(it != this->promises.end());
		if (it == this->promises.end()) return;

		auto &promise = it->second;
		if (e.error.size() > 0) {
			std::visit([&](auto &promise) { promise.Reject(e.error.data()); }, promise);
			return;
		}

		if (!e.returnValue) {
			std::visit(overloaded{
			[&](Promise<int> &p) { p.Resolve(0); },
			[&](Promise<Void> &p) { p.Resolve(Void()); },
			[&](Promise<std::optional<Instance>> &p) { p.Resolve(std::nullopt); },
			[&](Promise<ActionsResult> &p) { p.Resolve({}); },
			[&](Promise<std::vector<Instance>> &p) { p.Resolve({}); },
			[&](Promise<TransactionResult> &p) { p.Resolve({}); }
				}, promise);
			return;
		}

		auto fStr = [&](const std::string &v) {
			auto asEi = EntityInfo::FromString(v);
			std::visit(overloaded{
			[&](auto &p) { assert(0);  p.Reject("Unexpected promise type"); },
			[&](Promise<std::optional<Instance>> &p) { p.Resolve(Instance(asEi.ToString())); }
				}, promise);
		};

		auto fDouble = [&](const double &v) {
			std::visit(overloaded{
			[&](auto &p) { assert(0);  p.Reject("Unexpected promise type"); },
			[&](Promise<int> &p) { p.Resolve((int)v); }
				}, promise);
		};

		auto fEntityManipResult = [&](EntityManipulationResult &v) {
			std::visit(overloaded{
			[&](auto &p) { assert(0);  p.Reject("Unexpected promise type"); },
			[&](Promise<ActionsResult> &p) { p.Resolve(ManipResultToActionsResult(v)); }
				}, promise);
		};

		auto fVectorEntityInfo = [&](std::vector<EntityInfo> &v) {
			std::visit(overloaded{
			[&](auto &p) { assert(0);  p.Reject("Unexpected promise type"); },
			[&](Promise<std::vector<Instance>> &p) { p.Resolve(VecEiToVecInstance(v)); }
				}, promise);
		};

		auto fTransactionResult = [&](const TransactionResult &v) {
			std::visit(overloaded{
			[&](auto &p) { assert(0);  p.Reject("Unexpected promise type"); },
			[&](Promise<TransactionResult> &p) { p.Resolve(v); }
				}, promise);
		};

		std::visit(overloaded{ fStr, fDouble, fEntityManipResult, fVectorEntityInfo, fTransactionResult }, *e.returnValue);

		promises.erase(it);
	}
};

Viet::ApiClient::ApiClient(const Settings &settings, const Listeners &listeners) : pImpl(new Impl) {
	const std::shared_ptr<Networking::IClient> netCl(new Reconnector([=] { 
		return settings.gamemodeNetPlugin.clientFactory(); 
	}));

	pImpl->gamemodeNetPlugin = settings.gamemodeNetPlugin;
	pImpl->seriPlugin = settings.seriPlugin;
	pImpl->listeners = listeners;

	pImpl->seriBase.reset(settings.seriPlugin.serializerFactory());
	pImpl->seri.reset(new Viet::MessageSerializer(pImpl->seriBase.get()));

	std::shared_ptr<Viet::Logging::ILoggerOutput> loggerOut(settings.loggingPlugin.loggerFactory());
	auto logger = std::make_shared<Viet::Logging::Logger>(loggerOut);

	pImpl->apic.reset(new ApiController(netCl, pImpl->seri, logger, settings.serverId, settings.devPassword, { settings.frontEndFiles }));
	pImpl->logger = logger;
}

Viet::ApiClient::~ApiClient() {
	delete pImpl;
}

void Viet::ApiClient::Tick() noexcept {
	auto events = pImpl->apic->Tick();

	for (auto &evn : *events) {

		pImpl->logger->Debug("ApiClient nextEvent");
		// CallResult, ScriptInit, UserConnect, UserDisconnect, UserCustomPacket, RaceMenuExit, GetMaxPlayers, ShowBrowserWindow, HideBrowserWindow, SendCustomPacket, FindEntity, ManipulateEntity, SetActor, GetActor>

		auto fUserConnect = [&](evn::UserConnect &e) {
			assert(pImpl->listeners.onUserEnter);
			if (pImpl->listeners.onUserEnter) {
				pImpl->listeners.onUserEnter(Instance(e.user.ToString()));
			}
		};

		auto fUserDisconnect = [&](evn::UserDisconnect &e) {
			assert(pImpl->listeners.onUserExit);
			if (pImpl->listeners.onUserExit) {
				pImpl->listeners.onUserExit(Instance(e.user.ToString()));
			}
		};

		auto fScriptInit = [&](evn::ScriptInit &e) {
			assert(pImpl->listeners.onConnect);
			if (pImpl->listeners.onConnect) {
				pImpl->listeners.onConnect(e.error ? "Handshake failed" : "", { e.propertiesByEntityName });
			}
		};

		auto fUserCustomPacket = [&](evn::UserCustomPacket &e) {
			assert(pImpl->listeners.onUserCustomPacket);
			if (pImpl->listeners.onUserCustomPacket) {
				pImpl->listeners.onUserCustomPacket(Instance(e.user.ToString()), e.data);
			}
		};

		std::visit(overloaded{
			[&](evn::CallResult &e) { pImpl->ProcessCallResult(e); },
			[&](evn::UserConnect &e) { fUserConnect(e); },
			[&](evn::UserDisconnect &e) { fUserDisconnect(e); },
			[&](evn::ScriptInit &e) { fScriptInit(e); },
			[&](evn::UserCustomPacket &e) { fUserCustomPacket(e); },
			[&](auto &) {} },
			evn);
	}
}

Viet::Logging::Logger &Viet::ApiClient::GetLogger() noexcept {
	return *pImpl->logger;
}

Viet::Promise<int> Viet::ApiClient::GetMaxPlayers() noexcept {
	return pImpl->MakePromise<int>(pImpl->apic->GetMaxPlayers());
}

Viet::Promise<std::vector<Viet::Instance>> Viet::ApiClient::Find(const char *entityName, FindRequest findReq) noexcept {
	return pImpl->MakePromise<std::vector<Instance>>(pImpl->apic->FindEntity(entityName, findReq));
}

Viet::Promise<Viet::ActionsResult> Viet::ApiClient::Create(const char *entityName, Actions actions) noexcept {
	auto manip = ActionsAccess::ActionsToManip(actions);
	return pImpl->MakePromise<ActionsResult>(pImpl->apic->ManipulateEntity(entityName, EntityInfo(), manip));
}

Viet::Promise<Viet::ActionsResult> Viet::ApiClient::Edit(const char *entityName, const Instance &instance, Actions actions) noexcept {
	auto manip = ActionsAccess::ActionsToManip(actions);
	return pImpl->MakePromise<ActionsResult>(pImpl->apic->ManipulateEntity(entityName, EntityInfo::FromString(instance.uniqueId), manip));
}

Viet::Promise<Viet::Void> Viet::ApiClient::SetAttachedInstance(const Instance &user, const char *entityName, const std::optional<Instance> &instance) noexcept {
	std::optional<EntityInfo> ei;
	if (instance) {
		ei = EntityInfo::FromString(instance->uniqueId);
	}
	return pImpl->MakePromise<Void>(pImpl->apic->SetActor(EntityInfo::FromString(user.uniqueId), entityName, ei));
}

Viet::Promise<std::optional<Viet::Instance>> Viet::ApiClient::GetAttachedInstance(const Instance &user, const char *entityName) noexcept {
	return pImpl->MakePromise<std::optional<Instance>>(pImpl->apic->GetActor(EntityInfo::FromString(user.uniqueId), entityName));
}

Viet::Promise<Viet::Void> Viet::ApiClient::SendCustomPacket(const Instance &user, const char *content) noexcept {
	return pImpl->MakePromise<Viet::Void>(pImpl->apic->SendCustomPacket(EntityInfo::FromString(user.uniqueId), content));
}

Viet::Promise<Viet::TransactionResult> Viet::ApiClient::RunTransaction(const Viet::Transaction &transaction) noexcept {
	return pImpl->MakePromise<Viet::TransactionResult>(pImpl->apic->RunTransaction(transaction));
}