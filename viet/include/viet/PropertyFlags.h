#pragma once

namespace Viet {
	namespace PropertyFlags {
		enum Enum {
			ClientWritable = 1 << 0,
			ClientReadable = 1 << 1,
			VisibleToNeighbour = 1 << 2,
			SavedToDb = 1 << 3,
			AffectsCellProcessSelection = 1 << 4,
			ReliableNetworking = 1 << 5
		};

		// These flags are added by default and can be removed with RemoveFlags method
		constexpr static int g_defaultFlags = ClientWritable | ClientReadable | VisibleToNeighbour | SavedToDb | ReliableNetworking;
	}
}