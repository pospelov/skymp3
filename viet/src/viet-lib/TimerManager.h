#pragma once
#include <ITimerManager.h>
#include <list>
#include <utility>
#include <algorithm>
#include <chrono>
#include <mutex>

class TimerManager : public ITimerManager {
public:
	// ITimerManager
	void SetTimeout(uint32_t ms, std::function<void()> fn) noexcept override {
		auto timePoint = std::chrono::steady_clock::now() + std::chrono::milliseconds(ms);

		std::lock_guard l(this->share.m);
		this->share.newTimeouts.push_back({ timePoint, fn, ms });
	}

	void Tick() noexcept {
		{
			std::lock_guard l(this->share.m);
			for (auto &t : this->share.newTimeouts) {
				this->timeouts.push_back(std::move(t));
			}
			this->share.newTimeouts.clear();
		}

		auto it = this->timeouts.begin();
		while (it != this->timeouts.end()) {
			auto &[timePoint, callback, duration] = *it;
			if (std::chrono::steady_clock::now() >= timePoint) {
				callback();
				it = this->timeouts.erase(it);
			}
			else {
				++it;
			}
		}
	}

private:
	struct Timeout {
		std::chrono::time_point<std::chrono::steady_clock> timePoint;
		std::function<void()> callback;
		uint32_t duration = 0;
	};

	struct {
		std::mutex m;
		std::list<Timeout> newTimeouts;
	} share;

	std::list<Timeout> timeouts;
};