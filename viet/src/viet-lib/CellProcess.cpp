#include <mutex>
#include <list>
#include <MakeID.h>
#include <Grid.h>
#include "ChildPath.h"
#include "EntityAccess.h"

#include "CellProcess.h"

namespace {
	using InvokedGridPos = std::vector<std::pair<ICellProcess::InstanceId, std::pair<int16_t, int16_t>>>;

	struct ActorData {
		std::unique_ptr<IActor> ac;
		bool anyGridPosInvoked = false;
	};

	template <class T>
	class InstanceIdMap {
	public:
		T &Get(const ICellProcess::InstanceId &key) {
			assert(key.actorId >= 0);
			assert(key.entityId >= 0);
			thread_local T wtf;
			if (key.actorId < 0 || key.entityId < 0) {
				return wtf;
			}

			if ((int)this->data.size() <= key.entityId)
				this->data.resize(key.entityId + 1);
			auto &sub = this->data[key.entityId];
			if ((int)sub.size() <= key.actorId)
				sub.resize(key.actorId + 1);
			return sub[key.actorId];
		}

	private:
		std::vector<std::vector<T>> data;
	};
}

struct CellProcess::Impl {
	OnVisionEvent onVisionEvent;
	OnSaveRequired onSaveRequired;
	std::vector<Viet::Entity> entities;

	Properties *GetPropList(int entityId) {
		return EntityAccess::GetPropList_(entities[entityId]);
	}

	// To be used in EditActor()
	std::vector<vision::Event<InstanceId>> tmpVisMy, tmpVisNei;

	struct Share {
		struct PerEntityData {
			std::unique_ptr<MakeID> actorIdGen;
			std::vector<ActorData> actors;
			std::list<ActorWaste> waste;
			std::list<std::pair<clock_t, InstanceId>> actorIdToRecycle;
		};

		std::vector<PerEntityData> perEntityData;

		std::mutex m;
	} share;

	struct Share2 {
		InvokedGridPos invokedGridPos;
		std::vector<InstanceId> disabledActors;
		std::mutex m;
	} share2;

	struct Share3 {
		Grid grid;

		// First index is entityId, second is instanceId (actorId)
		InstanceIdMap<std::set<InstanceId>> streamedInActors, acsIAmStreamedTo;

		std::vector<InstanceId> tmpReceiversToStreamOut;
		std::mutex m;

		bool StreamIn(const InstanceId &receiver, const InstanceId &source, const OnVisionEvent &onVision, const vision::StreamIn &streamInEvn) {
			const bool streamIn = this->streamedInActors.Get(receiver).insert(source).second;
			if (streamIn) {
				const vision::Event<InstanceId> evn{ source, streamInEvn };
				onVision(std::unique_ptr<VisionEventData>(new VisionEventData(receiver, evn)));
				
				bool inserted = this->acsIAmStreamedTo.Get(source).insert(receiver).second;
				assert(inserted);
				return true;
			}
			return false;
		}

		void StreamOut(const InstanceId &receiver, const InstanceId &source, const OnVisionEvent &onVision) {
			this->acsIAmStreamedTo.Get(source).erase(receiver);
			this->streamedInActors.Get(receiver).erase(source);
			if (onVision) {
				onVision(std::unique_ptr<VisionEventData>(
					new VisionEventData(receiver, vision::Event<InstanceId>{ source, vision::StreamOut() })
				));
			}
		}
	} share3;

	// LOCKS share3
	void SendVisionEventToNeighbours(const vision::Event<InstanceId> &evn) {
		std::lock_guard l(this->share3.m);
		for (auto &nei : this->share3.grid.GetNeighbours(evn.owner.ToUInt64())) {
			this->onVisionEvent(std::unique_ptr<VisionEventData>(new VisionEventData(InstanceId(nei), evn)));
		}
	}

	// LOCKS share3
	void MoveOnGrid(const InstanceId &id, int16_t gridX, int16_t gridY) noexcept {
		std::lock_guard l(this->share3.m);
		this->share3.grid.Move(id.ToUInt64(), gridX, gridY);
	}

	// LOCKS share3
	void ProcessInvokedGridPos(const InstanceId &instance, int16_t gridX, int16_t gridY) noexcept {
		auto propList = this->GetPropList(instance.entityId);
		assert(propList);
		if (!propList) return;

		auto &onVision = this->onVisionEvent;
		assert(onVision);

		this->MoveOnGrid(instance, gridX, gridY);

		EntityProps props;
		{
			std::lock_guard l(this->share.m);
			auto &ac = this->share.perEntityData[instance.entityId].actors[instance.actorId].ac;
			assert(ac);
			if (!ac) return;
			props = ac->GetProps();
			this->share.perEntityData[instance.entityId].actors[instance.actorId].anyGridPosInvoked = true;
		}

		std::unique_lock l(this->share3.m);
		auto &neiSet = this->share3.grid.GetNeighbours(instance.ToUInt64());
		for (auto nei_ : neiSet) {
			const InstanceId nei(nei_);

			if (this->share3.StreamIn(nei, instance, onVision, vision::StreamIn{ instance.entityId, props.Filter(Viet::PropertyFlags::VisibleToNeighbour, propList) })) {
				std::optional<vision::StreamIn> streamInEvn;
				l.unlock();
				{
					std::lock_guard l(this->share.m);
					if (nei_ >= 0 && (int)this->share.perEntityData[nei.entityId].actors.size() > nei.actorId) {
						auto &neiAc = this->share.perEntityData[nei.entityId].actors[nei.actorId].ac;
						assert(neiAc);
						if (neiAc) {
							streamInEvn = vision::StreamIn();
							streamInEvn->entityId = instance.entityId;
							streamInEvn->allProps = neiAc->GetProps().Filter(Viet::PropertyFlags::VisibleToNeighbour, propList);
						}
					}
				}
				l.lock();
				assert(streamInEvn);
				if (streamInEvn) {
					this->share3.StreamIn(instance, nei, onVision, *streamInEvn);
				}
			}

			///const vision::Event<AnyId> evn{ actorId, vision::Mov{ mov } };
			///onVision(nei, evn);
		}

		this->share3.tmpReceiversToStreamOut.clear();
		for (auto receiverActorId : this->share3.acsIAmStreamedTo.Get(instance)) {
			if (!neiSet.count(receiverActorId.ToUInt64())) {
				this->share3.tmpReceiversToStreamOut.push_back(receiverActorId);
			}
		}
		for (auto receiverActorId : this->share3.tmpReceiversToStreamOut) {
			this->share3.StreamOut(receiverActorId, instance, this->onVisionEvent);
		}
	}

	auto _OnSaveRequired(InstanceId acId, IActor &ac) noexcept {
		return this->onSaveRequired(acId, ac);
	}
};


CellProcess::CellProcess(std::vector<Viet::Entity> entities) noexcept : pImpl(new Impl) {
	pImpl->entities = entities;
	pImpl->share.perEntityData.resize(entities.size());
	for (auto &data : pImpl->share.perEntityData) {
		const int maxId = 10'000;
		data.actorIdGen.reset(new MakeID(maxId));
		data.actors.resize(maxId + 1);
	}
}

CellProcess::~CellProcess() {
	delete pImpl;
}

void CellProcess::SetOnVisionEvent(OnVisionEvent cb) noexcept {
	assert(!pImpl->onVisionEvent);
	assert(cb);
	pImpl->onVisionEvent = cb;
}

void CellProcess::SetOnSaveRequired(OnSaveRequired cb) noexcept {
	assert(!pImpl->onSaveRequired);
	assert(cb);
	pImpl->onSaveRequired = cb;
}

CellProcess::ActorId CellProcess::AddActor(int entityId, std::unique_ptr<IActor> ac) noexcept {
	assert(entityId >= 0);
	assert(entityId < (int)pImpl->share.perEntityData.size());
	if (entityId < 0 || (int)pImpl->share.perEntityData.size() <= entityId) {
		return g_invalidActorId;
	}

	auto propList = pImpl->GetPropList(entityId);
	assert(propList);
	if (!propList) return g_invalidActorId;

	uint32_t id = ~0;
	const auto props = ac->GetProps();
	{
		std::lock_guard l(pImpl->share.m);
		auto &perEntityData = pImpl->share.perEntityData[entityId];
		if (!perEntityData.actorIdGen->CreateID(id)) {
			assert(0);
			const InstanceId instanceId(entityId, CellProcess::g_invalidActorId);
			perEntityData.waste.push_back({ std::move(ac), instanceId });
			return g_invalidActorId;
		}
		perEntityData.actors[id] = { std::move(ac) };
	}

	const InstanceId instanceId(entityId, id);
	if (auto v = props.GetAffectsGrid(propList)) {
		pImpl->MoveOnGrid(instanceId, v->first, v->second);
	}
	this->Edit(instanceId, props, true);
	return (ActorId)id;
}

void CellProcess::DisableActor(const InstanceId &inst) noexcept {
	assert(inst.entityId >= 0);
	assert(inst.entityId < (int)pImpl->share.perEntityData.size());
	if (inst.entityId < 0 || (int)pImpl->share.perEntityData.size() <= inst.entityId) return;

	std::lock_guard l(pImpl->share2.m);
	pImpl->share2.disabledActors.push_back(inst);
}

std::optional<ICellProcess::ActorWaste> CellProcess::GetNextActorWaste(int entityId) noexcept {
	std::optional<ActorWaste> res;

	assert(entityId >= 0);
	assert(entityId < (int)pImpl->share.perEntityData.size());
	if (entityId < 0 || (int)pImpl->share.perEntityData.size() <= entityId) return res;
	
	std::lock_guard l(pImpl->share.m);

	auto &perEntity = pImpl->share.perEntityData[entityId];
	if (!perEntity.waste.empty()) {
		res = std::move(perEntity.waste.front());
		perEntity.waste.pop_front();

		perEntity.actorIdToRecycle.push_back({ clock(), res->inst });
	}

	return res;
}

// +
ICellProcess::EditRes CellProcess::Edit(const InstanceId &inst, const EntityProps &entityProps, bool sendChangesToMe) noexcept {
	assert(entityProps.IsEmpty() == false);

	assert(inst.entityId >= 0);
	assert(inst.entityId < (int)pImpl->share.perEntityData.size());
	if (inst.entityId < 0 || (int)pImpl->share.perEntityData.size() <= inst.entityId) return {};

	auto propList = pImpl->GetPropList(inst.entityId);
	assert(propList);
	if (!propList) return {};

	if (auto v = entityProps.GetAffectsGrid(propList)) {

		// Is it still required here?
		pImpl->MoveOnGrid(inst, v->first, v->second); // Locks share3

		{
			std::lock_guard l(pImpl->share2.m);
			pImpl->share2.invokedGridPos.push_back({ inst, *v });
		}
	}

	pImpl->tmpVisMy.clear();
	pImpl->tmpVisNei.clear();

	EditRes res;

	bool isCachingRequired = true;
	{
		std::unique_lock l(pImpl->share.m);
		auto &perEntity = pImpl->share.perEntityData[inst.entityId];
		const int sz = (int)perEntity.actors.size();
		assert(sz > inst.actorId);
		if (sz <= inst.actorId) return {};
		auto &acData = perEntity.actors[inst.actorId];
		auto &ac = acData.ac;

		// Save if need
		auto changesToSave = entityProps;
		changesToSave.Filter(Viet::PropertyFlags::SavedToDb, propList);

		bool forceSave = false;

		auto propsWas = acData.ac->GetProps();
		changesToSave.ForEach([&] (PropId propId, const std::unique_ptr<IPropertyValue> &value) {
			auto prop = propList->GetById(propId);
			assert(prop);
			if (!prop) return;

			auto &lastSaved = propsWas.GetPropertyValue(propId);
			const bool diff = prop->NeedsToBeSaved(lastSaved ? lastSaved->Clone() : prop->GetDefaultValue(), value);
			if (diff) {
				forceSave = true;
			}
		});

		// Edit actor
		ac->ApplyPropsChanges(entityProps);

		// Send changes to users:

		EntityProps toNei, toMe;
		entityProps.ForEach([&] (PropId propId, const std::unique_ptr<IPropertyValue> &value) {
			auto prop = propList->GetById(propId);
			assert(prop);
			if (!prop) return;
			auto flags = prop->GetFlags();
			if (flags & Viet::PropertyFlags::VisibleToNeighbour) {
				toNei += { propId, value->Clone() };
			}
			toMe += { propId, value->Clone() };
		});
		pImpl->tmpVisNei.push_back(vision::Event<InstanceId>({ inst, vision::ActorPropertyChange{ inst.entityId, toNei } }));
		pImpl->tmpVisMy.push_back(vision::Event<InstanceId>({ inst, vision::MyPropertyChange{ inst.entityId, toMe } }));

		isCachingRequired = ac->GetProps().IsCachingRequired(propList);

		if (forceSave || !isCachingRequired) {
			auto p = pImpl->_OnSaveRequired(inst, *ac);
			if (!isCachingRequired) {
				res.promise = Viet::Promise<Viet::Void>();
				p.Then(*res.promise);
			}
		}
	}
	if (isCachingRequired) {
		for (auto &v : pImpl->tmpVisNei) {
			pImpl->SendVisionEventToNeighbours(v);
		}
		if (sendChangesToMe) {
			for (auto &v : pImpl->tmpVisMy) {
				pImpl->onVisionEvent(std::unique_ptr<VisionEventData>(new VisionEventData(inst, v)));
			}
		}
	}

	if (!isCachingRequired) {
		this->DisableActor(inst);
	}

	return res;
}

std::optional<EntityProps> CellProcess::GetEntityProps(const InstanceId &inst) const noexcept {
	// No assets here because it's expected to have an invalid instance as argument here

	if (inst.entityId < 0 || (int)pImpl->share.perEntityData.size() <= inst.entityId) return std::nullopt;

	std::lock_guard l(pImpl->share.m);
	auto &perEntity = pImpl->share.perEntityData[inst.entityId];
	const int sz = (int)perEntity.actors.size();
	if (sz <= inst.actorId) return std::nullopt;

	auto &ac = perEntity.actors[inst.actorId].ac;
	if (!ac) return std::nullopt;
	return ac->GetProps();
}

bool CellProcess::HasActor(const InstanceId &inst) const noexcept {
	return this->GetEntityProps(inst) != std::nullopt;
}

namespace {
	bool PropsMatchesCondition(const EntityProps &props, Properties *propList, const Viet::FindCondition &cond, int nestedLevel = 0) noexcept {
		constexpr int maxNestedLevel = 32;
		
		assert(propList);
		if (!propList) return false;

		for (auto &entry : cond.entries) {
			if (entry.comp) {
				auto p = ChildPath(entry.comp->path);
				assert(p.IsEmpty() == false);
				if (p.IsEmpty()) return false;

				auto propName = p.GetTokens()[0].data();
				auto &propValue = props.GetPropertyValue(propName, propList);
				auto prop = propList->GetByName(propName);

				assert(prop);
				if (!prop) return false;

				auto j = json::object();
				if (props.HasProperty(propName, propList)) {
					j[propName] = prop->ToApiFormat(propValue);
				}
				else {
					j[propName] = prop->ToApiFormat(prop->GetDefaultValue());
				}
				
				json childValue = ChildPath::GetObjectChild(j, p);
			

				json v = entry.comp->value.v;
				if (v != childValue) {
					return false;
				}
			}
			else if (entry._and) {
				for (auto &subCond : entry._and->childs) {
					if (!PropsMatchesCondition(props, propList, subCond, 1 + nestedLevel)) return false;
				}
			}
			else if (entry._or) {
				bool matches = false;
				for (auto &subCond : entry._or->childs) {
					matches = PropsMatchesCondition(props, propList, subCond);
					if (matches) break;
				}
				if (!matches) return false;
			}
		}
		return true;
	}
}

std::vector<ICellProcess::ActorId> CellProcess::FindMatches(int entityId, const Viet::FindCondition &cond, uint32_t limit) const noexcept {
	std::vector<ICellProcess::ActorId> res;

	std::lock_guard l(pImpl->share.m);

	assert(entityId >= 0);
	assert(entityId < (int)pImpl->share.perEntityData.size());
	if (entityId < 0 || (int)pImpl->share.perEntityData.size() <= entityId) return {};

	auto propList = EntityAccess::GetPropList_(pImpl->entities[entityId]);
	assert(propList);
	if (!propList) return {};

	auto &perEntity = pImpl->share.perEntityData[entityId];
	for (size_t id = 0; id < perEntity.actors.size(); ++id) {
		auto &acData = perEntity.actors[id];
		if (!acData.ac) continue;
		if (PropsMatchesCondition(acData.ac->GetProps(), propList, cond)) {
			res.push_back(id);
			if (limit > 0 && res.size() >= limit) {
				break;
			}
		}
	}

	assert(limit <= 0 || res.size() <= limit);
	return res;
}

void CellProcess::StreamOutAll(const InstanceId &inst) noexcept {
	assert(inst.entityId >= 0);
	assert(inst.entityId < (int)pImpl->share.perEntityData.size());
	if (inst.entityId < 0 || (int)pImpl->share.perEntityData.size() <= inst.entityId) return;

	std::lock_guard l(pImpl->share3.m);

	// Copying is required
	const auto streamedInActors = pImpl->share3.streamedInActors.Get(inst);
	for (auto sourceActorId : streamedInActors) {
		pImpl->share3.StreamOut(inst, sourceActorId, nullptr);
		pImpl->share3.StreamOut(sourceActorId, inst, nullptr);
	}
}

void CellProcess::Tick() noexcept {
	InvokedGridPos invokedGridPos;
	std::vector<InstanceId> disabledAcs;

	{
		std::lock_guard l(pImpl->share2.m);

		invokedGridPos = std::move(pImpl->share2.invokedGridPos);
		pImpl->share2.invokedGridPos.clear();

		disabledAcs = std::move(pImpl->share2.disabledActors);
		pImpl->share2.disabledActors.clear();
	}


	//
	// Recycle Actor Ids
	//
	{
		std::lock_guard l(pImpl->share.m);
		for (int entityId = 0; entityId < (int)pImpl->share.perEntityData.size(); ++entityId) {
			auto &perEntity = pImpl->share.perEntityData[entityId];
			auto ids = std::move(perEntity.actorIdToRecycle);
			perEntity.actorIdToRecycle.clear();
			for (auto[wastedMoment, inst] : ids) {
				if (clock() - wastedMoment > CLOCKS_PER_SEC * 10) {
					// Release Ids after the delay
					assert(inst.entityId == entityId);
					bool success = perEntity.actorIdGen->DestroyID(inst.actorId);
					assert(success);
				}
				else {
					// Maybe next time, sorry
					perEntity.actorIdToRecycle.push_back({ wastedMoment, inst });
				}
			}
		}
	}

	//
	// Invoked Grid posotions
	//
	for (auto &[actorId, pair] : invokedGridPos) {
		pImpl->ProcessInvokedGridPos(actorId, pair.first, pair.second);
	}

	//
	// Disable actors
	//
	{
		std::lock(pImpl->share.m, pImpl->share3.m);
		std::lock_guard<std::mutex> lock_from(pImpl->share.m, std::adopt_lock);
		std::lock_guard<std::mutex> lock_to(pImpl->share3.m, std::adopt_lock);

		std::vector<InstanceId> wasted;
		{
			for (const auto &inst : disabledAcs) {
				auto &perEntity = pImpl->share.perEntityData[inst.entityId];
				auto &acData = perEntity.actors[inst.actorId];
				auto ac = std::move(acData.ac);
				acData = ActorData();

				perEntity.waste.push_back({ std::move(ac), inst });
				wasted.push_back(inst);
			}
		}

		for (auto inst : wasted) {
			auto acsIAmStreamedTo = std::move(pImpl->share3.acsIAmStreamedTo.Get(inst));
			pImpl->share3.acsIAmStreamedTo.Get(inst).clear();
			for (auto receiver : acsIAmStreamedTo) {
				// Stream out
				pImpl->share3.StreamOut(receiver, inst, pImpl->onVisionEvent);
			}
			pImpl->share3.grid.Forget(inst.ToUInt64());
		}
	}
}