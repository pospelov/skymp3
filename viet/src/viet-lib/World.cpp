#include "World.h"

#include <map>
#include <set>
#include <EntityAccess.h>
#include <ChildPath.h>
#include <boost/bimap/bimap.hpp>

namespace {
	struct CellProcData {
		CellProcData(size_t numEntities) {
			this->perEntity.resize(numEntities);
		}

		CellProcessId cpId;
		std::shared_ptr<ICellProcess> cp;

		struct EntityInfoData {
			EntityInfo ei;
			bool exited = false;
		};

		struct PerEntityData {
			std::set<ICellProcess::ActorId> actors;
			std::vector<EntityInfoData> eiByActorId;
		};

		std::vector<PerEntityData> perEntity;
	};

	std::string GetCollectionName(const Viet::Entity &entity) noexcept {
		return (std::string)"viet_entity_" + entity.GetName();
	}

	void JsonToActor(json &j, IActor &outAc, EntityInfo &outEi, Properties *propList) {
		assert(j.count("props"));
		auto &jProps = j["props"];
		assert(jProps.is_object());
		if (jProps.is_object()) {
			EntityProps props;
			for (auto it = jProps.begin(); it != jProps.end(); ++it) {
				auto prop = propList->GetByName(it.key().data());
				assert(prop);
				if (!prop) continue;

				// TODO: handle exceptions thrown by FromDbFormat
				const PropId propId = propList->GetPropertyId(it.key().data());
				props += { propId, prop->FromDbFormat(it.value()) };
			}
			outAc.SetProps(props);
		}

		assert(j.count("ei"));
		if (j.count("ei") && j.at("ei").is_string()) {
			auto jEi = j.at("ei");
			outEi = EntityInfo::FromString(jEi.get<std::string>());
		}
	}

	json ActorToJsonUpdate(const IActor &ac, IDBOperators &dbOps, Properties *propList) {
		json res = json::object();
		json jProps = json::object();
		auto props = ac.GetProps();
		props.Filter(Viet::PropertyFlags::SavedToDb, propList);

		props.ForEach([&](PropId propId, const std::unique_ptr<IPropertyValue> &value) {
			auto prop = propList->GetById(propId);
			assert(prop);
			if (!prop)  return;
			std::string key;
			key += "props."; 
			key += propList->GetPropertyName(propId);
			jProps[key] = prop->ToDbFormat(value);
		});
		res[dbOps.Set()] = jProps;
		return res;
	}

	json GetEntityInfoFilter(const EntityInfo &ei, IDBOperators &dbOps) {
		json res = json::object();
		res["ei"] = json{
			{ dbOps.Eq(), ei.ToString() }
		};
		return res;
	}

	const char *GetOp(IDBOperators &ops, Viet::FindCondition::ComparisonOperator op) {
		switch (op) {
		case Viet::FindCondition::ComparisonOperator::Equal:
			return ops.Eq();
		case Viet::FindCondition::ComparisonOperator::NotEqual:
			return ops.Ne();
		case Viet::FindCondition::ComparisonOperator::Greater:
			return ops.Gt();
		case Viet::FindCondition::ComparisonOperator::GreaterOrEqual:
			return ops.Gte();
		case Viet::FindCondition::ComparisonOperator::Less:
			return ops.Lt();
		case Viet::FindCondition::ComparisonOperator::LessOrEqual:
			return ops.Lte();
		default:
			assert(0);
			return ops.Eq();
		}
	}

	bool Matches(const json &jDefault, const json &jRequired, Viet::FindCondition::ComparisonOperator op) {
		using Cmp = Viet::FindCondition::ComparisonOperator;
		switch (op) {
		case Cmp::Equal:
			return jDefault == jRequired;
		case Cmp::NotEqual:
			return jDefault != jRequired;
		case Cmp::Greater:
			if (jDefault.is_number() && jRequired.is_number())
				return jDefault > jRequired;
			return false;
		case Cmp::GreaterOrEqual:
			if (jDefault.is_number() && jRequired.is_number())
				return jDefault >= jRequired;
			return false;
		case Cmp::Less:
			if (jDefault.is_number() && jRequired.is_number())
				return jDefault < jRequired;
			return false;
		case Cmp::LessOrEqual:
			if (jDefault.is_number() && jRequired.is_number())
				return jDefault <= jRequired;
			return false;
		default:
			assert(0);
			return false;
		}
	}

	json FindConditionToJson(const Viet::FindCondition &cond, IDBOperators &ops, Properties *propList) {
		json res = json::object();

		json jChilds = json::array();
		for (auto &child : cond.entries) {
			if (child.comp) {
				std::string key = "props." + child.comp->path;
				const bool badKey = key.find_first_not_of("qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890_.") != std::string::npos;
				assert(!badKey);
				if (badKey) return json::object();
				
				json jC = json::object();
				jC[key] = json::object();
				jC[key][GetOp(ops, child.comp->op)] = child.comp->value.v;

				std::unique_ptr<json> jChildDefault;
				auto p = ChildPath(child.comp->path);
				if (p.GetTokens().size()) {
					auto propName = p.GetTokens()[0].data();
					auto prop = propList->GetByName(propName);
					assert(prop);
					if (prop) {
						auto jDefault = prop->ToDbFormat(prop->GetDefaultValue());
						auto j = ChildPath::GetObjectChild(json{ {propName, jDefault} }, p);
						jChildDefault.reset(new json(j));
					}
				}

				if (!jChildDefault || !Matches(*jChildDefault, child.comp->value.v, child.comp->op)) {
					jChilds.push_back(jC);
				}
				else {
					json jOr = json::array();
					jOr.push_back(jC);

					json jNotExist = json::object();
					jNotExist[key] = json{ {ops.Exist(), false} };
					jOr.push_back(jNotExist);

					jChilds.push_back(json{ { ops.Or(), jOr } });
				}
			}
			else if (child._and) {
				json arr = json::array();
				for (auto &simple : child._and->childs) {
					arr.push_back(FindConditionToJson(simple, ops, propList));
				}
				json jC = json::object();
				jC[ops.And()] = arr;
				jChilds.push_back(jC);
			}
			else if (child._or) {
				json arr = json::array();
				for (auto &simple : child._or->childs) {
					arr.push_back(FindConditionToJson(simple, ops, propList));
				}
				json jC = json::object();
				jC[ops.Or()] = arr;
				jChilds.push_back(jC);
			}
		}

		res[ops.And()] = jChilds;
		return res;
	}
}

class Index {
public:
	using Pair = std::pair<CellProcData *, ICellProcess::InstanceId>;

	void Set(const EntityInfo &key, const Pair &value) {
		data.insert({ key, value });
	}

	void Erase(const Pair &value) {
		auto it = data.right.find(value);
		if (it != data.right.end()) {
			data.right.erase(it);
			return;
		}
		assert(0);
	}

	const Pair *Find(const EntityInfo &key) {
		auto it = data.left.find(key);
		if (it == data.left.end()) return nullptr;
		return &it->second;
	}

private:
	boost::bimaps::bimap<EntityInfo, Pair> data;
};

struct World::Impl {
	IDBOperators &dbOps;
	const CellProcFactory fac;
	const ActorFactory acFac;
	const std::shared_ptr<IAsyncDatabase> db;
	const std::shared_ptr<Viet::Logging::Logger> logger;
	const std::shared_ptr<ITimerManager> tm;
	const OnVisionEvent onVisionEvent;
	const std::vector<Viet::Entity> entities;

	std::map<CellProcessId, CellProcData> cellProc;
	Index index;

	struct PerEntityData {
		std::map<EntityInfo, json> filterAndUpdate;
		std::vector<Viet::Promise<Viet::Void>> promises;
		bool saveInProgress = false;
	};
	std::vector<PerEntityData> perEntity;

	EntityInfo GetEntityInfo(const CellProcessId &cpId, const ICellProcess::InstanceId &instanceId, bool *outExited = nullptr) noexcept;

	// These two functions must be threadsafe
	void ProcessVisionEvent(std::unique_ptr<VisionEventData> visionEventData) noexcept;
	Viet::Promise<Viet::Void> ProcessSaveRequired(const CellProcessId &cpId, const ICellProcess::InstanceId &instanceId, IActor &ac) noexcept;

	void CreateCellProcess(const CellProcessId &cpId) noexcept;

	// Loads cell
	// Must load enabled, but non-loaded Actors if cell is already loaded
	// Creates CellProcess
	void LoadCell(const CellProcessId &cpId, std::function<void()> cb, IDBOperators &dbOps) noexcept;

	void SaveActorDb(const EntityInfo &ei, ActorDbAddress &acAddr, IDBOperators &dbOps, std::function<void()> cb) noexcept;

	Viet::Promise<Viet::Void> SetActorCached(int entityId, const EntityInfo &ei, IDBOperators &dbOps) noexcept;
};

World::World(CellProcFactory fac, ActorFactory acFac, std::shared_ptr<IAsyncDatabase> db, IDBOperators &dbOps_, std::shared_ptr<Viet::Logging::Logger> logger, std::shared_ptr<ITimerManager> tm, OnVisionEvent onVisionEvn, std::vector<Viet::Entity> entities) : dbOps(dbOps_), pImpl(new Impl{ dbOps_, fac, acFac, db, logger, tm, onVisionEvn, entities }) {
	assert(db);
	assert(acFac);
	assert(fac);
	assert(logger);
	assert(tm);
	assert(onVisionEvn);
	assert(entities.size() > 0);
	pImpl->perEntity.resize(entities.size());
}

World::~World() {
}

void World::Tick() noexcept {
	// Handle save operations
	for (int entityId = 0; entityId < (int)pImpl->entities.size(); ++entityId) {
		auto &perEntity = pImpl->perEntity[entityId];
		if (!perEntity.filterAndUpdate.empty() && !perEntity.saveInProgress) {
			const int n = (int)perEntity.filterAndUpdate.size();
			perEntity.saveInProgress = true;

			std::vector<std::pair<json, json>> filterAndUpd;
			for (auto &[ei, jUpd] : perEntity.filterAndUpdate) {
				filterAndUpd.push_back({ GetEntityInfoFilter(std::move(ei), this->dbOps), std::move(jUpd) });
			}
			perEntity.filterAndUpdate.clear();

			auto promises = std::move(perEntity.promises);
			perEntity.promises.clear();

			auto collName = GetCollectionName(pImpl->entities[entityId]);
			pImpl->db->UpdateMany(collName, std::move(filterAndUpd), false, [this, n, entityId, collName, promises] {
				pImpl->perEntity[entityId].saveInProgress = false;
				pImpl->logger->Info("Saved %d cached instances of %s", n, collName.data());
				for (auto &promise : promises) {
					promise.Resolve(Viet::Void());
				}
			});
		}
	}

	// Handle actor waste
	for (auto &[cellOrWorld, data] : pImpl->cellProc) {
		assert(data.cp);
		if (data.cp == nullptr) continue;
		for (int entityId = 0; entityId < (int)pImpl->entities.size(); ++entityId) {
			auto &perEntity = data.perEntity[entityId];
			while (auto waste = data.cp->GetNextActorWaste(entityId)) {
				perEntity.actors.erase(waste->inst.actorId);

				pImpl->index.Erase({ &data, waste->inst });

				assert(waste->inst.actorId >= 0);
				if (waste->inst.actorId < 0) continue;

				assert((int)perEntity.eiByActorId.size() > waste->inst.actorId);
				if ((int)perEntity.eiByActorId.size() <= waste->inst.actorId) continue;

				auto &[ei, exited] = perEntity.eiByActorId[waste->inst.actorId];
				assert(!exited);
				///pImpl->Save(entityId, ei, *waste->ac);
				// Will be saved by CellProcess
				exited = true;
			}
		}
	}
}

ICellProcess *World::GetCellProcLocally(const CellProcessId &cpId) noexcept {
	auto it = pImpl->cellProc.find(cpId);
	if (it != pImpl->cellProc.end()) {
		return &*(it->second.cp);
	}
	return nullptr;
}

void World::GetCellProc(const CellProcessId &cpId, GetCellProcCb cb) noexcept {
	auto it = pImpl->cellProc.find(cpId);
	if (it != pImpl->cellProc.end()) {
		return cb(it->second.cp);
	}
	auto pImpl_ = pImpl;
	pImpl->LoadCell(cpId, [cb, pImpl_, cpId] {
		cb(pImpl_->cellProc.at(cpId).cp);
	}, this->dbOps);
}

EntityInfo World::Impl::GetEntityInfo(const CellProcessId &cpId, const ICellProcess::InstanceId &acId, bool *outExited) noexcept {
	if (outExited) *outExited = false;

	auto it = this->cellProc.find(cpId);
	assert(it != this->cellProc.end());
	if (it == this->cellProc.end()) return {};

	if (acId.entityId < 0 || acId.entityId >= (int)it->second.perEntity.size()) {
		assert(0);
		return {};
	}
	const auto &eiById = it->second.perEntity[acId.entityId].eiByActorId;

	assert((int)eiById.size() > acId.actorId);
	if ((int)eiById.size() <= acId.actorId) return {};

	auto v = eiById[acId.actorId];
	if (v.exited) {
		if (outExited) *outExited = true;
	}
	return v.ei;
}

void World::Impl::ProcessVisionEvent(std::unique_ptr<VisionEventData> visionEventData) noexcept {
	assert(visionEventData);
	assert(visionEventData->reservedCpId);
	if (!visionEventData || !visionEventData->reservedCpId) return;

	this->tm->SetTimeout(0, [this, visionEventData_ = visionEventData.release()] {
		std::unique_ptr<VisionEventData> visionEventData(visionEventData_);
		// Convert ICellProcess::AnyId to EntityInfo
		std::visit([&](const auto &concrete) {
			const auto &cpId = *(CellProcessId *)visionEventData->reservedCpId;
			auto it = this->cellProc.find(cpId);
			assert(it != this->cellProc.end());
			if (it == this->cellProc.end()) return;

			const ICellProcess::InstanceId &viewer = visionEventData->viewer,
				&owner = visionEventData->v.owner;

			if (viewer.entityId < 0 || viewer.entityId >= (int)it->second.perEntity.size()) {
				assert(0);
				return;
			}
			if (viewer.actorId < 0 || (int)it->second.perEntity[viewer.entityId].eiByActorId.size() <= viewer.actorId) {
				assert(0);
				return;
			}
			const EntityInfo &viewerEi = it->second.perEntity[viewer.entityId].eiByActorId[viewer.actorId].ei;

			if (owner.entityId < 0 || owner.entityId >= (int)it->second.perEntity.size()) {
				assert(0);
				return;
			}
			if (owner.actorId < 0 || (int)it->second.perEntity[owner.entityId].eiByActorId.size() <= owner.actorId) {
				assert(0);
				return;
			}
			const EntityInfo &seenEi = it->second.perEntity[owner.entityId].eiByActorId[owner.actorId].ei;

			assert(viewerEi.IsEmpty() == false);
			assert(seenEi.IsEmpty() == false);

			vision::Event<EntityInfo> resultEvn{ seenEi, visionEventData->v.evn };
			this->onVisionEvent(viewerEi, resultEvn);
		}, visionEventData->v.evn);
	});
}

Viet::Promise<Viet::Void> World::Impl::ProcessSaveRequired(const CellProcessId &cpId, const ICellProcess::InstanceId &inst, IActor &ac) noexcept {
	Viet::Promise<Viet::Void> promise;

	if (inst.entityId < 0 || inst.entityId >= (int)this->entities.size()) {
		assert(0);
		return promise; // TODO: reject?
	}

	auto propList = EntityAccess::GetPropList_(this->entities[inst.entityId]);
	assert(propList);
	if (!propList) return promise; // TODO: reject?

	auto jUpdate = ActorToJsonUpdate(ac, this->dbOps, propList);
	this->tm->SetTimeout(0, [this, cpId, inst, jUpdate, promise] {
		bool isExited = false;
		auto ei = this->GetEntityInfo(cpId, inst, &isExited);
		if (ei.IsEmpty()) {
			return promise.Reject("ProcessSaveRequired failed");
		}

		auto &perEntity = this->perEntity[inst.entityId];
		perEntity.filterAndUpdate[ei] = std::move(jUpdate);
		perEntity.promises.push_back(promise);
	});

	return promise;
}

void World::Impl::CreateCellProcess(const CellProcessId &cpId) noexcept {
	if (this->cellProc.count(cpId) > 0) return;

	const auto num = this->entities.size();
	assert(num > 0);
	auto &cellProc = this->cellProc.insert({ cpId, CellProcData(num) }).first->second;
	cellProc.cpId = cpId;

	const auto cpIdPtr = &cellProc.cpId;

	cellProc.cp = this->fac();
	cellProc.cp->SetOnVisionEvent([this, cpIdPtr](std::unique_ptr<VisionEventData> visionEventData) {
		visionEventData->reservedCpId = cpIdPtr;
		this->ProcessVisionEvent(std::move(visionEventData));
	});
	cellProc.cp->SetOnSaveRequired([this, cpId](const ICellProcess::InstanceId &inst, IActor &ac) {
		return this->ProcessSaveRequired(cpId, inst, ac);
	});
}

void PropsToJsonFilter(const EntityProps &props, json &jFilter, IDBOperators &dbOps, Properties *propList) {
	props.ForEach([&](PropId propId, const std::unique_ptr<IPropertyValue> &value) {
		auto prop = propList->GetById(propId);
		assert(prop);
		if (!prop) return;
		const std::string name = prop->GetName();
		jFilter["props." + name] = json{
			{ dbOps.Eq(), prop->ToDbFormat(value) }
		};
	});
}

void World::Impl::LoadCell(const CellProcessId &cpId, std::function<void()> cb, IDBOperators &dbOps) noexcept {
	this->CreateCellProcess(cpId);

	auto cpIdStr = cpId.ToHumanReadableString();
	this->logger->Notice("Loading cell %s", cpIdStr.data());

	json jFilter = json::object();

	for (int entityId = 0; entityId < (int)this->entities.size(); ++entityId) {

		// Find only entities with 
		// (1) properties equal to properties used to generate this cpId
		// (2) one or more properties forcing the entity to be in a cache ("enabled")
		auto propList = EntityAccess::GetPropList_(this->entities[entityId]);
		PropsToJsonFilter(cpId.GetPropsUsed(), jFilter, dbOps, propList);
		propList->ForEach([&](const char *name, Properties::IPropertyAny *prop) {
			if (auto v = prop->GetValueForcesEntityCaching()) {
				auto jObj = json::object();
				jObj["props." + (std::string)name] = prop->ToDbFormat(*v);
				jFilter[dbOps.Or()].push_back(jObj);
			}
		});

		// TODO: use pImpl instead of 'this'?
		auto collName = GetCollectionName(this->entities[entityId]);
		this->db->Find(collName, jFilter, [this, cpId, cb, propList, entityId](std::vector<json> result) {
			for (auto &j : result) {
				auto ac = this->acFac();

				EntityInfo ei;
				JsonToActor(j, *ac, ei, &*propList);
				assert(ei.IsEmpty() == false);

				if (this->index.Find(ei)) {
					std::string eiStr = ei.ToString();
					auto cpIdStr = cpId.ToHumanReadableString();
					this->logger->Info("%s in cell %s is already loaded, skipping", eiStr.data(), cpIdStr.data());
					continue;
				}

				assert(this->cellProc.count(cpId));
				if (!this->cellProc.count(cpId)) break;
				auto &cellProc = this->cellProc.at(cpId);
				assert(cellProc.cp);
				if (!cellProc.cp) break;

				auto id = cellProc.cp->AddActor(entityId, std::move(ac));
				auto eiStr = ei.ToString();
				this->logger->Info("Added Actor %s to cellProc", eiStr.data());
				assert(id != ICellProcess::g_invalidActorId);
				if (id == ICellProcess::g_invalidActorId) continue;

				auto &perEntity = cellProc.perEntity[entityId];
				auto[it, inserted] = perEntity.actors.insert(id);
				assert(inserted);

				if (perEntity.eiByActorId.size() <= (size_t)id)
					perEntity.eiByActorId.resize(id + 1);
				perEntity.eiByActorId[id] = {};
				perEntity.eiByActorId[id].ei = ei;

				assert(this->index.Find(ei) == nullptr);
				this->index.Set(ei, { &cellProc, ICellProcess::InstanceId(entityId, id) });
			}
			cb();
		});
	}
}

void World::Impl::SaveActorDb(const EntityInfo &ei, World::ActorDbAddress &acAddr, IDBOperators &dbOps, std::function<void()> cb) noexcept {
	if (acAddr.entityId >= 0 && acAddr.entityId < (int)this->entities.size()) {
		auto propList = EntityAccess::GetPropList_(this->entities[acAddr.entityId]);
		auto jUpd = ActorToJsonUpdate(*acAddr.ac, dbOps, propList);
		auto jFilter = GetEntityInfoFilter(ei, dbOps);
		auto collName = GetCollectionName(this->entities[acAddr.entityId]);
		this->db->UpdateOne(collName, jFilter, jUpd, false, cb);
	}
	else {
		assert(0);
	}
}

Viet::Promise<Viet::Void> World::Impl::SetActorCached(int entityId, const EntityInfo &ei, IDBOperators &dbOps) noexcept {
	Viet::Promise<Viet::Void> promise;

	if (entityId < 0 || entityId >= (int)this->entities.size()) {
		assert(0);
		return promise; // TODO: reject?
	}
	auto propList = EntityAccess::GetPropList_(this->entities[entityId]);
	assert(propList);
	if (!propList) return promise; // TODO: reject?

	this->logger->Info("SetAtorCached %s", ei.hash.data());
	auto jFilter = GetEntityInfoFilter(ei, dbOps);
	auto pDbOps = &dbOps;

	auto collName = GetCollectionName(this->entities[entityId]);
	this->db->FindOne(collName, jFilter, [=] (json j){
		assert(!j.is_null());
		if (j.is_null()) return Viet::Promise<Viet::Void>(promise).Reject("Not found");

		EntityInfo ei_;
		auto ac = this->acFac();
		JsonToActor(j, *ac, ei_, propList);
		assert(ei_.ToString() == ei.ToString());

		this->LoadCell(CellProcessId(ac->GetProps(), propList), [promise] {
			Viet::Promise<Viet::Void>(promise).Resolve(Viet::Void());
		}, *pDbOps);
	});

	return promise;
}

void World::FindActor(int entityId, const EntityInfo &ei, FindActorCb cb) noexcept {
	if (entityId < 0 || entityId >= (int)pImpl->entities.size()) {
		assert(0);
		return;
	}

	if (auto local = this->FindActorLocally(ei)) {
		if (local->inst.entityId != entityId) {
			assert(0);
			return;
		}
		return cb(*local);
	}

	json filter = json::object();
	filter["ei"] = json{
		{ this->dbOps.Eq(), ei.ToString() }
	};

	auto pImpl_ = pImpl;
	auto *dbOps = &this->dbOps;
	auto collName = GetCollectionName(pImpl->entities[entityId]);
	pImpl->db->FindOne(collName, filter, [ei, cb, pImpl_, dbOps, entityId](json j) {
		if (j.is_null()) return cb(std::nullopt);

		assert(j.count("ei"));
		assert(j.at("ei").is_string());
		assert(j["ei"].get<std::string>() == ei.ToString());

		auto propList = EntityAccess::GetPropList_(pImpl_->entities[entityId]);
		assert(propList);
		if (!propList) return;

		auto ac = pImpl_->acFac();
		EntityInfo _;
		JsonToActor(j, *ac, _, propList);

		const bool isCachingRequired = ac->GetProps().IsCachingRequired(propList);
		const CellProcessId cpId(ac->GetProps(), propList);

		// Force cell loading
		if (isCachingRequired) {
			pImpl_->LoadCell(cpId, [=] {
				auto it = pImpl_->index.Find(ei);
				assert(it != nullptr);
				if (!it) return cb(std::nullopt);
				auto &[cellProcessData, inst] = *it;
				return cb(ActorAddress{ cpId, *cellProcessData->cp, inst });
			}, *dbOps);

		}
		else {
			EntityInfo ei_;
			std::unique_ptr<IActor> ac = pImpl_->acFac();
			JsonToActor(j, *ac, ei_, propList);
			ActorDbAddress acAddr{ cpId, std::move(ac), entityId, ei_ };
			assert(ei_.ToString() == ei.ToString());

			auto saveFn = [ei_, pImpl_, dbOps](ActorDbAddress &acAddr, std::function<void()> cb) {
				pImpl_->SaveActorDb(ei_, acAddr, *dbOps, cb);
			};

			auto setCachedFn = [=](const EntityInfo &ei) {
				return pImpl_->SetActorCached(entityId, ei, *dbOps);
			};

			ActorFindResult acFindRes(std::move(acAddr), saveFn, setCachedFn);

			cb(std::move(acFindRes));
		}
	});
}

std::optional<World::ActorAddress> World::FindActorLocally(const EntityInfo &ei) noexcept {
	auto it = pImpl->index.Find(ei);
	if (it != nullptr) { // Found locally (cell loaded)
		auto &[cellProcessData, actorId] = *it;

		auto res = ActorAddress{ cellProcessData->cpId, *cellProcessData->cp, actorId };
		return res;
	}
	return std::nullopt;
}

inline void WritePropsToJson(const EntityProps &props, Properties *propList, json &jDoc) {
	if (jDoc["props"].is_object() == false) {
		jDoc["props"] = json::object();
	}
	auto &jProps = jDoc["props"];
	props.ForEach([&](PropId propId, const std::unique_ptr<IPropertyValue> &value) {
		auto prop = propList->GetById(propId);
		assert(prop);
		if (!prop) return;
		jProps[prop->GetName()] = prop->ToDbFormat(value);
	});
}

void World::CreateActor(int entityId, const EntityInfo &ei, const EntityProps &props, CreateActorCb cb) noexcept {
	if (entityId < 0 || entityId >= (int)pImpl->entities.size()) {
		assert(0);
		return;
	}
	const auto propList = EntityAccess::GetPropList_(pImpl->entities[entityId]);
	const auto collName = GetCollectionName(pImpl->entities[entityId]);

	const CellProcessId cpId(props, propList);
	json jDoc = json::object();
	jDoc["ei"] = ei.ToString();
	WritePropsToJson(props, propList, jDoc);
	const bool cachingRequired = props.IsCachingRequired(propList);

	if (cachingRequired) {
		pImpl->db->InsertOne(collName, jDoc, [=] {
			// Find actor forces cell to load if not loaded
			this->FindActor(entityId, ei, [this, cb](std::optional<ActorFindResult> ac) {
				assert(ac.has_value());
				cb();
			});
		});
	}
	else {
		pImpl->db->InsertOne(collName, jDoc, cb);
	}
}

#include <fstream>

Viet::Promise<std::vector<EntityInfo>> World::ResolveDbActors(int entityId, const Viet::FindRequest &findReq) noexcept {
	Viet::Promise<std::vector<EntityInfo>> p;
	
	if (entityId < 0 || entityId >= (int)pImpl->entities.size()) {
		assert(0);
		return p; // TODO: reject?
	}
	const auto propList = EntityAccess::GetPropList_(pImpl->entities[entityId]);
	const auto collName = GetCollectionName(pImpl->entities[entityId]);
	const auto jFilter = FindConditionToJson(findReq.condition, this->dbOps, propList);

	if (findReq.limit == 1) {
		pImpl->db->FindOne(collName, jFilter, [=](json j) {
			if (j.is_null()) return p.Resolve({});
			assert(j.count("ei"));
			assert(j.at("ei").is_string());
			auto &jEi = j["ei"];
			if (jEi.is_string()) {
				EntityInfo ei = EntityInfo::FromString(jEi.get<std::string>());
				assert(ei.IsEmpty() == false);
				p.Resolve({ ei });
			}
		});
	}
	else {
		pImpl->db->Find(collName, jFilter, [=](std::vector<json> res) {
			std::vector<EntityInfo> vec;

			for (auto &j : res) {
				assert(j.count("ei"));
				assert(j.at("ei").is_string());
				auto &jEi = j["ei"];
				if (jEi.is_string()) {
					EntityInfo ei = EntityInfo::FromString(jEi.get<std::string>());
					assert(ei.IsEmpty() == false);
					vec.push_back(ei);
				}
			}

			if (findReq.limit > 0 && vec.size() > findReq.limit) {
				vec.resize(findReq.limit);
			}

			p.Resolve(vec);
		});
	}

	return p;
}

std::vector<EntityInfo> World::ResolveActorsLocally(int entityId, const Viet::FindRequest &findReq) noexcept {
	assert(entityId >= 0);
	if (entityId < 0) return {};

	std::vector<EntityInfo> res;
	for (auto &[cpId, cpData] : pImpl->cellProc) {
		auto matchedIds = cpData.cp->FindMatches(entityId, findReq.condition, findReq.limit);
		for (auto id : matchedIds) {

			if ((int)cpData.perEntity.size() <= entityId) {
				assert(0);
				continue;
			}

			auto &ei = cpData.perEntity[entityId].eiByActorId[id].ei;
			assert(ei.IsEmpty() == false);
			if (!ei.IsEmpty()) {
				res.push_back(ei);
			}
		}
	}
	return res;
}