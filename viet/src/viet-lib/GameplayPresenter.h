#include <IGameplayPresenter.h>
#include <IGameplayController.h>
#include <IMessageListener.h>
#include <INetController.h>
#include <viet/Logging.h>
#include <viet/Entity.h>

// IMessageListener methods must be used from only one thread.
// IGameplayPresenter and IGameplayController methods are threadsafe.
class GameplayPresenter : public IGameplayMessageListener, public IConnectionMessageListener, public IGameplayPresenter, public IGameplayController {
public:
	GameplayPresenter(std::shared_ptr<INetController> netCont, std::vector<Viet::Entity> entities) noexcept;
	~GameplayPresenter();

	// IConnectionMessageListener
	void OnConnect(ConnectResult res) noexcept override;
	void OnDisconnect() noexcept override;

	// IGameplayMessageListener
	void OnMessage(const msg::ActorId &) noexcept override;
	void OnMessage(const msg::CreatePlayer &) noexcept override;
	void OnMessage(const msg::DestroyPlayer &) noexcept override;
	void OnMessage(const msg::SimpleCommand &) noexcept override;
	void OnMessage(const msg::UpdateMyProperties &) noexcept override;
	void OnMessage(const msg::UpdateActorProperties &) noexcept override;

	// IGameplayPresenter
	void ProcessModel(const Callback &cb) noexcept override;

	// IGameplyController
	void SendPropertyChange(const char *propertyName, const IPropertyValue *propertyValue) noexcept override;

	void Tick();

private:
	struct Impl;
	Impl *const pImpl;
};