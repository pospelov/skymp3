#pragma once
#include <utility>
#include <string>
#include <functional>
#include <nlohmann/json.hpp>

using json = nlohmann::json;

class IAsyncDatabase {
public:
	using FindCallback = std::function<void(std::vector<json>)>;
	using FindOneCallback = std::function<void(json)>;
	using InsertOneCallback = std::function<void()>;
	using UpdateOneCallback = std::function<void()>;
	using UpdateManyCallback = std::function<void()>;
	using DropCallback = std::function<void()>;

	virtual void Find(const std::string &coll, json filter, const FindCallback &cb) noexcept = 0;
	virtual void FindOne(const std::string &coll, json filter, const FindOneCallback &cb) noexcept = 0;
	virtual void InsertOne(const std::string &coll, json doc, const InsertOneCallback &cb) noexcept = 0;
	virtual void UpdateOne(const std::string &coll, json filter, json upd, bool upsert, const UpdateOneCallback &cb) noexcept = 0;
	virtual void UpdateMany(const std::string &coll, std::vector<std::pair<json, json>> filterAndUpdate, bool upsert, const UpdateManyCallback &cb) noexcept = 0;
	virtual void Drop(const std::string &coll, const DropCallback &cb) noexcept = 0;
};