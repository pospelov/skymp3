#pragma once
#include <viet/Actions.h>
#include "EntityManipulation.h"

namespace Viet {
	class ActionsAccess {
	public:
		static EntityManipulation ActionsToManip(const Viet::Actions &a) {
			EntityManipulation res;
			for (auto &action : a.actions) {
				if (action.index() == 0) {
					auto &m = std::get<Viet::Actions::MCallMethod>(action);
					res.manipulations.push_back(EntityManipulation::EMCallMethod{ m.name, m.args });
				}
				else if (action.index() == 1) {
					auto &m = std::get<Viet::Actions::MGet>(action);
					res.manipulations.push_back(EntityManipulation::EMGetProp{ m.prop });
				}
				else if (action.index() == 2) {
					auto &m = std::get<Viet::Actions::MSet>(action);
					res.manipulations.push_back(EntityManipulation::EMSetProp{ m.prop, m.newValue });
				}
				else {
					assert(0 && "Unknown action type");
				}
			}
			return res;
		}
	};
}