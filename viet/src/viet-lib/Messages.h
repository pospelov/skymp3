#pragma once
#include <iostream> // std::cerr
#include <cstdint>
#include <optional>
#include <serialization.h>
#include <viet/Networking.h>
#include <ApiEvents.h>
#include <FrontEndFiles.h>

enum class UpdatePropertyOp : uint8_t {
	Update = 0,
	Assign
};

enum class MessageId : uint8_t {
	// From server to client
	Spawn = Viet::Networking::MinPacketId,
	ActorId,
	CreatePlayer,
	DestroyPlayer,
	SimpleCommand,
	GuiJson,

	// From client to server
	Handshake,
	GMHandshake, // From gamemode to server

	// From any to any
	UpdateActorProperties,
	UpdateMyProperties,
	GMEvent, // From gamemode to server or server to gamemode
	CustomPacket
};

namespace msg {
	// Meaning of Ids is defined by the server
	// Note: Invalid value is always -1
	// Note: Ids should be in ordinal order for performance reasons because the client will allocate vectors with (`maximum id` + 1) size
	constexpr int g_badId = -1;
	struct ServerAcId {
		ServerAcId() = default;
		ServerAcId(int idx_, uint64_t eiHash_) noexcept {
			this->idx = idx_;
			this->eiHash = eiHash_;
		}

		int idx = g_badId;
		uint64_t eiHash = 0;

		template <class Archive>
		void serialize(Archive &ar, unsigned int version) {
			ar & idx & eiHash;
		}
	};

	struct ActorId {
		static constexpr auto Id = MessageId::ActorId;

		int entityId = -1;
		msg::ServerAcId actorId;

		template <class Archive>
		void serialize(Archive &ar, unsigned int version) {
			ar & entityId & actorId;
		}
	};

	struct CreatePlayer {
		static constexpr auto Id = MessageId::CreatePlayer;

		int entityId = -1;
		ServerAcId instanceId;
		EntityProps props;

		template<class Archive>
		void serialize(Archive &ar, unsigned int version) {
			ar & entityId & instanceId & props;
		}
	};

	struct DestroyPlayer {
		static constexpr auto Id = MessageId::DestroyPlayer;

		uint64_t instanceEiHash;

		template <class Archive>
		void serialize(Archive &ar, unsigned int version) {
			ar & instanceEiHash;
		}
	};

	struct SimpleCommand {
		static constexpr auto Id = MessageId::SimpleCommand;

		enum class Opcode : uint8_t {
			Invalid,
		};

		Opcode opcode = Opcode::Invalid;

		template <class Archive>
		void serialize(Archive &ar, unsigned int version) {
			ar & opcode;
		}
	};

	struct GuiJson {
		static constexpr auto Id = MessageId::GuiJson;

		FrontEndFiles files;

		template <class Archive>
		void serialize(Archive &ar, unsigned int version) {
			ar & files;
		}
	};

	struct Handshake {
		static constexpr auto Id = MessageId::Handshake;

		std::string mySessionHash; // Should be generated on the client

		template <class Archive>
		void serialize(Archive &ar, unsigned int version) {
			ar & mySessionHash;
		}
	};

	struct GMHandshake {
		static constexpr auto Id = MessageId::GMHandshake;

		std::string serverId;
		std::string devPassword;
		FrontEndFiles frontEnd;

		template <class Archive>
		void serialize(Archive &ar, unsigned int version) {
			ar & serverId & devPassword & frontEnd;
		}
	};

	struct UpdateActorProperties {
		static constexpr auto Id = MessageId::UpdateActorProperties;
		int entityId = -1;
		ServerAcId instanceId;
		EntityProps props;

		template<class Archive>
		void serialize(Archive &ar, unsigned int version) const {
			ar & entityId & instanceId & props;
		}

	};

	struct UpdateMyProperties {
		static constexpr auto Id = MessageId::UpdateMyProperties;
		int entityId = -1;
		EntityProps props;
		UpdatePropertyOp op = UpdatePropertyOp::Update;

		template<class Archive>
		void serialize(Archive &ar, unsigned int version) const {
			ar & entityId & op & props;
		}
	};

	struct GMEvent {
		static constexpr auto Id = MessageId::GMEvent;

		evn::Any event;

		template<class Archive>
		void save(Archive &ar, unsigned int version) const {
			std::visit([&ar](const auto &concreteEvent) {
				ar & concreteEvent.Id & concreteEvent;
			}, event);
		}
		template<class Archive>
		void load(Archive &ar, unsigned int version) {
			EventId id = EventId::Invalid;
			ar & id;
			switch (id) {
			case EventId::CallResult: {
				evn::CallResult e;
				ar & e;
				event = e;
				break;
			}
			case EventId::ScriptInit: {
				evn::ScriptInit e;
				ar & e;
				event = e;
				break;
			}
			case EventId::UserConnect: {
				evn::UserConnect e;
				ar & e;
				event = e;
				break;
			}
			case EventId::UserDisconnect: {
				evn::UserDisconnect e;
				ar & e;
				event = e;
				break;
			}
			case EventId::UserCustomPacket: {
				evn::UserCustomPacket e;
				ar & e;
				event = e;
				break;
			}
			case EventId::RaceMenuExit: {
				evn::RaceMenuExit e;
				ar & e;
				event = e;
				break;
			}
			case EventId::GetMaxPlayers: {
				evn::GetMaxPlayers e;
				ar & e;
				event = e;
				break;
			}
			case EventId::ShowBrowserWindow: {
				evn::ShowBrowserWindow e;
				ar & e;
				event = e;
				break;
			}
			case EventId::HideBrowserWindow: {
				evn::HideBrowserWindow e;
				ar & e;
				event = e;
				break;
			}
			case EventId::SendCustomPacket: {
				evn::SendCustomPacket e;
				ar & e;
				event = e;
				break;
			}
			case EventId::FindEntity: {
				evn::FindEntity e;
				ar & e;
				event = e;
				break;
			}
			case EventId::ManipulateEntity: {
				evn::ManipulateEntity e;
				ar & e;
				event = e;
				break;
			}
			case EventId::SetActor: {
				evn::SetActor e;
				ar & e;
				event = e;
				break;
			}
			case EventId::GetActor: {
				evn::GetActor e;
				ar & e;
				event = e;
				break;
			}
			case EventId::RunTransaction: {
				evn::RunTransaction e;
				ar & e;
				event = e;
				break;
			}
			default: {
				std::cerr << "EventId was " << (int)id << std::endl;
				assert(0 && "Failed to load event");
				break;
			}
			}
		}
		SERIALIZATION_SPLIT_MEMBER();
	};

	struct CustomPacket {
		static constexpr auto Id = MessageId::CustomPacket;

		std::string data;
		std::string obsoleteSystemName;

		template <class Archive>
		void serialize(Archive &ar, unsigned int version) {
			ar & obsoleteSystemName & data;
		}
	};
}
