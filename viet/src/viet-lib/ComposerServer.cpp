#include <cassert>

#include "ComposerServer.h"

using namespace Viet::Networking;

namespace {
	struct RealUserId {
		int16_t realServerIdx = -1;
		UserId realUserId = g_invalidUserId;

		bool IsEmpty() {
			return realServerIdx < 0;
		}

		bool operator==(const RealUserId &rhs) noexcept {
			return realServerIdx == rhs.realServerIdx && realUserId == rhs.realUserId;
		}
	};
	static_assert(sizeof(RealUserId) == 4);

	class Mapping {
	public:
		UserId For(const RealUserId &real) noexcept {
			assert(real.realServerIdx >= 0);
			assert(real.realUserId != g_invalidUserId);
			if (real.realServerIdx < 0 || real.realUserId == g_invalidUserId) return g_invalidUserId;

			auto comp = this->Find(real);
			if (comp == g_invalidUserId) {
				comp = this->Allocate(real);
				assert(comp != g_invalidUserId);
				assert(this->Find(real) == comp);
			}
			assert(this->Find(comp) == real);
			return comp;
		}

		RealUserId Find(UserId comp) const noexcept {
			return realByComposerId.size() > comp ? realByComposerId[comp] : RealUserId();
		}

		void Free(const RealUserId &real) noexcept {
			if (auto comp = this->Find(real); comp != g_invalidUserId) {
				this->Free(comp);
			}
			assert(this->Find(real) == g_invalidUserId);
		}

	private:
		std::vector<RealUserId> realByComposerId;
		std::vector<std::vector<UserId>> composerIdByReal; // composerIdByReal[realServerIdx][realUserId]
		size_t availableCompId = 0;

		// Doesn't check uniqueness
		UserId Allocate(const RealUserId &real) noexcept {
			assert(real.realServerIdx >= 0);
			if (real.realServerIdx < 0) return g_invalidUserId;

			assert(availableCompId < 65536);
			auto composerId = (UserId)availableCompId;
			assert(realByComposerId.size() >= composerId);
			if (realByComposerId.size() == composerId) {
				realByComposerId.push_back(real);
			}
			else {
				assert(realByComposerId[composerId].IsEmpty());
				realByComposerId[composerId] = real;
			}
			while (1) {
				availableCompId++;
				assert(realByComposerId.size() >= availableCompId);
				if (realByComposerId.size() == availableCompId) {
					realByComposerId.push_back({});
				}
				if (realByComposerId[availableCompId].IsEmpty()) break;
			}
			assert(composerId != g_invalidUserId);

			if ((size_t)real.realServerIdx >= composerIdByReal.size()) {
				composerIdByReal.resize(real.realServerIdx + 1);
			}
			auto &byRealId = composerIdByReal[real.realServerIdx];
			if (real.realUserId >= byRealId.size()) {
				byRealId.resize(real.realUserId + 1, g_invalidUserId);
			}
			byRealId[real.realUserId] = composerId;

			return composerId;
		}

		void Free(UserId compId) noexcept {
			assert(compId != g_invalidUserId);
			assert(realByComposerId.size() > compId);
			if (realByComposerId.size() <= compId) return;
			
			auto &real = realByComposerId[compId];
			assert(real.realServerIdx >= 0);
			assert(real.realUserId != g_invalidUserId);
			if (real.realServerIdx < 0 || real.realUserId == g_invalidUserId) return;

			assert(composerIdByReal.size() > (size_t)real.realServerIdx);
			if (composerIdByReal.size() <= (size_t)real.realServerIdx) return;

			auto &byRealId = composerIdByReal[real.realServerIdx];
			assert(byRealId.size() > real.realUserId);
			if (byRealId.size() <= real.realUserId) return;

			byRealId[real.realUserId] = g_invalidUserId;
			real = {};

			if (compId < availableCompId) availableCompId = compId;
		}

		UserId Find(const RealUserId &real) const noexcept {
			if (real.realServerIdx < 0 
				|| composerIdByReal.size() <= (size_t)real.realServerIdx) {
				return g_invalidUserId;
			}
			auto &byRealId = composerIdByReal[(size_t)real.realServerIdx];

			if (byRealId.size() <= real.realUserId) return g_invalidUserId;

			return byRealId[real.realUserId];
		}
	};
}

struct ComposerServer::Impl {
	ServersVector realServers;
	Mapping mapping;
};

ComposerServer::ComposerServer(ServersVector realServers_) noexcept : pImpl(new Impl) {
	pImpl->realServers = realServers_;
}

ComposerServer::~ComposerServer() {
	delete pImpl;
}

void ComposerServer::Tick(const OnPacket &onPacket) noexcept {
	for (size_t svr = 0; svr < pImpl->realServers.size(); ++svr) {
		auto &server = pImpl->realServers[svr];
		server->Tick([&](const Packet &p, UserId userId) {
			const RealUserId real = { (int16_t)svr, userId };
			const UserId comp = pImpl->mapping.For(real);
			const Packet compPacket = { p.type, p.data, p.length };
			switch (p.type) {
			case PacketType::UserConnect:
			case PacketType::Message:
				onPacket(compPacket, comp);
				break;
			case PacketType::UserDisconnect:
				onPacket(compPacket, comp);
				pImpl->mapping.Free(real);
				break;
			default:
				assert(0 && "Unsupported PacketType");
				break;
			}
		});
	}
}

void ComposerServer::Send(UserId id, const unsigned char *packet, size_t length, bool reliable) noexcept {
	auto real = pImpl->mapping.Find(id);
	assert(real.realServerIdx >= 0);
	assert(real.realUserId != g_invalidUserId);
	if (real.realServerIdx >= 0 && real.realUserId != g_invalidUserId) {
		assert((size_t)real.realServerIdx < pImpl->realServers.size());
		if ((size_t)real.realServerIdx < pImpl->realServers.size()) {
			auto &server = pImpl->realServers[real.realServerIdx];
			assert(server != nullptr);
			if (server) {
				server->Send(real.realUserId, packet, length, reliable);
			}
		}
	}
}