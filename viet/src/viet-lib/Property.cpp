#include <string>
#include <viet/Property.h>

#include "Properties.h"

using namespace Viet;

using Value = std::unique_ptr<IPropertyValue>;

struct Property::Impl {
	std::string name;

	struct Info {
		ClientContextFactory fac;
		OnUpdate onUpdate;

		GridAttachFn gridAttachFn;
		SaveConditionFn saveConditionFn;
		int flags = Viet::PropertyFlags::g_defaultFlags;
		std::optional<Value> valueForcesEntityCaching;
		ToJsonFn toApiFormat, toDbFormat;
		FromJsonFn fromApiFormat, fromDbFormat;
		Value defaultValue;
		FromBinaryFn fromBinary;
		ToBinaryFn toBinary;
	};

	std::shared_ptr<Info> info;

	void Validate() const {
		std::string err;
		if (!info->saveConditionFn) err = "No save condition";
		else if (!info->toApiFormat) err = "No convertion to API format";
		else if (!info->fromApiFormat) err = "No convertion from API format";
		else if (!info->toDbFormat) err = "No convertion to Database format";
		else if (!info->fromDbFormat) err = "No convertion from Database format";
		else if (!info->defaultValue) err = "No default value";
		else if (!info->fromBinary) err = "No convertion from binary";
		else if (!info->toBinary) err = "No convertion to binary";
		if (err.size() > 0) {
			throw std::logic_error("Error in Property '" + name + "': " + err);
		}
	}

	Properties::IPropertyAny *AsPropertyAny() const {
		class MyPropertyAny : public Properties::IPropertyAny {
		public:

			MyPropertyAny(const Property::Impl *pImplCopy_) : pImplCopy(*pImplCopy_){

			};

			const char *GetName() noexcept override {
				assert(!this->pImplCopy.name.empty());
				return this->pImplCopy.name.data();
			}
			
			int GetValueFlags(const Value &value) noexcept override {
				return 0; // Unused
			}
			
			int GetFlags() noexcept override {
				return this->pImplCopy.info->flags;
			}
			
			std::optional<std::pair<int16_t, int16_t>> GetAffectsGrid(const Value &value) noexcept override {
				auto &fn = this->pImplCopy.info->gridAttachFn;
				if (!fn) return std::nullopt;
				return fn(value);
			}
			
			bool NeedsToBeSaved(const std::optional<Value> &lastSavedValue, const Value &currentValue) noexcept override {
				auto &fn = this->pImplCopy.info->saveConditionFn;
				if (!fn) return false;
				assert(lastSavedValue);
				if (!lastSavedValue) return true;
				return fn(*lastSavedValue, currentValue);
			}
			
			std::optional<Value> GetValueForcesEntityCaching() noexcept override {
				auto &r = this->pImplCopy.info->valueForcesEntityCaching;
				if (r.has_value()) {
					assert(r->get());
					if (r->get()) {
						return r->get()->Clone();
					}
				}
				return std::nullopt;
			}
			
			json ToApiFormat(const Value& value) noexcept override {
				return this->pImplCopy.info->toApiFormat(value);
			}
			
			Value FromApiFormat(const json &j) override {
				return this->pImplCopy.info->fromApiFormat(j);
			}
			
			json ToDbFormat(const Value& value) noexcept override {
				return this->pImplCopy.info->toDbFormat(value);
			}
			
			Value FromDbFormat(const json &j) noexcept override {
				return this->pImplCopy.info->fromDbFormat(j);
			}
			
			void ToBinary(MessageSerializer &seri, Viet::SeriToUse seriToUse, const Value& value, uint8_t *&outData, size_t &outLength) noexcept override {
				return this->pImplCopy.info->toBinary(seri, seriToUse, value, outData, outLength);
			}
			
			Value FromBinary(MessageSerializer &seri, Viet::SeriToUse seriToUse, const uint8_t *in, size_t length) noexcept override {
				return this->pImplCopy.info->fromBinary(seri, seriToUse, in, length);
			}
			
			Methods GetMethods() noexcept override {
				return {};
			}
			
			Value GetDefaultValue() noexcept override {
				return this->pImplCopy.info->defaultValue->Clone();
			}

			const Property::Impl pImplCopy;
		};
		return new MyPropertyAny(this);
	}
};

Property::Property() : pImpl(new Impl) {
	pImpl->info.reset(new Impl::Info);
}

namespace {
	class PUser {
	public:
		static constexpr auto name = "user";
		using Value = Viet::SimplePropertyValue<std::string>;

		static Viet::Property Build() {
			return Viet::Property().Register<PUser>().RemoveFlags(Viet::PropertyFlags::ClientWritable 
				| Viet::PropertyFlags::ClientReadable 
				| Viet::PropertyFlags::VisibleToNeighbour 
				| Viet::PropertyFlags::SavedToDb);
		}
	};

	class PCustomPacket {
	public:
		static constexpr auto name = "customPacket";
		using Value = Viet::SimplePropertyValue<JsonWrapper>;

		static Viet::Property Build() {
			return Viet::Property().Register<PCustomPacket>().RemoveFlags(Viet::PropertyFlags::ClientWritable 
				| Viet::PropertyFlags::ClientReadable 
				| Viet::PropertyFlags::VisibleToNeighbour 
				| Viet::PropertyFlags::SavedToDb);
		}
	};
}

Property Property::User() {
	return PUser::Build();
}

Property Property::CustomPacket() {
	return PCustomPacket::Build();
}

Property::~Property() {
	delete pImpl;
}

Property::Property(const Property &rhs) : pImpl(new Impl) {
	*this = rhs;
}

Property &Property::operator=(const Property &rhs) {
	pImpl->name = rhs.pImpl->name;
	pImpl->info = rhs.pImpl->info;
	return *this;
}

const char *Property::GetName() const noexcept {
	return pImpl->name.data();
}

void Property::AttachToGridImpl(const GridAttachFn &fn) {
	pImpl->info->gridAttachFn = fn;
}

void Property::SetSaveConditionImpl(const SaveConditionFn &fn) {
	pImpl->info->saveConditionFn = fn;
}

void Property::AddCastImpl(PropertyCastTarget castTarget, const ToJsonFn &toJson, const FromJsonFn &fromJson) {
	switch (castTarget) {
	case PropertyCastTarget::Api:
		pImpl->info->toApiFormat = toJson;
		pImpl->info->fromApiFormat = fromJson;
		break;
	case PropertyCastTarget::Database:
		pImpl->info->toDbFormat = toJson;
		pImpl->info->fromDbFormat = fromJson;
		break;
	case PropertyCastTarget::All:
		this->AddCastImpl(PropertyCastTarget::Api, toJson, fromJson);
		this->AddCastImpl(PropertyCastTarget::Database, toJson, fromJson);
		break;
	default:
		assert(0 && "Unknown cast target");
		break;
	}
}

void Property::SetDefaultValueImpl(Value &value) {
	assert(value);
	pImpl->info->defaultValue = std::move(value);
}

void Property::SetFromToBinary(ToBinaryFn toBinary, FromBinaryFn fromBinary) {
	pImpl->info->toBinary = toBinary;
	pImpl->info->fromBinary = fromBinary;
}

void Property::AddFlagImpl(int flag) {
	pImpl->info->flags |= flag;
}

void Property::RemoveFlagImpl(int flag) {
	pImpl->info->flags &= ~flag;
}

void Property::SetValueForcesCachingImpl(Value v) {
	pImpl->info->valueForcesEntityCaching = v->Clone();
}

void Property::SetName(const char *name) {
	pImpl->name = name;
}

void Property::SetClientContextFactory(const ClientContextFactory &fac) {
	pImpl->info->fac = fac;
}

void Property::SetOnUpdate(const OnUpdate &onUpdate) {
	pImpl->info->onUpdate = onUpdate;
}

void *Property::AsPropertyAny() const {
	pImpl->Validate();
	return pImpl->AsPropertyAny();
}

ClientContextFactory Property::GetClientContextFactory() const {
	return pImpl->info->fac;
}

Property::OnUpdate Property::GetOnUpdate() const {
	return pImpl->info->onUpdate;
}