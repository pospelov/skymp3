#pragma once
#include <memory>
#include <functional>
#include <vector>
#include <map>
#include <IProperty.h>

using PropId = int;

class Properties {
public:
	static constexpr size_t g_maxProperties = 32;

	class IPropertyAny : public IProperty<std::unique_ptr<IPropertyValue>> {
	public:
	};

	IPropertyAny *GetByName(const char *name) noexcept;

	using ForEachCb = std::function<void(const char *name, IPropertyAny *prop)>;

	void ForEach(const ForEachCb &cb) noexcept;
	const std::vector<const char *> &GetPropertyNames() noexcept;

	PropId GetPropertyId(const char *name) noexcept {
		auto &propNames = this->GetPropertyNames();
		const int n = (int)propNames.size();
		for (int i = 0; i < n; ++i) {
			if (!strcmp(name, propNames[i])) {
				return i;
			}
		}
		return -1;
	}

	const char *GetPropertyName(PropId id) noexcept {
		auto &propNames = this->GetPropertyNames();
		if (id >= 0 && id < (int)propNames.size()) {
			return propNames[id];
		}
		return "";
	}

	IPropertyAny *GetById(PropId id) noexcept {
		auto &propNames = this->GetPropertyNames();
		if (id < 0 || id >= (PropId)propNames.size()) {
			return nullptr;
		}
		if (this->propById.empty()) {
			this->propById.resize(propNames.size());
		}

		if (!this->propById[id]) {
			this->propById[id] = this->GetByName(propNames[id]);
		}
		return this->propById[id];
	}

	using PropMap = std::map<std::string, IPropertyAny *>;

	template <class ... Ts>
	Properties(Ts ... ts) {
		this->Init(ToPropMap(ts ...));
	}

	Properties(const PropMap &propMap_) {
		this->Init(propMap_);
	}

private:
	void Init(PropMap props);

	template <class T, class ... Ts>
	static PropMap ToPropMap(T t, Ts ... ts) {
		auto res = ToPropMap(ts ...);
		res[t->GetName()] = t;
		return res;
	}

	static PropMap ToPropMap() {
		return PropMap();
	}

	struct Impl;
	std::shared_ptr<Impl> pImpl;

	std::vector<IPropertyAny *> propById;

	Properties();
};