#pragma once
#include <variant>
#include <functional>
#include <memory>
#include <vector>
#include <stdexcept>

#include <nlohmann/json.hpp>

namespace Viet {
	using json = nlohmann::json;

	namespace NoSqlDatabase {
		class INoSqlCollection {
		public:
			virtual std::vector<json> Find(const json &filter) noexcept = 0;
			virtual json FindOne(const json &filter) noexcept = 0;
			virtual void InsertOne(const json &doc) noexcept = 0;
			virtual void UpdateOne(const json &filter, const json &update, bool upsert) noexcept = 0;
			virtual void UpdateMany(const std::vector<std::pair<json, json>> &filterAndUpdate, bool upsert) noexcept = 0;
			virtual void Drop() noexcept = 0;

			virtual ~INoSqlCollection() = default;
		};

		class INoSqlOperators {
		public:
			virtual const char *And() const noexcept = 0;
			virtual const char *Or() const noexcept = 0;

			virtual const char *Eq() const noexcept = 0;
			virtual const char *Gt() const noexcept = 0;
			virtual const char *Gte() const noexcept = 0;
			virtual const char *Lt() const noexcept = 0;
			virtual const char *Lte() const noexcept = 0;
			virtual const char *Ne() const noexcept = 0;

			virtual const char *Exist() const noexcept = 0;

			virtual const char *Set() const noexcept = 0;
			virtual const char *Unset() const noexcept = 0;
		};

		class INoSqlDatabase {
		public:
			virtual std::shared_ptr<INoSqlCollection> GetCollection(const char *collectionName) noexcept = 0;
			virtual INoSqlOperators &GetOperators() const noexcept = 0;

			virtual ~INoSqlDatabase() = default;
		};

		struct Plugin {
			std::function<INoSqlDatabase *()> databaseFactory;
		};
	}
}