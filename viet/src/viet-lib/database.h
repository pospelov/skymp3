#pragma once
#include <string>
#include <map>
#include <functional>
#include <vector>
#include <EntityInfo.h>

class ISessionStorage {
public:
	struct Session {
		std::map<std::string, std::string> browserWindows;
	};

	virtual std::vector<std::pair<EntityInfo, Session>> LoadSync() noexcept = 0;
	virtual void Set(const EntityInfo &key, std::optional<Session> session) noexcept = 0;

	virtual ~ISessionStorage() = default;
};