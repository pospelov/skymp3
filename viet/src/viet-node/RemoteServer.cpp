#include <map>
#include <vector>
#include <viet/ApiClient.h>
#include <viet-node/VietNode.h>
#include "TickSource.h"
#include "JsonUtils.h"

#include "RemoteServer.h"

using namespace VietNode;
Viet::Logging::Plugin GetLoggingPlugin();

namespace {
	Viet::Instance ToInstance(Napi::Value v) {
		return Viet::Instance(v.As<Napi::String>());
	}

	inline Viet::Actions ParseActions(Napi::Array manips) {
		Viet::Actions actions;
		for (uint32_t i = 0; i < manips.Length(); ++i) {
			auto manip = manips.Get(i).As<Napi::Object>();
			auto type = (std::string)manip.Get("type").As<Napi::String>();
			auto propName = (std::string)manip.Get("name").As<Napi::String>();
			if (type == "Set") {
				auto value = ToJson(manips.Env(), manip.Get("value"));
				actions.Set(propName.data(), value);
			}
			else if (type == "Get") {
				actions.Get(propName.data());
			}
			else {
				assert(0 && "Unknown type");
			}
		}
		return actions;
	}

	inline Napi::Value ToActionsResult(Viet::ActionsResult res, Napi::Env env) {
		if (!res.instance) {
			return env.Null();
		}
		auto resObj = Napi::Object::New(env);
		resObj.Set("instance", Napi::String::New(env, res.instance->uniqueId));

		auto returned = Napi::Array::New(env, res.returnedValues.size());
		for (uint32_t i = 0; i < returned.Length(); ++i) {
			returned.Set(i, ToJson(env, res.returnedValues[i]));
		}

		resObj.Set("returnedValues", returned);
		return resObj;
	}

	inline Napi::Array ToArray(std::vector<Napi::Value> vec, Napi::Env env) {
		auto arr = Napi::Array::New(env, vec.size());
		for (size_t i = 0; i < vec.size(); ++i) {
			arr[i] = vec[i];
		}
		return arr;
	}

	inline Napi::Object ToTransactionResult(const Viet::TransactionResult &transRes, Napi::Env env) {
		auto res = Napi::Object::New(env);

		const size_t n = transRes.entries.size();

		auto entries = Napi::Array::New(env, n);
		for (size_t i = 0; i < n; ++i) {
			auto resultEntry = Napi::Object::New(env);

			std::vector<Napi::Value> actionsResults;
			for (auto &actionsResult : transRes.entries[i].perInstanceResults) {
				actionsResults.push_back(ToActionsResult(actionsResult, env));
			}

			resultEntry.Set("actionsResults", ToArray(actionsResults, env));
			entries[i] = resultEntry;
		}
		res.Set("entries", entries);
		return res;
	}

}

struct RemoteServer::Impl {
	std::shared_ptr<Viet::ApiClient> apiClient;
	std::unique_ptr<TickSource> tickSource;

	std::vector<std::function<void(Napi::Env)>> envTasks;
	std::map<std::string, std::vector<Napi::FunctionReference>> eventHandlers;

	auto &logger() {
		return this->apiClient->GetLogger();
	}

	void Emit(const char *eventName, std::initializer_list<napi_value> values_) {
		auto &handlers = this->eventHandlers[eventName];
		for (auto &handler : handlers) {
			handler.Value().Call(values_);
		}
	}
};

Napi::Object RemoteServer::Init(Napi::Env env, Napi::Object exports) {
    Napi::Function func = DefineClass(env, "RemoteServer", {
        InstanceMethod("connect", &RemoteServer::Connect),
		InstanceMethod("on", &RemoteServer::On),
		InstanceMethod("kill", &RemoteServer::Kill),
		InstanceMethod("manipulateInstance", &RemoteServer::ManipulateInstance),
		InstanceMethod("setAttachedInstance", &RemoteServer::SetAttachedInstance),
		InstanceMethod("getAttachedInstance", &RemoteServer::GetAttachedInstance),
		InstanceMethod("findInstance", &RemoteServer::FindInstance),
		InstanceMethod("sendCustomPacket", &RemoteServer::SendCustomPacket),
		InstanceMethod("runTransaction", &RemoteServer::RunTransaction)
    });
    constructor = Napi::Persistent(func);
    constructor.SuppressDestruct();
    exports.Set("RemoteServer", func);
    return exports;
}

RemoteServer::RemoteServer(const Napi::CallbackInfo &info) : Napi::ObjectWrap<RemoteServer>(info) {
	pImpl.reset(new Impl);
}

Napi::FunctionReference RemoteServer::constructor;

void RemoteServer::Connect(const Napi::CallbackInfo &info) {
	auto ip = (std::string)info[0].As<Napi::String>();
	int port = (int)info[1].As<Napi::Number>();
	auto serverId = (std::string)info[2].As<Napi::String>();
	auto devPass = (std::string)info[3].As<Napi::String>();

	Napi::Object front = Napi::Object::New(info.Env());
	if (info[4].IsObject()) {
		front = (Napi::Object)info[4].As<Napi::Object>();
	}

	return this->Connect(info.Env(), ip.data(), port, serverId.data(), devPass.data(), front);
}

void RemoteServer::Connect(Napi::Env env, const char *ip, int port, const char *serverId, const char *devPass, Napi::Object front) {
	if (pImpl->apiClient) {
		this->Kill(env);
	}

	Viet::ApiClient::Settings settings;
	settings.serverId = serverId;
	settings.devPassword = devPass;
	settings.gamemodeNetPlugin = GetNetworkingPlugin(ip, (uint16_t)port);
	settings.seriPlugin = GetSeriPlugin();
	settings.loggingPlugin = GetLoggingPlugin();

	auto frontKeys = front.GetPropertyNames();
	for (int i = 0; i < (int)frontKeys.Length(); ++i) {
		auto key = frontKeys.Get(i).As<Napi::String>();
		auto value = front.Get(key).As<Napi::Buffer<uint8_t>>();

		auto &v = settings.frontEndFiles[std::string(key)];
		v.resize(value.Length());
		memcpy(v.data(), value.Data(), v.size());
	}

	Viet::ApiClient::Listeners listeners;

	listeners.onConnect = [this](const std::string &error, const Viet::ApiClient::ServerEntityList &list) {
		pImpl->envTasks.push_back([=](Napi::Env env) {
			auto entityList = Napi::Object::New(env);
			for (auto &[entityName, properties] : list.propertiesByEntityName) {
				auto arrayProps = Napi::Array::New(env, properties.size());
				for (size_t i = 0; i < properties.size(); ++i) {
					arrayProps.Set(i, Napi::String::New(env, properties[i]));
				}
				entityList.Set(entityName, arrayProps);
			}

			pImpl->Emit("connect", { Napi::String::New(env, error), entityList });
			if (error.size() > 0) {
				this->Kill(env);
			}
		});
	};

	listeners.onUserEnter = [this](const Viet::Instance &user) {
		pImpl->envTasks.push_back([=](Napi::Env env) {
			pImpl->Emit("userEnter", { Napi::String::New(env, user.uniqueId) });
		});
	};

	listeners.onUserExit = [this](const Viet::Instance &user) {
		pImpl->envTasks.push_back([=](Napi::Env env) {
			pImpl->Emit("userExit", { Napi::String::New(env, user.uniqueId) });
		});
	};

	listeners.onUserCustomPacket = [this](const Viet::Instance &user, std::string content) {
		pImpl->envTasks.push_back([=](Napi::Env env) {
			pImpl->Emit("userCustomPacket", { Napi::String::New(env, user.uniqueId), Napi::String::New(env, content) });
		});
	};

	pImpl->apiClient.reset(new Viet::ApiClient(settings, listeners));

	auto setInterval = env.Global().Get("setInterval").As<Napi::Function>();

	auto pImpl_ = pImpl;
	if (pImpl->tickSource) {
		pImpl->tickSource->Destroy(env);
	}
	pImpl->tickSource.reset(new TickSource(env, [pImpl_](Napi::Env env) {
		for (auto &t : pImpl_->envTasks) {
			t(env);
		}
		pImpl_->envTasks.clear();

		if (pImpl_->apiClient) {
			pImpl_->apiClient->Tick();
		}
	}));
}

void RemoteServer::On(const Napi::CallbackInfo &info) {
	auto eventName = (std::string)info[0].As<Napi::String>();
	auto fn = info[1].As<Napi::Function>();
	this->On(eventName.data(), fn);
}

void RemoteServer::On(const char *eventName, Napi::Function fn) {
	pImpl->eventHandlers[eventName].push_back(Napi::Persistent(fn));
}

void RemoteServer::Kill(const Napi::CallbackInfo &info) {
	return this->Kill(info.Env());
}

void RemoteServer::Kill(Napi::Env env) {
	if (pImpl->tickSource) {
		pImpl->tickSource->Destroy(env);
		pImpl->tickSource.reset();
	}
	pImpl->apiClient.reset();
}

Napi::Value RemoteServer::ManipulateInstance(const Napi::CallbackInfo &info) {
	auto entityName = (std::string)info[0].As<Napi::String>();
	auto actions = ParseActions(info[1].As<Napi::Array>());
	std::optional<std::string> existingInst;
	if (info[2].IsString()) {
		existingInst = (std::string)info[2].As<Napi::String>();
	}

	auto deferred = Napi::Promise::Deferred::New(info.Env());
	Viet::Promise<Viet::ActionsResult> vietPromise;
	if (existingInst == std::nullopt) {
		vietPromise = pImpl->apiClient->Create(entityName.data(), actions);
	}
	else {
		vietPromise = pImpl->apiClient->Edit(entityName.data(), Viet::Instance(*existingInst), actions);
	}

	auto pImpl_ = pImpl;
	vietPromise.Then([deferred, pImpl_, existingInst](const Viet::ActionsResult &res) {
		pImpl_->envTasks.push_back([=](Napi::Env env) {
			deferred.Resolve(ToActionsResult(res, env));
		});
	});
	vietPromise.Catch([deferred, pImpl_](const std::string &error) {
		pImpl_->envTasks.push_back([=](Napi::Env env) {
			deferred.Reject(Napi::Error::New(env, error).Value());
		});
	});

	return deferred.Promise();
}

Napi::Value RemoteServer::SetAttachedInstance(const Napi::CallbackInfo &info) {
	auto user = ToInstance(info[0]);
	auto entityName = (std::string)info[1].As<Napi::String>();

	std::optional<Viet::Instance> instance;
	if ((bool)info[2].ToBoolean()) {
		instance = ToInstance(info[2]);
	}

	auto deferred = Napi::Promise::Deferred::New(info.Env());
	auto vietPromise = pImpl->apiClient->SetAttachedInstance(user, entityName.data(), instance);

	auto pImpl_ = pImpl;
	vietPromise.Then([deferred, pImpl_](const Viet::Void &res) {
		pImpl_->envTasks.push_back([=](Napi::Env env) {
			deferred.Resolve(env.Null());
		});
	});
	vietPromise.Catch([deferred, pImpl_](const std::string &error) {
		pImpl_->envTasks.push_back([=](Napi::Env env) {
			deferred.Reject(Napi::Error::New(env, error).Value());
		});
	});

	return deferred.Promise();
}

Napi::Value RemoteServer::GetAttachedInstance(const Napi::CallbackInfo &info) {
	auto user = ToInstance(info[0]);
	auto entityName = (std::string)info[1].As<Napi::String>();

	auto deferred = Napi::Promise::Deferred::New(info.Env());
	auto vietPromise = pImpl->apiClient->GetAttachedInstance(user, entityName.data());

	auto pImpl_ = pImpl;
	vietPromise.Then([deferred, pImpl_](const std::optional<Viet::Instance> &res) {
		pImpl_->envTasks.push_back([=](Napi::Env env) {
			if (!res) {
				return deferred.Resolve(env.Null());
			}
			deferred.Resolve(Napi::String::New(env, res->uniqueId));
		});
	});
	vietPromise.Catch([deferred, pImpl_](const std::string &error) {
		pImpl_->envTasks.push_back([=](Napi::Env env) {
			deferred.Reject(Napi::Error::New(env, error).Value());
		});
	});

	return deferred.Promise();
}

namespace FindConditionParser {

	enum class JValueType {
		Value,
		ComparisonsObject,
	};

	JValueType GetValueType(const Viet::json &j) {
		if (j.is_object()) {
			for (auto it = j.begin(); it != j.end(); ++it) {
				if (it.key().size() > 0 && it.key()[0] == '$') {
					return JValueType::ComparisonsObject;
				}
			}
		}
		return JValueType::Value;
	}

	using Cmp = Viet::FindCondition::ComparisonOperator;

	void ParseComparisonObject(Viet::FindCondition &res, const char *path, const Viet::json &j) {
		for (auto it = j.begin(); it != j.end(); ++it) {

			Cmp op;
			if (it.key() == "$greater") {
				op = Cmp::Greater;
			}
			else if (it.key() == "$greaterOrEqual") {
				op = Cmp::GreaterOrEqual;
			}
			else if (it.key() == "$less") {
				op = Cmp::Less;
			}
			else if (it.key() == "$lessOrEqual") {
				op = Cmp::LessOrEqual;
			}
			else if (it.key() == "$equal") {
				op = Cmp::Equal;
			}
			else if (it.key() == "$notEqual") {
				op = Cmp::NotEqual;
			}
			else {
				assert(0);
				continue;
			}
			res.Compare(path, op, it.value());
		}
	}

	Viet::FindCondition ParseFindCondition(const Viet::json &j) {
		Viet::FindCondition res;
		for (auto it = j.begin(); it != j.end(); ++it) {
			auto &key = it.key();
			if (key == "$or" || key == "$and") {
				if (it.value().is_array()) {
					std::vector<Viet::FindCondition> subConds;
					for (size_t i = 0; i < it.value().size(); ++i) {
						subConds.push_back(ParseFindCondition(it.value()[i]));
					}
					if (key == "$and") res.And(subConds);
					else if (key == "$or") res.Or(subConds);
					else assert(0);
				}
			}
			else if (key.size() && key[0] == '$') {
				assert(0 && "Unknown operator");
			}
			else {
				const auto objT = GetValueType(it.value());
				if (objT == JValueType::Value) {
					res.Compare(key.data(), Cmp::Equal, it.value());
				}
				else if (objT == JValueType::ComparisonsObject) {
					ParseComparisonObject(res, key.data(), it.value());
				}
				else {
					assert(0);
				}
			}
		}

		return res;
	}
}

Napi::Value RemoteServer::FindInstance(const Napi::CallbackInfo &info) {
	auto entityName = (std::string)info[0].As<Napi::String>();
	auto cond = info[1].As<Napi::Object>();
	auto limit = (uint32_t)info[2].As<Napi::Number>();

	Viet::FindRequest findRequest;
	findRequest.condition = FindConditionParser::ParseFindCondition(ToJson(info.Env(), cond));
	findRequest.limit = limit;

	auto pImpl_ = pImpl;
	auto deferred = Napi::Promise::Deferred::New(info.Env());
	auto vietPromise = pImpl->apiClient->Find(entityName.data(), findRequest);

	vietPromise.Then([=](std::vector<Viet::Instance> instances) {
		pImpl_->envTasks.push_back([=](Napi::Env env) {
			auto arr = Napi::Array::New(env, instances.size());
			for (int i = 0; i < (int)instances.size(); ++i) {
				arr.Set(i, Napi::String::New(env, instances[i].uniqueId));
			}
			deferred.Resolve(arr);
		});

	});
	vietPromise.Catch([=](std::string err) {
		pImpl_->envTasks.push_back([=](Napi::Env env) {
			deferred.Reject(Napi::Error::New(env, err).Value());
		});
	});

	return deferred.Promise();
}

Napi::Value RemoteServer::SendCustomPacket(const Napi::CallbackInfo &info) {
	auto user = ToInstance(info[0]);
	auto content = (std::string)info[1].As<Napi::String>();

	auto pImpl_ = pImpl;
	auto deferred = Napi::Promise::Deferred::New(info.Env());
	auto vietPromise = pImpl->apiClient->SendCustomPacket(user, content.data());

	vietPromise.Then([=](Viet::Void) {
		pImpl_->envTasks.push_back([=](Napi::Env env) {
			deferred.Resolve(env.Undefined());
		});

	});
	vietPromise.Catch([=](std::string err) {
		pImpl_->envTasks.push_back([=](Napi::Env env) {
			deferred.Reject(Napi::Error::New(env, err).Value());
		});
	});

	return deferred.Promise();
}

inline Viet::Transaction ToTransaction(Napi::Array entries) {
	Viet::Transaction trans;
	for (int i = 0; i < (int)entries.Length(); ++i) {
		auto entry = entries.Get(i).As<Napi::Object>();

		auto actions = ParseActions(entry.Get("actions").As<Napi::Array>());

		auto selectorType = (std::string)entry.Get("selectorType").As<Napi::String>();
		if (selectorType == "arrayOfInstances") {
			auto instances = entry.Get("selector").As<Napi::Array>();
			std::vector<Viet::Instance> inst;
			std::string entityName; // Expected to be common for all instances
			for (int j = 0; j < (int)instances.Length(); ++j) {
				auto data = instances.Get(j).As<Napi::Object>();
				entityName = (std::string)data.Get("entityName").As<Napi::String>();
				auto ei = (std::string)data.Get("ei").As<Napi::String>();
				inst.push_back(Viet::Instance(ei));
			}
			trans.entries.push_back(Viet::TransactionEntry::ForArrayOfInstances(entityName, inst, actions));
		}
		else if (selectorType == "instance") {
			auto entityName = entry.Get("selector").As<Napi::Object>().Get("entityName").As<Napi::String>();
			auto ei = entry.Get("selector").As<Napi::Object>().Get("ei").As<Napi::String>();
			trans.entries.push_back(Viet::TransactionEntry::ForInstance((std::string)entityName, Viet::Instance((std::string)ei), actions));
		}
		else if (selectorType == "newInstance") {
			auto entityName = entry.Get("selector").As<Napi::String>();
			trans.entries.push_back(Viet::TransactionEntry::ForNewInstance(entityName, actions));
		}
		else if (selectorType == "findOne") {
			auto selector = entry.Get("selector").As<Napi::Object>();
			auto filter = selector.Get("filter").As<Napi::Object>();
			auto entityName = selector.Get("entityName").As<Napi::String>();
			auto filterCpp = FindConditionParser::ParseFindCondition(ToJson(entries.Env(), filter));
			trans.entries.push_back(Viet::TransactionEntry::ForFindOne(entityName, filterCpp, actions));

		}
		else if (selectorType == "findMany") {
			auto selector = entry.Get("selector").As<Napi::Object>();
			auto filter = selector.Get("filter").As<Napi::Object>();
			auto entityName = selector.Get("entityName").As<Napi::String>();
			auto limit = (uint32_t)selector.Get("limit").As<Napi::Number>();
			auto filterCpp = FindConditionParser::ParseFindCondition(ToJson(entries.Env(), filter));
			trans.entries.push_back(Viet::TransactionEntry::ForFindMany(entityName, filterCpp, limit, actions));
		}
		else {
			assert(0 && "Unknown selectorType");
		}
	}
	return trans;
}

Napi::Value RemoteServer::RunTransaction(const Napi::CallbackInfo &info) {
	auto options = info[0].As<Napi::Object>();
	auto entries = options.Get("entries").As<Napi::Array>();

	auto pImpl_ = pImpl;
	auto deferred = Napi::Promise::Deferred::New(info.Env());
	auto vietPromise = pImpl->apiClient->RunTransaction(ToTransaction(entries));

	vietPromise.Then([=](Viet::TransactionResult res) {
		pImpl_->envTasks.push_back([=](Napi::Env env) {
			deferred.Resolve(ToTransactionResult(res, env));
		});

	});
	vietPromise.Catch([=](std::string err) {
		pImpl_->envTasks.push_back([=](Napi::Env env) {
			deferred.Reject(Napi::Error::New(env, err).Value());
		});
	});

	return deferred.Promise();
}