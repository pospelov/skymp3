#pragma once
#include <memory>
#include <functional>
#include <cstdint>
#include <AsyncDatabase.h>
#include <viet/Networking.h>
#include <viet/MessageSerializer.h>
#include <IEntityHashGen.h>
#include <IGameData.h>
#include <viet/Logging.h>
#include <IGamemodeController.h>
#include <ITimerManager.h>
#include <database.h>
#include <ICellProcess.h>
#include <viet/Entity.h>
#include <Messages.h>

// For internal use:
#include <MsgWrapper.h>

class SkympServerView {
public:
	struct Settings {
		uint32_t maxPlayers = 0;
		std::shared_ptr<Viet::Networking::IServer> server;
		std::shared_ptr<IEntityHashGen> hashGen;
		std::shared_ptr<IGamemodeController> gamemodeController;
		std::shared_ptr<Viet::Logging::Logger> logger;
		std::shared_ptr<ITimerManager> timerManager;
		std::shared_ptr<AsyncDatabase> db;
		std::vector<Viet::Entity> entities;

		std::function<std::shared_ptr<ICellProcess>()> cellProcessFactory;
		std::function<std::unique_ptr<IActor>()> actorFactory;
		std::shared_ptr<Viet::MessageSerializer> seri;
		Viet::NoSqlDatabase::INoSqlOperators *dbOps = nullptr;
	};

	SkympServerView(const Settings &settings) noexcept;

	void Tick() noexcept;

private:
	void OnPacket(const Viet::Networking::Packet &p, Viet::Networking::UserId userId) noexcept;
	void OnConnect(Viet::Networking::UserId id) noexcept;
	void OnDisconnect(Viet::Networking::UserId id) noexcept;
	void OnMessage(Viet::Networking::UserId id, MsgWrapper<msg::UpdateActorProperties> &msg) noexcept;
	void OnMessage(Viet::Networking::UserId id, MsgWrapper<msg::CustomPacket> &msg) noexcept;
	void OnMessage(Viet::Networking::UserId id, MsgWrapper<msg::Handshake> &msg) noexcept;

	void OnApiMethodCallCommon(const evn::Any &evn) noexcept;

	// API Implementation
	Viet::Promise<uint32_t> GetMaxPlayers() noexcept;
	Viet::Promise<Viet::Void> SendCustomPacket(const EntityInfo &user, const std::string &jsonContent) noexcept;
	Viet::Promise<std::vector<EntityInfo>> Find(const std::string &entityName, const Viet::FindRequest &findReq) noexcept;
	Viet::Promise<EntityManipulationResult> Manipulate(const std::string &entityName, const EntityInfo &optionalExistingEi, const EntityManipulation &manip) noexcept;
	Viet::Promise<Viet::Void> SetActor(const EntityInfo &user, const std::string &entityName, const EntityInfo &newActor) noexcept;
	Viet::Promise<EntityInfo> GetActor(const std::string &entityName, const EntityInfo &user) noexcept;
	Viet::Promise<Viet::TransactionResult> RunTransaction(const Viet::Transaction &trans) noexcept;

	void ProcessCallResult(const evn::CallResult &cr, const char *jsEvnName) noexcept;
	void UpdateGui(const EntityInfo &session) noexcept;
	void CreateActor(const EntityInfo &session, int entityId, const EntityInfo &actor, const EntityProps &props) noexcept;
	void DestroyActor(const EntityInfo &session, const EntityInfo &actor) noexcept;
	void SendActorProperties(const EntityInfo &session, const EntityInfo &ownerActor, int entityId, const EntityProps &properties) noexcept;
	void SendMyProperties(const EntityInfo &session, int entityId, const EntityProps &properties) noexcept;
	void ClearMyProperties(const EntityInfo &session, int entityId) noexcept;
	void SendActorId(const EntityInfo &session, const ICellProcess::InstanceId &inst, const EntityInfo &actor) noexcept;

	//void SendMovement(const EntityInfo &session, const EntityInfo &ownerActor, const Movement &mov) noexcept;
	//void SendLook(const EntityInfo &session, const EntityInfo &ownerActor, const Look &look) noexcept;
	//void SendName(const EntityInfo &session, const EntityInfo &ownerActor, const std::string &name) noexcept;
	//void SendAnim(const EntityInfo &session, const EntityInfo &ownerActor, const std::string &anim) noexcept;
	///void ShowRaceMenu(const EntityInfo &session) noexcept;
	///void Teleport(const EntityInfo &session, const Point3D &pos) noexcept;
	///void Rotate(const EntityInfo &session, float angleZ) noexcept;

	struct Impl;
	const std::shared_ptr<Impl> pImpl;
};