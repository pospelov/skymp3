#pragma once
#include <Messages.h>
#include <vector>

class IConnectionMessageListener {
public:
	enum ConnectResult {
		Accepted,
		Failed,
		Denied
	};

	virtual void OnConnect(ConnectResult res) noexcept = 0;
	virtual void OnDisconnect() noexcept = 0;

	virtual ~IConnectionMessageListener() = default;
};

class IGuiMessageListener {
public:
	virtual void OnMessage(const msg::GuiJson &) noexcept = 0;

	virtual ~IGuiMessageListener() = default;
};

class IGameplayMessageListener {
public:
	virtual void OnMessage(const msg::UpdateMyProperties &) noexcept = 0;
	virtual void OnMessage(const msg::UpdateActorProperties &) noexcept = 0;
	virtual void OnMessage(const msg::ActorId &) noexcept = 0;
	virtual void OnMessage(const msg::CreatePlayer &) noexcept = 0;
	virtual void OnMessage(const msg::DestroyPlayer &) noexcept = 0;
	virtual void OnMessage(const msg::SimpleCommand &) noexcept = 0;

	virtual ~IGameplayMessageListener() = default;
};

class ICustomPacketMessageListener {
public:
	virtual void OnMessage(const msg::CustomPacket &) noexcept = 0;
};

struct MessageListeners {
	std::vector<IConnectionMessageListener *> connMsgListeners;
	IGuiMessageListener *guiMsgListener = nullptr;
	IGameplayMessageListener *gameplayMsgListener = nullptr;
	ICustomPacketMessageListener *cpMsgListener = nullptr;
};