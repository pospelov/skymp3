#include <memory>
#include "EntityHashGen.h"

// See https://stackoverflow.com/questions/440133/how-do-i-create-a-random-alpha-numeric-string-in-c

namespace {
	std::vector<char> GetCharacters() {
		return { '0','1','2','3','4',
			'5','6','7','8','9',
			'A','B','C','D','E','F',
			'G','H','I','J','K',
			'L','M','N','O','P',
			'Q','R','S','T','U',
			'V','W','X','Y','Z',
			'a','b','c','d','e','f',
			'g','h','i','j','k',
			'l','m','n','o','p',
			'q','r','s','t','u',
			'v','w','x','y','z' };
	};
}

EntityHashGen::EntityHashGen() {
	this->chars = GetCharacters();

	this->rng.reset(new std::default_random_engine(std::random_device{}()));
	this->dist.reset(new std::uniform_int_distribution<>(0, this->chars.size() - 1));

	this->randchar = [this] {
		return this->chars[(*this->dist)(*rng)]; 
	};
}

std::string EntityHashGen::Generate() noexcept {
	// (12*5)^24 == 4.7383813e+42
	const size_t length = 24;
	std::string str(length, 0);
	std::generate_n(str.begin(), length, this->randchar);
	return str;
}