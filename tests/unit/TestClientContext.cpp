#include "Common.h"

class TestBaseCtx : public Viet::IBaseContext {
public:
	uint32_t n = 0;
};

class TestView : public Viet::IInstanceView {
public:
};

template <class Prop>
using TestCtx = Viet::ClientContext<Prop, TestBaseCtx, TestView>;

class PropX {
public:
	static constexpr auto name = "x";
	using Value = Viet::SimplePropertyValue<int>;
	using State = Viet::Void;
	using GlobalState = Viet::Void;

	static Viet::Property Build() {
		return Viet::Property().Register<PropX>()
			.AddClient<TestBaseCtx, TestView>()
			.AttachToGrid([](Value v) { return std::pair<int16_t, int16_t>(v.value, 0); });
	}

	static void OnUpdate(TestCtx<PropX> &ctx) {
		if (!ctx.View()) {
			++ctx.Base().n;
		}
	}
};

class PropIsEnabled {
public:
	static constexpr auto name = "isEnabled";
	using Value = Viet::SimplePropertyValue<bool>;
	using State = Viet::Void;
	using GlobalState = Viet::Void;

	static Viet::Property Build() {
		return Viet::Property().Register<PropIsEnabled>()
			.SetValueForcesCaching(PropIsEnabled::Value(true));
	}
};

class PropCellOrWorld {
public:
	static constexpr auto name = "cellOrWorld";
	using Value = Viet::SimplePropertyValue<uint32_t>;
	using State = Viet::Void;
	using GlobalState = Viet::Void;

	static Viet::Property Build() {
		return Viet::Property().Register<PropCellOrWorld>()
			.AddFlags(Viet::PropertyFlags::AffectsCellProcessSelection);
	}
};

constexpr auto g_dummy = "Dummy";

std::vector<Viet::Entity> Entities() {
	return { Viet::Entity(g_dummy, { PropX::Build(), PropIsEnabled::Build(), PropCellOrWorld::Build() })
	};
}

void Tick(VietEnv &env) {
	for (int i = 0; i < 50; ++i) {
		env.Client().Tick();
		env.Server().Tick();
		env.ApiClient().Tick();
	}
}

TEST_CASE("<description>", "[client_ctx]") {
	Viet::ApiClient::Listeners li;

	std::unique_ptr<VietEnv> e;
	std::shared_ptr<bool> finish(new bool(false));

	auto onErr = [](const char *text) {
		throw std::runtime_error(std::string(text));
	};

	li.onUserEnter = [=, &e](Viet::Instance user) {
		e->ApiClient().Create(g_dummy, Viet::Actions().Set("isEnabled", true).Set("cellOrWorld", 0x3cul).Set("x", 2.f)).Then([=, &e](const Viet::ActionsResult &res) {
			e->ApiClient().SetAttachedInstance(user, g_dummy, res.instance).Then([=, &e](Viet::Void) {
				*finish = true;
			}).Catch(onErr);
		}).Catch(onErr);
	};
	e.reset(new VietEnv{ Entities(), li });


	while (!*finish) {
		Tick(*e);
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}

	int stop = 0;
	while (!stop) {
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
		Tick(*e);

		e->Client().ProcessWorldState([&](const Viet::IWorldState *wst) {
			e->Client().SendPropertyChange<PropX>(PropX::Value(1));
			if (strlen(wst->GetMyEntity()) > 0 && wst->GetInstances(g_dummy).GetPoolSize() > 0)
				stop = 1;
		});
	}

	TestBaseCtx base;
	CommonViewHolder<TestView> holder;
	e->Client().UpdateProperties(base, &holder, true);

	
	REQUIRE(base.n == 1);
}