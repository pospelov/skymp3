message(STATUS "[skymp] Using local Viet distribution ${VIET_PATH}")
message(STATUS "[skymp] VIET_DIST_PATH is ${VIET_DIST_PATH}")

set(VIET_INCLUDE
  ${VIET_DIST_PATH}/include
  ${CMAKE_CURRENT_SOURCE_DIR}/src/viet-raknet
  ${CMAKE_CURRENT_SOURCE_DIR}/src/viet-node
  ${CMAKE_CURRENT_SOURCE_DIR}/src/viet-mongocxx
)
message(STATUS "VIET_INCLUDE=${VIET_INCLUDE}")

# VIET_LIBS
set(libs
  _RakNet
  assetslib
  bson-1.0
  bsoncxx
  mongoc-1.0
  mongocxx
  vietlib
  viet-mongocxx
  viet-node
  viet-raknet
)
file(READ "${VIET_DIST_PATH}/non_tracked.txt" nontracked_libs)
list(APPEND VIET_LIBS ${nontracked_libs})

list(APPEND VIET_LIBS nlohmann_json::nlohmann_json)

if (WIN32)
  foreach(lib ${libs})
    list(APPEND VIET_LIBS "${VIET_DIST_PATH}/lib$<$<CONFIG:Debug>:_dbg>/${CMAKE_STATIC_LIBRARY_PREFIX}${lib}${CMAKE_STATIC_LIBRARY_SUFFIX}")
  endforeach()
endif()

if (UNIX)
  if (${CMAKE_BUILD_TYPE} STREQUAL "Debug")
    set(VIET_LIB_DIR_SUFFIX "_dbg")
  endif()

  file(GLOB lib_paths "${VIET_DIST_PATH}/lib${VIET_LIB_DIR_SUFFIX}/*")
  foreach(lib_path ${lib_paths})
    list(APPEND VIET_LIBS ${lib_path})
  endforeach()
endif()

message(STATUS "[skymp] Found VIET_LIBS: ${VIET_LIBS}")
