#pragma once
#include <memory>
#include <optional>
#include <cassert>
#include <any>
#include <functional>
#include <typeindex>

namespace Viet {
	class Client;
	class IWorldState;

	template <uint32_t ms>
	class Timeout {
	public:
		static constexpr auto timeoutMs = ms;

	private:
		Timeout() = default;
	};

	template <uint32_t ms>
	class GlobalTimeout {
	public:
		static constexpr auto globalTimeoutMs = ms;

	private:
		GlobalTimeout() = default;
	};

	class Client;
	class IPropertyValue;

	struct PropertyChange {
		std::shared_ptr<IPropertyValue> previousValue;
	};

	class ClientContextImplAccess;
	class ClientContextImpl {
		friend class Viet::ClientContextImplAccess;
	public:
		virtual ~ClientContextImpl() = default;

	protected:
		typedef std::any(*StateCtor)();
		typedef std::any(*GlobalStateCtor)();

		ClientContextImpl();

		void Init(void *base, void *view, void *vietInstance, StateCtor *stateCtor, GlobalStateCtor *globalStateCtor, std::any *state, std::any *globalState, Viet::Client *client, const Viet::IWorldState *wst, std::any *persistent, std::any *globalPersistent);
		void *Base();
		void *View();
		std::any &State();
		std::any &GlobalState();
		void SendPropertyChange(const char *propName, Viet::IPropertyValue *propValue);
		std::optional<PropertyChange> NextChange(const char *currentCbPropName, const char *propName);
		std::shared_ptr<void> NextEvent(const char *currentCbPropName, const std::type_info &typeInfo);
		bool NextTimeout(const char *currentCbPropName, const std::type_info &typeInfo, uint32_t timeoutMs);
		bool TryLockGlobalTimeout(const std::type_info &typeInfo, uint32_t globalTimeoutMs);
		Viet::IPropertyValue *GetValue(const char *propName);

		void Tick();

	private:
		void AddEvent(const char *currentCbPropName, const std::type_index &typeInfo, std::shared_ptr<void> e);

		struct Impl;
		std::shared_ptr<Impl> pImpl;
	};

	class IBaseContext;
	class IInstanceView;
	class InstanceModel;

	using ClientContextFactory = std::function<
		std::shared_ptr<ClientContextImpl>(
			Viet::IBaseContext &,
			Viet::IInstanceView *,
			Viet::Client &,
			const Viet::IWorldState *,
			std::any &pers,
			std::any &globalPers,
			InstanceModel *,
			std::any &state,
			std::any &gState)>;

	template <class Prop, class BaseContext, class InstanceView>
	class ClientContext : public ClientContextImpl {
	public:
		using StatePtr = std::shared_ptr<typename Prop::State>;
		using GlobalStatePtr = std::shared_ptr<typename Prop::GlobalState>;

		static ClientContextFactory GetFactory() {
			return [](IBaseContext &base, IInstanceView *view, Client &cl, const Viet::IWorldState *wst, std::any &pers, std::any &gPers, InstanceModel *instanceModel, std::any &state, std::any &gState) {
				auto res = std::shared_ptr<ClientContextImpl>();
				auto baseConcrete = dynamic_cast<BaseContext *>(&base);
				auto viewConcrete = dynamic_cast<InstanceView *>(view);
				bool viewConcreteOk = viewConcrete || !view;
				
				assert(baseConcrete);
				assert(viewConcreteOk);
				if (baseConcrete && viewConcreteOk) {
					res.reset(new ClientContext<Prop, BaseContext, InstanceView>(*baseConcrete, viewConcrete, instanceModel, state, gState, cl, wst, pers, gPers));
				}
				return res;
			};
		}

		ClientContext(BaseContext &base, InstanceView *view, void *vietInstance, std::any &state, std::any &globalState, Viet::Client &cl, const Viet::IWorldState *wst, std::any &pers, std::any &globalPers) {
			static auto stateCtor = []()->std::any {
				using T = typename Prop::State;
				return std::shared_ptr<T>(new T);
			};

			static auto globalStateCtor = []()->std::any {
				using T = typename Prop::GlobalState;
				return std::shared_ptr<T>(new T);
			};

			static auto stateCtorC = static_cast<StateCtor>(stateCtor);
			static auto globalStateCtorC = static_cast<GlobalStateCtor>(globalStateCtor);

			this->Init(&base, view, vietInstance, &stateCtorC, &globalStateCtorC, &state, &globalState, &cl, wst, &pers, &globalPers);
		}

		BaseContext &Base() noexcept {
			return *(BaseContext *)this->ClientContextImpl::Base();
		}

		InstanceView *View() noexcept {
			return (InstanceView *)this->ClientContextImpl::View();
		}

		StatePtr State() noexcept {
			return std::any_cast<StatePtr>(this->ClientContextImpl::State());
		}

		GlobalStatePtr GlobalState() noexcept {
			return std::any_cast<GlobalStatePtr>(this->ClientContextImpl::GlobalState());
		}

		template <class PropClass, class PropValue>
		void SendPropertyChange(const PropValue &v) noexcept {
			this->ClientContextImpl::SendPropertyChange(PropClass::name, const_cast<PropValue *>(&v));
		}

		template <class PropClass>
		std::optional<PropertyChange> NextChange() noexcept {
			return this->ClientContextImpl::NextChange(Prop::name, PropClass::name);
		}

		template <class Event>
		std::optional<Event> NextEvent() noexcept {
			auto p = this->ClientContextImpl::NextEvent(Prop::name, typeid(Event));
			if (!p) return std::nullopt;
			return *(Event *)p.get();
		}

		template <class Timeout>
		bool NextTimeout() noexcept {
			return this->ClientContextImpl::NextTimeout(Prop::name, typeid(Timeout), Timeout::timeoutMs);
		}

		template <class GlobalTimeout>
		bool TryLockGlobalTimeout() noexcept {
			return this->ClientContextImpl::TryLockGlobalTimeout(typeid(GlobalTimeout), GlobalTimeout::globalTimeoutMs);
		}

		template <class PropClass>
		std::optional<typename PropClass::Value> GetValue() noexcept {
			using T = typename PropClass::Value;
			auto p = dynamic_cast<T *>(this->ClientContextImpl::GetValue(PropClass::name));
			if (!p) return std::nullopt;
			return *p;
		}
	};
}