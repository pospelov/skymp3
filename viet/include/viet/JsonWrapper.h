#pragma once
#include <viet/MessageSerializer.h> // SERIALIZATION_SPLIT_MEMBER
#include <nlohmann/json.hpp>

namespace Viet {
	using json = nlohmann::json;

	struct JsonWrapper {
		JsonWrapper() = default;
		JsonWrapper(json v_) : v(v_) {
		}

		json v;

		operator json &() noexcept {
			return v;
		}

		operator const json &() const noexcept {
			return v;
		}

		inline friend bool operator==(const JsonWrapper &lhs, const JsonWrapper &rhs) noexcept {
			return lhs.v == rhs.v;
		}

		inline friend bool operator!=(const JsonWrapper &lhs, const JsonWrapper &rhs) noexcept {
			return !(lhs == rhs);
		}

		template <class Archive>
		void save(Archive &ar, unsigned int version) {
			ar & v.dump();
		}

		template <class Archive>
		void load(Archive &ar, unsigned int version) {
			try {
				std::string dump;
				ar & dump;
				v = json::parse(dump);
			}
			catch (std::exception &) {
				assert(0 && "Failed to parse");
			}
		}

		SERIALIZATION_SPLIT_MEMBER();
	};
}