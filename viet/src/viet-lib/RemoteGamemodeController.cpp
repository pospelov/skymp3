#include <cassert>
#include <ctime>
#include <map>
#include <list>
#include <Messages.h>
#include <MsgWrapper.h>

#include "RemoteGamemodeController.h"

namespace {
	struct GamemodeState {
		bool auth = false;
	};
}

struct RemoteGamemodeController::Impl {
	std::map<std::string, std::vector<std::string>> propertiesByEntityName;

	std::string devPassword;
	std::shared_ptr<Viet::Networking::IServer> server;
	std::shared_ptr<Viet::Logging::Logger> logger;
	std::shared_ptr<Viet::MessageSerializer> seri;
	std::shared_ptr<ITimerManager> timerManager;

	Viet::Networking::IServer::OnPacket onPacket;

	std::map<Viet::Networking::UserId, GamemodeState> gamemodes;
	std::list<std::pair<evn::Any, gmid>> pendingMethodCalls;
	std::list<FrontEndFiles> pendingFrontEndFiles;

	std::vector<std::pair<clock_t, evn::Any>> eventsBackup;

	int dtor = 0;

	int online = 0;
};

RemoteGamemodeController::RemoteGamemodeController(
	std::string devPassword_,
	std::shared_ptr<Viet::Logging::Logger> logger_, 
	std::shared_ptr<Viet::Networking::IServer> server_, 
	std::shared_ptr<Viet::MessageSerializer> seri_,
	std::shared_ptr<ITimerManager> timerManager_,
	std::map<std::string, std::vector<std::string>> propertiesByEntityName_) {
	assert(devPassword_.size() > 0);
	assert(server_);
	assert(logger_);
	assert(seri_);
	assert(timerManager_);
	assert(propertiesByEntityName_.size() > 0);

	pImpl.reset(new Impl);
	pImpl->devPassword = devPassword_;
	pImpl->server = server_;
	pImpl->logger = logger_;
	pImpl->seri = seri_;
	pImpl->timerManager = timerManager_;
	pImpl->propertiesByEntityName = propertiesByEntityName_;

	pImpl->onPacket = [this](const Viet::Networking::Packet &p, Viet::Networking::UserId userId) {
		this->OnPacket(p, userId);
	};
}

RemoteGamemodeController::~RemoteGamemodeController() {
	pImpl->dtor = 1;
}

int RemoteGamemodeController::GetNumActiveGamemodes() const noexcept {
	return pImpl->online;
}

void RemoteGamemodeController::Tick() {
	pImpl->server->Tick(pImpl->onPacket);
}

void RemoteGamemodeController::SendEvent(const evn::Any &event) noexcept {
	return this->SendEventImpl(event, -1);
}

std::optional<evn::Any> RemoteGamemodeController::GetNextMethodCalled() noexcept {
	if (!pImpl->pendingMethodCalls.empty()) {
		const auto res = std::move(pImpl->pendingMethodCalls.front());
		pImpl->pendingMethodCalls.pop_front();
		return res.first;
	}
	return std::nullopt;
}

std::optional<FrontEndFiles> RemoteGamemodeController::GetNextFrontEndChange() noexcept {
	if (!pImpl->pendingFrontEndFiles.empty()) {
		const auto res = std::move(pImpl->pendingFrontEndFiles.front());
		pImpl->pendingFrontEndFiles.pop_front();
		return res;
	}
	return std::nullopt;
}

void RemoteGamemodeController::SendEventImpl(const evn::Any &event, gmid gmId) noexcept {
	if (gmId == -1) {
		if (pImpl->gamemodes.empty()) {
			int ms = 100;

			// TODO: find a better solution
			auto pImpl_ = pImpl;
			pImpl->logger->Info("No gamemodes found, will repeat SendEvent after %d ms", ms);
			pImpl->timerManager->SetTimeout(ms, [this, pImpl_, event, gmId]{
				if (!pImpl_->dtor) {
					pImpl->logger->Info("Event repeat");
					this->SendEventImpl(event, gmId);
				}
				});
			return;
		}
		for (auto &[id, state] : pImpl->gamemodes) {
			this->SendEventImpl(event, id);
		}
		return;
	}
	MsgWrapper<msg::GMEvent> msgWrapper(&*pImpl->seri, msg::GMEvent{ event });
	auto[data, length] = msgWrapper.GetBinary();
	pImpl->server->Send(gmId, data, length, true);

	std::visit([&](const auto &concrete) {
		pImpl->logger->Info("Sending event %s to gm %d", concrete.JSName, gmId);
	}, event);
}

void RemoteGamemodeController::OnPacket(const Viet::Networking::Packet &p, Viet::Networking::UserId userId) {
	switch (p.type) {
	case Viet::Networking::PacketType::UserConnect:
		return this->OnConnect(userId);
	case Viet::Networking::PacketType::UserDisconnect:
		return this->OnDisconnect(userId);
	case Viet::Networking::PacketType::Message:
		return this->OnMessage(userId, p.data, p.length);
	default:
		assert(0);
		break;
	}
}

void RemoteGamemodeController::OnConnect(Viet::Networking::UserId id) {
	assert(pImpl->gamemodes.count(id) == 0);
	pImpl->gamemodes[id] = {};

	pImpl->logger->Notice("Added gamemode %d (waiting for a handshake)", id);
	pImpl->online++;
}

void RemoteGamemodeController::OnDisconnect(Viet::Networking::UserId id) {
	auto it = pImpl->gamemodes.find(id);
	assert(it != pImpl->gamemodes.end());
	if (it != pImpl->gamemodes.end()) {
		pImpl->gamemodes.erase(it);
	}

	pImpl->logger->Warning("Removed gamemode %d. Reason: Disconnect", id);
	pImpl->online--;
}

void RemoteGamemodeController::OnMessage(Viet::Networking::UserId id, const uint8_t *data, size_t length) {
	const auto it = pImpl->gamemodes.find(id);
	if (it == pImpl->gamemodes.end()) return;

	auto &gamemode = it->second;

	switch ((MessageId)data[0]) {
	case MessageId::GMEvent: {
		if (!gamemode.auth) {
			pImpl->logger->Error("Ignoring GMEvent from gamemode. Reason: Not authorized");
			break;
		}
		MsgWrapper<msg::GMEvent> msgWrap(&*pImpl->seri, data, length);
		auto msg = msgWrap.GetMessage();
		pImpl->pendingMethodCalls.push_back({ msg.event, id });
		pImpl->logger->Info("Adding GMEvent (index=%d) to queue", msgWrap.GetMessage().event.index());
		break;
	}
	case MessageId::GMHandshake: {
		bool handshakeOk = true;

		MsgWrapper<msg::GMHandshake> msgWrap(&*pImpl->seri, data, length);
		auto &pass = msgWrap.GetMessage().devPassword;
		if (pass != pImpl->devPassword) {
			pImpl->logger->Error("Handshake with gamemode failed serverId='%s', devPassword='%s'", msgWrap.GetMessage().serverId.data(), pass.data());
			handshakeOk = false;
		}
		else if (gamemode.auth) {
			pImpl->logger->Warning("Already handshaked");
		}
		else {
			gamemode.auth = true;
			auto front = msgWrap.GetMessage().frontEnd;
			if (front.data.empty()) {
				pImpl->logger->Warning("Empty frontEndFiles from gmid %d", (int)id);
			}
			else {
				pImpl->pendingFrontEndFiles.push_back(front);
				pImpl->logger->Notice("Adding frontEndFiles (size=%d) to queue from gmid %d", (int)front.data.size(), (int)id);
			}
			pImpl->logger->Notice("Successfull handshake with gmid %d", (int)id);
		}

		// Send ScriptInit event
		evn::ScriptInit scriptInit;
		scriptInit.propertiesByEntityName = pImpl->propertiesByEntityName;
		if (!handshakeOk) {
			scriptInit.error = true;
			scriptInit.propertiesByEntityName.clear();
		}
		MsgWrapper<msg::GMEvent> msgWrapper(&*pImpl->seri, msg::GMEvent{ scriptInit });
		auto[data, length] = msgWrapper.GetBinary();
		pImpl->server->Send(id, data, length, true);

		break;
	}
	default:
		pImpl->logger->Error("Non-GM* MessageId from gamemode (id=%d, length=%d)", (int)data[0], (int)length);
		break;
	}
}