#include "Properties.h"

class PropertyAny : public Properties::IPropertyAny {
public:
	using Ptr = std::unique_ptr<IPropertyValue>;

	template <class Prop> 
	PropertyAny(Prop *p) {
		this->name = p->GetName();

		this->toApiFormat = [p](const Ptr &value) {
			auto valueConcrete = dynamic_cast<typename Prop::ValueType *>(value.get());
			assert(valueConcrete);
			if (valueConcrete) {
				return p->ToApiFormat(*valueConcrete);
			}
			return json(nullptr);
		};
		this->fromApiFormat = [p](const json &j) {
			auto valueConcrete = p->FromApiFormat(j);
			return Ptr(new decltype(valueConcrete)(valueConcrete));
		};

		this->toDbFormat = [p](const Ptr &value) {
			auto valueConcrete = dynamic_cast<typename Prop::ValueType *>(value.get());
			assert(valueConcrete);
			if (valueConcrete) {
				return p->ToDbFormat(*valueConcrete);
			}
			return json(nullptr);
		};
		this->fromDbFormat = [p](const json &j) {
			auto valueConcrete = p->FromDbFormat(j);
			return Ptr(new decltype(valueConcrete)(valueConcrete));
		};
		this->getValueFlags = [p](const Ptr &value) {
			auto valueConcrete = dynamic_cast<typename Prop::ValueType *>(value.get());
			assert(valueConcrete);
			if (valueConcrete) {
				return p->GetValueFlags(*valueConcrete);
			}
			return 0;
		};
		this->getFlags = [p] {
			return p->GetFlags();
		};
		this->getAffectsGrid = [p](const Ptr &value) -> std::optional<std::pair<int16_t, int16_t>> {
			auto valueConcrete = dynamic_cast<typename Prop::ValueType *>(value.get());
			assert(valueConcrete);
			if (valueConcrete) {
				return p->GetAffectsGrid(*valueConcrete);
			}
			return std::nullopt;
		};
		this->needsToBeSaved = [p](const std::optional<Ptr> &prevSaved, const Ptr &newValue) {
			auto valueConcrete = dynamic_cast<typename Prop::ValueType *>(newValue.get());
			assert(valueConcrete);
			std::optional<typename Prop::ValueType> valueConcretePrev;
			if (prevSaved.has_value()) {
				auto p = dynamic_cast<typename Prop::ValueType *>(prevSaved->get());
				assert(p);
				if (p) {
					valueConcretePrev = *p;
				}
			}
			bool res = valueConcrete && p->NeedsToBeSaved(valueConcretePrev, *valueConcrete);
			return res;
		};
		this->getValueForcesEntityCaching = [p] {
			auto v = p->GetValueForcesEntityCaching();
			if (v == std::nullopt) return std::optional<Ptr>(std::nullopt);
			std::optional<Ptr> res = Ptr(new typename Prop::ValueType(*v));
			return res;
		};
		this->toBinary = [p](Viet::MessageSerializer &seri, const Ptr& value, uint8_t *&outData, size_t &outLength) {
			auto valueConcrete = dynamic_cast<typename Prop::ValueType *>(value.get());
			assert(valueConcrete);
			if (valueConcrete) {
				return p->ToBinary(seri, *valueConcrete, outData, outLength);
			}
			outData = nullptr;
			outLength = 0;
		};
		this->fromBinary = [p](Viet::MessageSerializer &seri, const uint8_t *data, size_t length) {
			using T = typename Prop::ValueType;
			return Ptr(new T(p->FromBinary(seri, data, length)));
		};
		this->getMethods = [p] {
			Methods methods;
			auto concreteMethods = p->GetMethods();

			auto copyMethods = [](const auto &source, auto &dest) {
				for (auto &m : source) {
					dest.push_back({ m.name, [m](auto &value, const PropertyMethodArguments &args) -> json {
						auto valueConcrete = dynamic_cast<typename Prop::ValueType *>(value.get());
						assert(valueConcrete);
						if (valueConcrete) {
							return m.fn(*valueConcrete, args);
						}
						return json(nullptr);
					} });
				}
			};
			copyMethods(concreteMethods.apiMethods, methods.apiMethods);
			copyMethods(concreteMethods.clientMethods, methods.clientMethods);
			copyMethods(concreteMethods.apiMethodsConst, methods.apiMethodsConst);
			copyMethods(concreteMethods.clientMethodsConst, methods.clientMethodsConst);

			return methods;
		};
		this->getDefaultValue = [p] {
			return p->GetDefaultValue().Clone();
		};
	}

	const char *GetName() noexcept override {
		return this->name;
	}

	int GetValueFlags(const Ptr &value) noexcept override {
		return this->getValueFlags(value);
	}

	int GetFlags() noexcept override {
		return this->getFlags();
	}

	json ToApiFormat(const Ptr &value) noexcept override {
		return this->toApiFormat(value);
	}

	Ptr FromApiFormat(const json &j) override {
		return this->fromApiFormat(j);
	}

	json ToDbFormat(const Ptr &value) noexcept override {
		return this->toDbFormat(value);
	}

	Ptr FromDbFormat(const json &j) override {
		return this->fromDbFormat(j);
	}

	std::optional<std::pair<int16_t, int16_t>> GetAffectsGrid(const Ptr &value) noexcept override {
		return this->getAffectsGrid(value);
	}

	bool NeedsToBeSaved(const std::optional<Ptr> &lastSavedValue, const Ptr &currentValue) noexcept override {
		return this->needsToBeSaved(lastSavedValue, currentValue);
	}

	std::optional<Ptr> GetValueForcesEntityCaching() noexcept override {
		return this->getValueForcesEntityCaching();
	}

	void ToBinary(Viet::MessageSerializer &seri, Viet::SeriToUse seriToUse, const Ptr& value, uint8_t *&outData, size_t &outLength) noexcept override {
		return this->toBinary(seri, seriToUse, value, outData, outLength);
	}

	Ptr FromBinary(Viet::MessageSerializer &seri, Viet::SeriToUse seriToUse, const uint8_t *in, size_t length) noexcept override {
		return this->fromBinary(seri, seriToUse, in, length);
	}

	Methods GetMethods() noexcept override {
		return this->getMethods();
	}

	Ptr GetDefaultValue() noexcept override {
		return this->getDefaultValue();
	}

private:
	const char *name = "";
	std::function<json(const Ptr &)> toApiFormat, toDbFormat;
	std::function<Ptr(const json &)> fromApiFormat, fromDbFormat;
	std::function<int(const Ptr &)> getValueFlags;
	std::function<int()> getFlags;
	std::function<std::optional<std::pair<int16_t, int16_t>>(const Ptr &)> getAffectsGrid;
	std::function<void(Viet::MessageSerializer &, Viet::SeriToUse seriToUse, const Ptr &, uint8_t *&, size_t &)> toBinary;
	std::function<Ptr(Viet::MessageSerializer &, Viet::SeriToUse seriToUse, const uint8_t *, size_t)> fromBinary;
	std::function<bool(const std::optional<Ptr> &, const Ptr &)> needsToBeSaved;
	std::function<std::optional<Ptr>()> getValueForcesEntityCaching;
	std::function<Methods()> getMethods;
	std::function<Ptr()> getDefaultValue;
};

struct Properties::Impl {
	PropMap map;
};

Properties::IPropertyAny *Properties::GetByName(const char *name) noexcept {
	try {
		return &*pImpl->map.at(name);
	}
	catch (...) {
		return nullptr;
	}
}

void Properties::ForEach(const ForEachCb &cb) noexcept {
	for (auto &[name, prop] : pImpl->map) {
		cb(name.data(), &*prop);
	}
}

const std::vector<const char *> &Properties::GetPropertyNames() noexcept {
	struct Data {
		Data(Properties &props) {
			props.ForEach([this](const char *name, auto prop) {
				this->names.push_back(name);
			});
			std::sort(this->names.begin(), this->names.end(), [](char const *lhs, char const *rhs) { 
				return strcmp(lhs, rhs) < 0; 
			});
		}
		std::vector<const char *> names;
	};
	static Data data(*this);
	assert(data.names.size() > 0);
	return data.names;
}

void Properties::Init(PropMap map) {
	pImpl.reset(new Impl);
	pImpl->map = map;
}