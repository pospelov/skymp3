#pragma once
#include <optional>
#include <viet/PropertyValue.h>

namespace Viet {
	struct WorldStateValue {
		IPropertyValue *value = nullptr;
		uint64_t numChanges = 0;

		template <class PropValue>
		std::optional<PropValue> As() noexcept {
			auto ptr = dynamic_cast<PropValue *>(this->value);
			if (!ptr) return std::nullopt;
			return *ptr;
		}
	};
}