#pragma once
#include <map>
#include <vector>

struct FrontEndFiles {
	std::map<std::string, std::vector<uint8_t>> data;

	template <class Archive>
	void serialize(Archive &ar, unsigned int version) {
		ar & data;
	}
};