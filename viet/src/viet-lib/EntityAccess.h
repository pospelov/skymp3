#include <viet/Entity.h>

class Properties;

class EntityAccess : public Viet::Entity {
public:
	static Properties *GetPropList_(const Viet::Entity &entity) {
		auto &entityAccess = (const EntityAccess &)entity;
		return (Properties *)entityAccess.GetPropList();
	}

	static const std::vector<Viet::Property> &GetProperties_(const Viet::Entity &entity) {
		auto &entityAccess = (const EntityAccess &)entity;
		return entityAccess.GetProperties();
	}
};