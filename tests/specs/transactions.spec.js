it('Selector: Instance', async () => {
  await setup();

  let actor1 = await remoteServer.createActor().setPos(1, 1, 1).setAngle(0, 0, 90);
  let actor2 = await remoteServer.createActor().setPos(2, 2, 2).setAngle(0, 0, 90);
  let actor3 = await remoteServer.createActor().setPos(3, 3, 3).setAngle(0, 0, 90);

  // Set
  await remoteServer
    .for(actor1).setPos(10, 10, 10)
    .for(actor2).setPos(20, 20, 20)
    .for(actor3).setPos(30, 30, 30);
  expect(await actor1.getPos()).to.eql([10, 10, 10]);
  expect(await actor2.getPos()).to.eql([20, 20, 20]);
  expect(await actor3.getPos()).to.eql([30, 30, 30]);

  // Get
  let [pos1, pos2, pos3] = await remoteServer
    .for(actor1).getPos()
    .for(actor2).getPos()
    .for(actor3).getPos();
  expect(pos1).to.eql([10, 10, 10]);
  expect(pos2).to.eql([20, 20, 20]);
  expect(pos3).to.eql([30, 30, 30]);

  // Get multiple
  let [[_pos1, angle1], [_pos2, angle2]] = await remoteServer
    .for(actor1).getPos().getAngle()
    .for(actor2).getPos().getAngle();
    expect(_pos1).to.eql([10, 10, 10]);
    expect(_pos2).to.eql([20, 20, 20]);
    expect(angle1).to.eql([0, 0, 90]);
    expect(angle2).to.eql([0, 0, 90]);
});

it('Selector: Array of instances', async () => {
  await setup();

  let actor1 = await remoteServer.createActor().setPos(1, 1, 1).setAngle(0, 0, 10);
  let actor2 = await remoteServer.createActor().setPos(2, 2, 2).setAngle(0, 0, 20);
  let actor3 = await remoteServer.createActor().setPos(3, 3, 3).setAngle(0, 0, 30);

  // Set
  await remoteServer
    .for([actor1, actor2, actor3]).setPos(99, 99, 99);
  expect(await actor1.getPos()).to.eql([99, 99, 99]);
  expect(await actor2.getPos()).to.eql([99, 99, 99]);
  expect(await actor3.getPos()).to.eql([99, 99, 99]);

  await remoteServer
    .for(actor1).setPos(1, 1, 1)
    .for(actor2).setPos(2, 2, 2)
    .for(actor3).setPos(3, 3, 3);

  // Get
  let [[pos1, pos2, pos3]] = await remoteServer
    .for([actor1, actor2, actor3]).getPos();
  expect(pos1).to.eql([1, 1, 1]);
  expect(pos2).to.eql([2, 2, 2]);
  expect(pos3).to.eql([3, 3, 3]);

  // Get multiple
  let r = await remoteServer
    .for([actor1, actor2]).getPos().getAngle();
  let [[[_pos1, angle1], [_pos2, angle2]]] = r;
  expect(_pos1).to.eql([1, 1, 1]);
  expect(_pos2).to.eql([2, 2, 2]);
  expect(angle1).to.eql([0, 0, 10]);
  expect(angle2).to.eql([0, 0, 20]);
});

it('Selector: New instance', async () => {
  await setup();

  // Set
  let [actor] = await remoteServer
    .forNewActor().setPos(99, 99, 99);
  expect(await actor.getPos()).to.eql([99, 99, 99]);

  // Get
  let [actor2] = await remoteServer
    .forNewActor().getPos().getAngle().getDefaultInt();
    // No way, it doesn't work. Just keep returning an actor itself
  expect(await actor2.getDefaultInt()).to.eql(108);

  // Get multiple
  let [actor3a, actor3b] = await remoteServer
    .forNewActor().getPos().getAngle().getDefaultInt()
    .forNewActor().getPos().getAngle().getDefaultInt();
  expect(await actor3a.getDefaultInt()).to.eql(108);
  expect(await actor3b.getDefaultInt()).to.eql(108);
});

it('Selector: Find one', async () => {
  await setup();

  let actor1 = await remoteServer.createActor().setPos(1, 1, 1).setAngle(0, 0, 90);
  let actor2 = await remoteServer.createActor().setPos(2, 2, 2).setAngle(0, 0, 82);
  let actor3 = await remoteServer.createActor().setPos(3, 3, 3).setAngle(0, 0, 83);

  // Set
  await remoteServer
    .forActor({ pos: [1, 1, 1] }).setPos(99, 99, 99);
  expect(await actor1.getPos()).to.eql([99, 99, 99]);
  expect(await actor2.getPos()).to.eql([2, 2, 2]);
  expect(await actor3.getPos()).to.eql([3, 3, 3]);

  // Get
  let [pos] = await remoteServer
    .forActor({ pos: [2, 2, 2] }).getPos();
  expect(pos).to.eql([2, 2, 2]);

  // Get multiple
  let [angle2, angle3] = await remoteServer
    .forActor({ pos: [2, 2, 2] }).getAngle()
    .forActor({ pos: [3, 3, 3] }).getAngle()
  expect(angle2).to.eql([0, 0, 82]);
  expect(angle3).to.eql([0, 0, 83]);
});

it('Selector: Find many', async () => {
  await setup();

  let actor1 = await remoteServer.createActor().setPos(1, 1, 1).setDefaultInt(1);
  let actor2 = await remoteServer.createActor().setPos(2, 2, 2).setDefaultInt(2);
  let actor3 = await remoteServer.createActor().setPos(3, 3, 3).setDefaultInt(3);

  // Set
  await remoteServer
    .forActors({ 'pos.0': { $greaterOrEqual: 0 }}).setPos(99, 99, 99);
  expect(await actor1.getPos()).to.eql([99, 99, 99]);
  expect(await actor2.getPos()).to.eql([99, 99, 99]);
  expect(await actor3.getPos()).to.eql([99, 99, 99]);

  // Get
  let [array] = await remoteServer
    .forActors({ 'pos.0': { $greaterOrEqual: 0 }}).getDefaultInt();
  expect(array.sort()).to.eql([1,2,3]);

  // Get multiple
  let [array2] = await remoteServer
    .forActors({ 'pos.0': { $greaterOrEqual: 0 }}).getDefaultInt().getPos()
  array2 = array2.sort((x, y) => x[0] >= y[0]);
  expect(array2).to.eql([[1, [99, 99, 99]], [2, [99, 99, 99]], [3, [99, 99, 99]]]);
});

it('User built-in property', async () => {
  await setup();

  let bot = newBot();

  let [user] = await nextEvent(remoteServer, 'userEnter', 5000);
  let actor = await remoteServer.createActor().setPos(1,1,1).setEnabled(true).setCellOrWorld(111);
  expect(await user.getActor()).to.eql(null);

  await remoteServer.for(actor).setUser(user.getId());
  expect(await user.getActor()).not.to.eql(null);

  await remoteServer.for(actor).setUser('');
  expect(await user.getActor()).to.eql(null);
});


it('CustomPacket built-in property', async () => {
  await setup();

  let bot = newBot();

  let [user] = await nextEvent(remoteServer, 'userEnter', 5000);

  expect(bot.nextCustomPacket()).to.eql(null);

  let actor = await remoteServer.createActor().setPos(1,1,1).setEnabled(true).setCellOrWorld(111);
  await user.setActor(actor);
  await remoteServer.for(actor).setCustomPacket({ x: 'y' });

  await new Promise(r => setTimeout(r, 1500));

  expect(bot.nextCustomPacket()).to.eql({ x: 'y' });
  expect(bot.nextCustomPacket()).to.eql(null);
});
