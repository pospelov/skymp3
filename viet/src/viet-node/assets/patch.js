let require = global.require || global.process.mainModule.constructor._load;
let EventEmitter = require('events');

function uppercaseFirstLetter(param) {
  if (typeof param === 'string') {
    return param.charAt(0).toUpperCase() + param.slice(1);
  }
  return param;
}

function validateFindCondition(cond) {
  if (typeof cond !== 'object') {
    throw new TypeError('find condition must be object');
  }
  for (let [key, value] of Object.entries(cond)) {
    if (value === undefined) {
      throw new TypeError(`find condition must not contain undefined (${key})`);
    }
  }
}

function generateTransactionApiRedirect(self, entityList, getRedirectTarget) {
  let transMethods = ['for'];

  for (let [entityName, propNames] of Object.entries(entityList)) {
    transMethods.push('forNew' + uppercaseFirstLetter(entityName));
    transMethods.push('for' + uppercaseFirstLetter(entityName));
    transMethods.push('for' + uppercaseFirstLetter(entityName) + 's');
  }

  transMethods.forEach(name => {
    self[name] = (...args) => {
      return getRedirectTarget()[name](...args);
    }
  });
}

let RemoteServerImpl;
let instanceForward;

class User {
  constructor(ei, svr) {
    this._ei = ei;
    this._svr = svr;

    for (let [entityName, propNames] of Object.entries(svr._entityList)) {
      this['set' + uppercaseFirstLetter(entityName)] = (instance) => {
        if (instance && !instance._ei) {
          throw new TypeError(`instance must be a valid ${entityName} or null but it's ${typeof instance}`);
        }
        return svr._impl.setAttachedInstance(ei, entityName, instance ? instance._ei : null);
      };

      this['get' + uppercaseFirstLetter(entityName)] = () => {
        return new Promise((resolve, reject) => {
          let p = svr._impl.getAttachedInstance(ei, entityName);
          p.then(instEi => resolve(instEi ? new instanceForward({ ei: instEi, svr, entityName, propNames }) : null));
          p.catch(reject);
        });
      };

      this['sendCustomPacket'] = (value) => {
        return svr._impl.sendCustomPacket(ei, JSON.stringify(value));
      };
    }
  }

  getId() {
    return this._ei;
  }

  get id() {
    return this.getId();
  }
}

class InstanceStream {
  constructor(propNames, svr) {
    this._actionsHolder = { actions: [] };

    propNames.forEach(propName => {
      let isBoolean = false;
      if (propName.startsWith('is') && propName[2].toUpperCase() === propName[2]) {
        isBoolean = true;
      }

      let set = 'set' + uppercaseFirstLetter(isBoolean ? propName.substr(2) : propName);
      let _this = this;
      this[set] = function (newValue) {

        if (arguments.length > 1) {
          newValue = Array.from(arguments);
        }

        _this._actionsHolder.actions.push({ type: 'Set', name: propName, value: newValue });
        if (!this.then && this._then) this.then = this._then;
        return this;
      };

      let get = (isBoolean ? propName : 'get' + uppercaseFirstLetter(propName));
      this[get] = () => {
        this._actionsHolder.actions.push({ type: 'Get', name: propName });
        if (!this.then && this._then) this.then = this._then;
        return this;
      };
    });
  }

  _moveActions() {
    let res = this._actionsHolder.actions;
    this._actionsHolder.actions = [];
    return res;
  }
}

class InstanceCreateStream extends InstanceStream {
  constructor(...args) {
    super(...args);
  }
}

class Instance extends InstanceStream {
  constructor(options) {
    super(options.propNames, options.svr);
    this._ei = options.ei;
    this._svr = options.svr;
    this._entityName = options.entityName;
  }

  getId() {
    return this._ei;
  }

  get id() {
    return this.getId();
  }

  _then(onFulfilled, onRejected) {
    let actions = this._moveActions();
    let p = this._svr._impl.manipulateInstance(this._entityName, actions, this._ei);
    p.then((results) => {
      if (results.instance !== this._ei) {
        throw new Error('Wow!');
      }
      onFulfilled(results.returnedValues.length === 1 ? results.returnedValues[0] : results.returnedValues);
    }).catch(onRejected);
  }
}

instanceForward = Instance;

class TransactionInstanceStream extends InstanceStream {
  constructor(propNames, svr, parentTransaction) {
    super(propNames, svr);
    generateTransactionApiRedirect(this, svr._entityList, () => parentTransaction);
    this._trans = parentTransaction;
  }

  _flush(entryId) {
    let entry = this._trans._entries[entryId === undefined ? this._trans._entries.length - 1 : entryId];
    if (!entry.actions) entry.actions = [];
    entry.actions = entry.actions.concat(this._moveActions());
  }

  _then(...args) {
    this._flush();
    return this._trans._then(...args);
  }
}

class Transaction {
  constructor(svr) {
    this._entries = [];
    this._svr = svr;
    this._streams = [];
    this._generateTransactionApi();
  }

  _then(onFulfilled, onRejected) {
    let trans = { entries: this._entries };

    let formatEntryRes = (entry, i) => {
      let selType = trans.entries[i].selectorType;
      if (selType === 'instance' || selType === 'findOne') {
        //console.log({entry:JSON.stringify(entry)})
        return entry[0].returnedValues.length === 1 ? entry[0].returnedValues[0] : entry[0].returnedValues;
      }
      if (selType === 'arrayOfInstances' || selType === 'findMany') {
        let r = entry.map(v => v.returnedValues.length === 1 ? v.returnedValues[0] : v.returnedValues);
        return r.length === 1 ? r[0] : r;
      }
      if (selType === 'newInstance') {
        let entityName = trans.entries[0].selector;
        let [, propNames] = Object.entries(this._svr._entityList).find(pair => pair[0] === entityName);
        return new Instance({
          ei: entry[0].instance,
          entityName,
          propNames,
          svr: this._svr
        });
      }
      return entry;
    };

    //console.log(JSON.stringify(trans, null, ' '))
    let p = this._svr._impl.runTransaction(trans);
    p.then((transRes) => {
      onFulfilled(transRes.entries.map((entry, i) => formatEntryRes(entry.actionsResults, i)));
    }).catch(onRejected);
  }

  _newStream(entityName) {
    let propNames;
    for (let [iEntityName, iPropNames] of Object.entries(this._svr._entityList)) {
      if (iEntityName === entityName) propNames = iPropNames;
    }
    this._streams.forEach((stream, i) => stream._flush(i));
    let stream = new TransactionInstanceStream(propNames, this._svr, this);
    this._streams.push(stream);
    return stream;
  }

  _forArrayInstances(arr) {
    if (arr.length === 0) {
      throw new TypeError(`transaction.for: Empty array`);
    }
    if (arr.filter(v => v instanceof Instance).length !== arr.length) {
      throw new TypeError('transaction.for: Array of instances expected');
    }
    this._entries.push({
      selectorType: 'arrayOfInstances',
      selector: arr.map(v => { return {entityName: v._entityName, ei: v._ei} })
    });
    return this._newStream(arr[0]._entityName);
  }

  _forInstance(instance) {
    if (!(instance instanceof Instance)) {
      throw new TypeError('transaction.for: Expected argument to be Instance');
    }
    this._entries.push({
      selectorType: 'instance',
      selector: {entityName: instance._entityName, ei: instance._ei}
    });
    return this._newStream(instance._entityName);
  }

  _forNewInstance(entityName) {
    this._entries.push({
      selectorType: 'newInstance',
      selector: entityName
    });
    return this._newStream(entityName);
  }

  _forFindOne(entityName, filter) {
    this._entries.push({
      selectorType: 'findOne',
      selector: { entityName, filter }
    });
    return this._newStream(entityName);
  }

  _forFindMany(entityName, filter, limit) {
    this._entries.push({
      selectorType: 'findMany',
      selector: { entityName, filter, limit }
    });
    return this._newStream(entityName);
  }

  _generateTransactionApi() {
    this['for'] = (any) => {
      this.then = this._then;
      if (Array.isArray(any)) {
        return this._forArrayInstances(any);
      }
      if (any instanceof Instance) {
        return this._forInstance(any);
      }
      throw new TypeError('transaction.for: Bad argument');
    };

    let entityList = this._svr._entityList;
    for (let [entityName, propNames] of Object.entries(entityList)) {
      this['forNew' + uppercaseFirstLetter(entityName)] = () => {
        this.then = this._then;
        return this._forNewInstance(entityName);
      };

      let findOneName = 'for' + uppercaseFirstLetter(entityName);
      this[findOneName] = (filter) => {
        this.then = this._then;
        return this._forFindOne(entityName, filter);
      };

      let findManyName = 'for' + uppercaseFirstLetter(entityName) + 's';
      this[findManyName] = (filter, limit) => {
        this.then = this._then;
        if (!limit) limit = 0;
        if (typeof limit !== 'number') {
          throw new TypeError(`transaction.${findManyName}: Bad limit`);
        }
        return this._forFindMany(entityName, filter, limit);
      };
    }
  }

};

class RemoteServer extends EventEmitter {
  constructor() {
    super();
    this._impl = new RemoteServerImpl;

    this._impl.on('connect', (err, entityList) => {
      this._processEntityList(entityList);
      this._generateTransactionApi(entityList);
      this.emit('connect', err);
    });

    this._impl.on('userEnter', (userEi) => {
      this.emit('userEnter', new User(userEi, this));
    });

    this._impl.on('userExit', (userEi) => {
      this.emit('userExit', new User(userEi, this));
    });

    this._impl.on('userCustomPacket', (userEi, contentStr) => {
      let content;
      try {
        content = JSON.parse(contentStr);
      }
      catch(e) {
        content = {};
      }
      this.emit('userCustomPacket', new User(userEi, this), content);
    });
  }

  connect(options) {
    if (typeof options !== 'object') {
      throw new TypeError('Expected options object');
    }
    let { ip, port, serverId, devPassword, frontEnd } = options;
    if (typeof ip !== 'string') {
      throw new TypeError('options.ip must be string');
    }
    if (typeof port !== 'number') {
      throw new TypeError('options.port must be number');
    }
    if (typeof serverId !== 'string') {
      throw new TypeError('options.serverId must be string');
    }
    if (typeof devPassword !== 'string') {
      throw new TypeError('options.devPassword must be string');
    }
    if (frontEnd && typeof frontEnd !== 'object') {
      throw new TypeError('options.frontEnd must be object');
    }

    this._impl.connect(ip, port, serverId, devPassword, frontEnd)
  }

  kill() {
    this._impl.kill();
  }

  _processEntityList(entityList) {
    this._entityList = entityList;
    for (let [entityName, propNames] of Object.entries(entityList)) {

      let find = 'find' + uppercaseFirstLetter(entityName) + 's';
      this[find] = (condition, limit) => {
        validateFindCondition(condition);
        if (!limit) limit = 0;
        let p = this._impl.findInstance(entityName, condition, limit);
        return new Promise((resolve, reject) => {
          p.then((instancesFound) => {
            resolve(instancesFound.map(ei => new Instance({ ei, entityName, propNames, svr: this })));
          }).catch(reject);
        });
      };

      let findOne = 'find' + uppercaseFirstLetter(entityName);
      this[findOne] = async (condition) => {
        return (await (this[find])(condition, 1))[0];
      };

      let create = 'create' + uppercaseFirstLetter(entityName);
      this[create] = () => {
        let stream = new InstanceCreateStream(propNames, this);
        stream.then = (onFulfilled, onRejected) => {
          let p = this._impl.manipulateInstance(entityName, stream._moveActions());
          p.then((results) => {
            let inst = new Instance({ ei: results.instance, entityName, propNames, svr: this });
            onFulfilled(inst);
          }).catch(onRejected);
        };
        return stream;
      }
    }
  }

  _generateTransactionApi(entityList) {
    generateTransactionApiRedirect(this, entityList, () => new Transaction(this));
  }
};

applyPatch = (moduleExports) => {
  RemoteServerImpl = moduleExports.RemoteServer;
  moduleExports.RemoteServer = RemoteServer;
  return moduleExports;
};
