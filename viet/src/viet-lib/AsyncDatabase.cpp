#include <thread>
#include <mutex>
#include <atomic>
#include <vector>
#include <functional>
#include <chrono>

#include "AsyncDatabase.h"

namespace {
	struct Task {
		std::function<void()> task;
	};
}

struct AsyncDatabase::Impl {
	const std::shared_ptr<Viet::NoSqlDatabase::INoSqlDatabase> db;
	std::unique_ptr<std::thread> thr;
	std::atomic<bool> dtor = false;

	struct {
		std::vector<Task> tasks;
		std::mutex m;
	} share;

	struct {
		std::vector<std::function<void()>> tickTasks;
		std::mutex m;
	} share2;

	void ThrLoop();
	void TickTask(std::function<void()> f) noexcept;
};

void AsyncDatabase::Impl::ThrLoop() {
	while (!this->dtor) {
		std::this_thread::sleep_for(std::chrono::milliseconds(1));

		decltype(this->share.tasks) tasksCopy;
		{
			std::lock_guard l(this->share.m);
			tasksCopy = std::move(this->share.tasks);
			this->share.tasks.clear();
		}

		for (auto &t : tasksCopy) t.task();
	}
}

void AsyncDatabase::Impl::TickTask(std::function<void()> f) noexcept {
	std::lock_guard l(this->share2.m);
	this->share2.tickTasks.push_back(f);
}

AsyncDatabase::AsyncDatabase(std::shared_ptr<Viet::NoSqlDatabase::INoSqlDatabase> db) {
	pImpl.reset(new Impl{ db });
	auto pImpl_ = pImpl;
	pImpl->thr.reset(new std::thread([pImpl_] { pImpl_->ThrLoop(); }));
}

AsyncDatabase::~AsyncDatabase() {
	pImpl->dtor = true;
	pImpl->thr->join();
}

void AsyncDatabase::Tick() {
	auto &share2 = pImpl->share2;
	decltype(share2.tickTasks) tasksCopy;
	{
		std::lock_guard l(share2.m);
		tasksCopy = std::move(share2.tickTasks);
		share2.tickTasks.clear();
	}

	for (auto &t : tasksCopy) t();
}

void AsyncDatabase::Find(const std::string &collName, json filter, const FindCallback &cb) noexcept {
	assert(filter.is_object());
	std::lock_guard l(pImpl->share.m);
	Task t{ [=] {
		auto coll = pImpl->db->GetCollection(collName.data());
		auto r = coll->Find(filter);
		std::lock_guard l(pImpl->share2.m);
		pImpl->share2.tickTasks.push_back([r, cb] { cb(r); });
	} };
	pImpl->share.tasks.push_back(t);
}

void AsyncDatabase::FindOne(const std::string &collName, json filter, const FindOneCallback &cb) noexcept {
	assert(filter.is_object());
	std::lock_guard l(pImpl->share.m);
	Task t{ [=] {
		auto coll = pImpl->db->GetCollection(collName.data());
		auto r = coll->FindOne(filter);
		std::lock_guard l(pImpl->share2.m);
		pImpl->share2.tickTasks.push_back([r, cb] { cb(r); });
	} };
	pImpl->share.tasks.push_back(t);
}

void AsyncDatabase::InsertOne(const std::string &collName, json doc, const InsertOneCallback &cb) noexcept {
	assert(doc.is_object());
	std::lock_guard l(pImpl->share.m);
	Task t{ [=] {
		auto coll = pImpl->db->GetCollection(collName.data());
		coll->InsertOne(doc);
		std::lock_guard l(pImpl->share2.m);
		pImpl->share2.tickTasks.push_back(cb);
	} };
	pImpl->share.tasks.push_back(t);
}

void AsyncDatabase::UpdateOne(const std::string &collName, json filter, json upd, bool upsert, const InsertOneCallback &cb) noexcept {
	assert(filter.is_object());
	assert(upd.is_object());
	std::lock_guard l(pImpl->share.m);
	Task t{ [=] {
		auto coll = pImpl->db->GetCollection(collName.data());
		coll->UpdateOne(filter, upd, upsert);
		std::lock_guard l(pImpl->share2.m);
		pImpl->share2.tickTasks.push_back(cb);
	} };
	pImpl->share.tasks.push_back(t);
}

void AsyncDatabase::UpdateMany(const std::string &collName, std::vector<std::pair<json, json>> filterAndUpdate, bool upsert, const UpdateManyCallback &cb) noexcept {
	std::lock_guard l(pImpl->share.m);
	Task t{ [=] {
		auto coll = pImpl->db->GetCollection(collName.data());
		coll->UpdateMany(filterAndUpdate, upsert);
		std::lock_guard l(pImpl->share2.m);
		pImpl->share2.tickTasks.push_back(cb);
	} };
	pImpl->share.tasks.push_back(t);
}

void AsyncDatabase::Drop(const std::string &collName, const DropCallback &cb) noexcept {
	std::lock_guard l(pImpl->share.m);
	Task t{ [=] {
		auto coll = pImpl->db->GetCollection(collName.data());
		coll->Drop();
		std::lock_guard l(pImpl->share2.m);
		pImpl->share2.tickTasks.push_back(cb);
	} };
	pImpl->share.tasks.push_back(t);
}