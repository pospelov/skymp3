#pragma once
#include <cassert>
#include <cmrc/cmrc.hpp>

CMRC_DECLARE(assets);

inline std::pair<const char *, size_t> assets_get_file(const char *fileName) {
	auto fs = cmrc::assets::get_filesystem();
	assert(fs.exists(fileName));
	assert(fs.is_file(fileName));
	if (!fs.exists(fileName) || !fs.is_file(fileName)) {
		return { "", 0 };
	}
	auto f = fs.open(fileName);
	return { f.begin(), f.end() - f.begin() };
}