#pragma once
#include <functional>
#include <vector>
#include <memory>

#include <viet/Entity.h>
#include <viet/Networking.h>
#include <viet/Serialization.h>
#include <viet/Logging.h>
#include <viet/NoSqlDatabase.h>

namespace Viet {
	class Server {
	public:
		struct Settings {
			const char *devPassword;
			int maxPlayers;

			std::vector<Entity> entities;
			Networking::Plugin netPlugin;
			Networking::Plugin gamemodeNetPlugin;
			Serialization::Plugin seriPlugin;
			Viet::Logging::Plugin loggingPlugin;
			Viet::NoSqlDatabase::Plugin nosqlPlugin;
		};

		Server(const Settings &settings);
		~Server();

		void Tick() noexcept;
		int GetNumActiveGamemodes() const noexcept;

	private:
		struct Impl;
		const std::shared_ptr<Impl> pImpl;
	};
}