#pragma once
#include <string>

namespace Viet {
	struct Instance {
		Instance() = default;
		Instance(const std::string &uniqueId_) : uniqueId(uniqueId_) {
		}

		// Must contain a valid instance id (i.e. "qwertyuiopasdfghjk:1000")
		// Empty/invalid values are not allowed
		std::string uniqueId;

		template <class Archive>
		void serialize(Archive &ar, unsigned int version) {
			ar & uniqueId;
		}
	};
}