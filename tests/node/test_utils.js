let path = require('path');
let childProcess = require('child_process');
let fs = require('fs');
let EventEmitter = require('events');

function getServerPath() {
  return process.env.SKYMP_TEST_SERVER_PATH;
}

function getApiDllPath() {
  return process.env.SKYMP_TEST_NODE_API_PATH;
}

function getConfigPath() {
  return path.join(process.env.SKYMP_TEST_TEMP_DIR, 'server_cfg.json');
}

function getMongoUri() {
  return process.env.SKYMP_TEST_MONGO_URI;
}

function propsMatches(wstInstance, propValues) {
  if (!wstInstance) return false;
  for (let propName in propValues) {
    if (JSON.stringify(wstInstance.getValue(propName)) !== JSON.stringify(propValues[propName])) {
      return false;
    }
  }
  return true;
}

function getInstancesMatchingPropValues(bot, entityName, propValues) {
  let res;
  bot.processWorldState(wst => {
    res = wst.getInstances(entityName).filter(i => propsMatches(i, propValues));
  });
  return res;
}

see = async(bot, entityName, propValues, timeout = 2000) => {
  let timeoutMoment = Date.now() + timeout;
  while (Date.now() < timeoutMoment) {
    await new Promise(r => setTimeout(r, 1));
    let r = getInstancesMatchingPropValues(bot, entityName, propValues);
    if (r && r.length) return;
  }
  throw new Error(`failed to see ${entityName} with properties ${JSON.stringify(propValues)} in ${timeout} ms`);
};

notSee = async(bot, entityName, propValues, timeout = 2000) => {
  let timeoutMoment = Date.now() + timeout;
  while (Date.now() < timeoutMoment) {
    await new Promise(r => setTimeout(r, 1));
    let r = getInstancesMatchingPropValues(bot, entityName, propValues);
    if (!r || !r.length) return;
  }
  throw new Error(`failed to not to see ${entityName} with properties ${JSON.stringify(propValues)} in ${timeout} ms`);
};

newBot = (userId) => {
  let bot = new skymp.Bot;
  bot.connect('127.0.0.1', server.getConfig().port, userId);
  bots.push(bot);
  return bot;
}

serverLogLine = async (server, lineRegex, timeout) => {
  if (!timeout) timeout = 10000;
  let p = new Promise((resolve) => {
    server.on('stdout', (str) => {
      if (str.match(lineRegex)) {
        resolve();
      }
    });
  });
  let p2 = new Promise((resolve) => {
    setTimeout(() => resolve('___expired___'), timeout);
  });

  let racePromise = Promise.race([p, p2]);
  let result = await racePromise;
  if (result === '___expired___') {
    throw new Error(`serverLogLine('${lineRegex}'): ${timeout} ms timeout expired`);
  }
  return result;
}

nextEvent = async (emitter, evn, timeout_) => {
  if (!emitter || !emitter.on) throw new Error('Bad emitter');

  let timeout = 7500;
  if (timeout_) timeout = timeout_;

  let p = new Promise((resolve) => {
    emitter.on(evn, function () {
      resolve(arguments);
    });
  });

  let p2 = new Promise(resolve => {
    setTimeout(() => { resolve('___expired___') }, timeout);
  });

  let racePromise = Promise.race([p, p2]);
  let result = await racePromise;
  if (result === '___expired___') {
    throw new Error(`nextEvent('${evn}'): ${timeout} ms timeout expired`);
  }
  return result;
}

createServer = (cfg) => {
  let serverObject = new EventEmitter;
  let linesHolder = { data: '' };

  if (!cfg) cfg = {};
  if (!cfg.maxPlayers) cfg.maxPlayers = 2000;
  if (!cfg.port) cfg.port = 7777;
  if (!cfg.serverId) {
    cfg.serverId = 'test_server_' + Date.now();
  }
  if (!cfg.devPassword) cfg.devPassword = 'qwerty';
  if (!cfg.mongoUri) cfg.mongoUri = getMongoUri();

  let configPath = getConfigPath();
  fs.writeFileSync(configPath, JSON.stringify(cfg));

  let p = childProcess.spawn(getServerPath(), [configPath], {

  });
  p.stdout.on('data', (data) => {
    let s = data.toString();
    if (s.length == 0) return;
    s = s.trim();
    linesHolder.data += s + '\n';

    let tokens = s.split('\n');
    tokens.forEach(token => {
      console.log(`Server says -> ${token}`);
    });
    serverObject.emit('stdout', s, linesHolder.data);
  });
  p.on('exit', (code) => {
    if (code === null) { // terminated by script

    }
    else if (code !== 0) {
      process.exit(10000 + code);
    }
  });

  serverObject.kill = () => {
    p.kill();
  };

  serverObject.getConfig = () => {
    return cfg;
  };

  return serverObject;
}

skymp = getApiDllPath() ? require(getApiDllPath()) : null;
