#pragma once
#include <viet/Networking.h>

class Reconnector : public Viet::Networking::IClient {
public:
	// Must not throw
	using CreateClientFn = std::function<Viet::Networking::IClient *()>;

	// IClient
	void Tick(const OnPacket &onPacket) noexcept override;
	void Send(const unsigned char *packet, size_t length, bool reliable) noexcept override;

	Reconnector(CreateClientFn createClient) noexcept;
	virtual ~Reconnector();

private:
	struct Impl;
	Impl *const pImpl;
};