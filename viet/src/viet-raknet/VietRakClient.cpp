﻿#include <stdexcept>

#include <raknet/MessageIdentifiers.h>
#include <raknet/RakPeer.h>
#include <raknet/PacketPriority.h>
#include <raknet/ReliabilityLayer.h>
#include <raknet/BitStream.h>

#include <viet-raknet/VietRakNet.h>

using namespace Viet::Networking;
using RakGUID = RakNet::RakNetGUID;

struct RakClient::Impl {
	std::string ip;
	unsigned short port;
	RakGUID serverGuid = RakNet::UNASSIGNED_CRABNET_GUID;
	std::unique_ptr<RakNet::RakPeer> peer;
	std::unique_ptr<RakNet::SocketDescriptor> socket;
};

RakClient::RakClient(const char *ipAddress, unsigned short port, int timeoutTimeMs) : pImpl(new Impl) {
	pImpl->ip = ipAddress;
	pImpl->port = port;

	pImpl->peer.reset(new RakNet::RakPeer);
	pImpl->socket.reset(new RakNet::SocketDescriptor(0, nullptr));
	const auto res = pImpl->peer->Startup(1, &*pImpl->socket, 1);
	if (res != RakNet::StartupResult::CRABNET_STARTED) {
		throw std::runtime_error("peer startup failed with code " + std::to_string((int)res));
	}
	const auto conRes = pImpl->peer->Connect(ipAddress, port, "", 0);
	if (conRes != RakNet::ConnectionAttemptResult::CONNECTION_ATTEMPT_STARTED) {
		throw std::runtime_error("peer connect failed with code " + std::to_string((int)res));
	}
	pImpl->peer->SetTimeoutTime(timeoutTimeMs, {});
}

RakClient::~RakClient() {
	pImpl->peer->Shutdown(0);
	pImpl->peer.reset();
	pImpl->socket.reset();
	delete pImpl;
}

void RakClient::Tick(const OnPacket &onPacket) noexcept {
	RakNet::Packet *packet;
	Packet p;
	for (packet = pImpl->peer->Receive(); packet; pImpl->peer->DeallocatePacket(packet), packet = pImpl->peer->Receive()) {
		if (pImpl->serverGuid == RakNet::UNASSIGNED_CRABNET_GUID) {
			pImpl->serverGuid = packet->guid;
		}
		const auto packetId = packet->data[0];
		const char *error = "";
		switch (packetId) {
		case ID_ALREADY_CONNECTED:
			p.type = PacketType::ConnectionDenied;
			error = "Already connected";
			break;
		case ID_CONNECTION_ATTEMPT_FAILED:
			p.type = PacketType::ConnectionFailed;
			break;
		case ID_CONNECTION_BANNED:
			p.type = PacketType::ConnectionDenied;
			error = "Banned";
			break;
		case ID_INVALID_PASSWORD:
			p.type = PacketType::ConnectionDenied;
			error = "Invalid password";
			break;
		case ID_INCOMPATIBLE_PROTOCOL_VERSION:
			p.type = PacketType::ConnectionDenied;
			error = "Incompatible protocol version";
			break;
		case ID_IP_RECENTLY_CONNECTED:
			p.type = PacketType::ConnectionDenied;
			error = "IP recently connected";
			break;
		case ID_CONNECTION_REQUEST_ACCEPTED:
			p.type = PacketType::ConnectionAccepted;
			break;
		case ID_NO_FREE_INCOMING_CONNECTIONS:
			p.type = PacketType::ConnectionDenied;
			error = "No free incoming connections";
			break;
		case ID_CONNECTION_LOST:
		case ID_DISCONNECTION_NOTIFICATION:
			p.type = PacketType::Disconnect;
			break;
		default:
			if (packetId >= MinPacketId) {
				p.data = packet->data;
				p.length = packet->length;
				p.type = PacketType::Message;
			}
			break;
		}
		if (p.type != PacketType::Invalid) {
			onPacket(p);
		}
	}
}

void RakClient::Send(const unsigned char *packet, size_t length, bool reliable) noexcept {
	pImpl->peer->Send((const char *)packet, length, MEDIUM_PRIORITY, reliable ? RELIABLE : UNRELIABLE, 0, pImpl->serverGuid, false);
}