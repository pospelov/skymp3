#pragma once
#include <cstdint>
#include <string>
#include <functional>

namespace Viet {
	namespace Serialization {
		class ISerializer {
		public:
			static constexpr auto g_maxStr = 5000;

			virtual void ResetWrite() noexcept = 0;
			virtual void WriteBool(bool v) noexcept = 0;
			virtual void WriteInt8(int8_t v) noexcept = 0;
			virtual void WriteInt16(int16_t v) noexcept = 0;
			virtual void WriteInt32(int32_t v) noexcept = 0;
			virtual void WriteInt64(int64_t v) noexcept = 0;
			virtual void WriteFloat(float v) noexcept = 0;
			virtual void WriteDouble(double v) noexcept = 0;
			virtual void WriteString(const char *str) noexcept = 0;
			virtual void GetWrittenData(uint8_t *&outData, size_t &outLength) noexcept = 0;

			virtual void ResetRead(const uint8_t *data, size_t length) noexcept = 0;
			virtual bool ReadBool() noexcept = 0;
			virtual int8_t ReadInt8() noexcept = 0;
			virtual int16_t ReadInt16() noexcept = 0;
			virtual int32_t ReadInt32() noexcept = 0;
			virtual int64_t ReadInt64() noexcept = 0;
			virtual float ReadFloat() noexcept = 0;
			virtual double ReadDouble() noexcept = 0;
			virtual std::string ReadString() noexcept = 0;

			virtual ISerializer *GetSubSerializer() noexcept = 0;

			virtual ~ISerializer() = default;
		};

		struct Plugin {
			std::function<ISerializer *()> serializerFactory;
		};
	}
}