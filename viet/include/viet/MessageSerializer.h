#pragma once
#pragma once
#include <sstream>
#include <array>
#include <vector>
#include <optional>
#include <variant>
#include <map>
#include <type_traits>
#include <cassert>
#include <viet/Serialization.h>

#define SERIALIZATION_SPLIT_MEMBER template <class A> void serialize(A &a, int) noexcept {\
	if constexpr (A::is_load) {\
		this->load(a, 0);\
	}\
	if constexpr (!A::is_load)  {\
		this->save(a, 0);\
	}\
};void _____________


namespace Viet {
	using IMessageSerializerBase = Viet::Serialization::ISerializer;

	namespace serializer_impl {
		static constexpr auto g_maxVector = 1'000'000'000; // we need to serialize FrontEndFiles
		static constexpr auto g_maxMap = 5000;

		class iarchive;
		class oarchive;

		template <class T>
		struct reader;
		template <class T>
		struct writer;

		class iarchive {
		public:
			static constexpr bool is_load = true;

			iarchive(IMessageSerializerBase &parent_, void *userData_) noexcept : parent(parent_), userData(userData_) {
			};

			template <class T>
			iarchive &operator&(const T &v) {
				return reader<T>{}(*this, (T&)v);
			}

			IMessageSerializerBase *GetSeri() const noexcept {
				return &parent;
			}

			void *GetUserData() const noexcept {
				return userData;
			}

			IMessageSerializerBase &parent;

		private:
			void *const userData;
		};

		class oarchive {
		public:
			static constexpr bool is_load = false;

			oarchive(IMessageSerializerBase &parent_, void *userData_) noexcept : parent(parent_), userData(userData_) {
			};

			template <class T>
			oarchive &operator&(const T &v) {
				return writer<T>{}(*this, (T &)v);
			}

			IMessageSerializerBase *GetSeri() const noexcept {
				return &parent;
			}

			void *GetUserData() const noexcept {
				return userData;
			}

			IMessageSerializerBase &parent;

		private:
			void *const userData;
		};

		template <bool isEnum>
		struct readers {

			template <class T>
			iarchive &operator()(iarchive &a, T &v) noexcept {
				v.serialize(a, 0);
				return a;
			}
		};

		template <>
		struct readers<true> {

			template <class T>
			iarchive &operator()(iarchive &a, T &v) noexcept {
				switch (sizeof(T)) {
				case 1:
					(T &)v = (T)a.parent.ReadInt8();
					break;
				case 2:
					(T &)v = (T)a.parent.ReadInt16();
					break;
				case 4:
					(T &)v = (T)a.parent.ReadInt32();
					break;
				case 8:
					(T &)v = (T)a.parent.ReadInt64();
					break;
				default:
					(T &)v = T();
					assert(0);
					break;
				}
				return a;
			}
		};

		template <class T>
		struct reader {
			iarchive &operator()(iarchive &a, T &v) noexcept {
				readers<std::is_enum<T>::value> readers;
				readers(a, v);
				return a;
			}
		};

		template <>
		struct reader<bool> {
			iarchive &operator()(iarchive &a, bool &v) noexcept {
				v = a.parent.ReadBool();
				return a;
			}
		};

		template <>
		struct reader<int8_t> {
			iarchive &operator()(iarchive &a, int8_t &v) noexcept {
				v = a.parent.ReadInt8();
				return a;
			}
		};

		template <>
		struct reader<int16_t> {
			iarchive &operator()(iarchive &a, int16_t &v) noexcept {
				v = a.parent.ReadInt16();
				return a;
			}
		};

		template <>
		struct reader<int32_t> {
			iarchive &operator()(iarchive &a, int32_t &v) noexcept {
				v = a.parent.ReadInt32();
				return a;
			}
		};

		template <>
		struct reader<int64_t> {
			iarchive &operator()(iarchive &a, int64_t &v) noexcept {
				v = a.parent.ReadInt64();
				return a;
			}
		};

		template <>
		struct reader<uint8_t> {
			iarchive &operator()(iarchive &a, uint8_t &v) noexcept {
				const auto r = a.parent.ReadInt8();
				v = (uint8_t &)r;
				return a;
			}
		};

		template <>
		struct reader<uint16_t> {
			iarchive &operator()(iarchive &a, uint16_t &v) noexcept {
				const auto r = a.parent.ReadInt16();
				v = (uint16_t &)r;
				return a;
			}
		};

		template <>
		struct reader<uint32_t> {
			iarchive &operator()(iarchive &a, uint32_t &v) noexcept {
				const auto r = a.parent.ReadInt32();
				v = (uint32_t &)r;
				return a;
			}
		};

		template <>
		struct reader<uint64_t> {
			iarchive &operator()(iarchive &a, uint64_t &v) noexcept {
				const auto r = a.parent.ReadInt64();
				v = (uint64_t &)r;
				return a;
			}
		};

		template <>
		struct reader<float> {
			iarchive &operator()(iarchive &a, float &v) noexcept {
				v = a.parent.ReadFloat();
				return a;
			}
		};

		template <>
		struct reader<double> {
			iarchive &operator()(iarchive &a, double &v) noexcept {
				v = a.parent.ReadDouble();
				return a;
			}
		};

		template <>
		struct reader<std::string> {
			iarchive &operator()(iarchive &a, std::string &v) noexcept {
				v = a.parent.ReadString();
				return a;
			}
		};

		template <class T>
		struct reader<std::vector<T>> {
			iarchive &operator()(iarchive &a, std::vector<T> &v) noexcept {
				uint32_t n = (uint32_t)a.parent.ReadInt32();
				assert(n <= serializer_impl::g_maxVector);
				if (n > serializer_impl::g_maxVector) n = serializer_impl::g_maxVector;
				v.resize(n);
				for (size_t i = 0; i < n; ++i) {
					reader<T>{}(a, v[i]);
				}
				return a;
			}
		};

		template <class T, size_t N>
		struct reader<std::array<T, N>> {
			iarchive &operator()(iarchive &a, std::array<T, N> &v) noexcept {
				for (size_t i = 0; i < N; ++i) {
					reader<T>{}(a, v[i]);
				}
				return a;
			}
		};

		template <class T, class T2>
		struct reader<std::pair<T, T2>> {
			iarchive &operator()(iarchive &a, std::pair<T, T2> &v) noexcept {
				reader<T>{}(a, (T&)v.first);
				reader<T2>{}(a, (T2 &)v.second);
				return a;
			}
		};

		template <class T, class T2>
		struct reader<std::map<T, T2>> {
			iarchive &operator()(iarchive &a, std::map<T, T2> &v) noexcept {
				uint32_t n = (uint32_t)a.parent.ReadInt32();
				assert(n <= serializer_impl::g_maxMap);
				if (n > serializer_impl::g_maxMap) n = serializer_impl::g_maxMap;
				for (size_t i = 0; i < n; ++i) {
					using P = std::pair<T, T2>;
					P tmp;
					reader<P>{}(a, (P&)tmp);
					v.insert(tmp);
				}
				return a;
			}
		};

		template <class T>
		struct reader<std::optional<T>> {
			iarchive &operator()(iarchive &a, std::optional<T> &v) noexcept {
				bool hasValue = a.parent.ReadBool();
				if (hasValue) {
					v = T();
					reader<T>{}(a, *v);
				}
				else {
					v = std::nullopt;
				}
				return a;
			}
		};

		template <class T>
		struct writer {
			oarchive &operator()(oarchive &a, T &v) noexcept {
				if constexpr (std::is_enum<T>::value) {
					switch (sizeof(T)) {
					case 1:
						a.parent.WriteInt8((int8_t)v);
						break;
					case 2:
						a.parent.WriteInt16((int16_t)v);
						break;
					case 4:
						a.parent.WriteInt32((int32_t)v);
						break;
					case 8:
						a.parent.WriteInt64((int64_t)v);
						break;
					default:
						assert(0);
					}
				}
				if constexpr (!std::is_enum<T>::value) {
					v.serialize(a, 0);
				}
				return a;
			}
		};

		template <>
		struct writer<bool> {
			oarchive &operator()(oarchive &a, bool &v) noexcept {
				a.parent.WriteBool(v);
				return a;
			}
		};

		template <>
		struct writer<int8_t> {
			oarchive &operator()(oarchive &a, int8_t &v) noexcept {
				a.parent.WriteInt8(v);
				return a;
			}
		};

		template <>
		struct writer<int16_t> {
			oarchive &operator()(oarchive &a, int16_t &v) noexcept {
				a.parent.WriteInt16(v);
				return a;
			}
		};

		template <>
		struct writer<int32_t> {
			oarchive &operator()(oarchive &a, int32_t &v) noexcept {
				a.parent.WriteInt32(v);
				return a;
			}
		};

		template <>
		struct writer<int64_t> {
			oarchive &operator()(oarchive &a, int64_t &v) noexcept {
				a.parent.WriteInt64(v);
				return a;
			}
		};

		template <>
		struct writer<uint8_t> {
			oarchive &operator()(oarchive &a, uint8_t &v) noexcept {
				a.parent.WriteInt8(static_cast<const int8_t &>(v));
				return a;
			}
		};

		template <>
		struct writer<uint16_t> {
			oarchive &operator()(oarchive &a, uint16_t &v) noexcept {
				a.parent.WriteInt16(static_cast<const int16_t &>(v));
				return a;
			}
		};

		template <>
		struct writer<uint32_t> {
			oarchive &operator()(oarchive &a, uint32_t &v) noexcept {
				a.parent.WriteInt32(static_cast<const int32_t &>(v));
				return a;
			}
		};

		template <>
		struct writer<uint64_t> {
			oarchive &operator()(oarchive &a, uint64_t &v) noexcept {
				a.parent.WriteInt64(static_cast<const int64_t &>(v));
				return a;
			}
		};

		template <>
		struct writer<float> {
			oarchive &operator()(oarchive &a, float &v) noexcept {
				a.parent.WriteFloat(v);
				return a;
			}
		};

		template <>
		struct writer<double> {
			oarchive &operator()(oarchive &a, double &v) noexcept {
				a.parent.WriteDouble(v);
				return a;
			}
		};

		template <>
		struct writer<std::string> {
			oarchive &operator()(oarchive &a, std::string &v) noexcept {
				a.parent.WriteString(v.data());
				return a;
			}
		};

		template <class T>
		struct writer<std::vector<T>> {
			oarchive &operator()(oarchive &a, std::vector<T> &v) noexcept {
				a.parent.WriteInt32((int32_t)v.size());
				for (const auto &el : v) {
					writer<T>{}(a, (T&)el);
				}
				return a;
			}
		};

		template <class T, size_t N>
		struct writer<std::array<T, N>> {
			oarchive &operator()(oarchive &a, std::array<T, N> &v) noexcept {
				for (const auto &el : v) {
					writer<T>{}(a, (T&)el);
				}
				return a;
			}
		};

		template <class T, class T2>
		struct writer<std::pair<T, T2>> {
			oarchive &operator()(oarchive &a, std::pair<T, T2> &v) noexcept {
				writer<T>{}(a, (T&)v.first);
				writer<T2>{}(a, (T2&)v.second);
				return a;
			}
		};

		template <class T, class T2>
		struct writer<std::map<T, T2>> {
			oarchive &operator()(oarchive &a, const std::map<T, T2> &v) noexcept {
				a.parent.WriteInt32((int32_t)v.size());
				for (const auto &el : v) {
					using P = std::pair<T, T2>;
					writer<P>{}(a, (P&)el);
				}
				return a;
			}
		};

		template <class T>
		struct writer<std::optional<T>> {
			oarchive &operator()(oarchive &a, std::optional<T> &v) noexcept {
				a.parent.WriteBool(v.has_value());
				if (v.has_value()) {
					writer<T>{}(a, *v);
				}
				return a;
			}
		};
	}

	enum class SeriToUse {
		This,
		SubSeri
	};

	class MessageSerializer {
	public:
		IMessageSerializerBase *const base;

		MessageSerializer(IMessageSerializerBase *base_) : base(base_) {
		}

		template <class T, bool MsgId = true>
		void Write(const T &m, uint8_t *&outData, size_t &outLength, SeriToUse seriToUse = SeriToUse::This, void *userData = nullptr) noexcept {
			const bool useThis = seriToUse == SeriToUse::This;
			auto seri = useThis ? this->base : this->base->GetSubSerializer();

			seri->ResetWrite();
			serializer_impl::oarchive a(*seri, userData);
			if constexpr (MsgId) {
				a & (int8_t)m.Id;
			}
			a & m;
			seri->GetWrittenData(outData, outLength);

			assert(outLength > 0);
		}

		template <class T, bool MsgId = true>
		void Read(T &outM, const uint8_t *data, size_t length, SeriToUse seriToUse = SeriToUse::This, void *userData = nullptr) noexcept {
			const bool useThis = seriToUse == SeriToUse::This;
			auto seri = useThis ? this->base : this->base->GetSubSerializer();

			assert(length > 0);
			if constexpr (MsgId) {
				assert(length > 1);
				++data;
				--length;
			}
			seri->ResetRead(data, length);
			serializer_impl::iarchive(*seri, userData) & outM;
		}

		virtual ~MessageSerializer() = default;

	private:
		std::string writeData;
	};
}