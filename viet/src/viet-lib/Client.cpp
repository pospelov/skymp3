#include <list>
#include <viet/Client.h>
#include <typeindex>
#include <tuple>
#include "NetController.h"
#include "EntityHashGen.h"
#include "Reconnector.h"
#include "GameplayPresenter.h"

namespace {
	struct InstanceKey {
		std::string entityName;
		uint32_t index = 0;

		friend bool operator<(const InstanceKey &lhs, const InstanceKey &rhs) noexcept {
			return std::make_tuple(lhs.entityName, lhs.index) < std::make_tuple(rhs.entityName, rhs.index);
		}
	};

	struct PendingEvent {
		std::type_index type;
		std::shared_ptr<void> evn;
	};
}

namespace Viet {
	class ClientContextImplAccess {
	public:
		static void AddEvent(ClientContextImpl &self, const char *currentCbPropName, const std::type_index &typeInfo, std::shared_ptr<void> e) {
			return self.AddEvent(currentCbPropName, typeInfo, e);
		}
	};
}

using namespace Viet;

struct Client::Impl : public IConnectionMessageListener, public ICustomPacketMessageListener, public IGuiMessageListener {
	Impl(std::vector<Entity> entities_, Networking::Plugin netPlugin_, Serialization::Plugin seriPlugin_) : entities(entities_), netPlugin(netPlugin_), seriPlugin(seriPlugin_) {
	}

	const std::vector<Entity> entities;
	const Networking::Plugin netPlugin;
	const Serialization::Plugin seriPlugin;

	EntityHashGen hashGen;
	std::unique_ptr<IMessageSerializerBase> seriBase;

	std::shared_ptr<NetController> netController;
	std::shared_ptr<GameplayPresenter> gameplayPresenter;

	MessageListeners messageListeners;

	struct {
		std::mutex m;
		std::map<std::string, std::vector<uint8_t>> front;
		int n = 0;
	} share;

	struct {
		std::mutex m;
		std::list<msg::CustomPacket> incomingPackets;
	} share2;

	struct {
		std::any globalPers;
		std::map<std::string, std::any> globalStates;

		struct PerInstance {
			std::any pers;
			std::map<std::string, std::any> states;
		};

		std::map<InstanceKey, PerInstance> perInstance;
	} forCtx;

	struct {
		std::vector<PendingEvent> pendingEvents;

		std::mutex m;
	} share3;

	void OnConnect(ConnectResult res) noexcept override {
	}

	void OnDisconnect() noexcept override {
	}

	void OnMessage(const msg::CustomPacket &m) noexcept override {
		std::lock_guard l(this->share2.m);
		this->share2.incomingPackets.push_back(m);
		assert(this->share2.incomingPackets.size() < 10000);
	}

	void OnMessage(const msg::GuiJson &m) noexcept override {
		std::lock_guard l(this->share.m);
		this->share.front = m.files.data;
		this->share.n++;
	}
};

Client::Client(std::vector<Entity> entities_, const Networking::Plugin &netPlugin_, const Serialization::Plugin &seriPlugin_, std::optional<std::string> sessionHashOrEi) : pImpl(new Impl{ entities_, netPlugin_, seriPlugin_ }) {
	const std::shared_ptr<Networking::IClient> netCl(new Reconnector([netPlugin_] { 
		return netPlugin_.clientFactory(); 
	}));

	pImpl->seriBase.reset(pImpl->seriPlugin.serializerFactory());
	std::shared_ptr<MessageSerializer> seri(new MessageSerializer(pImpl->seriBase.get()));

	std::string hash = sessionHashOrEi ? *sessionHashOrEi : pImpl->hashGen.Generate();

	// Support passing stringified EntityInfo instead of only hash
	if (hash.find(':') != std::string::npos) {
		hash = EntityInfo::FromString(hash).hash;
	}

	pImpl->netController.reset(new NetController(netCl, seri, hash, entities_));

	std::shared_ptr<GameplayPresenter> gameplayPresenter(new GameplayPresenter(pImpl->netController, entities_));
	pImpl->gameplayPresenter = gameplayPresenter;

	pImpl->messageListeners.connMsgListeners.push_back(pImpl);
	pImpl->messageListeners.cpMsgListener = pImpl;
	pImpl->messageListeners.gameplayMsgListener = &*gameplayPresenter;
	pImpl->messageListeners.guiMsgListener = pImpl;
}

Client::~Client() {
	// The hacky way to save entities from destroying
	new std::vector<Viet::Entity>(std::move(pImpl->entities));

	delete pImpl;
}

void Client::Tick() noexcept {
	assert(pImpl->netController);
	pImpl->netController->Tick(pImpl->messageListeners);
	pImpl->gameplayPresenter->Tick();
}

void Client::ProcessWorldState(const Callback &cb) {
	assert(pImpl->gameplayPresenter);
	pImpl->gameplayPresenter->ProcessModel([&cb](const GameplayModel &model) {
		cb(&model);
	});
}

void Client::UpdateProperties(IBaseContext &baseContext, IInstanceViewHolder *holder, bool showMe) noexcept {
	this->ProcessWorldState([&](const Viet::IWorldState *wst) {
		// Me
		const auto myEntityName = wst->GetMyEntity();
		
		auto myEntity = std::find_if(pImpl->entities.begin(), pImpl->entities.end(), [myEntityName](const Viet::Entity &e) {
			return !strcmp(e.GetName(), myEntityName);
		});
		if (myEntity != pImpl->entities.end()) {
			this->UpdateImpl(nullptr, *myEntity, ~0, baseContext, wst);
		}
		
		if (!holder) return;

		// Neighbours
		for (auto &entity : pImpl->entities) {
			auto entityName = entity.GetName();
			auto instances = wst->GetInstances(entity.GetName());

			for (uint32_t i = 0; i < instances.GetPoolSize(); ++i) {
				auto inst = instances[i];
				if (!inst.IsValid() || (inst.IsMe() && !showMe)) {
					if (holder->Find(entityName, i)) {
						holder->Delete(entityName, i);
					}
					continue;
				}

				IInstanceView *view = holder->Find(entityName, i);
				if (!view) view = holder->Create(entityName, i);
				assert(view);
				if (!view) continue;

				if (holder->IsStateResetNeeded(entityName, i)) {
					InstanceKey k{ entityName, i };
					for (auto &[_, st] : pImpl->forCtx.perInstance[k].states) {
						st = std::any();
					}
				}

				this->UpdateImpl(view, entity, i, baseContext, wst);
			}
		}
	});
}

Viet::json Client::ToApiFormat(const char *entityName, const char *propertyName, const IPropertyValue *value) noexcept {
	if (value) {
		for (size_t entityId = 0; entityId < pImpl->entities.size(); ++entityId) {
			auto &entity = pImpl->entities[entityId];
			if (strcmp(entity.GetName(), entityName) != 0) continue;

			auto propList = EntityAccess::GetPropList_(entity);
			assert(propList);
			if (!propList) continue;

			auto prop = propList->GetByName(propertyName);
			assert(prop);
			if (!prop) continue;

			return prop->ToApiFormat(value->Clone());
		}
	}
	return nullptr;
}

std::unique_ptr<IPropertyValue> Client::FromApiFormat(const char *entityName, const char *propertyName, Viet::json value) noexcept {
	for (size_t entityId = 0; entityId < pImpl->entities.size(); ++entityId) {
		auto &entity = pImpl->entities[entityId];
		if (strcmp(entity.GetName(), entityName) != 0) continue;

		auto propList = EntityAccess::GetPropList_(entity);
		assert(propList);
		if (!propList) continue;

		auto prop = propList->GetByName(propertyName);
		assert(prop);
		if (!prop) continue;

		return prop->FromApiFormat(value);
	}
	return nullptr;
}

std::map<std::string, std::vector<uint8_t>> Client::GetFrontEndFiles() const noexcept {
	std::lock_guard l(pImpl->share.m);
	return pImpl->share.front;
}

int Client::GetNumFrontChanges() const noexcept {
	return pImpl->share.n;
}

std::optional<std::string> Client::NextCustomPacket() noexcept {
	std::lock_guard l(pImpl->share2.m);
	if (pImpl->share2.incomingPackets.empty()) {
		return std::nullopt;
	}
	auto res = std::move(pImpl->share2.incomingPackets.front());
	pImpl->share2.incomingPackets.pop_front();
	return res.data;
}

void Client::SendPropertyChange(const char *propertyName, const std::unique_ptr<IPropertyValue> &value) noexcept {
	return this->SendPropertyChangeImpl(propertyName, value.get());
}

void Client::SendCustomPacket(const char *content) noexcept {
	msg::CustomPacket cp;
	cp.data = content;
	pImpl->netController->Send(cp);
}

void Client::SendPropertyChangeImpl(const char *propertyName, const IPropertyValue *propertyValue) noexcept {
	assert(pImpl->gameplayPresenter);
	if (pImpl->gameplayPresenter) {
		pImpl->gameplayPresenter->SendPropertyChange(propertyName, propertyValue);
	}
}

void Client::SendContextEventImpl(const std::type_info &typeInfo, const std::shared_ptr<void> &evn) noexcept {
	std::lock_guard l(pImpl->share3.m);
	pImpl->share3.pendingEvents.push_back({ typeInfo, evn });
}

// instanceIndex == ~0 && view == nullptr means local player
void Client::UpdateImpl(IInstanceView *view, const Viet::Entity &entity, uint32_t instanceIndex, IBaseContext &base, const IWorldState *wst) {
	assert((view && instanceIndex != (uint32_t)~0) || (!view && instanceIndex == (uint32_t)~0));

	std::lock_guard l(pImpl->share3.m);

	Viet::InstanceModel *inst = nullptr;
	auto instances = wst->GetInstances(entity.GetName());
	auto instance = instances[instanceIndex];
	if (instanceIndex != (uint32_t)~0) {
		assert(instance.IsValid());
		inst = &instance;
	}

	auto &props = EntityAccess::GetProperties_(entity);
	for (auto &prop : props) {
		auto fac = prop.GetClientContextFactory();
		if (!fac) continue;

		InstanceKey key = { prop.GetName(), instanceIndex };
		auto &perInstance = pImpl->forCtx.perInstance[key];

		auto &state = perInstance.states[prop.GetName()];
		auto &gState = pImpl->forCtx.globalStates[prop.GetName()];

		auto ctx = fac(base, view, *this, wst, perInstance.pers, pImpl->forCtx.globalPers, inst, state, gState);
		assert(ctx);
		if (!ctx) continue;

		
		for (auto &pendingEvent : pImpl->share3.pendingEvents) {
			ClientContextImplAccess::AddEvent(*ctx, prop.GetName(), pendingEvent.type, pendingEvent.evn);
		}

		auto onUpdate = prop.GetOnUpdate();
		assert(onUpdate);
		if (!onUpdate) continue;
		onUpdate(ctx.get());
	}

	pImpl->share3.pendingEvents.clear();
}