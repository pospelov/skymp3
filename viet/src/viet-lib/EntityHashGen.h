#pragma once
#include <vector>
#include <random>
#include <functional>
#include <algorithm> // std::generate_n
#include <random>

#include <IEntityHashGen.h>

class EntityHashGen : public IEntityHashGen {
public:
	EntityHashGen();

	std::string Generate() noexcept override;

private:
	std::vector<char> chars;
	std::shared_ptr<std::default_random_engine> rng;
	std::shared_ptr<std::uniform_int_distribution<>> dist;
	std::function<char()> randchar;
};