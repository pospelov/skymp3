#pragma once

// Must be threadsafe
class INetController {
public:
	virtual void Send(const msg::UpdateActorProperties &) noexcept = 0;
	virtual void Send(const msg::CustomPacket &) noexcept = 0;

	virtual ~INetController() = default;
};
