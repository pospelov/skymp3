#include <chrono>
#include <typeindex>
#include <typeinfo>
#include <unordered_map>
#include <viet/ClientContext.h>
#include <viet/Client.h>

namespace {
	using PropName = std::string;
	using OptTimepoint = std::optional<std::chrono::time_point<std::chrono::steady_clock>>;

	struct Persistent {
		struct Data {
			struct PerProp {
				uint64_t numChanges = 0;
				std::shared_ptr<Viet::IPropertyValue> value;

				std::optional<Viet::PropertyChange> nextChange;
			};
			std::map<PropName, PerProp> perProp;

			struct PerTimeout {
				bool fire = false;
			};
			std::unordered_map<std::type_index, PerTimeout> perTimeout;

			struct PerEvent {
				std::shared_ptr<void> evn;
			};
			std::unordered_map<std::type_index, PerEvent> perEvent;
		};

		std::map<PropName, Data> perCurrentPropCb;

		struct TimeoutInfo {
			const std::chrono::milliseconds interval;
			OptTimepoint lastFire;
		};

		std::unordered_map<std::type_index, TimeoutInfo> timeouts;
	};

	struct GlobalPersistent {
		struct PerGlobalTimeout {
			const std::chrono::milliseconds interval;
			OptTimepoint lastLock;
		};
		std::unordered_map<std::type_index, PerGlobalTimeout> perGtm;
	};

	std::shared_ptr<Viet::IPropertyValue> UniqueToShared(std::unique_ptr<Viet::IPropertyValue> ptr) {
		std::shared_ptr<Viet::IPropertyValue> res;
		if (ptr) {
			res.reset(ptr.release());
		}
		return res;
	}
}

struct Viet::ClientContextImpl::Impl {
	void *base = nullptr;
	void *view = nullptr;
	Viet::IWorldState::Instance *instance = nullptr;
	StateCtor *stateCtor = nullptr;
	GlobalStateCtor *globalStateCtor = nullptr;
	std::any *stateHolder = nullptr;
	std::any *globalStateHolder = nullptr;
	Viet::Client *client = nullptr;
	const Viet::IWorldState *wst = nullptr;
	std::any *persistent = nullptr;
	std::any *globalPersistent = nullptr;

	Persistent &Pers() {
		assert(this->persistent);
		if (!this->persistent->has_value() || this->persistent->type() != typeid(Persistent)) {
			*this->persistent = Persistent();
		}
		return std::any_cast<Persistent &>(*this->persistent);
	}

	GlobalPersistent &GlobalPers() {
		assert(this->globalPersistent);
		if (!this->globalPersistent->has_value() || this->globalPersistent->type() != typeid(GlobalPersistent)) {
			*this->globalPersistent = GlobalPersistent();
		}
		return std::any_cast<GlobalPersistent &>(*this->globalPersistent);
	}

	auto GetValue(const char *propName) {
		return this->instance ? this->instance->GetValue(propName) : this->wst->GetMyPropertyValue(propName);
	}
};

Viet::ClientContextImpl::ClientContextImpl() {
	pImpl.reset(new Impl);
}

void Viet::ClientContextImpl::Init(void *base, void *view, void *vietInstance, StateCtor *stateCtor, GlobalStateCtor *globalStateCtor, std::any *stateHolder, std::any *globalStateHolder, Viet::Client *client, const Viet::IWorldState *wst, std::any *persistent, std::any *globalPersistent) {
	pImpl->base = base;
	pImpl->view = view;
	pImpl->instance = (Viet::IWorldState::Instance *) vietInstance;
	pImpl->stateCtor = stateCtor;
	pImpl->globalStateCtor = globalStateCtor;
	pImpl->stateHolder = stateHolder;
	pImpl->globalStateHolder = globalStateHolder;
	pImpl->client = client;
	pImpl->persistent = persistent;
	pImpl->globalPersistent = globalPersistent;
	pImpl->wst = wst;

	assert(pImpl->stateHolder);
	assert(pImpl->globalStateHolder);
	assert(pImpl->persistent);
	assert(pImpl->globalPersistent);

	assert((vietInstance && view) || (!vietInstance && !view));

	this->Tick();
}

void *Viet::ClientContextImpl::Base() {
	assert(pImpl->base);
	return pImpl->base;
}

void *Viet::ClientContextImpl::View() {
	return pImpl->view;
}

std::any &Viet::ClientContextImpl::State() {
	auto emptyState = (*pImpl->stateCtor)();
	if (!pImpl->stateHolder->has_value() || pImpl->stateHolder->type() != emptyState.type()) {
		*pImpl->stateHolder = emptyState;
	}
	return *pImpl->stateHolder;
}

std::any &Viet::ClientContextImpl::GlobalState() {
	auto emptyState = (*pImpl->globalStateCtor)();
	if (!pImpl->globalStateHolder->has_value() || pImpl->globalStateHolder->type() != emptyState.type()) {
		*pImpl->globalStateHolder = emptyState;
	}
	return *pImpl->globalStateHolder;
}

void Viet::ClientContextImpl::SendPropertyChange(const char *propName, Viet::IPropertyValue *propValue) {
	pImpl->client->SendPropertyChange(propName, propValue->Clone());
}

std::optional<Viet::PropertyChange> Viet::ClientContextImpl::NextChange(const char *currentCbPropName, const char *propName) {
	auto &pers = pImpl->Pers();
	auto &perCb = pers.perCurrentPropCb[currentCbPropName];
	auto &perProp = perCb.perProp[propName];

	std::optional<Viet::PropertyChange> res = std::move(perProp.nextChange);
	perProp.nextChange = std::nullopt;
	return res;
}

std::shared_ptr<void> Viet::ClientContextImpl::NextEvent(const char *currentCbPropName, const std::type_info &typeInfo) {
	auto &pers = pImpl->Pers();
	auto &perCb = pers.perCurrentPropCb[currentCbPropName];
	auto &perEvent = perCb.perEvent[typeInfo];
	std::shared_ptr<void> res = std::move(perEvent.evn);
	perEvent.evn.reset();
	return res;
}

bool Viet::ClientContextImpl::NextTimeout(const char *currentCbPropName, const std::type_info &typeInfo, uint32_t timeoutMs) {
	auto &pers = pImpl->Pers();
	auto &perCb = pers.perCurrentPropCb[currentCbPropName];

	pers.timeouts.insert({ typeInfo, Persistent::TimeoutInfo{std::chrono::milliseconds(timeoutMs)} });

	auto &perTm = perCb.perTimeout[typeInfo];

	if (perTm.fire) {
		perTm.fire = false;
		return true;
	}
	return false;
}

bool Viet::ClientContextImpl::TryLockGlobalTimeout(const std::type_info &typeInfo, uint32_t globalTimeoutMs) {
	auto &gpers = pImpl->GlobalPers();

	auto &[_, perGtm] = *gpers.perGtm.insert(
		{ typeInfo, GlobalPersistent::PerGlobalTimeout{std::chrono::milliseconds(globalTimeoutMs)} }
	).first;

	auto now = std::chrono::steady_clock::now();
	bool lock = !perGtm.lastLock || now - *perGtm.lastLock > perGtm.interval;

	if (lock) {
		perGtm.lastLock = now;
		return true;
	}
	return false;
}

Viet::IPropertyValue *Viet::ClientContextImpl::GetValue(const char *propName) {
	return pImpl->GetValue(propName).value;
}

// Do once after ctor (once ever, not for each property)
// Call for each instance (neighbour) and for the local player (view==nullptr)
void Viet::ClientContextImpl::Tick() {
	auto &pers = pImpl->Pers();

	const auto propNames = pImpl->instance ? pImpl->instance->GetKeys() : pImpl->wst->GetMyPropertyNames();
	
	for (auto &[cbPropName, perCb] : pers.perCurrentPropCb) {
		// NextChange
		for (auto propName : propNames) {
			auto &perProp = perCb.perProp[propName];

			auto v = pImpl->GetValue(propName);
			if (v.numChanges != perProp.numChanges) {

				perProp.nextChange = Viet::PropertyChange();
				perProp.nextChange->previousValue = perProp.value;

				perProp.numChanges = v.numChanges;
				perProp.value = UniqueToShared(v.value->Clone());
			}
		}
	}

	// Timeouts
	for (auto &[typeIndex, timeoutInfo] : pers.timeouts) {
		if (!timeoutInfo.lastFire || std::chrono::steady_clock::now() >= *timeoutInfo.lastFire) {
			timeoutInfo.lastFire = std::chrono::steady_clock::now();

			for (auto &[cbPropName, perCb] : pers.perCurrentPropCb) {
				perCb.perTimeout[typeIndex].fire = true;
			}
		}
	}
}

void Viet::ClientContextImpl::AddEvent(const char *currentCbPropName, const std::type_index &typeInfo, std::shared_ptr<void> e) {
	auto &pers = pImpl->Pers();
	pers.perCurrentPropCb[currentCbPropName].perEvent[typeInfo].evn = e;
}