it('property flag VisibleToNeighbour works', async () => {
  await setup();
  let bot1 = newBot();
  let [user1] = await nextEvent(remoteServer, 'userEnter', 5000);
  let bot2 = newBot();
  let [user2] = await nextEvent(remoteServer, 'userEnter', 5000);

  let i = 0;
  for (let user of [user1, user2]) {
    let actor = await remoteServer.createActor()
      .setPos([++i, 0, 0])
      .setCellOrWorld(0x3c)
      .setVisibleToNeighbourInt(98)
      .setNonVisibleToNeighbourInt(-601)
      .setEnabled(true);
    await user.setActor(actor);
  }

  for (let bot of [bot1, bot2]) {
    await see(bot, 'Actor', { pos: [1, 0, 0], visibleToNeighbourInt: 98 });
    await see(bot, 'Actor', { pos: [2, 0, 0], visibleToNeighbourInt: 98 });
    await notSee(bot, 'Actor', { pos: [1, 0, 0], nonVisibleToNeighbourInt: -601 });
    await notSee(bot, 'Actor', { pos: [2, 0, 0], nonVisibleToNeighbourInt: -601 });
  }
});
