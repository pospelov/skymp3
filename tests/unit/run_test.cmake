get_cmake_property(_variableNames VARIABLES)
list (SORT _variableNames)
foreach (_variableName ${_variableNames})
  if (_variableName MATCHES "^SKYMP_TEST_.*")
    set(ENV{${_variableName}} ${${_variableName}})
  endif()
endforeach()

execute_process(
  COMMAND ${VIET_CURRENT_TEST_CMD}
  RESULTS_VARIABLE res
)

if (NOT ${res} STREQUAL "0")
  message(FATAL_ERROR "Bad exit code: ${res}")
endif()
