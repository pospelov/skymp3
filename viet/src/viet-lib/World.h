#pragma once
#include <functional>
#include <optional>
#include <memory>
#include <ICellProcess.h>
#include <IAsyncDatabase.h>
#include <viet/NoSqlDatabase.h>
#include <viet/Logging.h>
#include <ITimerManager.h>
#include <IProperty.h>
#include <VisionEvents.h>
#include <FindMethodCall.h>
#include <viet/Entity.h>
#include <viet/Promise.h>
#include <viet/FindRequest.h>

// helper type for the visitor #4
template<class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
template<class... Ts> overloaded(Ts...)->overloaded<Ts...>;

struct CellProcessId {
public:
	CellProcessId() : CellProcessId(EntityProps(), nullptr) {
	}

	CellProcessId(const EntityProps &props_, Properties *propList) {
		this->propList = propList;
		this->props = props_;
		if (!propList) return;

		this->props.Filter(Viet::PropertyFlags::AffectsCellProcessSelection, propList);
		json j = json::object();
		this->props.ForEach([&](PropId propId, const std::unique_ptr<IPropertyValue> &value) {
			auto prop = propList->GetById(propId);
			assert(prop);
			if (!prop) return;
			j[prop->GetName()] = prop->ToDbFormat(value);
		});
		this->id = j.dump();
	}

	std::string ToHumanReadableString() const noexcept {
		return id;
	}

	const EntityProps &GetPropsUsed() const noexcept {
		return this->props;
	}

	friend bool operator<(const CellProcessId &lhs, const CellProcessId &rhs) noexcept {
		return lhs.id < rhs.id;
	}

private:
	std::string id;
	EntityProps props;
	Properties *propList = nullptr;
};

using IDBOperators = Viet::NoSqlDatabase::INoSqlOperators;

class World {
public:
	using CellProcFactory = std::function<std::shared_ptr<ICellProcess>()>;
	using ActorFactory = std::function<std::unique_ptr<IActor>()>;
	using GetCellProcCb = std::function<void(std::shared_ptr<ICellProcess>)>;
	using CreateActorCb = std::function<void()>;
	using GetUserCb = std::function<void(EntityInfo user)>; // user can be empty 

	struct ActorDbAddress {
		CellProcessId cellProcessId;
		std::unique_ptr<IActor> ac;
		int entityId = -1;
		EntityInfo ei;
	};

	struct ActorAddress {
		CellProcessId cellProcessId;
		ICellProcess &cp;
		ICellProcess::InstanceId inst;
	};

	struct ActorFindResult  {
	public:
		using SaveFn = std::function<void(ActorDbAddress &, std::function<void()> cb)>;
		using SetCachedFn = std::function<Viet::Promise<Viet::Void>(const EntityInfo &ei)>;

		ActorFindResult(ActorDbAddress ac, SaveFn saveFn_, SetCachedFn setCachedFn_) noexcept : data(std::move(ac)), saveFn(saveFn_), setCachedFn(setCachedFn_) {
		}

		ActorFindResult(ActorAddress acAddr) noexcept : data(acAddr) {
		}

		bool IsInCache() const noexcept {
			return data.index() == 0; // ActorAddress
		}

		std::unique_ptr<IPropertyValue> GetPropertyValue(PropId propId) const noexcept {
			std::unique_ptr<IPropertyValue> res;
			std::visit(overloaded{
			[&](const ActorDbAddress &acAddr) {
				res = acAddr.ac->GetProps().GetPropertyValue(propId)->Clone();
			},
			[&](const ActorAddress &acAddr) {
				if (auto entityProps = acAddr.cp.GetEntityProps(acAddr.inst)) {
					res = entityProps->GetPropertyValue(propId)->Clone();
				}
			} }, this->data);
			return res;
		}

		EntityProps GetProperties() const noexcept {
			EntityProps res;
			std::visit(overloaded{
			[&](const ActorDbAddress &acAddr) {
				res = acAddr.ac->GetProps();
			},
			[&](const ActorAddress &acAddr) {
				if (auto entityProps = acAddr.cp.GetEntityProps(acAddr.inst)) {
					res = *entityProps;
				}
			} }, this->data);
			return res;
		}

		std::optional<Viet::Promise<Viet::Void>> Edit(const EntityProps &entityProps, Properties *propList) noexcept {
			std::optional<Viet::Promise<Viet::Void>> res;
			if (!entityProps.IsEmpty()) {
				std::visit([&](auto &x) {
					res = this->EditImpl(entityProps, x, propList); 
				}, this->data);
			}
			return res;
		}

	private:

		Viet::Promise<Viet::Void> EditImpl(const EntityProps &entityProps, ActorDbAddress &acAddr, Properties *propList) noexcept {
			Viet::Promise<Viet::Void> promise;

			acAddr.ac->ApplyPropsChanges(entityProps);
			bool cachingRequired = acAddr.ac->GetProps().IsCachingRequired(propList);
			auto setCachedFn = this->setCachedFn;
			auto ei = acAddr.ei;
			this->saveFn(acAddr, [cachingRequired, setCachedFn, ei, promise] {
				if (cachingRequired) {
					setCachedFn(ei).Then(promise);
				}
				else {
					Viet::Promise<Viet::Void>(promise).Resolve(Viet::Void());
				}
			});

			return promise;
		}

		std::optional<Viet::Promise<Viet::Void>> EditImpl(const EntityProps &entityProps, ActorAddress &acAddr, Properties *propList) noexcept {
			return acAddr.cp.Edit(acAddr.inst, entityProps, true).promise;
		}

		std::variant<ActorAddress, ActorDbAddress> data;
		SaveFn saveFn;
		SetCachedFn setCachedFn;
	};

	using FindActorCb = std::function<void(std::optional<ActorFindResult>)>;

	using OnVisionEvent = std::function<void(const EntityInfo &viewerAc, vision::Event<EntityInfo>)>;

	World(CellProcFactory fac, 
		ActorFactory acFac, 
		std::shared_ptr<IAsyncDatabase> db, 
		IDBOperators &dbOps, 
		std::shared_ptr<Viet::Logging::Logger> logger, 
		std::shared_ptr<ITimerManager> tm,
		OnVisionEvent onVisionEvent,
		std::vector<Viet::Entity> entities
	);
	~World();

	// Note: Methods can call callbacks sync
	void Tick() noexcept;
	ICellProcess *GetCellProcLocally(const CellProcessId &cpId) noexcept; // Search in loaded cells
	void GetCellProc(const CellProcessId &cpId, GetCellProcCb fn) noexcept; // May force cell loading
	void FindActor(int entityId, const EntityInfo &actor, FindActorCb cb) noexcept; // May force cell loading
	std::optional<ActorAddress> FindActorLocally(const EntityInfo &actor) noexcept; // No cell loading, just search in loaded cells
	void CreateActor(int entityId, const EntityInfo &actorEi, const EntityProps &props, CreateActorCb cb) noexcept; // Creates actor in DB (not in cache)
	Viet::Promise<std::vector<EntityInfo>> ResolveDbActors(int entityId, const Viet::FindRequest &findReq) noexcept;
	std::vector<EntityInfo> ResolveActorsLocally(int entityId, const Viet::FindRequest &findReq) noexcept;

private:
	struct Impl;
	std::shared_ptr<Impl> pImpl;
	IDBOperators &dbOps;
};