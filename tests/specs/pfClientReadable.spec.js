it('reads from property with ClientReadable flag', async () => {
  await setup();
  let bot = newBot();
  let [user] = await nextEvent(remoteServer, 'userEnter', 5000);

  let actor = await remoteServer.createActor()
    .setPos([1, 2, 100])
    .setCellOrWorld(0x3c)
    .setClientReadableInt(22)
    .setNonClientReadableInt(24)
    .setEnabled(true);
  await user.setActor(actor);

  await see(bot, 'Actor', { pos: [1, 2, 100], clientReadableInt: 22 });

  let err = null;
  try {
    await see(bot, 'Actor', { nonClientReadableInt: 24 });
  }
  catch(e) {
    err = e;
  }
  expect(err).not.to.eql(null);
});
