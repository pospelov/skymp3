#include <iostream>
#include <fstream>
#include <filesystem>
#include <stdexcept>
#include <mutex>
#include <thread>
#include <ctime>
#include <chrono>
#include <string>

#include <viet/Server.h>
#include <viet-raknet/VietRakNet.h>
#include <viet-mongocxx/VietMongocxx.h>

#include "Entities.h"

namespace {
	class StdLoggerOutput : public Viet::Logging::ILoggerOutput {
	public:
		void Write(Viet::Logging::Severity severity, const char *text) noexcept override {
			// Since there will be more than one instance of the logger class
			static std::mutex m;
			std::lock_guard l(m);

			if (Viet::Logging::GetSeverityName(severity) == std::string("Debug")) return;

			std::cout << "[" << Viet::Logging::GetSeverityName(severity) << "] " << text << std::endl;
		}
	};
}

int main(int argc, const char *argv[]) {
	try {
    using namespace std::chrono;
    milliseconds ms = duration_cast<milliseconds>(system_clock::now().time_since_epoch());

    std::string serverId = "test_server_" + std::to_string(ms.count());
    int timeoutTimeMs = 4000;
	int gmTimeoutTimeMs = 4000;
	int maxGamemodes = 10;
    int port = 7777;
    int gamemodesPort = 7000;

		Viet::Server::Settings settings;
		settings.devPassword = "qwerty";
		settings.maxPlayers = 1000;
		settings.entities = GetTestEntities();
		settings.netPlugin.serverFactory = [=] {
			return new Viet::Networking::RakServer(settings.maxPlayers, port, timeoutTimeMs);
		};
		settings.gamemodeNetPlugin.serverFactory = [=] {
			return new Viet::Networking::RakServer(maxGamemodes, gamemodesPort, gmTimeoutTimeMs);
		};
		settings.seriPlugin.serializerFactory = [] {
			return new Viet::Serialization::RakSerializer();
		};
		settings.loggingPlugin.loggerFactory = [] {
			return new StdLoggerOutput;
		};
		settings.nosqlPlugin.databaseFactory = [=] {
			const std::string dbName = "skymp_db_test_server" + serverId;

			Viet::NoSqlDatabase::Mongo::Settings s;
			auto envVal = getenv("SKYMP_TEST_MONGO_URI");
			assert(envVal);
			if (envVal) {
				s.mongoUri = envVal;
			}
			printf("SKYMP_TEST_MONGO_URI = %s\n", s.mongoUri.data());
			s.dbName = dbName;
			s.startsWith = "skymp_db_test_server";
			s.dbIgnore = { dbName };
			s.logger.reset(new Viet::Logging::Logger(new StdLoggerOutput));

			auto db = new Viet::NoSqlDatabase::Mongo(s);
			return db;
		};

		StdLoggerOutput l;
		Viet::Server server(settings);
		clock_t activeGmMoment = clock();

		std::stringstream ss;
		ss << "gamemodesPort is " << gamemodesPort;
		auto ssData = ss.str();
		l.Write(Viet::Logging::Severity::Notice, ssData.data());

		while (1) {
			std::this_thread::sleep_for(std::chrono::milliseconds(1));
			server.Tick();
			int n = server.GetNumActiveGamemodes();
			if (n > 0) activeGmMoment = clock();

			constexpr int timeoutSeconds = 5;
			if (clock() - activeGmMoment > timeoutSeconds * CLOCKS_PER_SEC) {
				std::string s = "There was no active gamemode for " +  std::to_string(timeoutSeconds) + " seconds, stopping the server";
				l.Write(Viet::Logging::Severity::Notice, s.data());
				break;
			}

		}
		l.Write(Viet::Logging::Severity::Notice, "The server exited");
		return 0;
	}
	catch (std::exception &e) {
		std::cerr << "Unhandled exception: " << e.what() << std::endl;
		return 1;
	}
}
